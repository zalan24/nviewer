#ifndef TEXTURE_H
#define TEXTURE_H

#include "common/Helper/vram.h"
#include "common/Math/include/tools.h"

class Texture
{
  int X,Y;
  Colour **m;
  bool needToDelete;
public:
  Texture(int,int);
  Texture(int,int,VRam&);
  Texture(string,VRam&);
  ~Texture();
  int getX() const;
  int getY() const;
  void set(int,int,Colour);
  Colour get(int,int) const;
  Colour* getPointer(int,int);
  void copy(Texture*) const;
};

#endif // TEXTURE_H
