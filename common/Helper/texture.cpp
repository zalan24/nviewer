#include "texture.h"

Texture::Texture(int X, int Y) : X(X), Y(Y)
{
  m = new Colour*[X];
  for (int i = 0; i < X; ++i) m[i] = new Colour[Y];
  needToDelete = true;
}

Texture::Texture(int X, int Y, VRam &vr) : X(X), Y(Y)
{
  m = (Colour**)vr.allocate(sizeof(Colour*)*X);
  for (int i = 0; i < X; ++i) m[i] = (Colour*)vr.allocate(sizeof(Colour)*Y);
  needToDelete = false;
}

Texture::~Texture()
{
  if (needToDelete) {
    for (int i = 0; i < X; ++i) delete [] m[i];
    delete [] m;
  }
}


int Texture::getX() const {return X;}
int Texture::getY() const {return Y;}

void Texture::set(int x, int y, Colour c)
{
  m[x][y] = c;
}

Colour Texture::get(int x, int y) const
{
  return m[x][y];
}

Colour* Texture::getPointer(int x, int y)
{
  return &m[x][y];
}

void Texture::copy(Texture *text2) const
{
    for (int i = 0; i < X; ++i) for (int j = 0; j < Y; ++j) text2->set(i,j,get(i,j));
}

Texture::Texture(string /*filename*/, VRam &/*vr*/) : needToDelete(false)
{
  /*FILE *fp = fopen(filename.c_str(), "rb");
  unsigned char header[8];
  int number = 8;
  if (!fp) throw Exception(FILE_NOT_FOUND, "File not found");
  fread(header, 1, number, fp);
  bool is_png = !png_sig_cmp(header, 0, number);
  if (!is_png) {
    fclose(fp);
    throw Exception(IO_ERROR5, "File is not PNG");
  }
  png_voidp user_error_ptr;
  png_error_ptr user_error_fn, user_warning_fn;
  png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, (png_voidp)user_error_ptr,user_error_fn, user_warning_fn);
  if (!png_ptr) {
    fclose(fp);
    throw Exception(IO_ERROR6, "Cannot load texture");
  }
  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr) {
    png_destroy_read_struct(&png_ptr,
    (png_infopp)NULL, (png_infopp)NULL);
    fclose(fp);
    throw Exception(IO_ERROR6, "Cannot load texture");
  }
  png_infop end_info = png_create_info_struct(png_ptr);
  if (!end_info) {
    png_destroy_read_struct(&png_ptr, &info_ptr,
    (png_infopp)NULL);
    fclose(fp);
    throw Exception(IO_ERROR6, "Cannot load texture");
  }
  if (setjmp(png_jmpbuf(png_ptr))) {
    png_destroy_read_struct(&png_ptr, &info_ptr,
    &end_info);
    fclose(fp);
    throw Exception(IO_ERROR6, "Cannot load texture");
  }
  png_init_io(png_ptr, fp);
  png_set_sig_bytes(png_ptr, number);
  png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);
  png_bytepp row_pointers = png_get_rows(png_ptr, info_ptr);
  X = png_get_image_width(png_ptr, info_ptr);
  Y = png_get_image_height(png_ptr, info_ptr);
  int colour_type = png_get_color_type(png_ptr, info_ptr);
  if (colour_type != PNG_COLOR_TYPE_RGB_ALPHA && colour_type != PNG_COLOR_TYPE_RGB) {
    png_destroy_read_struct(&png_ptr, &info_ptr,
    &end_info);
    fclose(fp);
    throw Exception(IO_ERROR7, "Cannot load texture (unsupported colour type)");
  }
  int pixel_size = png_get_channels(png_ptr, info_ptr);
  int bitdepth = png_get_bit_depth(png_ptr, info_ptr);
  if (bitdepth != 8 / *&& bitdepth != 16* /) {
    png_destroy_read_struct(&png_ptr, &info_ptr,
    &end_info);
    fclose(fp);
    throw Exception(IO_ERROR8, "Cannot load texture (unsupported bit depth)");
  }
  m = (Colour**)vr.allocate(sizeof(Colour*)*X);
  for (int i = 0; i < X; ++i) {
    m[i] = (Colour*)vr.allocate(sizeof(Colour)*Y);
    for (int j = 0; j < Y; ++j) {
      unsigned int r,g,b,a;
      if (bitdepth == 8) {
    r = row_pointers[Y-j-1][i*pixel_size];
    g = row_pointers[Y-j-1][i*pixel_size+1];
    b = row_pointers[Y-j-1][i*pixel_size+2];
    if (pixel_size == 4) a = row_pointers[Y-j-1][i*pixel_size+3];
    else a = 255;
    m[i][j] = Colour((float)r/256, (float)g/256, (float)b/256, (float)a/256);
      } else if (bitdepth == 16) {
    r = row_pointers[Y-j-1][i*pixel_size*2]*256 + row_pointers[Y-j-1][i*pixel_size*2+1];
    g = row_pointers[Y-j-1][i*pixel_size*2+2]*256 + row_pointers[Y-j-1][i*pixel_size*2+3];
    b = row_pointers[Y-j-1][i*pixel_size*2+4]*256 + row_pointers[Y-j-1][i*pixel_size*2+5];
    if (pixel_size == 4) r = row_pointers[Y-j-1][i*pixel_size*6]*256 + row_pointers[Y-j-1][i*pixel_size*2+7];
    else a = 256*256-1;
    m[i][j] = Colour((float)r/256/256, (float)g/256/256, (float)b/256/256, (float)a/256/256);
      }
    }
  }

  png_destroy_read_struct(&png_ptr, &info_ptr,&end_info);
  fclose(fp);
  */
}
