#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include "common/Math/include/render.h"
#include "common/Math/include/errors.h"
#include <fstream>
#include <iostream>
#include <vector>

#define NAME_LENGTH 64

using namespace std;

class SceneRecord
{
    string name;
    int dim;
    int index;
    uint createDate;
    uint modifyDate;

    string nameTon256(string s) const;
    string n256ToName(string s) const;
public:
    SceneRecord(ifstream &in, int dim);
    SceneRecord(fstream &out, const Camera<RTYPE> *cam, string name);
    string getName() const {return name;}
    void rewrite(fstream &out, const Camera<RTYPE> *cam, string name);
    void load(ifstream &in, Camera<RTYPE> *cam) const;
    uint getCreateDate() const {return createDate;}
    uint getModifyDate() const {return modifyDate;}
};

class SceneManager
{
    static int version;

    string filename;
    vector<SceneRecord*> records;
    int dim;
    int countp;

    void read(ifstream &in, void *data, int length);
    void write(fstream &out, void *data, int length);
    bool readFile();
    void writeFile();
    void rewriteRecord(int i, const Camera<RTYPE> *cam, string name);
public:
    SceneManager(string filename, int dim);
    ~SceneManager();
    int getVersion() const {return version;}
    bool addEditRecord(const Camera<RTYPE> *cam, string name);
    bool deleteRecord(string name);
    bool loadRecord(string name, Camera<RTYPE> *cam) const;
    bool isRecord(string name);
    int size() const;
    SceneRecord *getRecord(int i) const;
    string getFilename() const {return filename;}
};

#endif // SCENEMANAGER_H
