#include "sessionfile.h"

SessionFile::SessionFile()
{

}

bool SessionFile::save(string file, const SessionFileData &data)
{
    filename = file;
    return save(data);
}

bool SessionFile::save(const SessionFileData &data)
{
    if (filename == "") return true;
    ofstream out(filename.c_str());
    if (!out.is_open()) return false;
    out<<"version="<<SESSIONFILE_VERSION<<endl;
    out<<"dim="<<data.dim<<endl;
    out<<"lastcollection="<<data.lastcollection<<endl;
    out<<"laststyle="<<data.laststyle<<endl;
    out<<"numrecentcollections="<<data.recentcollections.size()<<endl;
    out<<"numrecentstyles="<<data.recentstyles.size()<<endl;
    out<<"script="<<data.script<<endl;
    for (int i = 0; i < data.recentcollections.size(); ++i) out<<"recentcollections_"<<i<<"="<<data.recentcollections[i]<<endl;
    for (int i = 0; i < data.recentstyles.size(); ++i) out<<"recentstyles_"<<i<<"="<<data.recentstyles[i]<<endl;
    out<<"#parameters"<<endl;
    for (int i = 0; i < data.param.size(); ++i) {
        string id = data.param.getStyleId(i);
        out<<data.param.getType(id)<<" "<<id<<"="<<data.param.getValue(id)<<endl;
    }
    out<<"#styles"<<endl;
    for (int i = 0; i < data.style.size(); ++i) {
        string id = data.style.getStyleId(i);
        out<<data.style.getType(id)<<" "<<id<<"="<<data.style.getValue(id)<<endl;
    }
    return true;
}

Pss stringToKeyValue(string s)
{
    int n = s.find('=');
    return make_pair(s.substr(0,n), s.substr(n+1,-1));
}

bool SessionFile::load(string file, SessionFileData &data)
{
    filename = file;
    return load(data);
}

bool SessionFile::load(SessionFileData &data)
{
    if (filename == "") return true;
    ifstream in(filename.c_str());
    if (!in.is_open()) return false;
    string s;
    getline(in,s);
    Pss pss = stringToKeyValue(s);
    if (pss.first != "version") return false;
    int version = readInt(pss.second);
    if (version != 1) return false;
    getline(in,s);
    pss = stringToKeyValue(s);
    if (pss.first != "dim") return false;
    data.dim = readInt(pss.second);
    if (data.dim < 3) return false;
    getline(in,s);
    pss = stringToKeyValue(s);
    if (pss.first != "lastcollection") return false;
    data.lastcollection = pss.second;
    getline(in,s);
    pss = stringToKeyValue(s);
    if (pss.first != "laststyle") return false;
    data.laststyle = pss.second;
    getline(in,s);
    pss = stringToKeyValue(s);
    if (pss.first != "numrecentcollections") return false;
    int numc = readInt(pss.second);
    getline(in,s);
    pss = stringToKeyValue(s);
    if (pss.first != "numrecentstyles") return false;
    int nums = readInt(pss.second);
    getline(in,s);
    pss = stringToKeyValue(s);
    if (pss.first != "script") return false;
    data.script = pss.second;

    data.recentcollections.resize(numc);
    data.recentstyles.resize(nums);
    for (int i = 0; i < numc; ++i) {
        getline(in,s);
        pss = stringToKeyValue(s);
        stringstream ss;
        ss<<"recentcollections_"<<i;
        if (pss.first != ss.str()) return false;
        data.recentcollections[i] = pss.second;
    }
    for (int i = 0; i < nums; ++i) {
        getline(in,s);
        pss = stringToKeyValue(s);
        stringstream ss;
        ss<<"recentstyles_"<<i;
        if (pss.first != ss.str()) return false;
        data.recentstyles[i] = pss.second;
    }
    getline(in,s);
    data.param.clear();
    data.style.clear();
    if (s == "#parameters") {
        while (getline(in,s)) {         //params
            if (s == "#styles") break;
            string type;
            string id;
            string value;
            int eq = s.find('=');
            int sp = s.find(' ');
            type = s.substr(0,sp);
            id = s.substr(sp+1,eq-(sp+1));
            value = s.substr(eq+1,-1);
            data.param.addStyle(type,id,value);
        }
        while (getline(in,s)) {         //styles
            string type;
            string id;
            string value;
            int eq = s.find('=');
            int sp = s.find(' ');
            type = s.substr(0,sp);
            id = s.substr(sp+1,eq-(sp+1));
            value = s.substr(eq+1,-1);
            data.style.addStyle(type,id,value);
        }
    }
    return true;
}
