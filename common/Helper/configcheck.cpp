#include "configcheck.h"

void checkMachineConfig(MachineConfig *cfg, int maxdisplaysize, bool warning)
{
    stringstream ss;
    ss<<maxdisplaysize;
    int light = readInt(cfg->getApp()->getAttribute("lighting", DEFAULT_LIGHTING));
    if (light < 0 || light > maxdisplaysize) throw Exception(INVALID_DATA, "Lightting cannot be negative.");
    int occlusion = readInt(cfg->getApp()->getAttribute("occlusion", DEFAULT_OCCLUSION));
    if (occlusion < 0 || occlusion > maxdisplaysize*2) throw Exception(INVALID_DATA, "Occlusion cannot be negative.");
    int reclimit = readInt(cfg->getApp()->getAttribute("reclimit", DEFAULT_RECMEMORY));
    if (reclimit < 1 || reclimit > 67108864) throw Exception(INVALID_DATA, "Recursion limit must be at least 1.");
    int threads = readInt(cfg->getApp()->getAttribute("threads", DEFAULT_THREADS));
    if (threads < 1 || threads > maxdisplaysize*3) throw Exception(INVALID_DATA, "Invalid thread number.");
    int vram = readInt(cfg->getApp()->getAttribute("vram", DEFAULT_VRAM));
    if (vram < 1024 || vram > 268435456) throw Exception(INVALID_DATA, "Too small runtime vram, use at least 1024.");
    int fvram = readInt(cfg->getApp()->getAttribute("functionvram", DEFAULT_FVRAM));
    if (fvram < 1024 || fvram > 2147483648) throw Exception(INVALID_DATA, "Too small script vram, use at least 1024.");
    string disp = cfg->getApp()->getAttribute("display","on");
    if (disp != "on" && disp != "off") throw Exception(INVALID_DATA, "display can only be 'on' or 'off'.");
    int density = readInt(cfg->getApp()->getAttribute("density", DEFAULT_SDENSITY));
    if (density < 1 || density > maxdisplaysize) throw Exception(INVALID_DATA, "Invalid display resolution (dnesity)");
    int depth = readInt(cfg->getApp()->getAttribute("depth", DEFAULT_SDEPTH));
    if (depth < 1 || depth > maxdisplaysize*maxdisplaysize) throw Exception(INVALID_DATA, "Invalid display depth");
    int numd = readInt(cfg->getApp()->getAttribute("numd", DEFAULT_SNUMD));
    if (numd < 1 || numd > maxdisplaysize) throw Exception(INVALID_DATA, "Number of steps exceeds the limit of "+ss.str());
    int screenX = readInt(cfg->getWindow()->getAttribute("width", DEFAULT_WIDTH));
    int screenY = readInt(cfg->getWindow()->getAttribute("height", DEFAULT_HEIGHT));
	if (warning) {
	  if (numd*density > screenX && numd*density > screenY) {
		  LOG<<WARNING<<"steps*density exceeds the dispaly resolution. It will lead to unneccessary calculations."<<endl<<NORMAL;
	  }
	}
}
