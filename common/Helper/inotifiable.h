#ifndef INOTIFIABLE
#define INOTIFIABLE

#include <iostream>

using namespace std;

class INotifiable
{
public:
    virtual void notify() {}
    virtual void notify(int) {}
    virtual void notify(float) {}
    virtual void notify(double) {}
    virtual void notify(char) {}
    virtual void notify(unsigned long long int) {}
    virtual void notify(void*) {}
    virtual void notify(string) {}
};

#endif // INOTIFIABLE

