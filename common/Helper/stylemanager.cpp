#include "stylemanager.h"

ostream& operator<<(ostream &out, StyleSheet &ss)
{
    out<<"#"<<ss.getName()<<endl;
    for (int i = 0; i < ss.size(); ++i) {
        string id = ss.getStyleId(i);
        out<<ss.getType(id)<<" "<<id<<" = "<<ss.getValue(id)<<endl;
    }
    return out;
}

StyleSheet::StyleSheet(string name) : name(name)
{
    creation = modification = getDate();
}

void StyleSheet::modify()
{
    modification = getDate();
}

StyleSheet::StyleSheet(string nameline, ifstream &in)
{
    read(nameline, in);
}

void StyleSheet::addStyle(string type, string id, string value)
{
    styles.push_back(StyleRecord(type, id,value));
}

int StyleSheet::find(string id) const
{
    int ret;
    for (ret = 0; ret < styles.size() && styles[ret].id != id; ++ret);
    if (ret >= styles.size()) ret = -1;
    return ret;
}

string StyleSheet::getValue(string id) const
{
    int f = find(id);
    if (f == -1) throw Exception(INTERNAL_ERROR, "Could not find StyleSheet: " + id);
    return styles[f].value;
}

string StyleSheet::getValue(string id, string def) const
{
    int f = find(id);
    if (f == -1) return def;
    return styles[f].value;
}

string StyleSheet::getType(string id) const
{
    int f = find(id);
    if (f == -1) throw Exception(INTERNAL_ERROR, "Could not find StyleSheet: " + id);
    return styles[f].type;
}

void StyleSheet::setType(string id, string type)
{
    int f = find(id);
    if (f == -1) throw Exception(INTERNAL_ERROR, "Could not find StyleSheet: " + id);
    styles[f].type = type;
}

bool StyleSheet::isStyle(string id) const
{
    int f = find(id);
    return f != -1;
}

void StyleSheet::setStyle(string id, string val)
{
    int f = find(id);
    if (f == -1) throw Exception(INTERNAL_ERROR, "Could not find StyleSheet: " + id);
    styles[f].value = val;
}

string StyleSheet::simplifyString(string s)
{
    string ret = "";
    for (int i = 0; i < s.length(); ++i) {
        if (s[i] == '\n' || s[i] == '\r' || s[i] == 0) continue;
        ret += s[i];
    }
    return ret;
}

void StyleSheet::read(string nameline, ifstream &in)
{
    styles.clear();
    name = simplifyString(nameline);
    string s;
    /*getline(in,s);
    try {
        creation = readInt(s);
    } catch (ERROR_CODE e) {
        Error::ERROR_STRING = "Style creation date is in an invalid format: " + name;
        throw e;
    }

    getline(in,s);
    try {
        modification = readInt(s);
    } catch (ERROR_CODE e) {
        Error::ERROR_STRING = "Style modification date is in an invalid format: " + name;
        throw e;
    }*/
    while (getline(in,s)) {
        s = simplifyString(s);
        if (s == "---") break;
        int eq = s.find('=');
        if (eq == -1) throw Exception(FORMAT_ERROR,"'=' missing from StyleSheet");
        string id, val;
        id = s.substr(0,eq);
        val = s.substr(eq+1,-1);
        if (id == "@created" || id == "@modified") {
            int num = 0;
            try {
                num = readInt(val);
            } catch (ERROR_CODE e) {
                Error::ERROR_STRING = "Style '" + id + "' is in an invalid format: " + name;
                throw e;
            }
            if (id == "@created") creation = num;
            else modification = num;
        } else {
            int n = id.length()-1;
            while (n > 0 && !iswspace(id[n])) n--;
            addStyle(id.substr(0,n),id.substr(n+1,-1),val);
        }
    }
}

void StyleSheet::write(ofstream &out)
{
    out<<"#"<<name<<endl;
    out<<"@created="<<creation<<endl;
    out<<"@modified="<<modification<<endl;
    for (int i = 0; i < styles.size(); ++i) out<<styles[i].type<<" "<<styles[i].id<<"="<<styles[i].value<<endl;
    out<<"---"<<endl;
}

int StyleManager::version = 1;

StyleManager::StyleManager(string filename) : filename(filename)
{
    if (filename != "") if (!read()) save();
}

bool StyleManager::read()
{
    clear();
    ifstream in(filename.c_str());
    if (!in.is_open()) return false;
    string ver;
    getline(in,ver);
    string nameline;
    while (getline(in,nameline)) {
        if (nameline.length() == 0) continue;
        if (nameline[0] != '#') throw Exception(FORMAT_ERROR,"A stylesheet must begin with #<name>");
        StyleSheet *ss = new StyleSheet(nameline.substr(1,-1),in);
        stylesheets.push_back(ss);
    }
    return true;
}

bool StyleManager::save()
{
    ofstream out(filename.c_str());
    if (!out.is_open()) return false;
    out<<version<<endl;
    for (int i = 0; i < stylesheets.size(); ++i) stylesheets[i]->write(out);
    return true;
}

void StyleManager::clear()
{
    for (int i = 0; i < stylesheets.size(); ++i) delete stylesheets[i];
    stylesheets.clear();
}

int StyleManager::find(string id) const
{
    int ret;
    for (ret = 0; ret < stylesheets.size() && stylesheets[ret]->getName() != id; ++ret);
    if (ret >= stylesheets.size()) ret = -1;
    return ret;
}

StyleSheet* StyleManager::getStyleSheet(string id)
{
    int f = find(id);
    if (f == -1) return NULL;
    return stylesheets[f];
}

void StyleManager::removeStyleSheet(string id)
{
    int f = find(id);
    if (f == -1) return;
    for (int i = f; i+1 < stylesheets.size(); ++i) stylesheets[i] = stylesheets[i+1];
    stylesheets.resize(stylesheets.size()-1);
}

bool StyleManager::isRecord(string id) const
{
    int f = find(id);
    return f != -1;
}

StyleManager::~StyleManager()
{
    save();
    clear();
}

