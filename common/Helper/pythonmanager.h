#ifndef PYTHONMANAGER_H
#define PYTHONMANAGER_H

#include <iostream>
#include <fstream>
#include <Python.h>
#include <vector>
#include <regex>

#include "pythoncodes.h"

#define PM (*PythonManager::instance)
#define TEMP_FILE "nvw_export_data/"

using namespace std;

typedef vector<pair<string,string> > Pairs;

class PythonManager
{
    string formatCode(Pairs &args, string code);
public:
    static PythonManager *instance;
    PythonManager(char* arhv0);
    ~PythonManager();


	void mkdir(string path);
	
    void createTemp(string path);
    void createScripts(string path);
    void copyFile(string file, string path);
    void copyScript(string filename, string path);
    void zipDir(string path, string filename);
    void deleteDir(string path);
	
	void unzipDir(string path, string filename);
};

#endif // PYTHONMANAGER_H
