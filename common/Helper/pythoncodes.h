#ifndef PYTHONCODES_H
#define PYTHONCODES_H

#define MKDIR "\
import os, shutil\n\
path = '@path'\n\
if not os.path.exists(path):\n\
	os.mkdir(path)"

#define CREATE_TEMP "\
import os, shutil\n\
path = '@path'\n\
if os.path.exists(path):\n\
    shutil.rmtree(path, ignore_errors=True)\n\
os.mkdir(path)"

#define CREATE_SCRIPTS "\
import os, shutil\n\
path = '@path'\n\
os.mkdir(os.path.join(path, 'Scripts'))"


#define COPY_FILE "\
import os, shutil\n\
path = '@path'\n\
file = '@file'\n\
filename = file.split('/')[-1]\n\
shutil.copyfile(file, os.path.join(path, filename))"


#define COPY_SCRIPT "\
import os, shutil\n\
def copyFile(file, path):\n\
    filename = file.split('/')[-1]\n\
    shutil.copyfile(file, os.path.join(path, filename))\n\
path = '@path'\n\
filename = '@filename'\n\
path = os.path.join(path,'Scripts')\n\
if ';' not in filename:\n\
    copyFile(filename, path)\n\
else:\n\
    begin, end = filename.split(';')\n\
    path = os.path.join(path, '/'.join(end.split('/')[0:-1]))\n\
    os.makedirs(path)\n\
    copyFile(os.path.join(begin,end), path)"


#define ZIP_DIR "\
import os, shutil, zipfile\n\
path = '@path'\n\
filename = '@filename'\n\
zipf = zipfile.ZipFile(filename, 'w', zipfile.ZIP_DEFLATED)\n\
for root, dirs, files in os.walk(path):\n\
  for file in files:\n\
      zipf.write(os.path.join(root, file))\n\
zipf.close()"

#define DELETE_DIR "\
import os, shutil\n\
path = '@path'\n\
if os.path.exists(path):\n\
    shutil.rmtree(path, ignore_errors=True)"
    
    
#define UNZIP_DIR "\
import os, shutil, zipfile\n\
path = '@path'\n\
filename = '@filename'\n\
zipf = zipfile.ZipFile(filename, 'r')\n\
zipf.extractall(path)\n\
zipf.close()"

#endif // PYTHONCODES_H

