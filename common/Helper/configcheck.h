#ifndef CONFIGCHECK_H
#define CONFIGCHECK_H

#include "common/Math/include/config.h"

void checkMachineConfig(MachineConfig *cfg, int maxdisplaysize, bool warning = true);

#endif // CONFIGCHECK_H
