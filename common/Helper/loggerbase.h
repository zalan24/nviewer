#ifndef LOGGERBASE_H
#define LOGGERBASE_H


enum LogModifier
{
    NORMAL,
    WARNING,
    ERROR
};

#include <iostream>

using namespace std;

class LoggerBase
{
public:
    virtual void log(string line) = 0;
    virtual void logWarning(string line) = 0;
    virtual void logError(string line) = 0;
    virtual void loadText(string s) = 0;
    virtual void logLoaded(LogModifier m) = 0;
    virtual ~LoggerBase() {}
};

#endif // LOGGERBASE_H

