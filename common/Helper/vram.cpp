#include "vram.h"

VRam::VRam(ulli length) : length(length), start(0), max(0) {
    //cout<<length<<endl;
    n = 0;
    bn = 0;
    bid = 0;
    blocknum = length / VBLOCK_SIZE;
    if (length % VBLOCK_SIZE != 0) blocknum++;
    ulli len = length;
    int bd = -1;
    try {
        data = new unsigned char**[blocknum];
        bd = 0;
        //cout<<"bl: "<<blocknum<<endl;
        while (len > 0) {
            ulli l = len;
            if (l > VBLOCK_SIZE) l = VBLOCK_SIZE;
            //cout<<bd<<": "<<l<<endl;
            data[bd] = new unsigned char*[l];
            bd++;
            len -= l;
        }
    } catch (std::bad_alloc& ba) {
        if (bd != -1) {
            while (bd > 0) {
                bd--;
                delete [] data[bd];
            }
            delete [] data;
        }
        data = NULL;
        throw RuntimeError(BAD_ALLOC, "Cannot allocate memory", ERROR_CONTENT);
    }

    stringstream ss;
    //ss<<"Vram initialised: "<<data<<" - "<<data+length;
    LOG.debug(ss.str());
}

VRam::~VRam() {
    for (int i = 0; i < blocknum; ++i) delete [] data[i];
    delete [] data;
}

ulli VRam::getMax() const
{
    if (getAllocated() > max) return getAllocated();
    return max;
}

void* VRam::allocate(ulli size) {
    //cout<<n<<" "<<size<<endl;
    if (n+size > length) throw RuntimeError(OUTOFMEMORY, "Not enough VRam. Try changing the system configuration",ERROR_CONTENT);
    if (bn+size > VBLOCK_SIZE) {
        if (size > VBLOCK_SIZE) throw RuntimeError(OUTOFMEMORY, "Array too long to allocate",ERROR_CONTENT);
        bid++;
        n = (ulli)bid * VBLOCK_SIZE;
        bn = 0;
        void *ret = (void*)(data[bid]+bn);
        n  += size;
        bn += size;
        return ret;
    } else {
        void *ret = (void*)(data[bid]+bn);
        n  += size;
        bn += size;
        return ret;
    }
    /*if (n+size > length) throw RuntimeError(OUTOFMEMORY, "Not enough VRam. Try changing the system configuration",ERROR_CONTENT);
    void *ret = (void*)(data+n);
    n += size;
    return ret;*/
}

void VRam::setStart(ulli s) {
    start = s;
}

void VRam::clear() {
    max = getMax();
    n = start;
    bn = start % VBLOCK_SIZE;
    bid = start / VBLOCK_SIZE;
}

ulli VRam::getAllocated() const {
    return n;
}

ulli VRam::size() const {
    return length;
}

string VRam::getStatus() const {
    stringstream ss;
    long long l = n;
    l *= 100;
    ss<<l/length<<"% ("<<(n/1024/1024)<<" Mib)";
    return ss.str();
}

void VRam::free(ulli i) {
    max = getMax();
    n -= (int)i;
    if ((int)n < 0) {
        n = 0;
        bn = 0;
        bid = 0;
    } else {
        bn -= i;
        while (bn < 0) {
            bn += VBLOCK_SIZE;
            bid--;
        }
    }
}

void VRam::freeAfter(ulli i) {
    n = i;
    bn = i % VBLOCK_SIZE;
    bid = i / VBLOCK_SIZE;
}

