#include "pythonmanager.h"

PythonManager* PythonManager::instance = NULL;

PythonManager::PythonManager(char* argv0)
{
    PythonManager::instance = this;
    Py_SetProgramName(argv0);
    Py_Initialize();
}

string PythonManager::formatCode(vector<pair<string,string> > &args, string code)
{
    for (int i = 0; i < args.size(); ++i) {
        code = regex_replace(code, regex(args[i].first), args[i].second);
    }
    return code;
}

void PythonManager::mkdir(string path)
{
    Pairs pairs;
    pairs.push_back(make_pair("@path", path));
    string code = formatCode(pairs, MKDIR);
    PyRun_SimpleString(code.c_str());
}

void PythonManager::createTemp(string path)
{
    Pairs pairs;
    pairs.push_back(make_pair("@path", path));
    string code = formatCode(pairs, CREATE_TEMP);
    PyRun_SimpleString(code.c_str());
}

void PythonManager::createScripts(string path)
{
    Pairs pairs;
    pairs.push_back(make_pair("@path", path));
    string code = formatCode(pairs, CREATE_SCRIPTS);
    PyRun_SimpleString(code.c_str());
}

void PythonManager::copyFile(string file, string path)
{
    Pairs pairs;
    pairs.push_back(make_pair("@path", path));
    pairs.push_back(make_pair("@file", file));
    string code = formatCode(pairs, COPY_FILE);
    PyRun_SimpleString(code.c_str());
}

void PythonManager::copyScript(string filename, string path)
{
    Pairs pairs;
    pairs.push_back(make_pair("@path", path));
    pairs.push_back(make_pair("@filename", filename));
    string code = formatCode(pairs, COPY_SCRIPT);
    PyRun_SimpleString(code.c_str());
}

void PythonManager::zipDir(string path, string filename)
{
    Pairs pairs;
    pairs.push_back(make_pair("@path", path));
    pairs.push_back(make_pair("@filename", filename));
    string code = formatCode(pairs, ZIP_DIR);
    PyRun_SimpleString(code.c_str());
}

void PythonManager::deleteDir(string path)
{
    Pairs pairs;
    pairs.push_back(make_pair("@path", path));
    string code = formatCode(pairs, DELETE_DIR);
    PyRun_SimpleString(code.c_str());
}

void PythonManager::unzipDir(string path, string filename)
{
  Pairs pairs;
  pairs.push_back(make_pair("@path", path));
  pairs.push_back(make_pair("@filename", filename));
  string code = formatCode(pairs, UNZIP_DIR);
  PyRun_SimpleString(code.c_str());
}

PythonManager::~PythonManager()
{
    Py_Finalize();
}
