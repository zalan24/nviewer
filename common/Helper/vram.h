#ifndef VRAM_H
#define VRAM_H

#define VBLOCK_SIZE 1048576
//#define VBLOCK_SIZE 1048576000
//#define VBLOCK_SIZE 40960

#include "common/Math/include/tools.h"

class VRam
{
  unsigned char ***data;
  ulli n;
  int bn;
  int bid;
  ulli length;
  ulli start;
  ulli max;
  int blocknum;
public:
  VRam(ulli length);
  ~VRam();
  void *allocate(ulli size);
  void setStart(ulli s);
  void clear();
  ulli getAllocated() const;
  ulli size() const;
  string getStatus() const;
  void free(ulli i);
  void freeAfter(ulli i);
  ulli getMax() const;
};

#endif // VRAM_H
