#ifndef SESSIONFILE_H
#define SESSIONFILE_H

#define SESSIONFILE_VERSION 1

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "common/Math/include/tools.h"
#include "common/Helper/stylemanager.h"

using namespace std;

/*
 * version=<version>  int
 * dim=<dim>  int
 * lastcollection=<lastcollection>  string
 * laststyle=<laststyle>  string
 * numrecentcollections=<numrecentcollections>  int
 * numrecentstyles=<numrecentstyles>  int
 * recentcollections_x=<recentcollections_x>  string
 * ...
 * recentstyles_x=<recentstyles_x>  string
 * ...
 * script=<script>  string
 * #parameters
 * type_x id_x=<val_x>  string
 * ...
 * #styles
 * type_X id_x=<val_x>  string
 * ...
 */

typedef pair<string,string> Pss;

Pss getKeyValue(string s);

struct SessionFileData
{
    int dim;
    string lastcollection;
    string laststyle;
    string script;
    StyleSheet style;
    StyleSheet param;
    vector<string> recentcollections;
    vector<string> recentstyles;
    SessionFileData() : dim(0), lastcollection(""), laststyle(""), script(""), style(""), param("") {}
    SessionFileData(int dim, string lastcollection, string laststyle, string script) :
        dim(dim),
        lastcollection(lastcollection),
        laststyle(laststyle),
        script(script),
        style("style"),
        param("parameter") {}
};

class SessionFile
{
    string filename;
public:
    SessionFile();

    bool save(string file, const SessionFileData &data);
    bool load(string file, SessionFileData &data);
    bool save(const SessionFileData &data);
    bool load(SessionFileData &data);

    string getFilename() const {return filename;}
};

#endif // SESSIONFILE_H
