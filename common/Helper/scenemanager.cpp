#include "scenemanager.h"

#include <ctime>

int SceneManager::version = 1;

/* .scnc file:
 * --- header ---
 * 4 : version
 * 4 : header size (17)
 * 4 : dimensions
 * 4 : record count
 * 1 : real type size (s(T) := sizeof : float / double / long long double)
 * ---  data  ---
 * One record:
 * |-64 : name
 * |-4 : create date
 * |-4 : modify date
 * |-s(T) : focus
 * |-s(T) : scale
 * |-s(T)*(dim+1)*(dim+1) : matrix
 */

SceneRecord::SceneRecord(ifstream &in, int dim) : dim(dim)
{
    index = in.tellg();
    char nm[NAME_LENGTH];
    in.read(nm,NAME_LENGTH);
    in.read((char*)&createDate,sizeof(createDate));
    in.read((char*)&modifyDate,sizeof(modifyDate));
    //cout<<string(nm).length()<<endl;
    name = n256ToName(string(nm).substr(0,NAME_LENGTH));
    int datlen = sizeof(RTYPE) * (1+1+(dim+1)*(dim+1));
    char data[datlen];
    in.read(data,datlen);
}

SceneRecord::SceneRecord(fstream &out, const Camera<RTYPE> *cam, string name) : name(name)
{
    out.seekp (0, ios::end);
    index = out.tellp();
    string nm = nameTon256(name);
    out.write(nm.c_str(),nm.length());
    modifyDate = createDate = getDate();
    out.write((char*)&createDate,sizeof(createDate));
    out.write((char*)&modifyDate,sizeof(modifyDate));
    RTYPE f = cam->getFocus();
    RTYPE s = cam->getScale();
    out.write((char*)&f,sizeof(RTYPE));
    out.write((char*)&s,sizeof(RTYPE));
    if (cam == NULL) {
        throw Exception(COULD_NOT_WRITE_FILE, "Scene record created camera");
        /*for (int i = 0; i < dim+1; ++i) for (int j = 0; j < dim+1; ++j) {
            RTYPE x = 0;
            out.write((char*)&x,sizeof(RTYPE));
        }*/
    } else {
        Matrix<RTYPE> m = cam->getTransform();
        dim = m.getRows()-1;
        for (int i = 0; i < m.getRows(); ++i) for (int j = 0; j < m.getColoumns(); ++j) {
            RTYPE x = m.get(i,j);
            out.write((char*)&x,sizeof(RTYPE));
        }
    }
}

void SceneRecord::rewrite(fstream &out, const Camera<RTYPE> *cam, string n)
{
    name = n;
    out.seekp(index, std::ios_base::beg);
    string nm = nameTon256(name).c_str();
    out.write(nm.c_str(),nm.length());
    modifyDate = getDate();
    out.write((char*)&createDate,sizeof(createDate));
    out.write((char*)&modifyDate,sizeof(modifyDate));
    if (cam == NULL) {
        RTYPE f = 0;
        RTYPE s = 0;
        out.write((char*)&f,sizeof(RTYPE));
        out.write((char*)&s,sizeof(RTYPE));
        for (int i = 0; i < dim+1; ++i) for (int j = 0; j < dim+1; ++j) {
            RTYPE x = 0;
            out.write((char*)&x,sizeof(RTYPE));
        }
    } else {
        RTYPE f = cam->getFocus();
        RTYPE s = cam->getScale();
        out.write((char*)&f,sizeof(RTYPE));
        out.write((char*)&s,sizeof(RTYPE));
        Matrix<RTYPE> m = cam->getTransform();
        for (int i = 0; i < m.getRows(); ++i) for (int j = 0; j < m.getColoumns(); ++j) {
            RTYPE x = m.get(i,j);
            out.write((char*)&x,sizeof(RTYPE));
        }
    }
}

string SceneRecord::nameTon256(string s) const
{
    while (s.length() < NAME_LENGTH) s = "#" + s;
    return s;
}

string SceneRecord::n256ToName(string s) const
{
    while (s.length() > 0 && s[0] == '#') s = s.substr(1,-1);
    return s;
}

void SceneRecord::load(ifstream &in, Camera<RTYPE> *cam) const
{
    in.seekg(index);
    char nm[NAME_LENGTH];
    in.read(nm,NAME_LENGTH);
    string n = n256ToName(string(nm).substr(0,NAME_LENGTH));
    if (n != name) throw Exception(COULD_NOT_READ_FILE, "Scene record read a different name from its own. Is it a new file?");
    in.read((char*)&createDate,sizeof(createDate));
    in.read((char*)&modifyDate,sizeof(modifyDate));
    RTYPE f,s;
    in.read((char*)&f,sizeof(f));
    in.read((char*)&s,sizeof(s));
    Matrix<RTYPE> m(dim+1,dim+1);
    for (int i = 0; i < m.getRows(); ++i) for (int j = 0; j < m.getColoumns(); ++j) {
        RTYPE x = 0;
        in.read((char*)&x,sizeof(RTYPE));
        m.set(i,j,x);
    }
    cam->setFocus(f);
    cam->setScale(s);
    cam->setTransform(m);
}

SceneManager::SceneManager(string filename, int dim) : filename(filename), dim(dim)
{
    countp = 12;
    if (!readFile()) writeFile();
}

SceneManager::~SceneManager()
{
    for (int i = 0; i < records.size(); ++i) delete records[i];
}

void SceneManager::writeFile()
{
    fstream out(filename.c_str(), ios::binary | ios::out | ios::trunc);
    if (!out.is_open()) throw Exception(CANNOT_OPEN_FILE, "Could not open scene collection file");

    int v = getVersion();
    int h = 17;
    int count = 0;
    char s = sizeof(RTYPE);
    write(out, &v, sizeof(v));
    write(out, &h, sizeof(h));
    write(out, &dim, sizeof(dim));
    write(out, &count, sizeof(count));
    write(out, &s, sizeof(s));
    LOG<<"New scene collection created: "<<filename<<endl;
}

bool SceneManager::readFile()
{
    ifstream in(filename.c_str(), ios::binary);
    if (!in.is_open()) return false; //throw Exception(CANNOT_OPEN_FILE, "Could not open scene collection file");
    in.seekg (0, ios::end);
    int length = in.tellg();
    in.seekg (0, ios::beg);
    if (length == 0) return false;

    int v = 0;
    int h = 0;
    int dim = 0;
    int count = 0;
    char s = 0;
    read(in, &v, sizeof(v));
    read(in, &h, sizeof(h));
    read(in, &dim, sizeof(dim));
    read(in, &count, sizeof(count));
    read(in, &s, sizeof(s));
    if (s != sizeof(RTYPE)) {
        throw Exception(DIFFERENT_REAL_TYPE, "The collection file uses a different real-type representation.");
        //LOG<<WARNING<<"The collection file uses a different real-type representation. Using autoconvert..."<<endl<<NORMAL;
    }
    for (int i = 0; i < count; ++i) {
        SceneRecord *r = new SceneRecord(in,dim);
        records.push_back(r);
    }
    LOG<<"Scene collection opened: "<<filename<<endl;

    return true;
}

void SceneManager::read(ifstream &in, void *data, int length)
{
    char *cs = (char*)data;
    in.read(cs,length);
}

void SceneManager::write(fstream &out, void *data, int length)
{
    char *cs = (char*)data;
    out.write(cs,length);
}

void SceneManager::rewriteRecord(int i, const Camera<RTYPE> *cam, string name)
{
    fstream out(filename.c_str(), ios::binary | ios::out | ios::in | ios::ate);
    records[i]->rewrite(out, cam, name);
}

bool SceneManager::addEditRecord(const Camera<RTYPE> *cam, string name)
{
    int found = -1;
    int found2 = -1;
    for (int i = 0; i < records.size() && found  == -1; ++i) found  = records[i]->getName() == ""   ? i : found;
    for (int i = 0; i < records.size() && found2 == -1; ++i) found2 = records[i]->getName() == name ? i : found2;
    if (found2 != -1) {
        rewriteRecord(found2, cam, name);
    } else if (found != -1) {
        rewriteRecord(found, cam, name);
    } else {
        fstream out(filename.c_str(), ios::binary | ios::out | ios::in | ios::ate);
        SceneRecord *r = new SceneRecord(out, cam, name);
        records.push_back(r);
        out.seekp(countp, std::ios_base::beg);
        int count = records.size();
        out.write((char*)&count,sizeof(count));
    }
    return true;
}

bool SceneManager::deleteRecord(string name)
{
    int found = -1;
    for (int i = 0; i < records.size() && found == -1; ++i) found = records[i]->getName() == name ? i : found;
    if (found != -1) {
        rewriteRecord(found,NULL,"");
        return true;
    } else return false;
}

bool SceneManager::loadRecord(string name, Camera<RTYPE> *cam) const
{
    int found = -1;
    for (int i = 0; i < records.size() && found == -1; ++i) found = records[i]->getName() == name ? i : found;
    if (found != -1) {
        ifstream in(filename.c_str(), ios::binary);
        records[found]->load(in, cam);
        return true;
    } else return false;
}

bool SceneManager::isRecord(string name)
{
    bool found = false;
    for (int i = 0; i < records.size() && !found; ++i) found = records[i]->getName() == name;
    return found;
}

int SceneManager::size() const
{
    return records.size();
}

SceneRecord *SceneManager::getRecord(int i) const
{
    return records[i];
}
