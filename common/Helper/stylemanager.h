#ifndef STYLEMANAGER_H
#define STYLEMANAGER_H

#include "common/Math/include/info.h"
#include "common/Math/include/errors.h"
#include "common/Math/include/tools.h"
#include <vector>
#include <iostream>

using namespace std;

typedef unsigned int uint;

/*
 * Style file:
 * --- header ---
 * int - version
 * ---  data  ---
 * one element:
 * |- #id
 * |- @created=<creation>
 * |- @modified=<modification>
 * |- type id0=val0
 * |- type id1=val1
 * |- ...
 * |- ---
 */

struct StyleRecord
{
    string id;
    string value;
    string type;
    StyleRecord() : id(""), value(""), type("") {}
    StyleRecord(string type, string id, string value) : id(id), value(value), type(type) {}
};

class StyleSheet
{
    vector<StyleRecord> styles;
    string name;
    int find(string id) const;
    string simplifyString(string s);
    uint modification;
    uint creation;
public:
    StyleSheet(string name);
    StyleSheet(string nameline, ifstream &in);
    void addStyle(string type, string id, string value);
    template<class T> void addStyleT(string type, string id, T value);
    string getValue(string id) const;
    string getValue(string id, string def) const;
    string getType(string id) const;
    void setStyle(string id, string val);
    void setType(string id, string type);
    int size() const {return styles.size();}
    string getStyleId(int i) const {return styles[i].id;}
    void read(string nameline, ifstream &in);
    void write(ofstream &out);
    string getName() const {return name;}
    bool isStyle(string s) const;
    void clear() {styles.clear();}
    uint getCreation() const {return creation;}
    uint getModification() const {return modification;}
    void modify();
};

ostream& operator<<(ostream &out, StyleSheet &ss);

template<class T> void StyleSheet::addStyleT(string type, string id, T value)
{
    stringstream ss;
    ss<<value;
    addStyle(type, id,ss.str());
}

class StyleManager
{
    static int version;

    string filename;
    vector<StyleSheet*> stylesheets;

    int find(string id) const;
    bool read();
    void clear();
public:
    StyleManager(string filename);
    ~StyleManager();
    StyleSheet* getStyleSheet(string id);
    StyleSheet* getStyleSheet(int id) {return stylesheets[id];}
    void removeStyleSheet(string id);
    bool save();
    void setFileName(string file) {filename = file;}
    string getFileName() const {return filename;}
    bool isRecord(string id) const;
    void addStyleSheet(StyleSheet *ss) {stylesheets.push_back(ss);}
    int size() const {return stylesheets.size();}
};

#endif // STYLEMANAGER_H
