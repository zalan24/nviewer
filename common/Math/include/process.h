#include <iostream>

#include "tools.h"

using namespace std;

template <class T> void getPoints(T x, T y, T z, T w, T h, int n, int m, T *vectors, bool *leave, bool *pause)
{
  for (int i = 0; i < n; ++i) {
      while (*pause && !(*leave)) {
        std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
      }
      if (*leave) break;
    for (int j = 0; j < m; ++j) {
      int index = j*n + i;
      vectors[index*3  ] = x + w*i;
      vectors[index*3+1] = y + h*j;
      vectors[index*3+2] = z;
    }
  }
}

template <class T> void transformPoints(int dim, T * matrix, int n, T * vectors, T *output, bool *leave, bool *pause)
{
  for (int d = 0; d < dim; ++d) {
    register T mid1 = matrix[d * (dim+1)  ];
    register T mid2 = matrix[d * (dim+1)+1];
    register T mid3 = matrix[d * (dim+1)+2];
    register T midt = matrix[(d+1)*(dim+1)-1];
    for (int i = 0; i < n; ++i) {
      output[i*dim + d] = mid1 * vectors[i*3  ]
			+ mid2 * vectors[i*3+1]
			+ mid3 * vectors[i*3+2]
			+ midt;
      while (*pause && !(*leave)) {
        std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
      }
      if (*leave) break;
    }
  }
}

bool addColours(int, float*, float*, int*, bool*, bool*, double, int);
