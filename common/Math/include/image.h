#pragma once

#ifndef IMAGE_H
#define IMAGE_H

#include <stdio.h>
#include <iostream>
#include <math.h>

using namespace std;

#define RATE 1.2

namespace Image {

  class RGB
  {
      public:
      unsigned char r,g,b;
      RGB() {r=b=g=0;}
      RGB(unsigned char R, unsigned char G, unsigned char B) {r = R; g = G, b = B;}
      void set(unsigned char R, unsigned char G, unsigned char B) {r = R; g = G, b = B;}
      unsigned char operator[](const int n)
      {
	  switch (n) {
	      case 0:  return r;
	      case 1:  return g;
	      case 2:  return b;
	      default: return 0;
	  }
      }
  };
  
  void CloseGeometryImage();
  void InitGeometryImage(int,int);
  void DrawBackGround(RGB c);
  void WriteBitmap(const char*);
  void setT(int,int,RGB);

};


#endif // IMAGE_H