#include "custumfunctionclass.h"
#include "scriptfunctions.h"
#include "scriptqueries.h"
#include "path.h"
#include "formats.h"

#define SCRIPT_NAME "NViewer Script"
#define SCRIPT_EXTENSION ".nvw"
#define TEXTURE_EXTENSION ".png"

using namespace std;

const string spcial_characters[] = {"()","{}","[]",""};
const string allowed_characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-*+/^&|{}[]()#<>=.;,%!:@";
const string opening_words[] = {"let", "parameter", "style", "module", "import", "using", "dimensions", "texture", "type", ""};


/*
 * TODO:
 * let a->b f (x) = @(+)<a,a:a> 
 * @(+)(3.0,_) :: (Real -> Real)
 */ 

struct StructureData
{
  string content;
  string type;
  int line;
  int col;
  string filename;
  StructureData() : content(""), type(""), line(0), col(0), filename("") {}
  StructureData(string type, string filename, int line, int col) : content(""), type(type), line(line), col(col), filename(filename) {}
};

template <class T>
struct ParameterHolder
{
  Function_const_value<T>* first;
  CustomParameterElement* second;
  string s;
  ParameterHolder(Function_const_value<T> *first, CustomParameterElement *second, string s) : first(first), second(second), s(s) {}
};

struct TypedefData
{
  string first;
  string second;
  Element *e;
  TypedefData() : first(""), second(""), e(NULL) {}
  TypedefData(string first, string second) : first(first), second(second), e(NULL) {}
  TypedefData(string first, string second, Element *e) : first(first), second(second), e(e) {}
};

template <class T>
class ScriptParser
{
  string module;
  string scriptFilename;
  FunctionClass<T> *main;
  FunctionClass<T> *shader;
  VRam *vram;
  int dim;
  Element *root_function;
  vector<ScriptParser<T>*> imported;
  ScriptParser<T> *parent;
  vector<string> usings;
  vector<FunctionClass<T>*> paramdep;
  void **paramdepmemory;
  bool preview;
  vector<FunctionRawData> genericFunctions;
  int numThreads;
  vector<FunctionClass<T>*> styleParseFunctions;
  
  bool isDividerCharacter(char) const;
  void setFunctions(CustomFunctionClass<T>*, Element*) const;
  void build(Element*, vector<TypedefData>&, int) const;
  void postBuild(Element*, bool) const;
  void postParse(Element*) const;
  void selectAll(Element*, vector<FunctionClass<T>*>&, int) const;
  void parse(Element*, vector<FunctionClass<T>*>&, VRam&, vector<Function_const_value<T>*>&,CustomFunctionClass<T>*,int) const;
  void constantiate(Element*, VRam&) const;
  void import(Element*);
  string getName(string, string) const;
  string convertOperators(string) const;
public:
  vector<ParameterHolder<T>> parameters;
  vector<ParameterHolder<T>> styles;
  
  void preCalc(VRam&, int);
  bool isParent(string) const;
  ScriptParser(string, VRam*, int, bool, int);
  ScriptParser(string, VRam*, int, bool, ScriptParser<T>*, int numThreads);
  ~ScriptParser();
  string getPackage() const {return module;}
  FunctionClass<T> *getMain() const {return main;}
  Element* getRoot() const {return root_function;}
  static string getPath(string pckg) {return replace(pckg,".","/");}
  bool isInPreview() const {return preview;}
  FunctionClass<T> *getShader() const {return shader;}
  pair<string, void*> evaluate(VRam *vram, VRam *calcto, string value, int numThreads);

  void collectFiles(vector<string>&) const;
};

template <class T>
string ScriptParser<T>::convertOperators(string s) const
{
    string ret = "";
    vector<string> ops;
    ops.push_back("or");
    ops.push_back("xor");
    ops.push_back("and");
    bool found = false;
    for (int i = 0; i < s.length(); ++i) {
        found = false;
        for (int j = 0; j < ops.size(); ++j) {
            if (i+ops[j].size() > s.length()) break;
            if (s.substr(i,ops[j].length()) == ops[j]) {
                if ((i == 0 || !isLetter(s[i-1])) && (i+ops[j].length() == s.length() || !isLetter(s[i+ops[j].length()]))) {
                    ret += "'" + ops[j] + "'";
                    i += ops[j].length()-1;
                    found = true;
                    break;
                }
            }
        }
        if (!found) ret += s[i];
    }
    return ret;
}

template <class T>
void ScriptParser<T>::collectFiles(vector<string> &files) const
{
    files.push_back(scriptFilename);
    for (int i = 0; i < imported.size(); ++i) imported[i]->collectFiles(files);
}

template <class T>
void ScriptParser<T>::preCalc(VRam &vr, int thread)
{
  for (int i = 0; i < paramdep.size(); ++i) paramdepmemory[i] = paramdep[i]->calc(NULL,NULL,vr,thread);
}

template <class T>
ScriptParser<T>::~ScriptParser()
{
  if (root_function != NULL) delete root_function;
}

template <class T>
string ScriptParser<T>::getName(string pckg, string name) const {
  if (pckg == "" || pckg == module) return name;
  for (unsigned int i = 0; i < usings.size(); ++i) if (usings[i] == pckg) return name;
  return pckg + "." + name;
}

template <class T>
bool ScriptParser<T>::isParent(string pckg) const {
  if (module == pckg) return true;
  if (parent == NULL) return false;
  return parent->isParent(pckg);
}

template <class T>
void ScriptParser<T>::import(Element *e) {
  string pckg = e->getAttribute("content","");
  if (isParent(pckg)) throw Exception(INVALID_RECURSION,"Recursive module structures are not allowed");
  ScriptParser *sp = new ScriptParser<T>(pckg, vram, dim, this, numThreads);
  imported.push_back(sp);
}

template <class T>
bool ScriptParser<T>::isDividerCharacter(char c) const {
  if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c == '_' || c == '.' || c == '#') return false;
  return true;
}

template <class T>
void ScriptParser<T>::selectAll(Element *root, vector<FunctionClass<T>*> &functions, int d) const
{
  void *tag = root->getTag();
  if (tag != NULL) {
    CustomFunctionClass<T> *f = (CustomFunctionClass<T>*)tag;
    functions.push_back(f);
  }
  if (d < 1) for (int i = 0; i < root->getChildCount(); ++i) selectAll(root->getChild(i), functions, d+1);
}

template <class T>
void ScriptParser<T>::constantiate(Element *root, VRam &temp) const
{
  for (int i = 0; i < root->getChildCount(); ++i) {
    Element *e = root->getChild(i);
    void *tag = e->getTag();
    if (tag != NULL) {
      CustomFunctionClass<T> *f = (CustomFunctionClass<T>*)tag;
      f->constantiate(temp);
    }
    constantiate(e, temp);
  }
}

template <class T>
void ScriptParser<T>::parse(Element *root, vector<FunctionClass<T>*> &functions, VRam &temp, vector<Function_const_value<T>*> &params, CustomFunctionClass<T> *prnt, int depth) const
{
  //cout<<"Parsing: "<<root->toString()<<endl;
  bool let = root->getAttribute("type","") == "let";
  int size = functions.size();
  CustomFunctionClass<T> *this_p = NULL;
  CustomFunctionClass<T> *parent = NULL;
  if (let) {
    void *tag = root->getTag();
    if (tag != NULL) {
      CustomFunctionClass<T> *f = (CustomFunctionClass<T>*)tag;
      //cout<<f->getName()<<endl;
      this_p = f;
      selectAll(root, functions, 0);
      temp.clear();
      Function_const<T> *inputf = new ((Function_const<T>*)vram->allocate(sizeof(Function_const<T>))) Function_const<T>(dim,f->getInput(),vram);
      f->parameters.push_back(inputf);
      if (prnt != NULL) parent = dynamic_cast<CustomFunctionClass<T>*>((dynamic_cast<RootFunctionClass<T>*> (prnt->root))->root);
      if (parent == NULL) {
	vector<pair<FunctionClass<T>*,string>> rootArgs;
	f->parse(FunctionParseInput<T>(&temp,NULL,&functions,dim,root->getAttribute("filename",""),atoi(root->getAttribute("line","0").c_str()),atoi(root->getAttribute("col","0").c_str()),depth,root_function,root,numThreads),params,rootArgs);
      } else {
	f->parse(FunctionParseInput<T>(&temp,NULL,&functions,dim,root->getAttribute("filename",""),atoi(root->getAttribute("line","0").c_str()),atoi(root->getAttribute("col","0").c_str()),depth,root_function,root,numThreads),params,parent->argument_functions);
      }
      functions.resize(size);
      //cout<<" *** (1) *** " << f->getName()<<endl;
      functions.push_back(f);
    }
  }
  for (int i = 0; i < root->getChildCount(); ++i) {
    Element *e = root->getChild(i);
    void *tag = e->getTag();
    if (tag != NULL) {
      CustomFunctionClass<T> *f = (CustomFunctionClass<T>*)tag;
      //cout<<" *** (2) *** " << f->getName()<<endl;
      functions.push_back(f);
    }
  }
  for (int i = 0; i < root->getChildCount(); ++i) {
    Element *e = root->getChild(i);
    parse(e,functions,temp,params,this_p,depth+1);
  }
  if (depth != 0) functions.resize(size);
}

template <class T>
void ScriptParser<T>::postBuild(Element *root, bool generic) const
{
  for (int i = 0; i < root->getChildCount(); ++i) {
    Element *e = root->getChild(i);
    void *tag = e->getTag();
    if (tag != NULL) {
      if (e->getChildCount() == 0) continue;
      CustomFunctionClass<T> *f = (CustomFunctionClass<T>*)tag;
      RootFunctionClass<T> *rootf = new ((RootFunctionClass<T>*)vram->allocate(sizeof(RootFunctionClass<T>))) RootFunctionClass<T>(dim, vram, f);
      CustomFunctionClass<T> *f2 = new ((CustomFunctionClass<T>*)vram->allocate(sizeof(CustomFunctionClass<T>))) CustomFunctionClass<T>(f->getName(), f->getInput(), f->getOutput(), dim, vram, rootf);
      if (!f->isGeneric() && generic) f->setGeneric();
      if (f->isGeneric()) f2->setGeneric();
      f2->setRootFunction_Manually(rootf);
      f->setRootFunction_Manually(rootf);
      e->setTag(f2);
      postBuild(e, f2->isGeneric());
    }
  }
}

template <class T>
void ScriptParser<T>::setFunctions(CustomFunctionClass<T> *f, Element *e) const
{
  RootFunctionClass<T> *rootf = dynamic_cast<RootFunctionClass<T>*>(f->root);
  for (int j = 0; j < e->getChildCount(); ++j) {
    Element *e2 = e->getChild(j);
    void *tag2 = e2->getTag();
    if (tag2 != NULL) {
      CustomFunctionClass<T> *cf = (CustomFunctionClass<T>*)tag2;
      if (!cf->isGeneric()) {
	if ((FunctionClass<T>::isConstant(cf) & FUNCTIONTYPE_ARGDEP) && cf->getInput() == "") rootf->functions.push_back(cf);
      } else {
	for (int k = 0; k < cf->getNumGenericVersions(); ++k) {
	  CustomFunctionClass<T> *cf2 = cf->getGenericVersion(k);
	  if ((FunctionClass<T>::isConstant(cf2) & FUNCTIONTYPE_ARGDEP) && cf2->getInput() == "") rootf->functions.push_back(cf2);
	}
      }
    }
  }
}

template <class T>
void ScriptParser<T>::postParse(Element *root) const
{
  for (int i = 0; i < root->getChildCount(); ++i) {
    Element *e = root->getChild(i);
    void *tag = e->getTag();
    if (tag != NULL) {
      if (e->getChildCount() == 0) continue;
      CustomFunctionClass<T> *f = (CustomFunctionClass<T>*)tag;
      if (!f->isGeneric()) setFunctions(f, e);
      else for (int i = 0; i < f->getNumGenericVersions(); ++i) setFunctions(f->getGenericVersion(i), e);
      postParse(e);
    }
  }
}

template <class T>
void ScriptParser<T>::build(Element *root, vector<TypedefData> &typedefs, int depth) const
{
  if (depth > 3) {
      throw CompileError(INVALID_DEF,"Nesting functions is not supported ",module,atoi(root->getAttribute("line","1").c_str()),atoi(root->getAttribute("col","1").c_str()));
  }
  bool genericParent = false;
  CustomFunctionClass<T> *parent;
  void *roottag = root->getTag();
  if (roottag != NULL) {
    parent = (CustomFunctionClass<T>*) roottag;
    genericParent = parent->isGeneric();
  }
  for (int i = 0; i < root->getChildCount(); ++i) {
    Element *e = root->getChild(i);
    if (e->getAttribute("type","") == "let") {
      string cont = e->getAttribute("content", "");
      string head,body;
      int eq = cont.find("=");
      if (eq == -1 || eq == 0 || eq == (int)cont.length()-1) throw SyntacsError(INVALID_DEF,"Invalid definition (1)",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      int hd = eq-1;
      while (hd > 0 && iswspace(cont[hd])) hd--;
      head = cont.substr(0,hd+1);
      unsigned int bd = eq+1;
      while (bd < cont.length() && iswspace(cont[bd])) bd++;
      body = cont.substr(bd,-1);
      string head2 = "";
      for (unsigned int j = 0; j < head.length(); ++j) {
	if (iswspace(head[j])) head2 += " ";
	else head2 += head[j];
      }
      bool def_generic = match(head2,FROMAT_DEFINITION_FUNCTION_GENERIC);
      bool def_const_generic = match(head2,FROMAT_DEFINITION_CONST_GENERIC);
      
      if (def_const_generic) head2 = "a "+head2;
      else if (def_generic) {
	string inputtype = "";
	string args = head2;
	while (args.length() > 0 && args[0] != '(') args = args.substr(1,-1);
	while (args.length() > 0 && args[args.length()-1] != ')') args = args.substr(0,args.length()-1);
	if (args.length() < 2) throw SyntacsError(MISSING_ARGS,"No arguments given",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
	args = args.substr(1,args.length()-2);
	int n = 0;
	//cout<<"args: "<<args<<endl;
	inputtype = getGenericTypeFromArgs(args,n,"a");
	int mainn = head2.find("main");
	int len = string("main").length();
	
	int shadern = head2.find("shader");
	int len2 = string("shader").length();
    if ((shadern == 0 || shadern > 0 && isDividerCharacter(head2[shadern-1])) && (shadern+len2 == head2.length() || shadern+len2 < head2.length() && isDividerCharacter(head2[shadern+len2]))) head2 = "("+TYPE_COLOUR_SIGN+","+TYPE_VECTOR_SIGN+","+TYPE_VECTOR_SIGN+")->"+TYPE_COLOUR_SIGN + " " + head2;
	else if ((mainn == 0 || mainn > 0 && isDividerCharacter(head2[mainn-1])) && (mainn+len == head2.length() || mainn+len < head2.length() && isDividerCharacter(head2[mainn+len]))) head2 = TYPE_VECTOR_SIGN+"->"+TYPE_COLOUR_SIGN + " " + head2;
	else head2 = inputtype+"->a "+head2;
	//cout<<head2<<endl;
      }
      //cout<<head2<<endl;
      
      bool def_const = match(head2,FROMAT_DEFINITION_CONST);
      bool def = match(head2,FROMAT_DEFINITION_FUNCTION);
      if (!def_const && !def) throw SyntacsError(INVALID_DEF, "Invalid definition (2)",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      
      head = head2;
      
      if (def_const) head = "->"+head+" ()";
      if (head.length() == 0 || head[head.length()-1] != ')') throw Exception(REGEXP, "Something went wrong (regular expressions)");
      int inside = 1;
      int n = head.length()-2;
      while (n >= 0 && inside > 0) {
	if (head[n] == ')') inside++;
	else if (head[n] == '(') inside--;
	n--;
      }
      if (n < 0) throw SyntacsError(INVALID_DEF, "Invalid definition (3)",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      string args = head.substr(n,-1);
      while (n >= 0 && iswspace(head[n])) n--;
      if (n < 0) throw SyntacsError(INVALID_DEF, "Invalid definition (4)",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      int n2 = n-1;
      while (n2 >= 0 && !iswspace(head[n2])) n2--;
      if (n2 < 0) throw SyntacsError(INVALID_DEF, "Invalid definition (5)",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      string name = head.substr(n2+1,n-n2);
      name = getName(e->getAttribute("module",""),name);
      while (n2 >= 0 && iswspace(head[n2])) n2--;
      if (n2 < 0) throw SyntacsError(INVALID_DEF, "Invalid definition (6)",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      string type = head.substr(0,n2+1);
      for (int j = 0; j < typedefs.size(); ++j) type = replace(type, typedefs[j].first, typedefs[j].second);
      type = FunctionClass<T>::getTypes(type);
      //cout<<name<<"   "<<type<<"   "<<(args + " " + body)<<endl;
      FunctionParseInput<T> pdata(NULL,NULL,NULL,dim,module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()),depth,root_function,e,numThreads);
      vector<string> tpparts;
      getParts(type,tpparts,',',0);
      if (tpparts.size() > 1) throw CompileError(FUNCTION_MULTIPLE_ARGS, "Functions with multiple parameters are not allowed",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      body = convertOperators(body);
      CustomFunctionClass<T> *func = new ((CustomFunctionClass<T>*)vram->allocate(sizeof(CustomFunctionClass<T>))) CustomFunctionClass<T>(name, type, args + " " + body, dim, vram, pdata, true);
      if (depth > 1) func->topfunction = false;
      if (genericParent) func->setGeneric();
      e->setTag(func);
      build(e, typedefs, depth+1);
    }
  }
}

template <class T>
ScriptParser<T>::ScriptParser(string module, VRam* vram, int dim, bool preview, int numThreads) : ScriptParser(module, vram, dim, preview, NULL, numThreads) {}

template <class T>
ScriptParser<T>::ScriptParser(string module, VRam* vram, int dim, bool preview, ScriptParser<T> *parent, int numThreads) : module(module), vram(vram), dim(dim), root_function(NULL), parent(parent), preview(preview), numThreads(numThreads)
{
  string path;
  ifstream in;
  scriptFilename = "";
  bool isFile = false;
  if (match(module,FORMAT_PACKAGE)) {
        //throw SyntacsError(INVALID_MODULE_NAME, "invalid module name", ERROR_CONTENT);
        path = getPath(module)+SCRIPT_EXTENSION;
        int n = 0;
        while (!in.is_open() && PATH_SCRIPTS[n] != "") {
            scriptFilename = PATH_SCRIPTS[n++]+";"+path;
            in.open((PATH_SCRIPTS[n++]+path).c_str());
        }
  } else if (match(module,FORMAT_NVWSCRIPT)) {
        path = module;
        scriptFilename = path;
        in.open(module.c_str());
        isFile = true;
  } else {
      throw SyntacsError(INVALID_MODULE_NAME, "invalid module/file format", ERROR_CONTENT);
  }
  if (!in.is_open()) throw Exception(MISSING_MODEULE, "module/file not found");
  //bool chars[256];
  char stack[8192];
  root_function = new HolderElement();
  Element *elements[8193];
  elements[0] = root_function;
  StructureData data[8192];
  int dstc = 0;
  pair<int,int> stack_pos[8192];
  int stc = 0;
  unsigned char c;
  bool chars_allowed[256];
  bool chars_special[256];
  int line = 1;
  int col = 0;
  for (int i = 0; i < 256; ++i) chars_special[i] = chars_allowed[i] = iswspace((char)i);
  for (unsigned int i = 0; i < allowed_characters.length(); ++i) chars_allowed[(unsigned char)(allowed_characters[i])] = true;
  int m = 0;
  while (spcial_characters[m] != "") {
    for (unsigned int i = 0; i < spcial_characters[m].length(); ++i) chars_special[(unsigned char)(spcial_characters[m][i])] = true;
    m++;
  }
  char c2;
  char lastc;
  string s = "";
  int inside = 0;
  pair<int,int> lastbeginning(0,0);
  bool comment = false;
  bool comment2 = false;
  string *laststring = NULL;
  bool skipping = false;
  while (in.get(c2)) {
    if (c2 == '\n') {
      line++;
      col = 0;
    }
    col++;
    char c3 = lastc;
    lastc = c2;
    if (c2 == '*' && c3 == '/') {
      comment = true;
      if (laststring != NULL) *laststring = laststring->substr(0,laststring->length()-1);
    }
    if (c2 == '/' && c3 == '/') {
      comment2 = true;
      if (laststring != NULL) *laststring = laststring->substr(0,laststring->length()-1);
    }
    if (c2 == '/') laststring = NULL;
    if (comment && c2 == '/' && c3 == '*') {
      comment = false;
      continue;
    }
    if (comment2 && c2 == '\n') {
      comment2 = false;
      continue;
    }
    if (comment || comment2) continue;
    if (skipping) {
      if (isDividerCharacter(c2)) {
	if (s == "#preview") skipping = !isInPreview();
	if (s == "#export") skipping = isInPreview();
	if (s == "#end") skipping = false;
	s = "";
      } else s += c2;
      continue;
    }
    if (c2 == '(' || c2 == '{' || c2 == '[') inside++;
    else if (c2 == ')' || c2 == '}' || c2 == ']') inside--;
    if (c2 == ';' && inside == 0) {
      if (dstc == 0) throw SyntacsError(UNEXPECTED_C, "Unexpected use of ';'",module,line,col);
      data[dstc-1].content += s;
      s = "";
      //for (int i = 0; i < dstc; ++i) cout<<" ";
      dstc--;
      string cont = data[dstc].content;
      while (cont.length() > 0 && iswspace(cont[0])) cont = cont.substr(1,-1);
      while (cont.length() > 0 && iswspace(cont[cont.length()-1])) cont = cont.substr(0,cont.length() -1);
      elements[dstc+1]->setAttribute("content",cont);
      //cout<<data[dstc].type<<": "<<data[dstc].content<<endl;
    } else {
      if (isDividerCharacter(c2)) {
	if (s != "") {
	  if (s == "#preview") {
	    skipping = !isInPreview();
	    s = "";
	    continue;
	  }
	  if (s == "#export") {
	    skipping = isInPreview();
	    s = "";
	    continue;
	  }
	  if (s == "#end") {
	    skipping = false;
	    s = "";
	    continue;
	  }
	  bool found = false;
	  for (int i = 0; opening_words[i] != ""; ++i) {
	    if (opening_words[i] == s) {
	      found = true;
	      break;
	    }
	  }
	  if (found) {
	    if (inside > 0) throw SyntacsError(INVALID_USE, string("Invalid use of '")+s+"'",module,lastbeginning.first,lastbeginning.second);
	    if (dstc > 8192) throw Exception(STACK_OVERFLOWN, "Data stack is overflown");
	    if (dstc != 0) if (data[dstc-1].type != "let" || s != "let") throw SyntacsError(INVALID_USE, string("Invalid use of '")+s+"'",module,line,col);
	    data[dstc] = StructureData(s,module,lastbeginning.first,lastbeginning.second);
	    HolderElement *e = new HolderElement();
	    elements[dstc+1] = e;
	    e->setAttribute("type",data[dstc].type);
	    e->setAttributeT("line",data[dstc].line);
	    e->setAttributeT("col",data[dstc].col);
	    e->setAttributeT("filename",data[dstc].filename);
	    elements[dstc]->addChild(e);
	    dstc++;
	  } else if (dstc > 0) data[dstc-1].content += s;
	}
	laststring = &data[dstc-1].content;
	data[dstc-1].content += c2;
	s = "";
      } else {
	if (s == "") lastbeginning = make_pair(line,col);
	s += c2;
      }
    }
    c = c2;
    //chars[c] = true;
    stringstream ss;
    if (!chars_allowed[c]) {
      ss<<"Unallowed character: '"<<c<<"'";
      throw SyntacsError(UNALLOWED_C, ss.str(),path,line,col);
    }
    m = 0;
    if (chars_special[c]) {
      while (spcial_characters[m] != "") {
	bool found = false;
	if (c == spcial_characters[m][0]) {
	  if (stc < 8192) {
	    stack_pos[stc] = make_pair(line,col);
	    stack[stc++] = c;
	    found = true;
	  } else throw Exception(STACK_OVERFLOWN, "Character stack is overflown");
	} else if (c == spcial_characters[m][1]) {
	  if (stc > 0 && stack[stc-1] == spcial_characters[m][0]) {
	    stc--;
	    found = true;
	  } else {
	    stringstream ss;
	    ss<<"'"<<spcial_characters[m][0]<<"' expected before '"<<c<<"'";
	    throw SyntacsError(EXPECTED_C, ss.str(),path,line,col);
	  }
	}
	if (found) break;
	m++;
      }
    }
  }
  if (stc > 0) {
    stringstream ss;
    ss<<"Unexpected '"<<stack[stc-1]<<"'";
    throw SyntacsError(UNEXPECTED_C, ss.str(),path,stack_pos[stc-1].first,stack_pos[stc-1].second);
  }
  vector<TypedefData> typedefs;
  in.close();
  for (int i = 0; i < root_function->getChildCount(); ++i) {
    Element *e = root_function->getChild(i);
    if (e->getAttribute("type","") == "import") import(e);
  }
  for (int i = 0; i < root_function->getChildCount(); ++i) {
    Element *e = root_function->getChild(i);
    if (e->getAttribute("type","") == "using") usings.push_back(e->getAttribute("content",""));
  }
  for (unsigned int i = 0; i < imported.size(); ++i) {
    for (int j = 0; j < imported[i]->getRoot()->getChildCount(); ++j) {
      Element *e = imported[i]->getRoot()->getChild(j);
      string type = e->getAttribute("type","");
      if (type == "type") {
	string content = noWhiteSpaces(e->getAttribute("content", ""));
    if (!match(content,FORMAT_TYPEDEF)) throw SyntacsError(INVALID_TYPEDEF, "Invalid type definition",e->getAttribute("module",module),atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
	int eq = content.find('=');
	string tpname = content.substr(0,eq);
	string tp = content.substr(eq+1,-1);
	bool found = false;
    for (unsigned int k = 0; k < usings.size(); ++k) {
	  if (usings[k] == imported[i]->getPackage()) {
	    found = true;
	    break;
	  }
	}
	if (!found) tpname = imported[i]->getPackage()+"."+tpname;
	typedefs.push_back(TypedefData(tpname,tp,e));
      }
      if (type == "let" || type == "style" || type == "parameter") {
    //e->setAttribute("module",imported[i]->getPackage());
	root_function->addChild(e);
      }
    }
  }
  for (int i = 0; i < root_function->getChildCount(); ++i) {
    Element *e = root_function->getChild(i);
    if (e->getAttribute("type","-") == "type") {
      string content = noWhiteSpaces(e->getAttribute("content", ""));
      if (!match(content,FORMAT_TYPEDEF)) throw SyntacsError(INVALID_TYPEDEF, "Invalid type definition",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      int eq = content.find('=');
      string tpname = content.substr(0,eq);
      string tp = content.substr(eq+1,-1);
      typedefs.push_back(TypedefData(tpname,tp,e));
    }
  }
  for (unsigned int i = 0; i < typedefs.size(); ++i) {
    string id = typedefs[i].first;
    if (id == TYPE_BOOL_NAME || id == TYPE_BOOL_SIGN || id == TYPE_INT_NAME || id == TYPE_INT_SIGN || id == TYPE_REAL_NAME || id == TYPE_REAL_SIGN || id == TYPE_CPLX_NAME || id == TYPE_CPLX_SIGN || id == TYPE_COLOUR_NAME || id == TYPE_COLOUR_SIGN || id == TYPE_VECTOR_NAME || id == TYPE_VECTOR_SIGN || id == TYPE_MATRIX_NAME || id == TYPE_MATRIX_SIGN || id == TYPE_LIST_NAME || id == TYPE_LIST_SIGN || id == TYPE_ARRAY_NAME || id == TYPE_ARRAY_SIGN) throw CompileError(REDEFINITION, "Type redefinition (primitive type)",typedefs[i].e->getAttribute("module",module),atoi(typedefs[i].e->getAttribute("line","1").c_str()),atoi(typedefs[i].e->getAttribute("col","1").c_str()));
    for (unsigned int j = 0; j < i; ++j) typedefs[i] = TypedefData(typedefs[i].first,replace(typedefs[i].second,typedefs[j].first,typedefs[j].second),typedefs[i].e);
    if ((int)typedefs[i].second.find(typedefs[i].first) != -1) throw CompileError(REC_TYPE, "Recursive type definition",typedefs[i].e->getAttribute("module",module),atoi(typedefs[i].e->getAttribute("line","1").c_str()),atoi(typedefs[i].e->getAttribute("col","1").c_str()));
  }
  string pckg = "";
  vector<Function_const_value<T>*> params;
  vector<FunctionClass<T>*> functions;
  for (int i = 0; i < root_function->getChildCount(); ++i) {
    Element *e = root_function->getChild(i);
    string cont0 = e->getAttribute("content", "");
    string cont = "";
    for (unsigned int j = 0; j < cont0.length(); ++j) {
      if (iswspace(cont0[j])) cont += " ";
      else cont += cont0[j];
    }
    if (e->getAttribute("type","-") == "texture") {
      string im = e->getAttribute("content","");
      string image = "";
      for (unsigned int j = 0; j < im.length(); ++j) if (!iswspace(im[j])) image += im[j];
      if (!match(image,FORMAT_TEXTURE)) throw SyntacsError(INVALID_TEXTURE_FORMAT, "Invalid texture format. Please use 'id : module.file'",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      int div = image.find(':');
      string textid = image.substr(0,div);
      string textfile = image.substr(div+1,-1);
      string path = getPath(textfile)+TEXTURE_EXTENSION;
      int n = 0;
      ifstream in;
      while (!in.is_open() && PATH_SCRIPTS[n] != "") in.open((PATH_SCRIPTS[n++]+path).c_str());
      n--;
      if (!in.is_open()) throw Exception(FILE_NOT_FOUND, "File not found: "+textfile+" ("+path+")");
      in.close();
      string filename = PATH_SCRIPTS[n]+path;
      Texture *texture = new ((Texture*)vram->allocate(sizeof(Texture))) Texture(filename,*vram);
      Query_texture<T> *text = new ((Query_texture<T>*)vram->allocate(sizeof(Query_texture<T>))) Query_texture<T>(dim,vram,textid,texture);
      functions.push_back(text);
    } else if (e->getAttribute("type","-") == "dimensions") {
      string dm = e->getAttribute("content","");
      string cdm = "";
      for (unsigned int j = 0; j < dm.length(); ++j) if (!iswspace(dm[j])) cdm += dm[j];
      if (!match(cdm,FORMAT_DIMENSIONS)) throw SyntacsError(INVALID_DIM, "Invalid dimensions format. Please use [x..<y>]",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      cdm = cdm.substr(1,cdm.length()-2);
      vector<string> parts;
      getParts(cdm,parts,"..",0);
      if (atoi(parts[0].c_str()) > dim || (parts.size() > 1 && atoi(parts[1].c_str()) < dim)) throw Exception(UNSUPPORTED_DIM, module + " does not support the specified dimension number");
    } else if (e->getAttribute("type","-") == "module") {
      if (pckg != "") throw SyntacsError(MULTIPLE_USE, "Multiple use of 'module'",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      pckg = e->getAttribute("content","");
      if (pckg != module && !isFile) throw Exception(INVALID_MODULE_NAME, "Incorrect module name. Please change it to '"+module+"'");
    } else if (e->getAttribute("type","-") == "style" || e->getAttribute("type","-") == "parameter") {
      if (!match(cont,FORMAT_PARAMETER)) throw SyntacsError(INVALID_PARAM, "Invalid paramater/style format",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      string type = "";
      string name;
      string value;
      int eq = cont.find('=');
      int vl = eq+1;
      while (iswspace(cont[vl])) vl++;
      value = cont.substr(vl,-1);
      int nm = eq-1;
      while (iswspace(cont[nm])) nm--;
      int nm0 = nm-1;
      while (!iswspace(cont[nm0])) nm0--;
      name = cont.substr(nm0+1,nm-nm0);
      name = getName(e->getAttribute("module",""),name);
      type = cont.substr(0,nm0);
      for (unsigned int j = 0; j < typedefs.size(); ++j) type = replace(type, typedefs[j].first, typedefs[j].second);
      type = FunctionClass<T>::getTypes(type);
      
      string tp = e->getAttribute("type","-");
      if (type.length() == 0) throw CompileError(INVALID_PARAM,"Invalid parameter type",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      FunctionParseInput<T> fdata(NULL,NULL,NULL,0,module,line,col,0,NULL,NULL,numThreads);
      CustomTypeConstructor<T> ctc(*vram,type,dim,fdata,NULL);
      if (ctc.getRoot() == NULL) throw CompileError(INVALID_PARAM, "Invalid parameter type",module,atoi(e->getAttribute("line","1").c_str()),atoi(e->getAttribute("col","1").c_str()));
      void *data = ctc.construct(*vram);
      Function_const_value<T> *param = new Function_const_value<T>(name,dim,vram,type,data);
      param->type |= FUNCTIONTYPE_PARAMDEP;
      CustomParameterElement *cpe = new CustomParameterElement();
      cpe->setAttribute("type",tp);
      cpe->setAttribute("valuetype",type);
      cpe->setAttribute("defaultvalue",value);
      cpe->setAttribute("id",name);
      if (tp == "style") styles.push_back(ParameterHolder<T>(param,cpe,""));
      else parameters.push_back(ParameterHolder<T>(param,cpe,""));
      params.push_back(param);
    }
  }
  build(root_function, typedefs, 1);
  postBuild(root_function, false);
  
  functions.push_back(new ((Function_constructor_v<T>*)vram->allocate(sizeof(Function_constructor_v<T>))) Function_constructor_v<T>(dim,vram));
  functions.push_back(new ((Function_constructor_m<T>*)vram->allocate(sizeof(Function_constructor_m<T>))) Function_constructor_m<T>(dim,vram));
  
  addScriptFunctions(functions,vram,dim);

  for (unsigned int i = 0; i < functions.size(); ++i) styleParseFunctions.push_back(functions[i]);

  functions.push_back(new ((Query_dim<T>*)vram->allocate(sizeof(Query_dim<T>))) Query_dim<T>(&(ScriptParser<T>::dim),vram));
  functions.push_back(new ((Query_campos<T>*)vram->allocate(sizeof(Query_campos<T>))) Query_campos<T>(ScriptParser<T>::dim,vram));
  functions.push_back(new ((Query_camdir<T>*)vram->allocate(sizeof(Query_camdir<T>))) Query_camdir<T>(ScriptParser<T>::dim,vram));
  functions.push_back(new ((Query_camtrans<T>*)vram->allocate(sizeof(Query_camtrans<T>))) Query_camtrans<T>(ScriptParser<T>::dim,vram));
  functions.push_back(new ((Query_light<T>*)vram->allocate(sizeof(Query_light<T>))) Query_light<T>(ScriptParser<T>::dim,vram));
  functions.push_back(new ((Query_ambient<T>*)vram->allocate(sizeof(Query_ambient<T>))) Query_ambient<T>(ScriptParser<T>::dim,vram));

  int start = functions.size();

  VRam temp(vram->size());
  parse(root_function, functions, temp, params, NULL, 0);
  postParse(root_function);
  //constantiate(root_function, temp);
  main = NULL;
  shader = NULL;

  
  FunctionClass<T> *proc;
  for (int i = 0; i < root_function->getChildCount(); ++i) {
    Element *e = root_function->getChild(i);
    void *tag = e->getTag();
    if (tag == NULL) continue;
    if (((CustomFunctionClass<T>*)tag)->getName() == "main" || ((CustomFunctionClass<T>*)tag)->getName() == "shader") {
      map<void*,FunctionClass<T>*> funcs;
      temp.clear();
      proc = (CustomFunctionClass<T>*)tag;
      if (((CustomFunctionClass<T>*)tag)->getName() == "main") {
	if (proc->getOutput() != TYPE_COLOUR_SIGN || proc->getInput() != TYPE_VECTOR_SIGN) throw CompileError(MAIN_FUNC_TYPE, "The function main must be the type of "+TYPE_VECTOR_NAME+"->"+TYPE_COLOUR_NAME,proc->parse_data.filename,proc->parse_data.line,proc->parse_data.col);
      } else {
	if (proc->getOutput() != TYPE_COLOUR_SIGN || proc->getInput() != TYPE_LIST_SIGN+"("+TYPE_COLOUR_SIGN+","+TYPE_VECTOR_SIGN+","+TYPE_VECTOR_SIGN+")") throw CompileError(MAIN_FUNC_TYPE, "The function shader must be the type of ("+TYPE_COLOUR_NAME+","+TYPE_VECTOR_NAME+","+TYPE_VECTOR_NAME+")->"+TYPE_COLOUR_NAME,proc->parse_data.filename,proc->parse_data.line,proc->parse_data.col);
      }
      FunctionClass<T>::checkTypes(proc, &temp);
      paramdep.clear();
      proc = FunctionClass<T>::collapse(proc,funcs);
      FunctionClass<T>::calcRec(proc,tag);

      FunctionClass<T>::updateRoots(proc,funcs);
      proc->setRootFunction(NULL);
      FunctionClass<T>::constantiate(proc,this,temp,paramdep);
      sort(paramdep.begin(), paramdep.end());			//sort by memory addresse
      auto it = unique (paramdep.begin(), paramdep.end());		//erase identical functions
      paramdep.resize(std::distance(paramdep.begin(),it));
      
      //paramdep.clear();
      
      if (paramdep.size() > 0) {
	paramdepmemory = (void**)vram->allocate(sizeof(void*)*paramdep.size());
	vector<FunctionClass<T>*> replaces;
    for (unsigned int j = 0; j < paramdep.size(); ++j) {
	  stringstream ss;
	  ss<<"paramdep_"<<j;
	  Function_const_value_from_array<T> *to = new ((Function_const_value_from_array<T>*)vram->allocate(sizeof(Function_const_value_from_array<T>))) Function_const_value_from_array<T>(ss.str(), dim, vram, paramdep[j]->getOutput(), paramdepmemory,j);
	  replaces.push_back(to);
	}
	FunctionClass<T>::replaceFunctions(proc,paramdep,paramdepmemory,replaces);
      }
      if (((CustomFunctionClass<T>*)tag)->getName() == "main") main = proc;
      else shader = proc;
      //break;
    }
  }
  for (unsigned int i = start; i < functions.size(); ++i) styleParseFunctions.push_back(functions[i]);

  //for (int i = 0; i < styleParseFunctions.size(); ++i) cout<<styleParseFunctions[i]->getName()<<endl;
}

template <class T>
pair<string, void*> ScriptParser<T>::evaluate(VRam *vram, VRam *calcto, string value, int numThreads)
{
    try {
        FunctionParseInput<T> pdata(NULL,NULL,NULL,0,"styles/parameters",0,0,0,NULL,NULL,numThreads);
        CustomFunctionClass<T> *cfc = (CustomFunctionClass<T>*)vram->allocate(sizeof(CustomFunctionClass<T>));
        cfc = new (cfc) CustomFunctionClass<T> ("styleparser"," ->* ","() " + value, dim, vram, pdata);

        vector<Function_const_value<T>*> parameters;
        vector<pair<FunctionClass<T>*,string>> rootArgs;
        FunctionParseInput<T> data(NULL,NULL,&styleParseFunctions, dim, pdata.filename, pdata.line, pdata.col,0,NULL,NULL,numThreads);

        cfc->parse(data,parameters,rootArgs);
        void *value = cfc->calc(NULL,NULL,*vram,0);
        //cout<<"calc: "<<*((T*)value)<<endl;
        value = cfc->getOutConstructor()->getRoot()->copy(*calcto, value, dim);

        return make_pair(cfc->getOutput(), value);
    } catch (int e) {
      throw CompileError(INVALID_PARAM, "Invalid expression: " + value, ERROR_CONTENT);
    }
}
