#pragma once

#include "matrix.h"

template <class T>
class Transformable
{
public:
  virtual Matrix<T> getTransform() const = 0;
  virtual LightDirection<T> *getLight() = 0;
  virtual T getAmbientLight() const = 0;
  virtual ~Transformable() {}
};
