#pragma once

#include "custumfunctionclass.h"

/*
* ----- Operations -----
*
* --- Bool ---
* b and b
* b or b
* b xor b
* b => b
* b = b
*
* --- Int ---
* i + i
* i - i
* -i
* i * i
* i / i i -> i -> r
* i % i
* i = i
* i < i
* i > i
* i <= i
* i >= i
* i^i
*
* --- Real ---
* r + r
* r - r
* -r
* r * r
* r / r
* r = r
* r < r
* r > r
* r <= r
* r >= r
* r^r
*
* --- Complex ---
* c + c
* c - c
* -c
* c * c
* c / c
* c = c
* c < c
* c > c
* c <= c
* c >= c
* c^c
*
* --- Colour ---
* u + u -- overlapping alpha++ (a+b)+c != a+(b+c), a+b = b+a
* u * u -- colouring alpha== (a*b)*c != a*(b*c), a*b != b*a
* u / u -- alpha== (a*b)*c != a*(b*c), a*b != b*a
*
* --- Vector ---
* v + v
* v - v
* v < v
* v <= v
* v > v
* v >= v
* v = v
*
* --- Matrix ---
* m + m
* m - m
* m * m
*
* --- List ---
*
* --- Tuple ---
*
* --- Cross ---
* i + r
* i + c
* r + i
* r + c
* c + i
* c + r
* i - r
* i - c
* r - i
* r - c
* c - i
* c - r
* i * r
* i * c
* r * i
* r * c
* c * i
* c * r
* i / r
* i / c
* r / i
* r / c
* c / i
* c / r
* i ^ r
* i ^ c
* r ^ i
* r ^ c
* c ^ i
* c ^ r
* i < r
* i < c
* r < i
* r < c
* c < i
* c < r
* i <= r
* i <= c
* r <= i
* r <= c
* c <= i
* c <= r
* i > r
* i > c
* r > i
* r > c
* c > i
* c > r
* i >= r
* i >= c
* r >= i
* r >= c
* c >= i
* c >= r
* i = r
* i = c
* r = i
* r = c
* c = i
* c = r
* i * v
* r * v
* i * m
* r * m
* m * v
*
*
* ----- Constructors -----
* Real(int)
* Real.pi
* Real.e
* Complex(Int)
* Complex(Real)
* Complex(Real,Int)
* Complex(Int,Real)
* Complex(Real,Real)
* Colour(Real,Real,Real,Real)
* Colour.red
* Colour.green
* Colour.blue
* Colour.white
* Colour.black
* Colour.none
* Vector.zero
* Vector.x
* Vector.y
* Vector.z
* Matrix.zero
* Matrix.identity
*
*
*
* ----- * -> A -----
* --- i ---
* xi(b)
* inc(i)
* pow2(i)
* round(r)
* floor(r)
* ceiling(r)
* truncate(r)
* factorial(i)
* div(i,i)
* mod(i,i)
* abs(i)
* exp(i)
*
* --- r ---
* imag(c)
* real(c)
* magnitude(v)
* sqrmagnitude(v)
* sin(r)
* cos(r)
* tan(r)
* asin(r)
* acos(r)
* atan(r)
* pow2(r)
* sqrt(r)
* log2(r)
* log10(r)
* log(r,r)
* abs(r)
* abs(c)
* degree(r)
* radian(r)
* dot(v,v)
* arg(c)
* norm(c)
* exp(r)
* sinh(r)
* cosh(t)
* tanh(r)
* asinh(r)
* acosh(r)
* atanh(r)
* clamp01(r)
* clamp(r,r,r) // clamp(r,a,b) r => [a;b]
* root(r,i,i) // Mth root of x with accuracy n
* lerp(r,r,r)
*
* --- c ---
* pow2(c)
* conj(c)
* proj(c)
* polar(r,r)
* exp(c)
* log(c)
* log10(c)
* sqrt(c)
* sin(c)
* cos(c)
* tan(c)
* asin(c)
* acos(c)
* atan(c)
* sinh(c)
* cosh(c)
* tanh(c)
* asinh(c)
* acosh(c)
* atanh(c)
* lerp(c,c,r)
*
*
* --- u ---
* lerp(u,u,r)
*
* --- v ---
* lerp(v,v,r)
*
* --- m ---
*
*
*
*
*/


// ----- Operations -----
// --- Bool ---
// b and b
template <class T> class Function_and_bb : public FunctionClass<T> {
public:
  Function_and_bb(int dim, VRam *vr) : FunctionClass<T>("and", dim, vr) {FunctionClass<T>::setUp(TYPE_BOOL_SIGN+","+TYPE_BOOL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
if (*((bool*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) == false) *ret = false;
else if (*((bool*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)) == false) *ret = false;
else *ret = true;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_and_bb<T> *r = (Function_and_bb<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_and_bb<T>));
    return new (r) Function_and_bb<T>(*this);
  }
};
// b or b
template <class T> class Function_or_bb : public FunctionClass<T> {
public:
  Function_or_bb(int dim, VRam *vr) : FunctionClass<T>("or", dim, vr) {FunctionClass<T>::setUp(TYPE_BOOL_SIGN+","+TYPE_BOOL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
if (*((bool*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) == true) *ret = true;
else if (*((bool*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)) == true) *ret = true;
else *ret = false;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_or_bb<T> *r = (Function_or_bb<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_or_bb<T>));
    return new (r) Function_or_bb<T>(*this);
  }
};
// b xor b
template <class T> class Function_xor_bb : public FunctionClass<T> {
public:
  Function_xor_bb(int dim, VRam *vr) : FunctionClass<T>("xor", dim, vr) {FunctionClass<T>::setUp(TYPE_BOOL_SIGN+","+TYPE_BOOL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((bool*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) xor *((bool*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_xor_bb<T> *r = (Function_xor_bb<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_xor_bb<T>));
    return new (r) Function_xor_bb<T>(*this);
  }
};
// b => b
template <class T> class Function_implication_bb : public FunctionClass<T> {
public:
  Function_implication_bb(int dim, VRam *vr) : FunctionClass<T>("=>", dim, vr) {FunctionClass<T>::setUp(TYPE_BOOL_SIGN+","+TYPE_BOOL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
if (*((bool*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) == false) *ret = true; //!a || b
else if (*((bool*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)) == true) *ret = true;
else *ret = false;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_implication_bb<T> *r = (Function_implication_bb<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_implication_bb<T>));
    return new (r) Function_implication_bb<T>(*this);
  }
};
// b = b
template <class T> class Function_eq_bb : public FunctionClass<T> {
public:
  Function_eq_bb(int dim, VRam *vr) : FunctionClass<T>("=", dim, vr) {FunctionClass<T>::setUp(TYPE_BOOL_SIGN+","+TYPE_BOOL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((bool*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) == *((bool*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_eq_bb<T> *r = (Function_eq_bb<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_eq_bb<T>));
    return new (r) Function_eq_bb<T>(*this);
  }
};


// --- Int ---
// i + i
template <class T> class Function_plus_ii : public FunctionClass<T> {
public:
  Function_plus_ii(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) + *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_ii<T> *r = (Function_plus_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_ii<T>));
    return new (r) Function_plus_ii<T>(*this);
  }
};

// i - i
template <class T> class Function_minus_ii : public FunctionClass<T> {
public:
  Function_minus_ii(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) - *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_minus_ii<T> *r = (Function_minus_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_minus_ii<T>));
    return new (r) Function_minus_ii<T>(*this);
  }
};

// -i
template <class T> class Function_negate_i : public FunctionClass<T> {
public:
  Function_negate_i(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
*ret = -(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_negate_i<T> *r = (Function_negate_i<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_negate_i<T>));
    return new (r) Function_negate_i<T>(*this);
  }
};

// i * i
template <class T> class Function_mult_ii : public FunctionClass<T> {
public:
  Function_mult_ii(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) * *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_ii<T> *r = (Function_mult_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_ii<T>));
    return new (r) Function_mult_ii<T>(*this);
  }
};

// i / i
template <class T> class Function_div_ii : public FunctionClass<T> {
public:
  Function_div_ii(int dim, VRam *vr) : FunctionClass<T>("/", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
int *p1 = ((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
if (*p1 == 0) {
LOG.debug("Division by 0 (ii)");
throw RuntimeError(DIVISION_BY_0, "Division by 0", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
}
*ret = ((T)(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)))) / ((T)(*p1));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_div_ii<T> *r = (Function_div_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_div_ii<T>));
    return new (r) Function_div_ii<T>(*this);
  }
};

// i % i
template <class T> class Function_mod_ii : public FunctionClass<T> {
public:
  Function_mod_ii(int dim, VRam *vr) : FunctionClass<T>("%", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
int *p1 = ((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
if (*p1 == 0) {
LOG.debug("Division by 0 (mod ii)");
throw RuntimeError(DIVISION_BY_0, "mod by 0", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
}
*ret = *((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) % *p1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mod_ii<T> *r = (Function_mod_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mod_ii<T>));
    return new (r) Function_mod_ii<T>(*this);
  }
};

// i = i
template <class T> class Function_eq_ii : public FunctionClass<T> {
public:
  Function_eq_ii(int dim, VRam *vr) : FunctionClass<T>("=", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) == *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_eq_ii<T> *r = (Function_eq_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_eq_ii<T>));
    return new (r) Function_eq_ii<T>(*this);
  }
};

// i < i
template <class T> class Function_lt_ii : public FunctionClass<T> {
public:
  Function_lt_ii(int dim, VRam *vr) : FunctionClass<T>("<", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) < *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lt_ii<T> *r = (Function_lt_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lt_ii<T>));
    return new (r) Function_lt_ii<T>(*this);
  }
};

// i > i
template <class T> class Function_gt_ii : public FunctionClass<T> {
public:
  Function_gt_ii(int dim, VRam *vr) : FunctionClass<T>(">", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) > *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_gt_ii<T> *r = (Function_gt_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gt_ii<T>));
    return new (r) Function_gt_ii<T>(*this);
  }
};

// i <= i
template <class T> class Function_le_ii : public FunctionClass<T> {
public:
  Function_le_ii(int dim, VRam *vr) : FunctionClass<T>("<=", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) <= *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_le_ii<T> *r = (Function_le_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_le_ii<T>));
    return new (r) Function_le_ii<T>(*this);
  }
};

// i >= i
template <class T> class Function_ge_ii : public FunctionClass<T> {
public:
  Function_ge_ii(int dim, VRam *vr) : FunctionClass<T>(">=", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) >= *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_ge_ii<T> *r = (Function_ge_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_ge_ii<T>));
    return new (r) Function_ge_ii<T>(*this);
  }
};

// i^i
template <class T> class Function_pow_ii : public FunctionClass<T> {
public:
  Function_pow_ii(int dim, VRam *vr) : FunctionClass<T>("^", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
*ret = pow_ai(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)), *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pow_ii<T> *r = (Function_pow_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pow_ii<T>));
    return new (r) Function_pow_ii<T>(*this);
  }
};




// --- Real ---
// r + r
template <class T> class Function_plus_rr : public FunctionClass<T> {
public:
  Function_plus_rr(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) + *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_rr<T> *r = (Function_plus_rr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_rr<T>));
    return new (r) Function_plus_rr<T>(*this);
  }
};

// r - r
template <class T> class Function_minus_rr : public FunctionClass<T> {
public:
  Function_minus_rr(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) - *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_minus_rr<T> *r = (Function_minus_rr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_minus_rr<T>));
    return new (r) Function_minus_rr<T>(*this);
  }
};

// -r
template <class T> class Function_negate_r : public FunctionClass<T> {
public:
  Function_negate_r(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = -(*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_negate_r<T> *r = (Function_negate_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_negate_r<T>));
    return new (r) Function_negate_r<T>(*this);
  }
};

// r * r
template <class T> class Function_mult_rr : public FunctionClass<T> {
public:
  Function_mult_rr(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) * *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_rr<T> *r = (Function_mult_rr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_rr<T>));
    return new (r) Function_mult_rr<T>(*this);
  }
};

// r / r
template <class T> class Function_div_rr : public FunctionClass<T> {
public:
  Function_div_rr(int dim, VRam *vr) : FunctionClass<T>("/", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *p1 = (T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
if (*p1 == 0) {
LOG.debug("Division by 0 (rr)");
throw RuntimeError(DIVISION_BY_0, "Division by 0", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
}
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) / *p1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_div_rr<T> *r = (Function_div_rr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_div_rr<T>));
    return new (r) Function_div_rr<T>(*this);
  }
};

// r = r
template <class T> class Function_eq_rr : public FunctionClass<T> {
public:
  Function_eq_rr(int dim, VRam *vr) : FunctionClass<T>("=", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) == *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_eq_rr<T> *r = (Function_eq_rr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_eq_rr<T>));
    return new (r) Function_eq_rr<T>(*this);
  }
};

// r < r
template <class T> class Function_lt_rr : public FunctionClass<T> {
public:
  Function_lt_rr(int dim, VRam *vr) : FunctionClass<T>("<", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) < *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lt_rr<T> *r = (Function_lt_rr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lt_rr<T>));
    return new (r) Function_lt_rr<T>(*this);
  }
};

// r > r
template <class T> class Function_gt_rr : public FunctionClass<T> {
public:
  Function_gt_rr(int dim, VRam *vr) : FunctionClass<T>(">", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) > *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_gt_rr<T> *r = (Function_gt_rr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gt_rr<T>));
    return new (r) Function_gt_rr<T>(*this);
  }
};

// r <= r
template <class T> class Function_le_rr : public FunctionClass<T> {
public:
  Function_le_rr(int dim, VRam *vr) : FunctionClass<T>("<=", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) <= *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_le_rr<T> *r = (Function_le_rr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_le_rr<T>));
    return new (r) Function_le_rr<T>(*this);
  }
};

// r >= r
template <class T> class Function_ge_rr : public FunctionClass<T> {
public:
  Function_ge_rr(int dim, VRam *vr) : FunctionClass<T>(">=", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) >= *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_ge_rr<T> *r = (Function_ge_rr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_ge_rr<T>));
    return new (r) Function_ge_rr<T>(*this);
  }
};

// r^r
template <class T> class Function_pow_rr : public FunctionClass<T> {
public:
  Function_pow_rr(int dim, VRam *vr) : FunctionClass<T>("^", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = pow_aa(*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)), *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pow_rr<T> *r = (Function_pow_rr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pow_rr<T>));
    return new (r) Function_pow_rr<T>(*this);
  }
};


// --- Complex ---
// c + c
template <class T> class Function_plus_cc : public FunctionClass<T> {
public:
  Function_plus_cc(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) + *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_cc<T> *r = (Function_plus_cc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_cc<T>));
    return new (r) Function_plus_cc<T>(*this);
  }
};

// c - c
template <class T> class Function_minus_cc : public FunctionClass<T> {
public:
  Function_minus_cc(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) - *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_minus_cc<T> *r = (Function_minus_cc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_minus_cc<T>));
    return new (r) Function_minus_cc<T>(*this);
  }
};

// -c
template <class T> class Function_negate_c : public FunctionClass<T> {
public:
  Function_negate_c(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = -(*((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_negate_c<T> *r = (Function_negate_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_negate_c<T>));
    return new (r) Function_negate_c<T>(*this);
  }
};

// c * c
template <class T> class Function_mult_cc : public FunctionClass<T> {
public:
  Function_mult_cc(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) * *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_cc<T> *r = (Function_mult_cc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_cc<T>));
    return new (r) Function_mult_cc<T>(*this);
  }
};

// c / c
template <class T> class Function_div_cc : public FunctionClass<T> {
public:
  Function_div_cc(int dim, VRam *vr) : FunctionClass<T>("/", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p1 = ((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
if (p1->real() == 0 && p1->imag() == 0) {
LOG.debug("Division by 0 (cc)");
throw RuntimeError(DIVISION_BY_0, "Division by 0+0*i", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
}
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) / *p1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_div_cc<T> *r = (Function_div_cc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_div_cc<T>));
    return new (r) Function_div_cc<T>(*this);
  }
};

// c = c
template <class T> class Function_eq_cc : public FunctionClass<T> {
public:
  Function_eq_cc(int dim, VRam *vr) : FunctionClass<T>("=", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) == *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_eq_cc<T> *r = (Function_eq_cc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_eq_cc<T>));
    return new (r) Function_eq_cc<T>(*this);
  }
};

// c < c
template <class T> class Function_lt_cc : public FunctionClass<T> {
public:
  Function_lt_cc(int dim, VRam *vr) : FunctionClass<T>("<", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) < *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lt_cc<T> *r = (Function_lt_cc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lt_cc<T>));
    return new (r) Function_lt_cc<T>(*this);
  }
};

// c > c
template <class T> class Function_gt_cc : public FunctionClass<T> {
public:
  Function_gt_cc(int dim, VRam *vr) : FunctionClass<T>(">", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) > *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_gt_cc<T> *r = (Function_gt_cc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gt_cc<T>));
    return new (r) Function_gt_cc<T>(*this);
  }
};

// c <= c
template <class T> class Function_le_cc : public FunctionClass<T> {
public:
  Function_le_cc(int dim, VRam *vr) : FunctionClass<T>("<=", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) <= *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_le_cc<T> *r = (Function_le_cc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_le_cc<T>));
    return new (r) Function_le_cc<T>(*this);
  }
};

// c >= c
template <class T> class Function_ge_cc : public FunctionClass<T> {
public:
  Function_ge_cc(int dim, VRam *vr) : FunctionClass<T>(">=", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) >= *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_ge_cc<T> *r = (Function_ge_cc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_ge_cc<T>));
    return new (r) Function_ge_cc<T>(*this);
  }
};

// c^c
template <class T> class Function_pow_cc : public FunctionClass<T> {
public:
  Function_pow_cc(int dim, VRam *vr) : FunctionClass<T>("^", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = pow_aa(*((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)), *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pow_cc<T> *r = (Function_pow_cc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pow_cc<T>));
    return new (r) Function_pow_cc<T>(*this);
  }
};



// --- Colour ---
// u + u -- overlapping alpha++ (a+b)+c != a+(b+c), a+b = b+a
template <class T> class Function_plus_uu : public FunctionClass<T> {
public:
  Function_plus_uu(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_COLOUR_SIGN+","+TYPE_COLOUR_SIGN,TYPE_COLOUR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
Colour *ret = (Colour*)FunctionClass<T>::outConstructor->construct(vr);
Colour *c1 = (Colour*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
Colour *c2 = (Colour*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
if (c1->a == 0) *ret = *c2;
else if (c2->a == 0) *ret = *c1;
else *ret = Colour((c1->r*c1->a + c2->r*c2->a)/(c1->a+c2->a),(c1->g*c1->a + c2->g*c2->a)/(c1->a+c2->a), (c1->b*c1->a + c2->b*c2->a)/(c1->a+c2->a), 1.0f - (1.0f-c1->a)*(1.0f-c2->a));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_uu<T> *r = (Function_plus_uu<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_uu<T>));
    return new (r) Function_plus_uu<T>(*this);
  }
};

// u * u -- colouring alpha== (a*b)*c != a*(b*c), a*b != b*a
template <class T> class Function_mult_uu : public FunctionClass<T> {
public:
  Function_mult_uu(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_COLOUR_SIGN+","+TYPE_COLOUR_SIGN,TYPE_COLOUR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
Colour *ret = (Colour*)FunctionClass<T>::outConstructor->construct(vr);
Colour *c1 = (Colour*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
Colour *c2 = (Colour*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
*ret = Colour(lerp(c1->r,1.0f,c2->r*c2->a), lerp(c1->g,1.0f,c2->g*c2->a), lerp(c1->b,1.0f,c2->b*c2->a), c1->a);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_uu<T> *r = (Function_mult_uu<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_uu<T>));
    return new (r) Function_mult_uu<T>(*this);
  }
};

// u / u
template <class T> class Function_div_uu : public FunctionClass<T> {
public:
  Function_div_uu(int dim, VRam *vr) : FunctionClass<T>("/", dim, vr) {FunctionClass<T>::setUp(TYPE_COLOUR_SIGN+","+TYPE_COLOUR_SIGN,TYPE_COLOUR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
Colour *ret = (Colour*)FunctionClass<T>::outConstructor->construct(vr);
Colour *c1 = (Colour*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
Colour *c2 = (Colour*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
*ret = Colour(lerp(c1->r,0.0f,c2->r*c2->a), lerp(c1->g,0.0f,c2->g*c2->a), lerp(c1->b,0.0f,c2->b*c2->a), c1->a);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_div_uu<T> *r = (Function_div_uu<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_div_uu<T>));
    return new (r) Function_div_uu<T>(*this);
  }
};


// --- Vector ---
// v + v
template <class T> class Function_plus_vv : public FunctionClass<T> {
public:
  Function_plus_vv(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_VECTOR_SIGN+","+TYPE_VECTOR_SIGN,TYPE_VECTOR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *v1 = ((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *v2 = ((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
for (int i = 0; i < FunctionClass<T>::dim; ++i) ret[i] = v1[i] + v2[i];
return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_vv<T> *r = (Function_plus_vv<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_vv<T>));
    return new (r) Function_plus_vv<T>(*this);
  }
};

// v - v
template <class T> class Function_minus_vv : public FunctionClass<T> {
public:
  Function_minus_vv(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_VECTOR_SIGN+","+TYPE_VECTOR_SIGN,TYPE_VECTOR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *v1 = ((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *v2 = ((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
for (int i = 0; i < FunctionClass<T>::dim; ++i) ret[i] = v1[i] - v2[i];
return ret;
  }
  FunctionClass<T>* copy() {
    Function_minus_vv<T> *r = (Function_minus_vv<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_minus_vv<T>));
    return new (r) Function_minus_vv<T>(*this);
  }
};

// v < v
template <class T> class Function_lt_vv : public FunctionClass<T> {
public:
  Function_lt_vv(int dim, VRam *vr) : FunctionClass<T>("<", dim, vr) {FunctionClass<T>::setUp(TYPE_VECTOR_SIGN+","+TYPE_VECTOR_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
T *v1 = ((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *v2 = ((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
T s1, s2;
s1 = s2 = 0;
for (int i = 0; i < FunctionClass<T>::dim; ++i) s1 += v1[i], s2 += v2[i];
*ret = s1 < s2;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lt_vv<T> *r = (Function_lt_vv<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lt_vv<T>));
    return new (r) Function_lt_vv<T>(*this);
  }
};

// v <= v
template <class T> class Function_le_vv : public FunctionClass<T> {
public:
  Function_le_vv(int dim, VRam *vr) : FunctionClass<T>("<=", dim, vr) {FunctionClass<T>::setUp(TYPE_VECTOR_SIGN+","+TYPE_VECTOR_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
T *v1 = ((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *v2 = ((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
T s1, s2;
s1 = s2 = 0;
for (int i = 0; i < FunctionClass<T>::dim; ++i) s1 += v1[i], s2 += v2[i];
*ret = s1 <= s2;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_le_vv<T> *r = (Function_le_vv<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_le_vv<T>));
    return new (r) Function_le_vv<T>(*this);
  }
};

// v > v
template <class T> class Function_gt_vv : public FunctionClass<T> {
public:
  Function_gt_vv(int dim, VRam *vr) : FunctionClass<T>(">", dim, vr) {FunctionClass<T>::setUp(TYPE_VECTOR_SIGN+","+TYPE_VECTOR_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
T *v1 = ((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *v2 = ((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
T s1, s2;
s1 = s2 = 0;
for (int i = 0; i < FunctionClass<T>::dim; ++i) s1 += v1[i], s2 += v2[i];
*ret = s1 > s2;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_gt_vv<T> *r = (Function_gt_vv<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gt_vv<T>));
    return new (r) Function_gt_vv<T>(*this);
  }
};

// v >= v
template <class T> class Function_ge_vv : public FunctionClass<T> {
public:
  Function_ge_vv(int dim, VRam *vr) : FunctionClass<T>(">=", dim, vr) {FunctionClass<T>::setUp(TYPE_VECTOR_SIGN+","+TYPE_VECTOR_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
T *v1 = ((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *v2 = ((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
T s1, s2;
s1 = s2 = 0;
for (int i = 0; i < FunctionClass<T>::dim; ++i) s1 += v1[i], s2 += v2[i];
*ret = s1 >= s2;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_ge_vv<T> *r = (Function_ge_vv<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_ge_vv<T>));
    return new (r) Function_ge_vv<T>(*this);
  }
};

// v = v
template <class T> class Function_eq_vv : public FunctionClass<T> {
public:
  Function_eq_vv(int dim, VRam *vr) : FunctionClass<T>("=", dim, vr) {FunctionClass<T>::setUp(TYPE_VECTOR_SIGN+","+TYPE_VECTOR_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
T *v1 = ((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *v2 = ((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
*ret = true;
for (int i = 0; i < FunctionClass<T>::dim && *ret; ++i) *ret = v1[i] == v2[i];
return ret;
  }
  FunctionClass<T>* copy() {
    Function_eq_vv<T> *r = (Function_eq_vv<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_eq_vv<T>));
    return new (r) Function_eq_vv<T>(*this);
  }
};


// --- Matrix ---
// m + m
template <class T> class Function_plus_mm : public FunctionClass<T> {
public:
  Function_plus_mm(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_MATRIX_SIGN+","+TYPE_MATRIX_SIGN,TYPE_MATRIX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T **ret = (T**)FunctionClass<T>::outConstructor->construct(vr);
T **v1 = ((T**)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T **v2 = ((T**)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
for (int i = 0; i < FunctionClass<T>::dim; ++i) for (int j = 0; j < FunctionClass<T>::dim; ++j) ret[i][j] = v1[i][j] + v2[i][j];
return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_mm<T> *r = (Function_plus_mm<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_mm<T>));
    return new (r) Function_plus_mm<T>(*this);
  }
};

// m - m
template <class T> class Function_minus_mm : public FunctionClass<T> {
public:
  Function_minus_mm(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_MATRIX_SIGN+","+TYPE_MATRIX_SIGN,TYPE_MATRIX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T **ret = (T**)FunctionClass<T>::outConstructor->construct(vr);
T **v1 = ((T**)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T **v2 = ((T**)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
for (int i = 0; i < FunctionClass<T>::dim; ++i) for (int j = 0; j < FunctionClass<T>::dim; ++j) ret[i][j] = v1[i][j] - v2[i][j];
return ret;
  }
  FunctionClass<T>* copy() {
    Function_minus_mm<T> *r = (Function_minus_mm<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_minus_mm<T>));
    return new (r) Function_minus_mm<T>(*this);
  }
};

// m * m
template <class T> class Function_mult_mm : public FunctionClass<T> {
public:
  Function_mult_mm(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_MATRIX_SIGN+","+TYPE_MATRIX_SIGN,TYPE_MATRIX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T **ret = (T**)FunctionClass<T>::outConstructor->construct(vr);
T **v1 = ((T**)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T **v2 = ((T**)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
for (int i = 0; i < FunctionClass<T>::dim; ++i) for (int j = 0; j < FunctionClass<T>::dim; ++j) {
ret[i][j] = 0;
for (int k = 0; k < FunctionClass<T>::dim; ++k) ret[i][j] += v1[i][k] * v2[k][j];
}
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_mm<T> *r = (Function_mult_mm<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_mm<T>));
    return new (r) Function_mult_mm<T>(*this);
  }
};



// --- Cross ---
// i + r
template <class T> class Function_plus_ir : public FunctionClass<T> {
public:
  Function_plus_ir(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = (T)(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))) + *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_ir<T> *r = (Function_plus_ir<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_ir<T>));
    return new (r) Function_plus_ir<T>(*this);
  }
};

// i + c
template <class T> class Function_plus_ic : public FunctionClass<T> {
public:
  Function_plus_ic(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) + *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_ic<T> *r = (Function_plus_ic<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_ic<T>));
    return new (r) Function_plus_ic<T>(*this);
  }
};

// r + i
template <class T> class Function_plus_ri : public FunctionClass<T> {
public:
  Function_plus_ri(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_INT_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) + *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_ri<T> *r = (Function_plus_ri<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_ri<T>));
    return new (r) Function_plus_ri<T>(*this);
  }
};

// r + c
template <class T> class Function_plus_rc : public FunctionClass<T> {
public:
  Function_plus_rc(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) + *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_rc<T> *r = (Function_plus_rc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_rc<T>));
    return new (r) Function_plus_rc<T>(*this);
  }
};

// c + i
template <class T> class Function_plus_ci : public FunctionClass<T> {
public:
  Function_plus_ci(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_INT_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) + complex<T>(*((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_ci<T> *r = (Function_plus_ci<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_ci<T>));
    return new (r) Function_plus_ci<T>(*this);
  }
};

// c + r
template <class T> class Function_plus_cr : public FunctionClass<T> {
public:
  Function_plus_cr(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_REAL_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) + complex<T>(*((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_cr<T> *r = (Function_plus_cr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_cr<T>));
    return new (r) Function_plus_cr<T>(*this);
  }
};

// i - r
template <class T> class Function_minus_ir : public FunctionClass<T> {
public:
  Function_minus_ir(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = (T)(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))) - *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_minus_ir<T> *r = (Function_minus_ir<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_minus_ir<T>));
    return new (r) Function_minus_ir<T>(*this);
  }
};

// i - c
template <class T> class Function_minus_ic : public FunctionClass<T> {
public:
  Function_minus_ic(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) - *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_minus_ic<T> *r = (Function_minus_ic<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_minus_ic<T>));
    return new (r) Function_minus_ic<T>(*this);
  }
};

// r - i
template <class T> class Function_minus_ri : public FunctionClass<T> {
public:
  Function_minus_ri(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_INT_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) - *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_minus_ri<T> *r = (Function_minus_ri<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_minus_ri<T>));
    return new (r) Function_minus_ri<T>(*this);
  }
};

// r - c
template <class T> class Function_minus_rc : public FunctionClass<T> {
public:
  Function_minus_rc(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) - *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_minus_rc<T> *r = (Function_minus_rc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_minus_rc<T>));
    return new (r) Function_minus_rc<T>(*this);
  }
};

// c - i
template <class T> class Function_minus_ci : public FunctionClass<T> {
public:
  Function_minus_ci(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_INT_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) - complex<T>(*((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_minus_ci<T> *r = (Function_minus_ci<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_minus_ci<T>));
    return new (r) Function_minus_ci<T>(*this);
  }
};

// c - r
template <class T> class Function_minus_cr : public FunctionClass<T> {
public:
  Function_minus_cr(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_REAL_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) - complex<T>(*((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_minus_cr<T> *r = (Function_minus_cr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_minus_cr<T>));
    return new (r) Function_minus_cr<T>(*this);
  }
};

// i * r
template <class T> class Function_mult_ir : public FunctionClass<T> {
public:
  Function_mult_ir(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = (T)(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))) * *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_ir<T> *r = (Function_mult_ir<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_ir<T>));
    return new (r) Function_mult_ir<T>(*this);
  }
};

// i * c
template <class T> class Function_mult_ic : public FunctionClass<T> {
public:
  Function_mult_ic(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) * *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_ic<T> *r = (Function_mult_ic<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_ic<T>));
    return new (r) Function_mult_ic<T>(*this);
  }
};

// r * i
template <class T> class Function_mult_ri : public FunctionClass<T> {
public:
  Function_mult_ri(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_INT_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) * *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_ri<T> *r = (Function_mult_ri<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_ri<T>));
    return new (r) Function_mult_ri<T>(*this);
  }
};

// r * c
template <class T> class Function_mult_rc : public FunctionClass<T> {
public:
  Function_mult_rc(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) * *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_rc<T> *r = (Function_mult_rc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_rc<T>));
    return new (r) Function_mult_rc<T>(*this);
  }
};

// c * i
template <class T> class Function_mult_ci : public FunctionClass<T> {
public:
  Function_mult_ci(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_INT_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) * complex<T>(*((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_ci<T> *r = (Function_mult_ci<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_ci<T>));
    return new (r) Function_mult_ci<T>(*this);
  }
};

// c * r
template <class T> class Function_mult_cr : public FunctionClass<T> {
public:
  Function_mult_cr(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_REAL_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) * complex<T>(*((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_cr<T> *r = (Function_mult_cr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_cr<T>));
    return new (r) Function_mult_cr<T>(*this);
  }
};

// i / r
template <class T> class Function_div_ir : public FunctionClass<T> {
public:
  Function_div_ir(int dim, VRam *vr) : FunctionClass<T>("/", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *p1 = (T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
if (*p1 == 0) {
LOG.debug("Division by 0 (ir)");
throw RuntimeError(DIVISION_BY_0, "Division by 0", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
}
*ret = (T)(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))) / *p1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_div_ir<T> *r = (Function_div_ir<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_div_ir<T>));
    return new (r) Function_div_ir<T>(*this);
  }
};

// i / c
template <class T> class Function_div_ic : public FunctionClass<T> {
public:
  Function_div_ic(int dim, VRam *vr) : FunctionClass<T>("/", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p1 = (complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
if (p1->real() == 0 && p1->imag() == 0) {
LOG.debug("Division by 0 (ic)");
throw RuntimeError(DIVISION_BY_0, "Division by 0+0*i", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
}
*ret = complex<T>((*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) / *p1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_div_ic<T> *r = (Function_div_ic<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_div_ic<T>));
    return new (r) Function_div_ic<T>(*this);
  }
};

// r / i
template <class T> class Function_div_ri : public FunctionClass<T> {
public:
  Function_div_ri(int dim, VRam *vr) : FunctionClass<T>("/", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_INT_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
int *p1 = (int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
if (*p1 == 0) {
LOG.debug("Division by 0 (ri)");
throw RuntimeError(DIVISION_BY_0, "Division by 0", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
}
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) / *p1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_div_ri<T> *r = (Function_div_ri<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_div_ri<T>));
    return new (r) Function_div_ri<T>(*this);
  }
};

// r / c
template <class T> class Function_div_rc : public FunctionClass<T> {
public:
  Function_div_rc(int dim, VRam *vr) : FunctionClass<T>("/", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p1 = (complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
if (p1->real() == 0 && p1->imag() == 0) {
LOG.debug("Division by 0 (rc)");
throw RuntimeError(DIVISION_BY_0, "Division by 0+0*i", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
}
*ret = complex<T>((*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) / *p1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_div_rc<T> *r = (Function_div_rc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_div_rc<T>));
    return new (r) Function_div_rc<T>(*this);
  }
};

// c / i
template <class T> class Function_div_ci : public FunctionClass<T> {
public:
  Function_div_ci(int dim, VRam *vr) : FunctionClass<T>("/", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_INT_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
int *p1 = (int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
if (*p1 == 0) {
LOG.debug("Division by 0 (ci)");
throw RuntimeError(DIVISION_BY_0, "Division by 0", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
}
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) / complex<T>(*p1,0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_div_ci<T> *r = (Function_div_ci<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_div_ci<T>));
    return new (r) Function_div_ci<T>(*this);
  }
};

// c / r
template <class T> class Function_div_cr : public FunctionClass<T> {
public:
  Function_div_cr(int dim, VRam *vr) : FunctionClass<T>("/", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_REAL_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
T *p1 = (T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
if (*p1 == 0) {
LOG.debug("Division by 0 (cr)");
throw RuntimeError(DIVISION_BY_0, "Division by 0", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
}
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) / complex<T>(*p1,0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_div_cr<T> *r = (Function_div_cr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_div_cr<T>));
    return new (r) Function_div_cr<T>(*this);
  }
};


// i ^ r
template <class T> class Function_pow_ir : public FunctionClass<T> {
public:
  Function_pow_ir(int dim, VRam *vr) : FunctionClass<T>("^", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = pow_aa((T)(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))), *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pow_ir<T> *r = (Function_pow_ir<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pow_ir<T>));
    return new (r) Function_pow_ir<T>(*this);
  }
};

// i ^ c
template <class T> class Function_pow_ic : public FunctionClass<T> {
public:
  Function_pow_ic(int dim, VRam *vr) : FunctionClass<T>("^", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = pow_aa(complex<T>((*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0), *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pow_ic<T> *r = (Function_pow_ic<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pow_ic<T>));
    return new (r) Function_pow_ic<T>(*this);
  }
};

// r ^ i
template <class T> class Function_pow_ri : public FunctionClass<T> {
public:
  Function_pow_ri(int dim, VRam *vr) : FunctionClass<T>("^", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_INT_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = pow_ai(*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)), *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pow_ri<T> *r = (Function_pow_ri<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pow_ri<T>));
    return new (r) Function_pow_ri<T>(*this);
  }
};

// r ^ c
template <class T> class Function_pow_rc : public FunctionClass<T> {
public:
  Function_pow_rc(int dim, VRam *vr) : FunctionClass<T>("^", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = pow_aa(complex<T>((*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0), *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pow_rc<T> *r = (Function_pow_rc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pow_rc<T>));
    return new (r) Function_pow_rc<T>(*this);
  }
};

// c ^ i
template <class T> class Function_pow_ci : public FunctionClass<T> {
public:
  Function_pow_ci(int dim, VRam *vr) : FunctionClass<T>("^", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_INT_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = pow_ai(*((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)), *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pow_ci<T> *r = (Function_pow_ci<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pow_ci<T>));
    return new (r) Function_pow_ci<T>(*this);
  }
};

// c ^ r
template <class T> class Function_pow_cr : public FunctionClass<T> {
public:
  Function_pow_cr(int dim, VRam *vr) : FunctionClass<T>("^", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_REAL_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
*ret = pow_aa(*((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)), complex<T>(*((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pow_cr<T> *r = (Function_pow_cr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pow_cr<T>));
    return new (r) Function_pow_cr<T>(*this);
  }
};

// i < r
template <class T> class Function_lt_ir : public FunctionClass<T> {
public:
  Function_lt_ir(int dim, VRam *vr) : FunctionClass<T>("<", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = (T)(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))) < *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lt_ir<T> *r = (Function_lt_ir<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lt_ir<T>));
    return new (r) Function_lt_ir<T>(*this);
  }
};

// i < c
template <class T> class Function_lt_ic : public FunctionClass<T> {
public:
  Function_lt_ic(int dim, VRam *vr) : FunctionClass<T>("<", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) < *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lt_ic<T> *r = (Function_lt_ic<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lt_ic<T>));
    return new (r) Function_lt_ic<T>(*this);
  }
};

// r < i
template <class T> class Function_lt_ri : public FunctionClass<T> {
public:
  Function_lt_ri(int dim, VRam *vr) : FunctionClass<T>("<", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) < *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lt_ri<T> *r = (Function_lt_ri<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lt_ri<T>));
    return new (r) Function_lt_ri<T>(*this);
  }
};

// r < c
template <class T> class Function_lt_rc : public FunctionClass<T> {
public:
  Function_lt_rc(int dim, VRam *vr) : FunctionClass<T>("<", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) < *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lt_rc<T> *r = (Function_lt_rc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lt_rc<T>));
    return new (r) Function_lt_rc<T>(*this);
  }
};

// c < i
template <class T> class Function_lt_ci : public FunctionClass<T> {
public:
  Function_lt_ci(int dim, VRam *vr) : FunctionClass<T>("<", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) < complex<T>(*((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lt_ci<T> *r = (Function_lt_ci<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lt_ci<T>));
    return new (r) Function_lt_ci<T>(*this);
  }
};

// c < r
template <class T> class Function_lt_cr : public FunctionClass<T> {
public:
  Function_lt_cr(int dim, VRam *vr) : FunctionClass<T>("<", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) < complex<T>(*((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lt_cr<T> *r = (Function_lt_cr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lt_cr<T>));
    return new (r) Function_lt_cr<T>(*this);
  }
};

// i <= r
template <class T> class Function_le_ir : public FunctionClass<T> {
public:
  Function_le_ir(int dim, VRam *vr) : FunctionClass<T>("<=", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = (T)(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))) <= *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_le_ir<T> *r = (Function_le_ir<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_le_ir<T>));
    return new (r) Function_le_ir<T>(*this);
  }
};

// i <= c
template <class T> class Function_le_ic : public FunctionClass<T> {
public:
  Function_le_ic(int dim, VRam *vr) : FunctionClass<T>("<=", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) <= *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_le_ic<T> *r = (Function_le_ic<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_le_ic<T>));
    return new (r) Function_le_ic<T>(*this);
  }
};

// r <= i
template <class T> class Function_le_ri : public FunctionClass<T> {
public:
  Function_le_ri(int dim, VRam *vr) : FunctionClass<T>("<=", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) <= *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_le_ri<T> *r = (Function_le_ri<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_le_ri<T>));
    return new (r) Function_le_ri<T>(*this);
  }
};

// r <= c
template <class T> class Function_le_rc : public FunctionClass<T> {
public:
  Function_le_rc(int dim, VRam *vr) : FunctionClass<T>("<=", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) <= *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_le_rc<T> *r = (Function_le_rc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_le_rc<T>));
    return new (r) Function_le_rc<T>(*this);
  }
};

// c <= i
template <class T> class Function_le_ci : public FunctionClass<T> {
public:
  Function_le_ci(int dim, VRam *vr) : FunctionClass<T>("<=", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) <= complex<T>(*((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_le_ci<T> *r = (Function_le_ci<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_le_ci<T>));
    return new (r) Function_le_ci<T>(*this);
  }
};

// c <= r
template <class T> class Function_le_cr : public FunctionClass<T> {
public:
  Function_le_cr(int dim, VRam *vr) : FunctionClass<T>("<=", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) <= complex<T>(*((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_le_cr<T> *r = (Function_le_cr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_le_cr<T>));
    return new (r) Function_le_cr<T>(*this);
  }
};

// i > r
template <class T> class Function_gt_ir : public FunctionClass<T> {
public:
  Function_gt_ir(int dim, VRam *vr) : FunctionClass<T>(">", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = (T)(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))) > *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_gt_ir<T> *r = (Function_gt_ir<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gt_ir<T>));
    return new (r) Function_gt_ir<T>(*this);
  }
};

// i > c
template <class T> class Function_gt_ic : public FunctionClass<T> {
public:
  Function_gt_ic(int dim, VRam *vr) : FunctionClass<T>(">", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) > *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_gt_ic<T> *r = (Function_gt_ic<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gt_ic<T>));
    return new (r) Function_gt_ic<T>(*this);
  }
};

// r > i
template <class T> class Function_gt_ri : public FunctionClass<T> {
public:
  Function_gt_ri(int dim, VRam *vr) : FunctionClass<T>(">", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) > *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_gt_ri<T> *r = (Function_gt_ri<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gt_ri<T>));
    return new (r) Function_gt_ri<T>(*this);
  }
};

// r > c
template <class T> class Function_gt_rc : public FunctionClass<T> {
public:
  Function_gt_rc(int dim, VRam *vr) : FunctionClass<T>(">", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) > *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_gt_rc<T> *r = (Function_gt_rc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gt_rc<T>));
    return new (r) Function_gt_rc<T>(*this);
  }
};

// c > i
template <class T> class Function_gt_ci : public FunctionClass<T> {
public:
  Function_gt_ci(int dim, VRam *vr) : FunctionClass<T>(">", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) > complex<T>(*((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_gt_ci<T> *r = (Function_gt_ci<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gt_ci<T>));
    return new (r) Function_gt_ci<T>(*this);
  }
};

// c > r
template <class T> class Function_gt_cr : public FunctionClass<T> {
public:
  Function_gt_cr(int dim, VRam *vr) : FunctionClass<T>(">", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) > complex<T>(*((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_gt_cr<T> *r = (Function_gt_cr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gt_cr<T>));
    return new (r) Function_gt_cr<T>(*this);
  }
};

// i >= r
template <class T> class Function_ge_ir : public FunctionClass<T> {
public:
  Function_ge_ir(int dim, VRam *vr) : FunctionClass<T>(">=", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = (T)(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))) >= *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_ge_ir<T> *r = (Function_ge_ir<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_ge_ir<T>));
    return new (r) Function_ge_ir<T>(*this);
  }
};

// i >= c
template <class T> class Function_ge_ic : public FunctionClass<T> {
public:
  Function_ge_ic(int dim, VRam *vr) : FunctionClass<T>(">=", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) >= *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_ge_ic<T> *r = (Function_ge_ic<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_ge_ic<T>));
    return new (r) Function_ge_ic<T>(*this);
  }
};

// r >= i
template <class T> class Function_ge_ri : public FunctionClass<T> {
public:
  Function_ge_ri(int dim, VRam *vr) : FunctionClass<T>(">=", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) >= *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_ge_ri<T> *r = (Function_ge_ri<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_ge_ri<T>));
    return new (r) Function_ge_ri<T>(*this);
  }
};

// r >= c
template <class T> class Function_ge_rc : public FunctionClass<T> {
public:
  Function_ge_rc(int dim, VRam *vr) : FunctionClass<T>(">=", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) >= *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_ge_rc<T> *r = (Function_ge_rc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_ge_rc<T>));
    return new (r) Function_ge_rc<T>(*this);
  }
};

// c >= i
template <class T> class Function_ge_ci : public FunctionClass<T> {
public:
  Function_ge_ci(int dim, VRam *vr) : FunctionClass<T>(">=", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) >= complex<T>(*((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_ge_ci<T> *r = (Function_ge_ci<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_ge_ci<T>));
    return new (r) Function_ge_ci<T>(*this);
  }
};

// c >= r
template <class T> class Function_ge_cr : public FunctionClass<T> {
public:
  Function_ge_cr(int dim, VRam *vr) : FunctionClass<T>(">=", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) >= complex<T>(*((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_ge_cr<T> *r = (Function_ge_cr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_ge_cr<T>));
    return new (r) Function_ge_cr<T>(*this);
  }
};

// i = r
template <class T> class Function_eq_ir : public FunctionClass<T> {
public:
  Function_eq_ir(int dim, VRam *vr) : FunctionClass<T>("=", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = (T)(*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))) == *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_eq_ir<T> *r = (Function_eq_ir<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_eq_ir<T>));
    return new (r) Function_eq_ir<T>(*this);
  }
};


// i = c
template <class T> class Function_eq_ic : public FunctionClass<T> {
public:
  Function_eq_ic(int dim, VRam *vr) : FunctionClass<T>("=", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) == *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_eq_ic<T> *r = (Function_eq_ic<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_eq_ic<T>));
    return new (r) Function_eq_ic<T>(*this);
  }
};

// r = i
template <class T> class Function_eq_ri : public FunctionClass<T> {
public:
  Function_eq_ri(int dim, VRam *vr) : FunctionClass<T>("=", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) == *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_eq_ri<T> *r = (Function_eq_ri<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_eq_ri<T>));
    return new (r) Function_eq_ri<T>(*this);
  }
};

// r = c
template <class T> class Function_eq_rc : public FunctionClass<T> {
public:
  Function_eq_rc(int dim, VRam *vr) : FunctionClass<T>("=", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_CPLX_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = complex<T>((*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))),0) == *((complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_eq_rc<T> *r = (Function_eq_rc<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_eq_rc<T>));
    return new (r) Function_eq_rc<T>(*this);
  }
};

// c = i
template <class T> class Function_eq_ci : public FunctionClass<T> {
public:
  Function_eq_ci(int dim, VRam *vr) : FunctionClass<T>("=", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_INT_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) == complex<T>(*((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_eq_ci<T> *r = (Function_eq_ci<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_eq_ci<T>));
    return new (r) Function_eq_ci<T>(*this);
  }
};

// c = r
template <class T> class Function_eq_cr : public FunctionClass<T> {
public:
  Function_eq_cr(int dim, VRam *vr) : FunctionClass<T>("=", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) == complex<T>(*((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread)),0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_eq_cr<T> *r = (Function_eq_cr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_eq_cr<T>));
    return new (r) Function_eq_cr<T>(*this);
  }
};

// i * v
template <class T> class Function_mult_iv : public FunctionClass<T> {
public:
  Function_mult_iv(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_VECTOR_SIGN,TYPE_VECTOR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
int *p1 = ((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *v2 = ((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
for (int i = 0; i < FunctionClass<T>::dim; ++i) ret[i] = *p1 * v2[i];
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_iv<T> *r = (Function_mult_iv<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_iv<T>));
    return new (r) Function_mult_iv<T>(*this);
  }
};

// r * v
template <class T> class Function_mult_rv : public FunctionClass<T> {
public:
  Function_mult_rv(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_VECTOR_SIGN,TYPE_VECTOR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *p1 = ((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *v2 = ((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
for (int i = 0; i < FunctionClass<T>::dim; ++i) ret[i] = *p1 * v2[i];
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_rv<T> *r = (Function_mult_rv<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_rv<T>));
    return new (r) Function_mult_rv<T>(*this);
  }
};

// i * m
template <class T> class Function_mult_im : public FunctionClass<T> {
public:
  Function_mult_im(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_MATRIX_SIGN,TYPE_MATRIX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T **ret = (T**)FunctionClass<T>::outConstructor->construct(vr);
int *p1 = ((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T **v2 = ((T**)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
for (int i = 0; i < FunctionClass<T>::dim; ++i) for (int j = 0; j < FunctionClass<T>::dim; ++j) ret[i][j] = *p1 + v2[i][j];
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_im<T> *r = (Function_mult_im<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_im<T>));
    return new (r) Function_mult_im<T>(*this);
  }
};

// r * m
template <class T> class Function_mult_rm : public FunctionClass<T> {
public:
  Function_mult_rm(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_MATRIX_SIGN,TYPE_MATRIX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T **ret = (T**)FunctionClass<T>::outConstructor->construct(vr);
T *p1 = ((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T **v2 = ((T**)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
for (int i = 0; i < FunctionClass<T>::dim; ++i) for (int j = 0; j < FunctionClass<T>::dim; ++j) ret[i][j] = *p1 + v2[i][j];
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_rm<T> *r = (Function_mult_rm<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_rm<T>));
    return new (r) Function_mult_rm<T>(*this);
  }
};

// m * v
template <class T> class Function_mult_mv : public FunctionClass<T> {
public:
  Function_mult_mv(int dim, VRam *vr) : FunctionClass<T>("*", dim, vr) {FunctionClass<T>::setUp(TYPE_MATRIX_SIGN+","+TYPE_VECTOR_SIGN,TYPE_VECTOR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T **v1 = ((T**)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *v2 = ((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
for (int i = 0; i < FunctionClass<T>::dim; ++i) {
ret[i] = 0;
for (int j = 0; j < FunctionClass<T>::dim; ++j) ret[i] += v1[i][j] * v2[j];
}
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mult_mv<T> *r = (Function_mult_mv<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mult_mv<T>));
    return new (r) Function_mult_mv<T>(*this);
  }
};



//----- Constructors -----
// Real(int)
template <class T> class Function_constr_i : public FunctionClass<T> {
public:
  Function_constr_i(int dim, VRam *vr) : FunctionClass<T>("Real", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
int *i = ((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
*ret = *i;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_constr_i<T> *r = (Function_constr_i<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constr_i<T>));
    return new (r) Function_constr_i<T>(*this);
  }
};

// Real.pi
template <class T> class Function_pi : public FunctionClass<T> {
public:
  Function_pi(int dim, VRam *vr) : FunctionClass<T>("Real.pi", dim, vr) {FunctionClass<T>::setUp("",TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = M_PI;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pi<T> *r = (Function_pi<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pi<T>));
    return new (r) Function_pi<T>(*this);
  }
};

// Real.e
template <class T> class Function_e : public FunctionClass<T> {
public:
  Function_e(int dim, VRam *vr) : FunctionClass<T>("Real.e", dim, vr) {FunctionClass<T>::setUp("",TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = M_E;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_e<T> *r = (Function_e<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_e<T>));
    return new (r) Function_e<T>(*this);
  }
};

// Complex(Int)
template <class T> class Function_constc_i : public FunctionClass<T> {
public:
  Function_constc_i(int dim, VRam *vr) : FunctionClass<T>("Complex", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
int *i = ((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
*ret = complex<T>(*i,0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_constc_i<T> *r = (Function_constc_i<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constc_i<T>));
    return new (r) Function_constc_i<T>(*this);
  }
};

// Complex(Real)
template <class T> class Function_constc_r : public FunctionClass<T> {
public:
  Function_constc_r(int dim, VRam *vr) : FunctionClass<T>("Complex", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
T *i = ((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
*ret = complex<T>(*i,0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_constc_r<T> *r = (Function_constc_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constc_r<T>));
    return new (r) Function_constc_r<T>(*this);
  }
};

// Complex(Real,Int)
template <class T> class Function_constc_ri : public FunctionClass<T> {
public:
  Function_constc_ri(int dim, VRam *vr) : FunctionClass<T>("Complex", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_INT_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
T *p1 = ((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
int *p2 = ((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
*ret = complex<T>(*p1,*p2);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_constc_ri<T> *r = (Function_constc_ri<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constc_ri<T>));
    return new (r) Function_constc_ri<T>(*this);
  }
};

// Complex(Int,Real)
template <class T> class Function_constc_ir : public FunctionClass<T> {
public:
  Function_constc_ir(int dim, VRam *vr) : FunctionClass<T>("Complex", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_REAL_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
int *p1 = ((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *p2 = ((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
*ret = complex<T>(*p1,*p2);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_constc_ir<T> *r = (Function_constc_ir<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constc_ir<T>));
    return new (r) Function_constc_ir<T>(*this);
  }
};

// Complex(Real,Real)
template <class T> class Function_constc_rr : public FunctionClass<T> {
public:
  Function_constc_rr(int dim, VRam *vr) : FunctionClass<T>("Complex", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
T *p1 = ((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *p2 = ((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
*ret = complex<T>(*p1,*p2);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_constc_rr<T> *r = (Function_constc_rr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constc_rr<T>));
    return new (r) Function_constc_rr<T>(*this);
  }
};

// Colour(Real,Real,Real,Real)
template <class T> class Function_constcol_rrrr : public FunctionClass<T> {
public:
  Function_constcol_rrrr(int dim, VRam *vr) : FunctionClass<T>("Colour", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN+","+TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_COLOUR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
Colour *ret = (Colour*)FunctionClass<T>::outConstructor->construct(vr);
ret->r = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
ret->g = *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread));
ret->b = *((T*)FunctionClass<T>::parameters[2]->calc(data,data2,vr,thread));
ret->a = *((T*)FunctionClass<T>::parameters[3]->calc(data,data2,vr,thread));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_constcol_rrrr<T> *r = (Function_constcol_rrrr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constcol_rrrr<T>));
    return new (r) Function_constcol_rrrr<T>(*this);
  }
};

// Colour.red
template <class T> class Function_red : public FunctionClass<T> {
public:
  Function_red(int dim, VRam *vr) : FunctionClass<T>("Colour.red", dim, vr) {FunctionClass<T>::setUp("",TYPE_COLOUR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
Colour *ret = (Colour*)FunctionClass<T>::outConstructor->construct(vr);
ret->r = 1;
ret->g = 0;
ret->b = 0;
ret->a = 1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_red<T> *r = (Function_red<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_red<T>));
    return new (r) Function_red<T>(*this);
  }
};

// Colour.green
template <class T> class Function_green : public FunctionClass<T> {
public:
  Function_green(int dim, VRam *vr) : FunctionClass<T>("Colour.green", dim, vr) {FunctionClass<T>::setUp("",TYPE_COLOUR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
Colour *ret = (Colour*)FunctionClass<T>::outConstructor->construct(vr);
ret->r = 0;
ret->g = 1;
ret->b = 0;
ret->a = 1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_green<T> *r = (Function_green<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_green<T>));
    return new (r) Function_green<T>(*this);
  }
};

// Colour.blue
template <class T> class Function_blue : public FunctionClass<T> {
public:
  Function_blue(int dim, VRam *vr) : FunctionClass<T>("Colour.blue", dim, vr) {FunctionClass<T>::setUp("",TYPE_COLOUR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
Colour *ret = (Colour*)FunctionClass<T>::outConstructor->construct(vr);
ret->r = 0;
ret->g = 0;
ret->b = 1;
ret->a = 1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_blue<T> *r = (Function_blue<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_blue<T>));
    return new (r) Function_blue<T>(*this);
  }
};

// Colour.white
template <class T> class Function_white : public FunctionClass<T> {
public:
  Function_white(int dim, VRam *vr) : FunctionClass<T>("Colour.white", dim, vr) {FunctionClass<T>::setUp("",TYPE_COLOUR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
Colour *ret = (Colour*)FunctionClass<T>::outConstructor->construct(vr);
ret->r = 1;
ret->g = 1;
ret->b = 1;
ret->a = 1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_white<T> *r = (Function_white<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_white<T>));
    return new (r) Function_white<T>(*this);
  }
};

// Colour.black
template <class T> class Function_black : public FunctionClass<T> {
public:
  Function_black(int dim, VRam *vr) : FunctionClass<T>("Colour.black", dim, vr) {FunctionClass<T>::setUp("",TYPE_COLOUR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
Colour *ret = (Colour*)FunctionClass<T>::outConstructor->construct(vr);
ret->r = 0;
ret->g = 0;
ret->b = 0;
ret->a = 1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_black<T> *r = (Function_black<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_black<T>));
    return new (r) Function_black<T>(*this);
  }
};

// Colour.none
template <class T> class Function_none : public FunctionClass<T> {
public:
  Function_none(int dim, VRam *vr) : FunctionClass<T>("Colour.none", dim, vr) {FunctionClass<T>::setUp("",TYPE_COLOUR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
Colour *ret = (Colour*)FunctionClass<T>::outConstructor->construct(vr);
ret->r = 0;
ret->g = 0;
ret->b = 0;
ret->a = 0;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_none<T> *r = (Function_none<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_none<T>));
    return new (r) Function_none<T>(*this);
  }
};

// Vector.zero
template <class T> class Function_vzero : public FunctionClass<T> {
public:
  Function_vzero(int dim, VRam *vr) : FunctionClass<T>("Vector.zero", dim, vr) {FunctionClass<T>::setUp("",TYPE_VECTOR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
for (int i = 0; i < FunctionClass<T>::dim; ++i) ret[i] = 0;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_vzero<T> *r = (Function_vzero<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_vzero<T>));
    return new (r) Function_vzero<T>(*this);
  }
};

// Vector.x
template <class T> class Function_vx : public FunctionClass<T> {
public:
  Function_vx(int dim, VRam *vr) : FunctionClass<T>("Vector.x", dim, vr) {FunctionClass<T>::setUp("",TYPE_VECTOR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
for (int i = 0; i < FunctionClass<T>::dim; ++i) ret[i] = 0;
ret[0] = 1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_vx<T> *r = (Function_vx<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_vx<T>));
    return new (r) Function_vx<T>(*this);
  }
};

// Vector.y
template <class T> class Function_vy : public FunctionClass<T> {
public:
  Function_vy(int dim, VRam *vr) : FunctionClass<T>("Vector.y", dim, vr) {FunctionClass<T>::setUp("",TYPE_VECTOR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
for (int i = 0; i < FunctionClass<T>::dim; ++i) ret[i] = 0;
ret[1] = 1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_vy<T> *r = (Function_vy<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_vy<T>));
    return new (r) Function_vy<T>(*this);
  }
};

// Vector.z
template <class T> class Function_vz : public FunctionClass<T> {
public:
  Function_vz(int dim, VRam *vr) : FunctionClass<T>("Vector.z", dim, vr) {FunctionClass<T>::setUp("",TYPE_VECTOR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
for (int i = 0; i < FunctionClass<T>::dim; ++i) ret[i] = 0;
ret[2] = 1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_vz<T> *r = (Function_vz<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_vz<T>));
    return new (r) Function_vz<T>(*this);
  }
};

// Matrix.zero
template <class T> class Function_mzero : public FunctionClass<T> {
public:
  Function_mzero(int dim, VRam *vr) : FunctionClass<T>("Matrix.zero", dim, vr) {FunctionClass<T>::setUp("",TYPE_MATRIX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T **ret = (T**)FunctionClass<T>::outConstructor->construct(vr);
for (int i = 0; i < FunctionClass<T>::dim; ++i) for (int j = 0; j < FunctionClass<T>::dim; ++j) ret[i][j] = 0;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mzero<T> *r = (Function_mzero<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mzero<T>));
    return new (r) Function_mzero<T>(*this);
  }
};

// Matrix.identity
template <class T> class Function_midentity : public FunctionClass<T> {
public:
  Function_midentity(int dim, VRam *vr) : FunctionClass<T>("Matrix.identity", dim, vr) {FunctionClass<T>::setUp("",TYPE_MATRIX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T **ret = (T**)FunctionClass<T>::outConstructor->construct(vr);
for (int i = 0; i < FunctionClass<T>::dim; ++i) for (int j = 0; j < FunctionClass<T>::dim; ++j) ret[i][j] = (i == j ? 1 : 0);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_midentity<T> *r = (Function_midentity<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_midentity<T>));
    return new (r) Function_midentity<T>(*this);
  }
};

//----- * -> A -----
// --- i ---
// xi(b)
template <class T> class Function_xi_b : public FunctionClass<T> {
public:
  Function_xi_b(int dim, VRam *vr) : FunctionClass<T>("xi", dim, vr) {FunctionClass<T>::setUp(TYPE_BOOL_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((bool*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) ? 1 : 0;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_xi_b<T> *r = (Function_xi_b<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_xi_b<T>));
    return new (r) Function_xi_b<T>(*this);
  }
};

// inc(i)
template <class T> class Function_inc_i : public FunctionClass<T> {
public:
  Function_inc_i(int dim, VRam *vr) : FunctionClass<T>("inc", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)) + 1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_inc_i<T> *r = (Function_inc_i<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_inc_i<T>));
    return new (r) Function_inc_i<T>(*this);
  }
};

// pow2(i)
template <class T> class Function_pow2_i : public FunctionClass<T> {
public:
  Function_pow2_i(int dim, VRam *vr) : FunctionClass<T>("pow2", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
int *p = (int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = *p * *p;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pow2_i<T> *r = (Function_pow2_i<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pow2_i<T>));
    return new (r) Function_pow2_i<T>(*this);
  }
};

// round(r)
template <class T> class Function_round_r : public FunctionClass<T> {
public:
  Function_round_r(int dim, VRam *vr) : FunctionClass<T>("round", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = *p + 0.5;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_round_r<T> *r = (Function_round_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_round_r<T>));
    return new (r) Function_round_r<T>(*this);
  }
};

// floor(r)
template <class T> class Function_floor_r : public FunctionClass<T> {
public:
  Function_floor_r(int dim, VRam *vr) : FunctionClass<T>("floor", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = *p;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_floor_r<T> *r = (Function_floor_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_floor_r<T>));
    return new (r) Function_floor_r<T>(*this);
  }
};

// ceiling(r)
template <class T> class Function_ceiling_r : public FunctionClass<T> {
public:
  Function_ceiling_r(int dim, VRam *vr) : FunctionClass<T>("ceiling", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = *p;
if (*ret < *p) *ret += 1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_ceiling_r<T> *r = (Function_ceiling_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_ceiling_r<T>));
    return new (r) Function_ceiling_r<T>(*this);
  }
};

// truncate(r)
template <class T> class Function_truncate_r : public FunctionClass<T> {
public:
  Function_truncate_r(int dim, VRam *vr) : FunctionClass<T>("truncate", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = mabs(*p + 0.5);
if (*p < 0) *ret = - *ret;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_truncate_r<T> *r = (Function_truncate_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_truncate_r<T>));
    return new (r) Function_truncate_r<T>(*this);
  }
};

// factorial(i)
template <class T> class Function_factorial_i : public FunctionClass<T> {
public:
  Function_factorial_i(int dim, VRam *vr) : FunctionClass<T>("factorial", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
int *p = (int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = fact(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_factorial_i<T> *r = (Function_factorial_i<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_factorial_i<T>));
    return new (r) Function_factorial_i<T>(*this);
  }
};

// div(i,i)
template <class T> class Function_div2_ii : public FunctionClass<T> {
public:
  Function_div2_ii(int dim, VRam *vr) : FunctionClass<T>("div", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
int *p1 = (int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
int *p2 = (int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
if (*p2 == 0) {
LOG.debug("Division by 0 (div2_ii)");
throw RuntimeError(DIVISION_BY_0, "Division by 0", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
}
*ret = *p1 / *p2;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_div2_ii<T> *r = (Function_div2_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_div2_ii<T>));
    return new (r) Function_div2_ii<T>(*this);
  }
};

// mod(i,i)
template <class T> class Function_mod2_ii : public FunctionClass<T> {
public:
  Function_mod2_ii(int dim, VRam *vr) : FunctionClass<T>("mod", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
int *p1 = (int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
int *p2 = (int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
if (*p2 == 0) {
LOG.debug("Division by 0 (mod2_ii)");
throw RuntimeError(DIVISION_BY_0, "Division by 0", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
}
*ret = *p1 % *p2;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mod2_ii<T> *r = (Function_mod2_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mod2_ii<T>));
    return new (r) Function_mod2_ii<T>(*this);
  }
};

// abs(i)
template <class T> class Function_abs_i : public FunctionClass<T> {
public:
  Function_abs_i(int dim, VRam *vr) : FunctionClass<T>("abs", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
int *p = (int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = mabs(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_abs_i<T> *r = (Function_abs_i<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_abs_i<T>));
    return new (r) Function_abs_i<T>(*this);
  }
};

// exp(i)
template <class T> class Function_exp_i : public FunctionClass<T> {
public:
  Function_exp_i(int dim, VRam *vr) : FunctionClass<T>("exp", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
int *p = (int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = exp((T)(*p));
return ret;
  }
  FunctionClass<T>* copy() {
    Function_exp_i<T> *r = (Function_exp_i<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_exp_i<T>));
    return new (r) Function_exp_i<T>(*this);
  }
};

// --- r ---
// imag(c)
template <class T> class Function_imag_c : public FunctionClass<T> {
public:
  Function_imag_c(int dim, VRam *vr) : FunctionClass<T>("imag", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = p->imag();
return ret;
  }
  FunctionClass<T>* copy() {
    Function_imag_c<T> *r = (Function_imag_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_imag_c<T>));
    return new (r) Function_imag_c<T>(*this);
  }
};

// real(c)
template <class T> class Function_real_c : public FunctionClass<T> {
public:
  Function_real_c(int dim, VRam *vr) : FunctionClass<T>("real", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = p->real();
return ret;
  }
  FunctionClass<T>* copy() {
    Function_real_c<T> *r = (Function_real_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_real_c<T>));
    return new (r) Function_real_c<T>(*this);
  }
};

// magnitude(v)
template <class T> class Function_mgn_v : public FunctionClass<T> {
public:
  Function_mgn_v(int dim, VRam *vr) : FunctionClass<T>("magnitude", dim, vr) {FunctionClass<T>::setUp(TYPE_VECTOR_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = 0;
for (int i = 0; i < FunctionClass<T>::dim; ++i) *ret += p[i]*p[i];
*ret = sqrt(*ret);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_mgn_v<T> *r = (Function_mgn_v<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_mgn_v<T>));
    return new (r) Function_mgn_v<T>(*this);
  }
};

// sqrmagnitude(v)
template <class T> class Function_sqrmgn_v : public FunctionClass<T> {
public:
  Function_sqrmgn_v(int dim, VRam *vr) : FunctionClass<T>("sqrmagnitude", dim, vr) {FunctionClass<T>::setUp(TYPE_VECTOR_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = 0;
for (int i = 0; i < FunctionClass<T>::dim; ++i) *ret += p[i]*p[i];
return ret;
  }
  FunctionClass<T>* copy() {
    Function_sqrmgn_v<T> *r = (Function_sqrmgn_v<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_sqrmgn_v<T>));
    return new (r) Function_sqrmgn_v<T>(*this);
  }
};

// sin(r)
template <class T> class Function_sin_r : public FunctionClass<T> {
public:
  Function_sin_r(int dim, VRam *vr) : FunctionClass<T>("sin", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = sin(*ret);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_sin_r<T> *r = (Function_sin_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_sin_r<T>));
    return new (r) Function_sin_r<T>(*this);
  }
};

// cos(r)
template <class T> class Function_cos_r : public FunctionClass<T> {
public:
  Function_cos_r(int dim, VRam *vr) : FunctionClass<T>("cos", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = cos(*ret);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_cos_r<T> *r = (Function_cos_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_cos_r<T>));
    return new (r) Function_cos_r<T>(*this);
  }
};

// tan(r)
template <class T> class Function_tan_r : public FunctionClass<T> {
public:
  Function_tan_r(int dim, VRam *vr) : FunctionClass<T>("tan", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = tan(*ret);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_tan_r<T> *r = (Function_tan_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_tan_r<T>));
    return new (r) Function_tan_r<T>(*this);
  }
};

// asin(r)
template <class T> class Function_asin_r : public FunctionClass<T> {
public:
  Function_asin_r(int dim, VRam *vr) : FunctionClass<T>("asin", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = asin(*ret);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_asin_r<T> *r = (Function_asin_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_asin_r<T>));
    return new (r) Function_asin_r<T>(*this);
  }
};

// acos(r)
template <class T> class Function_acos_r : public FunctionClass<T> {
public:
  Function_acos_r(int dim, VRam *vr) : FunctionClass<T>("acos", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = acos(*ret);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_acos_r<T> *r = (Function_acos_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_acos_r<T>));
    return new (r) Function_acos_r<T>(*this);
  }
};

// atan(r)
template <class T> class Function_atan_r : public FunctionClass<T> {
public:
  Function_atan_r(int dim, VRam *vr) : FunctionClass<T>("atan", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = atan(*ret);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_atan_r<T> *r = (Function_atan_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_atan_r<T>));
    return new (r) Function_atan_r<T>(*this);
  }
};

// pow2(r)
template <class T> class Function_pow2_r : public FunctionClass<T> {
public:
  Function_pow2_r(int dim, VRam *vr) : FunctionClass<T>("pow2", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = *ret * *ret;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pow2_r<T> *r = (Function_pow2_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pow2_r<T>));
    return new (r) Function_pow2_r<T>(*this);
  }
};

// sqrt(r)
template <class T> class Function_sqrt_r : public FunctionClass<T> {
public:
  Function_sqrt_r(int dim, VRam *vr) : FunctionClass<T>("sqrt", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = sqrt(*ret);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_sqrt_r<T> *r = (Function_sqrt_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_sqrt_r<T>));
    return new (r) Function_sqrt_r<T>(*this);
  }
};

// log(r)
template <class T> class Function_log_r : public FunctionClass<T> {
public:
  Function_log_r(int dim, VRam *vr) : FunctionClass<T>("ln", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = log(*ret);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_log_r<T> *r = (Function_log_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_log_r<T>));
    return new (r) Function_log_r<T>(*this);
  }
};

// log2(r)
template <class T> class Function_log2_r : public FunctionClass<T> {
public:
  Function_log2_r(int dim, VRam *vr) : FunctionClass<T>("log2", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = log2(*ret);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_log2_r<T> *r = (Function_log2_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_log2_r<T>));
    return new (r) Function_log2_r<T>(*this);
  }
};

// log10(r)
template <class T> class Function_log10_r : public FunctionClass<T> {
public:
  Function_log10_r(int dim, VRam *vr) : FunctionClass<T>("log10", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = log10(*ret);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_log10_r<T> *r = (Function_log10_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_log10_r<T>));
    return new (r) Function_log10_r<T>(*this);
  }
};

// abs(r)
template <class T> class Function_abs_r : public FunctionClass<T> {
public:
  Function_abs_r(int dim, VRam *vr) : FunctionClass<T>("abs", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = mabs(*ret);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_abs_r<T> *r = (Function_abs_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_abs_r<T>));
    return new (r) Function_abs_r<T>(*this);
  }
};

// abs(c)
template <class T> class Function_abs_c : public FunctionClass<T> {
public:
  Function_abs_c(int dim, VRam *vr) : FunctionClass<T>("abs", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = abs(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_abs_c<T> *r = (Function_abs_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_abs_c<T>));
    return new (r) Function_abs_c<T>(*this);
  }
};

// degree(r)
template <class T> class Function_degree_r : public FunctionClass<T> {
public:
  Function_degree_r(int dim, VRam *vr) : FunctionClass<T>("degree", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = *ret * 180 / M_PI;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_degree_r<T> *r = (Function_degree_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_degree_r<T>));
    return new (r) Function_degree_r<T>(*this);
  }
};

// radian(r)
template <class T> class Function_radian_r : public FunctionClass<T> {
public:
  Function_radian_r(int dim, VRam *vr) : FunctionClass<T>("radian", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
*ret = *ret * M_PI / 180;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_radian_r<T> *r = (Function_radian_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_radian_r<T>));
    return new (r) Function_radian_r<T>(*this);
  }
};

// dot(v,v)
template <class T> class Function_dot_vv : public FunctionClass<T> {
public:
  Function_dot_vv(int dim, VRam *vr) : FunctionClass<T>("dot", dim, vr) {FunctionClass<T>::setUp(TYPE_VECTOR_SIGN+","+TYPE_VECTOR_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *v1 = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
T *v2 = (T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
*ret = 0;
for (int i = 0; i < FunctionClass<T>::dim; ++i) *ret += v1[i]*v2[i];
return ret;
  }
  FunctionClass<T>* copy() {
    Function_dot_vv<T> *r = (Function_dot_vv<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_dot_vv<T>));
    return new (r) Function_dot_vv<T>(*this);
  }
};

// arg(c)
template <class T> class Function_arg_c : public FunctionClass<T> {
public:
  Function_arg_c(int dim, VRam *vr) : FunctionClass<T>("arg", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = arg(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_arg_c<T> *r = (Function_arg_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_arg_c<T>));
    return new (r) Function_arg_c<T>(*this);
  }
};

// norm(c)
template <class T> class Function_norm_c : public FunctionClass<T> {
public:
  Function_norm_c(int dim, VRam *vr) : FunctionClass<T>("norm", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = norm(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_norm_c<T> *r = (Function_norm_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_norm_c<T>));
    return new (r) Function_norm_c<T>(*this);
  }
};

// exp(r)
template <class T> class Function_exp_r : public FunctionClass<T> {
public:
  Function_exp_r(int dim, VRam *vr) : FunctionClass<T>("exp", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = exp(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_exp_r<T> *r = (Function_exp_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_exp_r<T>));
    return new (r) Function_exp_r<T>(*this);
  }
};

// sinh(r)
template <class T> class Function_sinh_r : public FunctionClass<T> {
public:
  Function_sinh_r(int dim, VRam *vr) : FunctionClass<T>("sinh", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = sinh(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_sinh_r<T> *r = (Function_sinh_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_sinh_r<T>));
    return new (r) Function_sinh_r<T>(*this);
  }
};

// cosh(t)
template <class T> class Function_cosh_r : public FunctionClass<T> {
public:
  Function_cosh_r(int dim, VRam *vr) : FunctionClass<T>("cosh", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = cosh(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_cosh_r<T> *r = (Function_cosh_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_cosh_r<T>));
    return new (r) Function_cosh_r<T>(*this);
  }
};

// tanh(r)
template <class T> class Function_tanh_r : public FunctionClass<T> {
public:
  Function_tanh_r(int dim, VRam *vr) : FunctionClass<T>("tanh", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = tanh(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_tanh_r<T> *r = (Function_tanh_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_tanh_r<T>));
    return new (r) Function_tanh_r<T>(*this);
  }
};

// asinh(r)
template <class T> class Function_asinh_r : public FunctionClass<T> {
public:
  Function_asinh_r(int dim, VRam *vr) : FunctionClass<T>("asinh", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = asinh(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_asinh_r<T> *r = (Function_asinh_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_asinh_r<T>));
    return new (r) Function_asinh_r<T>(*this);
  }
};

// acosh(r)
template <class T> class Function_acosh_r : public FunctionClass<T> {
public:
  Function_acosh_r(int dim, VRam *vr) : FunctionClass<T>("acosh", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = acosh(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_acosh_r<T> *r = (Function_acosh_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_acosh_r<T>));
    return new (r) Function_acosh_r<T>(*this);
  }
};

// atanh(r)
template <class T> class Function_atanh_r : public FunctionClass<T> {
public:
  Function_atanh_r(int dim, VRam *vr) : FunctionClass<T>("atanh", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *p = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = atanh(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_atanh_r<T> *r = (Function_atanh_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_atanh_r<T>));
    return new (r) Function_atanh_r<T>(*this);
  }
};

// clamp01(r)
template <class T> class Function_clamp01_r : public FunctionClass<T> {
public:
  Function_clamp01_r(int dim, VRam *vr) : FunctionClass<T>("clamp01", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
if (*ret < 0) *ret = 0;
else if (*ret > 1) *ret = 1;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_clamp01_r<T> *r = (Function_clamp01_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_clamp01_r<T>));
    return new (r) Function_clamp01_r<T>(*this);
  }
};

// clamp(r,r,r)
template <class T> class Function_clamp_rrr : public FunctionClass<T> {
public:
  Function_clamp_rrr(int dim, VRam *vr) : FunctionClass<T>("clamp", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread));
T *a = (T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
if (*ret < *a) *ret = *a;
else {
T *b = (T*)FunctionClass<T>::parameters[2]->calc(data,data2,vr,thread);
if (*ret > *b) *ret = *b;
}
return ret;
  }
  FunctionClass<T>* copy() {
    Function_clamp_rrr<T> *r = (Function_clamp_rrr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_clamp_rrr<T>));
    return new (r) Function_clamp_rrr<T>(*this);
  }
};

// root(r,i,i) // Mth root of x with accuracy n
template <class T> class Function_root_rii : public FunctionClass<T> {
public:
  Function_root_rii(int dim, VRam *vr) : FunctionClass<T>("root", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *A = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
int *m = (int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
int *acc = (int*)FunctionClass<T>::parameters[2]->calc(data,data2,vr,thread);
*ret = root(*A,*m,*acc);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_root_rii<T> *r = (Function_root_rii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_root_rii<T>));
    return new (r) Function_root_rii<T>(*this);
  }
};

// lerp(r,r,r)
template <class T> class Function_lerp_rrr : public FunctionClass<T> {
public:
  Function_lerp_rrr(int dim, VRam *vr) : FunctionClass<T>("lerp", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *a = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
T *b = (T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
T *r = (T*)FunctionClass<T>::parameters[2]->calc(data,data2,vr,thread);
*ret = lerp(*a,*b,*r);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lerp_rrr<T> *r = (Function_lerp_rrr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lerp_rrr<T>));
    return new (r) Function_lerp_rrr<T>(*this);
  }
};

// --- c ---
// pow2(c)
template <class T> class Function_pow2_c : public FunctionClass<T> {
public:
  Function_pow2_c(int dim, VRam *vr) : FunctionClass<T>("pow2", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = *p * *p;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_pow2_c<T> *r = (Function_pow2_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_pow2_c<T>));
    return new (r) Function_pow2_c<T>(*this);
  }
};

// conj(c)
template <class T> class Function_conj_c : public FunctionClass<T> {
public:
  Function_conj_c(int dim, VRam *vr) : FunctionClass<T>("conj", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = conj(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_conj_c<T> *r = (Function_conj_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_conj_c<T>));
    return new (r) Function_conj_c<T>(*this);
  }
};

// proj(c)
template <class T> class Function_proj_c : public FunctionClass<T> {
public:
  Function_proj_c(int dim, VRam *vr) : FunctionClass<T>("proj", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = proj(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_proj_c<T> *r = (Function_proj_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_proj_c<T>));
    return new (r) Function_proj_c<T>(*this);
  }
};

// polar(r,r)
template <class T> class Function_polar_rr : public FunctionClass<T> {
public:
  Function_polar_rr(int dim, VRam *vr) : FunctionClass<T>("polar", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
T *a = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
T *b = (T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
*ret = polar(*a,*b);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_polar_rr<T> *r = (Function_polar_rr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_polar_rr<T>));
    return new (r) Function_polar_rr<T>(*this);
  }
};

// exp(c)
template <class T> class Function_exp_c : public FunctionClass<T> {
public:
  Function_exp_c(int dim, VRam *vr) : FunctionClass<T>("exp", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = exp(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_exp_c<T> *r = (Function_exp_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_exp_c<T>));
    return new (r) Function_exp_c<T>(*this);
  }
};

// log(c)
template <class T> class Function_log_c : public FunctionClass<T> {
public:
  Function_log_c(int dim, VRam *vr) : FunctionClass<T>("log", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = log(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_log_c<T> *r = (Function_log_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_log_c<T>));
    return new (r) Function_log_c<T>(*this);
  }
};

// log10(c)
template <class T> class Function_log10_c : public FunctionClass<T> {
public:
  Function_log10_c(int dim, VRam *vr) : FunctionClass<T>("log10", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = log10(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_log10_c<T> *r = (Function_log10_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_log10_c<T>));
    return new (r) Function_log10_c<T>(*this);
  }
};

// sqrt(c)
template <class T> class Function_sqrt_c : public FunctionClass<T> {
public:
  Function_sqrt_c(int dim, VRam *vr) : FunctionClass<T>("sqrt", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = sqrt(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_sqrt_c<T> *r = (Function_sqrt_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_sqrt_c<T>));
    return new (r) Function_sqrt_c<T>(*this);
  }
};

// sin(c)
template <class T> class Function_sin_c : public FunctionClass<T> {
public:
  Function_sin_c(int dim, VRam *vr) : FunctionClass<T>("sin", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = sin(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_sin_c<T> *r = (Function_sin_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_sin_c<T>));
    return new (r) Function_sin_c<T>(*this);
  }
};

// cos(c)
template <class T> class Function_cos_c : public FunctionClass<T> {
public:
  Function_cos_c(int dim, VRam *vr) : FunctionClass<T>("cos", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = cos(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_cos_c<T> *r = (Function_cos_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_cos_c<T>));
    return new (r) Function_cos_c<T>(*this);
  }
};

// tan(c)
template <class T> class Function_tan_c : public FunctionClass<T> {
public:
  Function_tan_c(int dim, VRam *vr) : FunctionClass<T>("tan", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = tan(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_tan_c<T> *r = (Function_tan_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_tan_c<T>));
    return new (r) Function_tan_c<T>(*this);
  }
};

// asin(c)
template <class T> class Function_asin_c : public FunctionClass<T> {
public:
  Function_asin_c(int dim, VRam *vr) : FunctionClass<T>("asin", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = asin(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_asin_c<T> *r = (Function_asin_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_asin_c<T>));
    return new (r) Function_asin_c<T>(*this);
  }
};

// acos(c)
template <class T> class Function_acos_c : public FunctionClass<T> {
public:
  Function_acos_c(int dim, VRam *vr) : FunctionClass<T>("acos", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = acos(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_acos_c<T> *r = (Function_acos_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_acos_c<T>));
    return new (r) Function_acos_c<T>(*this);
  }
};

// atan(c)
template <class T> class Function_atan_c : public FunctionClass<T> {
public:
  Function_atan_c(int dim, VRam *vr) : FunctionClass<T>("atan", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = atan(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_atan_c<T> *r = (Function_atan_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_atan_c<T>));
    return new (r) Function_atan_c<T>(*this);
  }
};

// sinh(c)
template <class T> class Function_sinh_c : public FunctionClass<T> {
public:
  Function_sinh_c(int dim, VRam *vr) : FunctionClass<T>("sinh", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = sinh(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_sinh_c<T> *r = (Function_sinh_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_sinh_c<T>));
    return new (r) Function_sinh_c<T>(*this);
  }
};

// cosh(c)
template <class T> class Function_cosh_c : public FunctionClass<T> {
public:
  Function_cosh_c(int dim, VRam *vr) : FunctionClass<T>("cosh", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = cosh(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_cosh_c<T> *r = (Function_cosh_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_cosh_c<T>));
    return new (r) Function_cosh_c<T>(*this);
  }
};

// tanh(c)
template <class T> class Function_tanh_c : public FunctionClass<T> {
public:
  Function_tanh_c(int dim, VRam *vr) : FunctionClass<T>("tanh", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = tanh(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_tanh_c<T> *r = (Function_tanh_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_tanh_c<T>));
    return new (r) Function_tanh_c<T>(*this);
  }
};

// asinh(c)
template <class T> class Function_asinh_c : public FunctionClass<T> {
public:
  Function_asinh_c(int dim, VRam *vr) : FunctionClass<T>("asinh", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = asinh(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_asinh_c<T> *r = (Function_asinh_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_asinh_c<T>));
    return new (r) Function_asinh_c<T>(*this);
  }
};

// acosh(c)
template <class T> class Function_ascosh_c : public FunctionClass<T> {
public:
  Function_ascosh_c(int dim, VRam *vr) : FunctionClass<T>("acosh", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = acosh(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_ascosh_c<T> *r = (Function_ascosh_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_ascosh_c<T>));
    return new (r) Function_ascosh_c<T>(*this);
  }
};

// atanh(c)
template <class T> class Function_atanh_c : public FunctionClass<T> {
public:
  Function_atanh_c(int dim, VRam *vr) : FunctionClass<T>("atanh", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *p = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
*ret = atanh(*p);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_atanh_c<T> *r = (Function_atanh_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_atanh_c<T>));
    return new (r) Function_atanh_c<T>(*this);
  }
};

// lerp(c,c,r)
template <class T> class Function_lerp_ccr : public FunctionClass<T> {
public:
  Function_lerp_ccr(int dim, VRam *vr) : FunctionClass<T>("lerp", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN+","+TYPE_CPLX_SIGN+","+TYPE_REAL_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
complex<T> *a = (complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
complex<T> *b = (complex<T>*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
T *r = (T*)FunctionClass<T>::parameters[2]->calc(data,data2,vr,thread);
*ret = lerp(*a,*b,*r);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lerp_ccr<T> *r = (Function_lerp_ccr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lerp_ccr<T>));
    return new (r) Function_lerp_ccr<T>(*this);
  }
};

// --- u ---
// lerp(u,u,r)
template <class T> class Function_lerp_u : public FunctionClass<T> {
public:
  Function_lerp_u(int dim, VRam *vr) : FunctionClass<T>("lerp", dim, vr) {FunctionClass<T>::setUp(TYPE_COLOUR_SIGN+","+TYPE_COLOUR_SIGN+","+TYPE_REAL_SIGN,TYPE_COLOUR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
Colour *ret = (Colour*)FunctionClass<T>::outConstructor->construct(vr);
Colour *c1 = (Colour*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
Colour *c2 = (Colour*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
T *r = (T*)FunctionClass<T>::parameters[2]->calc(data,data2,vr,thread);
ret->r = lerp(c1->r,c2->r,*r);
ret->g = lerp(c1->g,c2->g,*r);
ret->b = lerp(c1->b,c2->b,*r);
ret->a = lerp(c1->a,c2->a,*r);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lerp_u<T> *r = (Function_lerp_u<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lerp_u<T>));
    return new (r) Function_lerp_u<T>(*this);
  }
};

// --- v ---
// lerp(v,v,r)
template <class T> class Function_lerp_vvr : public FunctionClass<T> {
public:
  Function_lerp_vvr(int dim, VRam *vr) : FunctionClass<T>("lerp", dim, vr) {FunctionClass<T>::setUp(TYPE_VECTOR_SIGN+","+TYPE_VECTOR_SIGN+","+TYPE_REAL_SIGN,TYPE_VECTOR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
T *a = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
T *b = (T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
T *r = (T*)FunctionClass<T>::parameters[2]->calc(data,data2,vr,thread);
for (int i = 0; i < FunctionClass<T>::dim; ++i) ret[i] = lerp(a[i],b[i],*r);
return ret;
  }
  FunctionClass<T>* copy() {
    Function_lerp_vvr<T> *r = (Function_lerp_vvr<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lerp_vvr<T>));
    return new (r) Function_lerp_vvr<T>(*this);
  }
};



template <class T> class Function_print_r : public FunctionClass<T> {
public:
  Function_print_r(int dim, VRam *vr) : FunctionClass<T>("print", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
cout<<*ret<<endl;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_print_r<T> *r = (Function_print_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_print_r<T>));
    return new (r) Function_print_r<T>(*this);
  }
};

template <class T> class Function_print_i : public FunctionClass<T> {
public:
  Function_print_i(int dim, VRam *vr) : FunctionClass<T>("printi", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
*ret = *((int*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread)));
cout<<*ret<<endl;
return ret;
  }
  FunctionClass<T>* copy() {
    Function_print_i<T> *r = (Function_print_i<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_print_i<T>));
    return new (r) Function_print_i<T>(*this);
  }
};


template <class T>
void addScriptFunctions(vector<FunctionClass<T>*> &functions, VRam *vram, int dim)
{ 
 functions.push_back(new ((Function_and_bb<T>*)vram->allocate(sizeof(Function_and_bb<T>))) Function_and_bb<T>(dim,vram)); 
 functions.push_back(new ((Function_or_bb<T>*)vram->allocate(sizeof(Function_or_bb<T>))) Function_or_bb<T>(dim,vram)); 
 functions.push_back(new ((Function_xor_bb<T>*)vram->allocate(sizeof(Function_xor_bb<T>))) Function_xor_bb<T>(dim,vram)); 
 functions.push_back(new ((Function_implication_bb<T>*)vram->allocate(sizeof(Function_implication_bb<T>))) Function_implication_bb<T>(dim,vram)); 
 functions.push_back(new ((Function_eq_bb<T>*)vram->allocate(sizeof(Function_eq_bb<T>))) Function_eq_bb<T>(dim,vram)); 
 functions.push_back(new ((Function_plus_ii<T>*)vram->allocate(sizeof(Function_plus_ii<T>))) Function_plus_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_minus_ii<T>*)vram->allocate(sizeof(Function_minus_ii<T>))) Function_minus_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_negate_i<T>*)vram->allocate(sizeof(Function_negate_i<T>))) Function_negate_i<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_ii<T>*)vram->allocate(sizeof(Function_mult_ii<T>))) Function_mult_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_div_ii<T>*)vram->allocate(sizeof(Function_div_ii<T>))) Function_div_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_mod_ii<T>*)vram->allocate(sizeof(Function_mod_ii<T>))) Function_mod_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_eq_ii<T>*)vram->allocate(sizeof(Function_eq_ii<T>))) Function_eq_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_lt_ii<T>*)vram->allocate(sizeof(Function_lt_ii<T>))) Function_lt_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_gt_ii<T>*)vram->allocate(sizeof(Function_gt_ii<T>))) Function_gt_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_le_ii<T>*)vram->allocate(sizeof(Function_le_ii<T>))) Function_le_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_ge_ii<T>*)vram->allocate(sizeof(Function_ge_ii<T>))) Function_ge_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_pow_ii<T>*)vram->allocate(sizeof(Function_pow_ii<T>))) Function_pow_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_plus_rr<T>*)vram->allocate(sizeof(Function_plus_rr<T>))) Function_plus_rr<T>(dim,vram)); 
 functions.push_back(new ((Function_minus_rr<T>*)vram->allocate(sizeof(Function_minus_rr<T>))) Function_minus_rr<T>(dim,vram)); 
 functions.push_back(new ((Function_negate_r<T>*)vram->allocate(sizeof(Function_negate_r<T>))) Function_negate_r<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_rr<T>*)vram->allocate(sizeof(Function_mult_rr<T>))) Function_mult_rr<T>(dim,vram)); 
 functions.push_back(new ((Function_div_rr<T>*)vram->allocate(sizeof(Function_div_rr<T>))) Function_div_rr<T>(dim,vram)); 
 functions.push_back(new ((Function_eq_rr<T>*)vram->allocate(sizeof(Function_eq_rr<T>))) Function_eq_rr<T>(dim,vram)); 
 functions.push_back(new ((Function_lt_rr<T>*)vram->allocate(sizeof(Function_lt_rr<T>))) Function_lt_rr<T>(dim,vram)); 
 functions.push_back(new ((Function_gt_rr<T>*)vram->allocate(sizeof(Function_gt_rr<T>))) Function_gt_rr<T>(dim,vram)); 
 functions.push_back(new ((Function_le_rr<T>*)vram->allocate(sizeof(Function_le_rr<T>))) Function_le_rr<T>(dim,vram)); 
 functions.push_back(new ((Function_ge_rr<T>*)vram->allocate(sizeof(Function_ge_rr<T>))) Function_ge_rr<T>(dim,vram)); 
 functions.push_back(new ((Function_pow_rr<T>*)vram->allocate(sizeof(Function_pow_rr<T>))) Function_pow_rr<T>(dim,vram)); 
 functions.push_back(new ((Function_plus_cc<T>*)vram->allocate(sizeof(Function_plus_cc<T>))) Function_plus_cc<T>(dim,vram)); 
 functions.push_back(new ((Function_minus_cc<T>*)vram->allocate(sizeof(Function_minus_cc<T>))) Function_minus_cc<T>(dim,vram)); 
 functions.push_back(new ((Function_negate_c<T>*)vram->allocate(sizeof(Function_negate_c<T>))) Function_negate_c<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_cc<T>*)vram->allocate(sizeof(Function_mult_cc<T>))) Function_mult_cc<T>(dim,vram)); 
 functions.push_back(new ((Function_div_cc<T>*)vram->allocate(sizeof(Function_div_cc<T>))) Function_div_cc<T>(dim,vram)); 
 functions.push_back(new ((Function_eq_cc<T>*)vram->allocate(sizeof(Function_eq_cc<T>))) Function_eq_cc<T>(dim,vram)); 
 functions.push_back(new ((Function_lt_cc<T>*)vram->allocate(sizeof(Function_lt_cc<T>))) Function_lt_cc<T>(dim,vram)); 
 functions.push_back(new ((Function_gt_cc<T>*)vram->allocate(sizeof(Function_gt_cc<T>))) Function_gt_cc<T>(dim,vram)); 
 functions.push_back(new ((Function_le_cc<T>*)vram->allocate(sizeof(Function_le_cc<T>))) Function_le_cc<T>(dim,vram)); 
 functions.push_back(new ((Function_ge_cc<T>*)vram->allocate(sizeof(Function_ge_cc<T>))) Function_ge_cc<T>(dim,vram)); 
 functions.push_back(new ((Function_pow_cc<T>*)vram->allocate(sizeof(Function_pow_cc<T>))) Function_pow_cc<T>(dim,vram)); 
 functions.push_back(new ((Function_plus_uu<T>*)vram->allocate(sizeof(Function_plus_uu<T>))) Function_plus_uu<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_uu<T>*)vram->allocate(sizeof(Function_mult_uu<T>))) Function_mult_uu<T>(dim,vram)); 
 functions.push_back(new ((Function_div_uu<T>*)vram->allocate(sizeof(Function_div_uu<T>))) Function_div_uu<T>(dim,vram)); 
 functions.push_back(new ((Function_plus_vv<T>*)vram->allocate(sizeof(Function_plus_vv<T>))) Function_plus_vv<T>(dim,vram)); 
 functions.push_back(new ((Function_minus_vv<T>*)vram->allocate(sizeof(Function_minus_vv<T>))) Function_minus_vv<T>(dim,vram)); 
 functions.push_back(new ((Function_lt_vv<T>*)vram->allocate(sizeof(Function_lt_vv<T>))) Function_lt_vv<T>(dim,vram)); 
 functions.push_back(new ((Function_le_vv<T>*)vram->allocate(sizeof(Function_le_vv<T>))) Function_le_vv<T>(dim,vram)); 
 functions.push_back(new ((Function_gt_vv<T>*)vram->allocate(sizeof(Function_gt_vv<T>))) Function_gt_vv<T>(dim,vram)); 
 functions.push_back(new ((Function_ge_vv<T>*)vram->allocate(sizeof(Function_ge_vv<T>))) Function_ge_vv<T>(dim,vram)); 
 functions.push_back(new ((Function_eq_vv<T>*)vram->allocate(sizeof(Function_eq_vv<T>))) Function_eq_vv<T>(dim,vram)); 
 functions.push_back(new ((Function_plus_mm<T>*)vram->allocate(sizeof(Function_plus_mm<T>))) Function_plus_mm<T>(dim,vram)); 
 functions.push_back(new ((Function_minus_mm<T>*)vram->allocate(sizeof(Function_minus_mm<T>))) Function_minus_mm<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_mm<T>*)vram->allocate(sizeof(Function_mult_mm<T>))) Function_mult_mm<T>(dim,vram)); 
 functions.push_back(new ((Function_plus_ir<T>*)vram->allocate(sizeof(Function_plus_ir<T>))) Function_plus_ir<T>(dim,vram)); 
 functions.push_back(new ((Function_plus_ic<T>*)vram->allocate(sizeof(Function_plus_ic<T>))) Function_plus_ic<T>(dim,vram)); 
 functions.push_back(new ((Function_plus_ri<T>*)vram->allocate(sizeof(Function_plus_ri<T>))) Function_plus_ri<T>(dim,vram)); 
 functions.push_back(new ((Function_plus_rc<T>*)vram->allocate(sizeof(Function_plus_rc<T>))) Function_plus_rc<T>(dim,vram)); 
 functions.push_back(new ((Function_plus_ci<T>*)vram->allocate(sizeof(Function_plus_ci<T>))) Function_plus_ci<T>(dim,vram)); 
 functions.push_back(new ((Function_plus_cr<T>*)vram->allocate(sizeof(Function_plus_cr<T>))) Function_plus_cr<T>(dim,vram)); 
 functions.push_back(new ((Function_minus_ir<T>*)vram->allocate(sizeof(Function_minus_ir<T>))) Function_minus_ir<T>(dim,vram)); 
 functions.push_back(new ((Function_minus_ic<T>*)vram->allocate(sizeof(Function_minus_ic<T>))) Function_minus_ic<T>(dim,vram)); 
 functions.push_back(new ((Function_minus_ri<T>*)vram->allocate(sizeof(Function_minus_ri<T>))) Function_minus_ri<T>(dim,vram)); 
 functions.push_back(new ((Function_minus_rc<T>*)vram->allocate(sizeof(Function_minus_rc<T>))) Function_minus_rc<T>(dim,vram)); 
 functions.push_back(new ((Function_minus_ci<T>*)vram->allocate(sizeof(Function_minus_ci<T>))) Function_minus_ci<T>(dim,vram)); 
 functions.push_back(new ((Function_minus_cr<T>*)vram->allocate(sizeof(Function_minus_cr<T>))) Function_minus_cr<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_ir<T>*)vram->allocate(sizeof(Function_mult_ir<T>))) Function_mult_ir<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_ic<T>*)vram->allocate(sizeof(Function_mult_ic<T>))) Function_mult_ic<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_ri<T>*)vram->allocate(sizeof(Function_mult_ri<T>))) Function_mult_ri<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_rc<T>*)vram->allocate(sizeof(Function_mult_rc<T>))) Function_mult_rc<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_ci<T>*)vram->allocate(sizeof(Function_mult_ci<T>))) Function_mult_ci<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_cr<T>*)vram->allocate(sizeof(Function_mult_cr<T>))) Function_mult_cr<T>(dim,vram)); 
 functions.push_back(new ((Function_div_ir<T>*)vram->allocate(sizeof(Function_div_ir<T>))) Function_div_ir<T>(dim,vram)); 
 functions.push_back(new ((Function_div_ic<T>*)vram->allocate(sizeof(Function_div_ic<T>))) Function_div_ic<T>(dim,vram)); 
 functions.push_back(new ((Function_div_ri<T>*)vram->allocate(sizeof(Function_div_ri<T>))) Function_div_ri<T>(dim,vram)); 
 functions.push_back(new ((Function_div_rc<T>*)vram->allocate(sizeof(Function_div_rc<T>))) Function_div_rc<T>(dim,vram)); 
 functions.push_back(new ((Function_div_ci<T>*)vram->allocate(sizeof(Function_div_ci<T>))) Function_div_ci<T>(dim,vram)); 
 functions.push_back(new ((Function_div_cr<T>*)vram->allocate(sizeof(Function_div_cr<T>))) Function_div_cr<T>(dim,vram)); 
 functions.push_back(new ((Function_pow_ir<T>*)vram->allocate(sizeof(Function_pow_ir<T>))) Function_pow_ir<T>(dim,vram)); 
 functions.push_back(new ((Function_pow_ic<T>*)vram->allocate(sizeof(Function_pow_ic<T>))) Function_pow_ic<T>(dim,vram)); 
 functions.push_back(new ((Function_pow_ri<T>*)vram->allocate(sizeof(Function_pow_ri<T>))) Function_pow_ri<T>(dim,vram)); 
 functions.push_back(new ((Function_pow_rc<T>*)vram->allocate(sizeof(Function_pow_rc<T>))) Function_pow_rc<T>(dim,vram)); 
 functions.push_back(new ((Function_pow_ci<T>*)vram->allocate(sizeof(Function_pow_ci<T>))) Function_pow_ci<T>(dim,vram)); 
 functions.push_back(new ((Function_pow_cr<T>*)vram->allocate(sizeof(Function_pow_cr<T>))) Function_pow_cr<T>(dim,vram)); 
 functions.push_back(new ((Function_lt_ir<T>*)vram->allocate(sizeof(Function_lt_ir<T>))) Function_lt_ir<T>(dim,vram)); 
 functions.push_back(new ((Function_lt_ic<T>*)vram->allocate(sizeof(Function_lt_ic<T>))) Function_lt_ic<T>(dim,vram)); 
 functions.push_back(new ((Function_lt_ri<T>*)vram->allocate(sizeof(Function_lt_ri<T>))) Function_lt_ri<T>(dim,vram)); 
 functions.push_back(new ((Function_lt_rc<T>*)vram->allocate(sizeof(Function_lt_rc<T>))) Function_lt_rc<T>(dim,vram)); 
 functions.push_back(new ((Function_lt_ci<T>*)vram->allocate(sizeof(Function_lt_ci<T>))) Function_lt_ci<T>(dim,vram)); 
 functions.push_back(new ((Function_lt_cr<T>*)vram->allocate(sizeof(Function_lt_cr<T>))) Function_lt_cr<T>(dim,vram)); 
 functions.push_back(new ((Function_le_ir<T>*)vram->allocate(sizeof(Function_le_ir<T>))) Function_le_ir<T>(dim,vram)); 
 functions.push_back(new ((Function_le_ic<T>*)vram->allocate(sizeof(Function_le_ic<T>))) Function_le_ic<T>(dim,vram)); 
 functions.push_back(new ((Function_le_ri<T>*)vram->allocate(sizeof(Function_le_ri<T>))) Function_le_ri<T>(dim,vram)); 
 functions.push_back(new ((Function_le_rc<T>*)vram->allocate(sizeof(Function_le_rc<T>))) Function_le_rc<T>(dim,vram)); 
 functions.push_back(new ((Function_le_ci<T>*)vram->allocate(sizeof(Function_le_ci<T>))) Function_le_ci<T>(dim,vram)); 
 functions.push_back(new ((Function_le_cr<T>*)vram->allocate(sizeof(Function_le_cr<T>))) Function_le_cr<T>(dim,vram)); 
 functions.push_back(new ((Function_gt_ir<T>*)vram->allocate(sizeof(Function_gt_ir<T>))) Function_gt_ir<T>(dim,vram)); 
 functions.push_back(new ((Function_gt_ic<T>*)vram->allocate(sizeof(Function_gt_ic<T>))) Function_gt_ic<T>(dim,vram)); 
 functions.push_back(new ((Function_gt_ri<T>*)vram->allocate(sizeof(Function_gt_ri<T>))) Function_gt_ri<T>(dim,vram)); 
 functions.push_back(new ((Function_gt_rc<T>*)vram->allocate(sizeof(Function_gt_rc<T>))) Function_gt_rc<T>(dim,vram)); 
 functions.push_back(new ((Function_gt_ci<T>*)vram->allocate(sizeof(Function_gt_ci<T>))) Function_gt_ci<T>(dim,vram)); 
 functions.push_back(new ((Function_gt_cr<T>*)vram->allocate(sizeof(Function_gt_cr<T>))) Function_gt_cr<T>(dim,vram)); 
 functions.push_back(new ((Function_ge_ir<T>*)vram->allocate(sizeof(Function_ge_ir<T>))) Function_ge_ir<T>(dim,vram)); 
 functions.push_back(new ((Function_ge_ic<T>*)vram->allocate(sizeof(Function_ge_ic<T>))) Function_ge_ic<T>(dim,vram)); 
 functions.push_back(new ((Function_ge_ri<T>*)vram->allocate(sizeof(Function_ge_ri<T>))) Function_ge_ri<T>(dim,vram)); 
 functions.push_back(new ((Function_ge_rc<T>*)vram->allocate(sizeof(Function_ge_rc<T>))) Function_ge_rc<T>(dim,vram)); 
 functions.push_back(new ((Function_ge_ci<T>*)vram->allocate(sizeof(Function_ge_ci<T>))) Function_ge_ci<T>(dim,vram)); 
 functions.push_back(new ((Function_ge_cr<T>*)vram->allocate(sizeof(Function_ge_cr<T>))) Function_ge_cr<T>(dim,vram)); 
 functions.push_back(new ((Function_eq_ir<T>*)vram->allocate(sizeof(Function_eq_ir<T>))) Function_eq_ir<T>(dim,vram)); 
 functions.push_back(new ((Function_eq_ic<T>*)vram->allocate(sizeof(Function_eq_ic<T>))) Function_eq_ic<T>(dim,vram)); 
 functions.push_back(new ((Function_eq_ri<T>*)vram->allocate(sizeof(Function_eq_ri<T>))) Function_eq_ri<T>(dim,vram)); 
 functions.push_back(new ((Function_eq_rc<T>*)vram->allocate(sizeof(Function_eq_rc<T>))) Function_eq_rc<T>(dim,vram)); 
 functions.push_back(new ((Function_eq_ci<T>*)vram->allocate(sizeof(Function_eq_ci<T>))) Function_eq_ci<T>(dim,vram)); 
 functions.push_back(new ((Function_eq_cr<T>*)vram->allocate(sizeof(Function_eq_cr<T>))) Function_eq_cr<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_iv<T>*)vram->allocate(sizeof(Function_mult_iv<T>))) Function_mult_iv<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_rv<T>*)vram->allocate(sizeof(Function_mult_rv<T>))) Function_mult_rv<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_im<T>*)vram->allocate(sizeof(Function_mult_im<T>))) Function_mult_im<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_rm<T>*)vram->allocate(sizeof(Function_mult_rm<T>))) Function_mult_rm<T>(dim,vram)); 
 functions.push_back(new ((Function_mult_mv<T>*)vram->allocate(sizeof(Function_mult_mv<T>))) Function_mult_mv<T>(dim,vram)); 
 functions.push_back(new ((Function_constr_i<T>*)vram->allocate(sizeof(Function_constr_i<T>))) Function_constr_i<T>(dim,vram)); 
 functions.push_back(new ((Function_pi<T>*)vram->allocate(sizeof(Function_pi<T>))) Function_pi<T>(dim,vram)); 
 functions.push_back(new ((Function_e<T>*)vram->allocate(sizeof(Function_e<T>))) Function_e<T>(dim,vram)); 
 functions.push_back(new ((Function_constc_i<T>*)vram->allocate(sizeof(Function_constc_i<T>))) Function_constc_i<T>(dim,vram)); 
 functions.push_back(new ((Function_constc_r<T>*)vram->allocate(sizeof(Function_constc_r<T>))) Function_constc_r<T>(dim,vram)); 
 functions.push_back(new ((Function_constc_ri<T>*)vram->allocate(sizeof(Function_constc_ri<T>))) Function_constc_ri<T>(dim,vram)); 
 functions.push_back(new ((Function_constc_ir<T>*)vram->allocate(sizeof(Function_constc_ir<T>))) Function_constc_ir<T>(dim,vram)); 
 functions.push_back(new ((Function_constc_rr<T>*)vram->allocate(sizeof(Function_constc_rr<T>))) Function_constc_rr<T>(dim,vram)); 
 functions.push_back(new ((Function_constcol_rrrr<T>*)vram->allocate(sizeof(Function_constcol_rrrr<T>))) Function_constcol_rrrr<T>(dim,vram)); 
 functions.push_back(new ((Function_red<T>*)vram->allocate(sizeof(Function_red<T>))) Function_red<T>(dim,vram)); 
 functions.push_back(new ((Function_green<T>*)vram->allocate(sizeof(Function_green<T>))) Function_green<T>(dim,vram)); 
 functions.push_back(new ((Function_blue<T>*)vram->allocate(sizeof(Function_blue<T>))) Function_blue<T>(dim,vram)); 
 functions.push_back(new ((Function_white<T>*)vram->allocate(sizeof(Function_white<T>))) Function_white<T>(dim,vram)); 
 functions.push_back(new ((Function_black<T>*)vram->allocate(sizeof(Function_black<T>))) Function_black<T>(dim,vram)); 
 functions.push_back(new ((Function_none<T>*)vram->allocate(sizeof(Function_none<T>))) Function_none<T>(dim,vram)); 
 functions.push_back(new ((Function_vzero<T>*)vram->allocate(sizeof(Function_vzero<T>))) Function_vzero<T>(dim,vram)); 
 functions.push_back(new ((Function_vx<T>*)vram->allocate(sizeof(Function_vx<T>))) Function_vx<T>(dim,vram)); 
 functions.push_back(new ((Function_vy<T>*)vram->allocate(sizeof(Function_vy<T>))) Function_vy<T>(dim,vram)); 
 functions.push_back(new ((Function_vz<T>*)vram->allocate(sizeof(Function_vz<T>))) Function_vz<T>(dim,vram)); 
 functions.push_back(new ((Function_mzero<T>*)vram->allocate(sizeof(Function_mzero<T>))) Function_mzero<T>(dim,vram)); 
 functions.push_back(new ((Function_midentity<T>*)vram->allocate(sizeof(Function_midentity<T>))) Function_midentity<T>(dim,vram)); 
 functions.push_back(new ((Function_xi_b<T>*)vram->allocate(sizeof(Function_xi_b<T>))) Function_xi_b<T>(dim,vram)); 
 functions.push_back(new ((Function_inc_i<T>*)vram->allocate(sizeof(Function_inc_i<T>))) Function_inc_i<T>(dim,vram)); 
 functions.push_back(new ((Function_pow2_i<T>*)vram->allocate(sizeof(Function_pow2_i<T>))) Function_pow2_i<T>(dim,vram)); 
 functions.push_back(new ((Function_round_r<T>*)vram->allocate(sizeof(Function_round_r<T>))) Function_round_r<T>(dim,vram)); 
 functions.push_back(new ((Function_floor_r<T>*)vram->allocate(sizeof(Function_floor_r<T>))) Function_floor_r<T>(dim,vram)); 
 functions.push_back(new ((Function_ceiling_r<T>*)vram->allocate(sizeof(Function_ceiling_r<T>))) Function_ceiling_r<T>(dim,vram)); 
 functions.push_back(new ((Function_truncate_r<T>*)vram->allocate(sizeof(Function_truncate_r<T>))) Function_truncate_r<T>(dim,vram)); 
 functions.push_back(new ((Function_factorial_i<T>*)vram->allocate(sizeof(Function_factorial_i<T>))) Function_factorial_i<T>(dim,vram)); 
 functions.push_back(new ((Function_div2_ii<T>*)vram->allocate(sizeof(Function_div2_ii<T>))) Function_div2_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_mod2_ii<T>*)vram->allocate(sizeof(Function_mod2_ii<T>))) Function_mod2_ii<T>(dim,vram)); 
 functions.push_back(new ((Function_abs_i<T>*)vram->allocate(sizeof(Function_abs_i<T>))) Function_abs_i<T>(dim,vram)); 
 functions.push_back(new ((Function_exp_i<T>*)vram->allocate(sizeof(Function_exp_i<T>))) Function_exp_i<T>(dim,vram)); 
 functions.push_back(new ((Function_imag_c<T>*)vram->allocate(sizeof(Function_imag_c<T>))) Function_imag_c<T>(dim,vram)); 
 functions.push_back(new ((Function_real_c<T>*)vram->allocate(sizeof(Function_real_c<T>))) Function_real_c<T>(dim,vram)); 
 functions.push_back(new ((Function_mgn_v<T>*)vram->allocate(sizeof(Function_mgn_v<T>))) Function_mgn_v<T>(dim,vram)); 
 functions.push_back(new ((Function_sqrmgn_v<T>*)vram->allocate(sizeof(Function_sqrmgn_v<T>))) Function_sqrmgn_v<T>(dim,vram)); 
 functions.push_back(new ((Function_sin_r<T>*)vram->allocate(sizeof(Function_sin_r<T>))) Function_sin_r<T>(dim,vram)); 
 functions.push_back(new ((Function_cos_r<T>*)vram->allocate(sizeof(Function_cos_r<T>))) Function_cos_r<T>(dim,vram)); 
 functions.push_back(new ((Function_tan_r<T>*)vram->allocate(sizeof(Function_tan_r<T>))) Function_tan_r<T>(dim,vram)); 
 functions.push_back(new ((Function_asin_r<T>*)vram->allocate(sizeof(Function_asin_r<T>))) Function_asin_r<T>(dim,vram)); 
 functions.push_back(new ((Function_acos_r<T>*)vram->allocate(sizeof(Function_acos_r<T>))) Function_acos_r<T>(dim,vram)); 
 functions.push_back(new ((Function_atan_r<T>*)vram->allocate(sizeof(Function_atan_r<T>))) Function_atan_r<T>(dim,vram)); 
 functions.push_back(new ((Function_pow2_r<T>*)vram->allocate(sizeof(Function_pow2_r<T>))) Function_pow2_r<T>(dim,vram)); 
 functions.push_back(new ((Function_sqrt_r<T>*)vram->allocate(sizeof(Function_sqrt_r<T>))) Function_sqrt_r<T>(dim,vram)); 
 functions.push_back(new ((Function_log_r<T>*)vram->allocate(sizeof(Function_log_r<T>))) Function_log_r<T>(dim,vram)); 
 functions.push_back(new ((Function_log2_r<T>*)vram->allocate(sizeof(Function_log2_r<T>))) Function_log2_r<T>(dim,vram)); 
 functions.push_back(new ((Function_log10_r<T>*)vram->allocate(sizeof(Function_log10_r<T>))) Function_log10_r<T>(dim,vram)); 
 functions.push_back(new ((Function_abs_r<T>*)vram->allocate(sizeof(Function_abs_r<T>))) Function_abs_r<T>(dim,vram)); 
 functions.push_back(new ((Function_abs_c<T>*)vram->allocate(sizeof(Function_abs_c<T>))) Function_abs_c<T>(dim,vram)); 
 functions.push_back(new ((Function_degree_r<T>*)vram->allocate(sizeof(Function_degree_r<T>))) Function_degree_r<T>(dim,vram)); 
 functions.push_back(new ((Function_radian_r<T>*)vram->allocate(sizeof(Function_radian_r<T>))) Function_radian_r<T>(dim,vram)); 
 functions.push_back(new ((Function_dot_vv<T>*)vram->allocate(sizeof(Function_dot_vv<T>))) Function_dot_vv<T>(dim,vram)); 
 functions.push_back(new ((Function_arg_c<T>*)vram->allocate(sizeof(Function_arg_c<T>))) Function_arg_c<T>(dim,vram)); 
 functions.push_back(new ((Function_norm_c<T>*)vram->allocate(sizeof(Function_norm_c<T>))) Function_norm_c<T>(dim,vram)); 
 functions.push_back(new ((Function_exp_r<T>*)vram->allocate(sizeof(Function_exp_r<T>))) Function_exp_r<T>(dim,vram)); 
 functions.push_back(new ((Function_sinh_r<T>*)vram->allocate(sizeof(Function_sinh_r<T>))) Function_sinh_r<T>(dim,vram)); 
 functions.push_back(new ((Function_cosh_r<T>*)vram->allocate(sizeof(Function_cosh_r<T>))) Function_cosh_r<T>(dim,vram)); 
 functions.push_back(new ((Function_tanh_r<T>*)vram->allocate(sizeof(Function_tanh_r<T>))) Function_tanh_r<T>(dim,vram)); 
 functions.push_back(new ((Function_asinh_r<T>*)vram->allocate(sizeof(Function_asinh_r<T>))) Function_asinh_r<T>(dim,vram)); 
 functions.push_back(new ((Function_acosh_r<T>*)vram->allocate(sizeof(Function_acosh_r<T>))) Function_acosh_r<T>(dim,vram)); 
 functions.push_back(new ((Function_atanh_r<T>*)vram->allocate(sizeof(Function_atanh_r<T>))) Function_atanh_r<T>(dim,vram)); 
 functions.push_back(new ((Function_clamp01_r<T>*)vram->allocate(sizeof(Function_clamp01_r<T>))) Function_clamp01_r<T>(dim,vram)); 
 functions.push_back(new ((Function_clamp_rrr<T>*)vram->allocate(sizeof(Function_clamp_rrr<T>))) Function_clamp_rrr<T>(dim,vram)); 
 functions.push_back(new ((Function_root_rii<T>*)vram->allocate(sizeof(Function_root_rii<T>))) Function_root_rii<T>(dim,vram)); 
 functions.push_back(new ((Function_lerp_rrr<T>*)vram->allocate(sizeof(Function_lerp_rrr<T>))) Function_lerp_rrr<T>(dim,vram)); 
 functions.push_back(new ((Function_pow2_c<T>*)vram->allocate(sizeof(Function_pow2_c<T>))) Function_pow2_c<T>(dim,vram)); 
 functions.push_back(new ((Function_conj_c<T>*)vram->allocate(sizeof(Function_conj_c<T>))) Function_conj_c<T>(dim,vram)); 
 functions.push_back(new ((Function_proj_c<T>*)vram->allocate(sizeof(Function_proj_c<T>))) Function_proj_c<T>(dim,vram)); 
 functions.push_back(new ((Function_polar_rr<T>*)vram->allocate(sizeof(Function_polar_rr<T>))) Function_polar_rr<T>(dim,vram)); 
 functions.push_back(new ((Function_exp_c<T>*)vram->allocate(sizeof(Function_exp_c<T>))) Function_exp_c<T>(dim,vram)); 
 functions.push_back(new ((Function_log_c<T>*)vram->allocate(sizeof(Function_log_c<T>))) Function_log_c<T>(dim,vram)); 
 functions.push_back(new ((Function_log10_c<T>*)vram->allocate(sizeof(Function_log10_c<T>))) Function_log10_c<T>(dim,vram)); 
 functions.push_back(new ((Function_sqrt_c<T>*)vram->allocate(sizeof(Function_sqrt_c<T>))) Function_sqrt_c<T>(dim,vram)); 
 functions.push_back(new ((Function_sin_c<T>*)vram->allocate(sizeof(Function_sin_c<T>))) Function_sin_c<T>(dim,vram)); 
 functions.push_back(new ((Function_cos_c<T>*)vram->allocate(sizeof(Function_cos_c<T>))) Function_cos_c<T>(dim,vram)); 
 functions.push_back(new ((Function_tan_c<T>*)vram->allocate(sizeof(Function_tan_c<T>))) Function_tan_c<T>(dim,vram)); 
 functions.push_back(new ((Function_asin_c<T>*)vram->allocate(sizeof(Function_asin_c<T>))) Function_asin_c<T>(dim,vram)); 
 functions.push_back(new ((Function_acos_c<T>*)vram->allocate(sizeof(Function_acos_c<T>))) Function_acos_c<T>(dim,vram)); 
 functions.push_back(new ((Function_atan_c<T>*)vram->allocate(sizeof(Function_atan_c<T>))) Function_atan_c<T>(dim,vram)); 
 functions.push_back(new ((Function_sinh_c<T>*)vram->allocate(sizeof(Function_sinh_c<T>))) Function_sinh_c<T>(dim,vram)); 
 functions.push_back(new ((Function_cosh_c<T>*)vram->allocate(sizeof(Function_cosh_c<T>))) Function_cosh_c<T>(dim,vram)); 
 functions.push_back(new ((Function_tanh_c<T>*)vram->allocate(sizeof(Function_tanh_c<T>))) Function_tanh_c<T>(dim,vram)); 
 functions.push_back(new ((Function_asinh_c<T>*)vram->allocate(sizeof(Function_asinh_c<T>))) Function_asinh_c<T>(dim,vram)); 
 functions.push_back(new ((Function_ascosh_c<T>*)vram->allocate(sizeof(Function_ascosh_c<T>))) Function_ascosh_c<T>(dim,vram)); 
 functions.push_back(new ((Function_atanh_c<T>*)vram->allocate(sizeof(Function_atanh_c<T>))) Function_atanh_c<T>(dim,vram)); 
 functions.push_back(new ((Function_lerp_ccr<T>*)vram->allocate(sizeof(Function_lerp_ccr<T>))) Function_lerp_ccr<T>(dim,vram)); 
 functions.push_back(new ((Function_lerp_u<T>*)vram->allocate(sizeof(Function_lerp_u<T>))) Function_lerp_u<T>(dim,vram)); 
 functions.push_back(new ((Function_lerp_vvr<T>*)vram->allocate(sizeof(Function_lerp_vvr<T>))) Function_lerp_vvr<T>(dim,vram)); 
 functions.push_back(new ((Function_print_r<T>*)vram->allocate(sizeof(Function_print_r<T>))) Function_print_r<T>(dim,vram)); 
 functions.push_back(new ((Function_print_i<T>*)vram->allocate(sizeof(Function_print_i<T>))) Function_print_i<T>(dim,vram));
}
