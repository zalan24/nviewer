#include "log.h"

typedef float RTYPE;

#define Tutorial_VERSION_MAJOR 
#define Tutorial_VERSION_MINOR 
#define USE_CUDA false
#define DEBUG_ENABLED true
#define NAME_SIGNATURE "nviewer (vegvarizalan@gmail.com)"

#define DOUBLESPINBOX_DECIMALS 10
#define DOUBLESPINBOX_STEP 0.01
