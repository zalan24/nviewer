#pragma once

#include <iostream>
#include <math.h>
#include <thread>
#include <vector>
#include <mutex>

#include "function.h"
#include "image.h"
#include "optimized_matrix.h"
#include "config.h"
#include "process.h"
#include "info.h"
#include "transformable.h"
#include "common/Helper/inotifiable.h"
#include "common/Math/base.h"

#if USE_CUDA
#include "cuda_process.h"
#include <cuda.h>
#include <cuda_runtime.h>
#endif

#define DEFAULT_DEPTH 16
#define DEFAULT_DENSITY 16
#define DEFAULT_NUMD 24
//60 Degree
#define DEFAULT_VIEW 1.047198
#define DEFAULT_FOCUS 1
#define DEFAULT_NEAR_CLIP 0.1
#define DEFAULT_FAR_CLIP 10
#define DEFAULT_ZOOMSPEED 0.5
#define DEFAULT_BG Colour(0,0,0,1)
#define DEFAULT_SPREAD 1
#define DEFAULT_SPREAD_QUALITY 0


using namespace std;

class NothingToRender: public exception
{
  virtual const char* what() const throw()
  {
    return "Trying to render without calculation";
  }
};

struct CalcThreadInfo
{
  int n,a,b;
  bool *leave;
  int id;
  CalcThreadInfo(int n, int a, int b, bool *leave, int id) : n(n), a(a), b(b), leave(leave), id(id) {}
};

template <class T>
class Camera : public Transformable<T>
{
    ERROR_CODE error;
    Function<T> *f;
    int density;
    int depth;
    int numD;
    Colour **tex;
    void clear();
    void setM();
    VectorN<T> getPoint(int, int,int,int,T);
    pair<pair<T,T>, T> getPoint3D(int, int,int,int,T);
    T view;
    Matrix<T> transform;
    T focus;
    T nearClip;
    T farClip;
    int lastCalc;
    vector<thread*> threads;
    bool** getPrevious(bool***, int);
    void calcLayer(T*, int, int, int, int, float***, bool***, int, int, int, int, int, bool*, bool*, double*, VRam**, INotifiable*);
    void updateLayer(float**, bool**, int, int, int, int, int, int, bool*, bool*);
    void updateLayerSet(float**, bool**, int, int, int, int, int, bool*, bool*);
    void updateLayerCols(float**, bool**, int, int, int, int, int, bool*, bool*);
    void updateLayerRows(float**, bool**, int, int, int, int, int, bool*, bool*);
    bool isSolid(float**, int, int);
    void applyLayer(float***, bool***, int, int, int, int, int, int, int, bool*, bool*);
    void applyPart(float***, bool***, bool**, int, int, int, int, int, int, bool*, bool*);
    void calcPart(T*, int, int, int, float**, bool**, bool**, int, int, int, int, bool*, bool*, double*, int, int, int, VRam*, INotifiable*);
    void lightHelper(float***, bool***, int, int, int, int, int, int, int&, int&);
    void makeLight(VRam**, LightData<T>***, T*, int, int*, float***, bool***, int, int, int, int, int, int, int, int, bool*, bool*);
    void makeLightPart(VRam*, int, LightData<T>**, T*, int, int*, float***, bool***, int, int, int, int, int, int, int, int, int, bool*, bool*);
    void calcLight(VRam*, int, LightData<T>**, T*, int&, float***, bool***, bool*, bool*);
    T scale;
    T zoomSpeed;
    Colour bg;
    int maxArraySize;
    Config *cfg;
    T spread;
    int spreadQuality;
    ulli vrammax;
public:
    void getCurrentStyle(StyleSheet *ss) const;
    void getCurrentParam(StyleSheet *ss) const;
    void setStyleSheet(StyleSheet *ss) {f->setStyle(ss);}
    void setStyleSheetP(StyleSheet *ss) {f->set(ss);}
    T getView() const {return view;}
    VectorN<T> getPosition() const;
    void setView(T v) {view = v;}
    void setVRamNotifiers(INotifiable *script, INotifiable *runtime);
    LightDirection<T> *getLight() {return f->getLight();}
    T getAmbientLight() const {return f->getAmbientLight();}
    Camera(Function<T>*,MachineConfig*);
    void setFunction(Function<T>*);
    ~Camera();
    void render(Texture&,bool*, bool*,double,double);
    void calc(int,int,bool*, bool*,double*,double,double,INotifiable*);
    void setD(int,int,int);
    void setSpread(int);
    int getSpread();
    void resetTransform();
    Function<T> *getFunction() const;
    Matrix<T> getTransform() const;
    //void setTransform(Matrix<T>&);
    void setTransform(Matrix<T>);
    void setFocus(Real);
    T getFocus() const;
    void translateV(VectorN<T>&);
    void translate(VectorN<T>);
    void setPositionV(VectorN<T>&);
    void setPosition(VectorN<T>);
    void setScale(T);
    T getScale() const;
    void setClipping(T,T);
    T getNearClip() const;
    T getFarClip() const;
    void setNearClip(T);
    void setFarClip(T);
    int getNumD() const;
    void setZoomSpeed(T);
    T getZoomSpeed() const;
    void moveForward(float);
    void moveBackward(float);
    void increaseFocus(float);
    void decreaseFocus(float);
    void rotate(double,int,int, bool local = true);
    void scaleIn(float);
    void scaleOut(float);
    void setBg(Colour);
    Colour getBg() const;
    void setMaxArraySize(int);
    int getMaxArraySize() const;
    void setSpread(T);
    T getSpread() const;
    void setSpreadQuality(int);
    T getSpreadQuality() const;
    void moveOnAxis(int,float);
    CameraElement *capture(string) const;
    void loadComposition(CameraElement*);
    void pauseThreads();
    VectorN<RTYPE> getDirection() const;
    void setDirection(const VectorN<RTYPE> &dir);
};

template <class T> Camera<T>::Camera(Function<T> *f, MachineConfig* cfg) :
    f(f),
    cfg(cfg),
    transform(f->getDim()+1, f->getDim()+1),
    error(ERROR_CODE::NONE)
{
  int max_num_array;
  stringstream ss;
  ss<<DEFAULT_MAX_ARRAY_SIZE;
  max_num_array = readInt(cfg->getProcess()->getAttribute("max_array_size", ss.str()));
  setMaxArraySize(max_num_array);
  numD = DEFAULT_NUMD;
  depth = DEFAULT_DEPTH;
  density = DEFAULT_DENSITY;
  view = DEFAULT_VIEW;
  focus = DEFAULT_FOCUS;
  nearClip = DEFAULT_NEAR_CLIP;
  farClip = DEFAULT_FAR_CLIP;
  zoomSpeed = DEFAULT_ZOOMSPEED;
  spread = DEFAULT_SPREAD;
  spreadQuality = DEFAULT_SPREAD_QUALITY;
  bg = DEFAULT_BG;
  lastCalc = -1;
  scale = 1;
  tex = NULL;
  setM();
  resetTransform();
}

template <class T> void Camera<T>::getCurrentStyle(StyleSheet *ss) const
{
    f->exportStyle(ss);
}

template <class T> void Camera<T>::getCurrentParam(StyleSheet *ss) const
{
    f->exportParam(ss);
}

template <class T> Function<T>* Camera<T>::getFunction() const {return f;}
template <class T> void Camera<T>::setFunction(Function<T> *f0) {f = f0;}
template <class T> void Camera<T>::setZoomSpeed(T v) {zoomSpeed = v;}
template <class T> T Camera<T>::getZoomSpeed() const {return zoomSpeed;}
template <class T> void Camera<T>::setClipping(T near,T far) {nearClip = near; farClip = far;}
template <class T> T Camera<T>::getNearClip() const {return nearClip;}
template <class T> T Camera<T>::getFarClip() const {return farClip;}
template <class T> void Camera<T>::setNearClip(T c) {nearClip = c;}
template <class T> void Camera<T>::setFarClip(T c) {farClip = c;}
template <class T> void Camera<T>::setFocus(Real f) {focus=f;}
template <class T> T Camera<T>::getFocus() const {return focus;}
template <class T> int Camera<T>::getNumD() const {return numD;}
template <class T> void Camera<T>::setBg(Colour c) {bg = c;}
template <class T> Colour Camera<T>::getBg() const {return bg;}
template <class T> void Camera<T>::setMaxArraySize(int m) {maxArraySize = m;}
template <class T> int Camera<T>::getMaxArraySize() const {return maxArraySize;}
template <class T> void Camera<T>::setSpread(T s) {spread = s;}
template <class T> T Camera<T>::getSpread() const {return spread;}
template <class T> void Camera<T>::setSpreadQuality(int q)  {spreadQuality = q;}
template <class T> T Camera<T>::getSpreadQuality() const {return spreadQuality;}
template <class T> void Camera<T>::pauseThreads()
{
    //for (int i = 0; i < threads.size(); ++i) threads[i]->
}

template <class T> void Camera<T>::setDirection(const VectorN<RTYPE> &dir)
{
    Matrix<RTYPE> m = getTransform();

    Base<RTYPE> b;
    b.setDefault(m.getColoumns()-1);
    b.setVector(2, dir, true);
    b.orthogonate(2);

    Matrix<RTYPE> m2 = b.toRotation();
    for (int i = 0; i < m2.getColoumns(); ++i) {
        for (int j = 0; j < m2.getRows(); ++j) {
            m.set(j,i,m2.get(j,i));
        }
    }
    setTransform(m);
}

template <class T> VectorN<T> Camera<T>::getPosition() const
{
    Matrix<RTYPE> m = getTransform();
    VectorN<T> vec(m.getRows()-1);
    for (int i = 0; i < m.getRows()-1; ++i) vec[i] = m.get(i, m.getColoumns()-1);
    return vec;
}

template <class T> VectorN<RTYPE> Camera<T>::getDirection() const
{
    Matrix<T> m = getTransform();
    for (int i = 0; i < m.getRows(); ++i) m.set(i,m.getColoumns()-1,0);
    VectorN<T> trans = m*VectorN<T>::identity(m.getColoumns()-1, 2);
    return trans.normalized();
}

template <class T> CameraElement* Camera<T>::capture(string id) const
{
  CameraElement *ce = new CameraElement("id=\""+id+"\"");
  for (int i = 0; i < transform.getRows(); ++i) {
    for (int j = 0; j < transform.getColoumns(); ++j) {
      stringstream ss;
      ss << "a" << i << "_" << j;
      ce->setAttributeT(ss.str(),transform.get(i,j));
    }
  }
  ce->setAttributeT("nearclip",nearClip);
  ce->setAttributeT("farclip",farClip);
  ce->setAttributeT("scale",scale);
  ce->setAttributeT("focus",focus);
  return ce;
}

template <class T> void Camera<T>::loadComposition(CameraElement *ce)
{
  for (int i = 0; i < transform.getRows(); ++i) {
    for (int j = 0; j < transform.getColoumns(); ++j) {
      stringstream ss;
      ss << "a" << i << "_" << j;
      string value = ce->getAttribute(ss.str(), "0");
      T v;
      readValue(value,v);
      transform.set(i,j,v);
    }
  }
  readValue(ce->getAttribute("nearclip", "0"),nearClip);
  readValue(ce->getAttribute("farclip", "0"),farClip);
  readValue(ce->getAttribute("scale", "0"),scale);
  readValue(ce->getAttribute("focus", "0"),focus);
}

template <class T> void Camera<T>::rotate(double angle, int x, int y, bool local)
{
  if (max(x,y)+1 >= transform.getRows()) return;
  Matrix<T> m = Matrix<T>::Rotation(transform.getRows()-1, angle, x, y);
  VectorN<T> translation(transform.getRows());
  Matrix<T> trm(transform);
  for (int i = 0; i < transform.getRows(); ++i) {
    translation[i] = transform.get(i, transform.getColoumns()-1);
    trm.set(i, transform.getColoumns()-1, 0);
  }
  if (local) trm = trm*m;
  else trm = m*trm;
  
  for (int i = 0; i < trm.getRows(); ++i) trm.set(i, trm.getColoumns()-1, translation[i]);
  transform = trm;
}

template <class T> void Camera<T>::scaleIn(float time)
{
  double s0 =  pow(zoomSpeed,time);
  scale = scale*s0;
}

template <class T> void Camera<T>::scaleOut(float time)
{
  double s0 =  pow(zoomSpeed,time);
  double s = 1.0 - s0;
  scale += scale*s;
}

template <class T> void Camera<T>::moveOnAxis(int n, float time)
{
  if (n >= transform.getRows()-1) return;
  double s0 =  pow(zoomSpeed,time);
  double s = 1.0 - s0;
  //VectorN<T> trans = VectorN<T>::zero(transform.getRows()-1);
  //trans[2] = focus*s;
  T z = focus*s;
  //focus = focus*s0;
  VectorN<T> v(transform.getRows()-1);// = transform * trans;
  for (int i = 0; i < v.size(); ++i) v[i] = transform.get(i,n)*z;
  //cout<<v<<endl;
  translateV(v);
}

template <class T> void Camera<T>::moveForward(float time)
{
  double s0 =  pow(zoomSpeed,time);
  double s = 1.0 - s0;
  //VectorN<T> trans = VectorN<T>::zero(transform.getRows()-1);
  //trans[2] = focus*s;
  T z = focus*s;
  focus = focus*s0;
  VectorN<T> v(transform.getRows()-1);// = transform * trans;
  for (int i = 0; i < v.size(); ++i) v[i] = transform.get(i,2)*z;
  //cout<<v<<endl;
  translateV(v);
}

template <class T> void Camera<T>::moveBackward(float time)
{
  double s0 =  pow(zoomSpeed,time);
  double s = 1.0 - s0;
  //VectorN<T> trans = VectorN<T>::zero(transform.getRows()-1);
  //trans[2] = -focus*s;
  T z = -focus*s;
  focus += focus*s;
  VectorN<T> v(transform.getRows()-1);// = transform * trans;
  for (int i = 0; i < v.size(); ++i) v[i] = transform.get(i,2)*z;
  //cout<<v<<endl;
  translateV(v);
}

template <class T> void Camera<T>::increaseFocus(float time)
{
  double s0 =  pow(zoomSpeed,time);
  double s = 1.0 - s0;
  focus += focus*s;
}

template <class T> void Camera<T>::decreaseFocus(float time)
{
  double s0 =  pow(zoomSpeed,time);
  focus = focus*s0;
}

template <class T> void Camera<T>::translateV(VectorN<T> &trans)
{
  for (int i = 0; i < transform.getRows()-1; ++i) transform.set(i, transform.getColoumns()-1, transform.get(i, transform.getColoumns()-1)+trans.get(i));
}

template <class T> void Camera<T>::translate(VectorN<T> trans)
{
  for (int i = 0; i < transform.getRows()-1; ++i) transform.set(i, transform.getColoumns()-1, transform.get(i, transform.getColoumns()-1)+trans.get(i));
}

template <class T> void Camera<T>::setPositionV(VectorN<T> &trans)
{
  for (int i = 0; i < transform.getRows()-1; ++i) transform.set(i, transform.getColoumns()-1, trans.get(i));
}

template <class T> void Camera<T>::setPosition(VectorN<T> trans)
{
  for (int i = 0; i < transform.getRows()-1; ++i) transform.set(i, transform.getColoumns()-1, trans.get(i));
}

template <class T> void Camera<T>::setScale(T s)
{
  scale = s;
}

template <class T> T Camera<T>::getScale() const {return scale;}

template <class T> void Camera<T>::resetTransform()
{
  transform = Matrix<T>::Identity(transform.getRows());
}

/*template <class T> void Camera<T>::setTransform(Matrix<T> &m)
{
  transform = m;
}*/

template <class T> void Camera<T>::setTransform(Matrix<T> m)
{
  transform = m;
}

template <class T> Matrix<T> Camera<T>::getTransform() const {return transform;}

template <class T> void Camera<T>::setM()
{
  if (tex != NULL) clear();
  tex = new Colour*[density*numD];
  for (int i = 0; i < density*numD; ++i) tex[i] = new Colour[density*numD];
}

template <class T> void Camera<T>::clear()
{
  if (tex == NULL) return;
  for (int i = 0; i < density*numD; ++i) delete [] tex[i];
  delete [] tex;
  tex = NULL;
}

template <class T> Camera<T>::~Camera()
{
  clear();
}

template <class T> void Camera<T>::setD(int den,int dep, int numd)
{
  clear();
  density = den;
  depth = dep;
  numD = numd;
  setM();
}

template <class T> void Camera<T>::render(Texture &text, bool * leave, bool *pause, double wlimit, double hlimit)
{
  if (lastCalc == -1) throw NothingToRender();
  Transformable<T> * trans = dynamic_cast<Transformable<T>*>(this);
  f->setCamera(trans);
  
  int x,y;
  x = text.getX();
  y = text.getY();
  int mx = (density*(lastCalc+1));
  int px,py;
  px = (1.0-wlimit)/2*mx;
  py = (1.0-hlimit)/2*mx;
  Real x2 = (Real)x/mx;
  Real y2 = (Real)y/mx;
//   if (x%(density*(lastCalc+1)) != 0) x2++;
//   if (y%(density*(lastCalc+1)) != 0) y2++;
  bool F = false;
  if (leave == NULL) leave = &F;
  if (pause == NULL) pause = &F;
  for (int i = 0; i < x; ++i) for (int j = 0; j < y; ++j) 
  {
      while (*pause && !(*leave)) {
        std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
      }
    if (*leave) return;
    if (j > 0 && (int)(j/y2) == (int)((j-1)/y2)) text.set(i,j,text.get(i,j-1));
    else if (i > 0 && (int)(i/x2) == (int)((i-1)/x2)) text.set(i,j,text.get(i-1,j));
    else {
      int tx,ty;
      tx = (wlimit*i/x2)+px;
      ty = (hlimit*j/y2)+py;
      Colour c = tex[tx][ty];
      text.set(i,j,c);
      /*Colour c2,c3;
      if (tx > 0) c2 = tex[tx-1][ty];
      else c2 = c;
      if (ty > 0) c3 = tex[tx][ty-1];
      else c3 = c;
      text.set(i,j,Colour((c.r*3+c2.r+c3.r)/5,(c.g*3+c2.g+c3.g)/5,(c.b*3+c2.b+c3.b)/5,(c.a*3+c2.a+c3.a)/5));*/
    }
  }
}


template <class T> VectorN<T> Camera<T>::getPoint(int nd, int x,int y,int z, T w)
{
  nd++;
  VectorN<T> v(f->getDim());
  T l = (T)z/(depth*nd);
  l = (sqrt(l) + l) / 2;
  v[2] = lerp<T>(nearClip,farClip,l);
  w = w*v[2];
  v[0] = w*x/(density*nd) - 0.5*w;
  v[1] = w*y/(density*nd) - 0.5*w;
  for (int i = 3; i < v.size(); ++i) v[i] = 0;
  return v*scale;
}

template <class T> pair<pair<T,T>, T> Camera<T>::getPoint3D(int nd, int x,int y,int z, T w)
{
  nd++;
  T vx,vy,vz;
  T l = (T)z/(depth*nd);
  l = (sqrt(l) + l) / 2;
  vz = lerp<T>(nearClip,farClip,l);
  w = w*vz;
  vx = w*x/(density*nd) - 0.5*w;
  vy = w*y/(density*nd) - 0.5*w;
  return make_pair(make_pair(vx*scale,vy*scale),vz*scale);
}

template <class T> void Camera<T>::calc(int n, int threads, bool *leave, bool *pause, double *status, double wlimit, double hlimit, INotifiable *noti)
{
  ERROR_CODE err = NONE;
  lastCalc = n;
  int numthr = threads;
  int mx = density*(n+1);
  int num = mx;
  int px,py;
  px = density*(n+1) * (1.0-wlimit)/2;
  py = density*(n+1) * (1.0-hlimit)/2;
  mx = mx*wlimit+px;
  num = num*wlimit;
  
  if (numthr > num) numthr = num;
  num /= numthr;
  if (mx % numthr > 0) num++;
  
  int length = atoi(MACHINE_CONFIG.getApp()->getAttribute("vram",DEFAULT_VRAM).c_str());
  VRam vram(length);
  f->preCalc(vram,0);
  
  //vector<thread*> thr;
  //cout<<num<<" "<<px<<" "<<mx<<endl;
  //cout<<endl;
  /*for (int i = 0; i < numthr; ++i) {
    //cout<<num*i+px<<" "<<min(num*(i+1)+px,mx)<<endl;
    CalcThreadInfo info(n,num*i+px,min(num*(i+1)+px,mx),leave,i);
    if (info.a < info.b) thr.push_back(new thread(&Camera<T>::calcThread, this, info, 1.0/numthr, status, wlimit, hlimit));
  }
  for (int i = 0; i < thr.size(); ++i) {
    thr[i]->join();
    delete thr[i];
  }*/
  int dp0 = depth*(n+1);
  int W0,H0;
  W0 = density*(n+1);
  H0 = density*(n+1);
  
  float blur0,blur1,blur2;
  blur0 = f->getBlur0();
  blur1 = f->getBlur1();
  blur2 = f->getBlur2();
  
  int numLight = atoi(MACHINE_CONFIG.getApp()->getAttribute("lighting",DEFAULT_LIGHTING).c_str());
  if (numLight*2+1 > dp0) numLight  = (dp0-1)/2;
  if (numLight < 0) numLight = 0;
  
  int dp = dp0 + numLight;
  int W = W0 + numLight*2;
  int H = H0 + numLight*2;
  
  float ***colours = new float**[dp];
  bool ***solid = new bool**[dp];
  for (int i = 0; i < dp; ++i) {
    colours[i] = NULL;
    solid[i] = NULL;
  }
  float **temp_colours = new float*[W];
  float **temp_colours2 = new float*[W];
  int **temp_d = new int*[W];
  int **depth = new int*[W];
  bool noTransparent = true;
  bg = f->getBackground();
  for (int i = 0; i < W; ++i) {
    temp_colours[i] = new float[H*4];
    temp_colours2[i] = new float[H*4];
    temp_d[i] = new int[H];
    depth[i] = new int[H];
    for (int j = 0; j < H; ++j) {
      temp_colours[i][j*4  ] = bg.r;
      temp_colours[i][j*4+1] = bg.g;
      temp_colours[i][j*4+2] = bg.b;
      temp_colours[i][j*4+3] = bg.a;
      depth[i][j] = 0;
      temp_d[i][j] = dp;
    }
  }
  LightData<T> ***data = new LightData<T>**[threads];
  int *dn = new int[threads];
  for (int i = 0; i < threads; ++i) {
    data[i] = new LightData<T>*[H];
    for (int j = 0; j < H; ++j) data[i][j] = new LightData<T>(transform.getRows()-1);
    dn[i] = 0;
  }
  if (status != NULL) *status = 0;
  VRam **vrams = new VRam*[threads];
  for (int i = 0; i < threads; ++i) vrams[i] = new VRam(length);
  int numOcclusion = atoi(MACHINE_CONFIG.getApp()->getAttribute("occlusion",DEFAULT_OCCLUSION).c_str())+1;
  if (numOcclusion > dp) numOcclusion = dp;
  if (numOcclusion < 0) numOcclusion = 0;
  //int len = transform.getColoumns()-1;
  T *matrix = new T[transform.getRows() * transform.getColoumns()];
  try {
      for (int i = 0; i < transform.getRows(); ++i) for (int j = 0; j < transform.getColoumns(); ++j) matrix[i*transform.getColoumns() + j] = transform.get(i,j);
      for (int i = 0; i < numOcclusion; ++i) {		//First always included
        int id = i * (dp-(numLight*2+1)) / numOcclusion;
        while (*pause && !(*leave)) {
          std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
        }
        if (*leave) break;
        if (colours[id] == NULL) calcLayer(matrix,numLight,n,px,py,colours,solid,id,threads,W,H,dp,leave,pause,status,vrams,noti);
      }
      for (int i = 0; i < numLight*2+1; ++i) {		//numLight is limited, won't exceed dp
        int id = dp-(numLight*2+1) + i;
        if (*leave) break;
        if (colours[id] == NULL) calcLayer(matrix,numLight,n,px,py,colours,solid,id,threads,W,H,dp,leave,pause,status,vrams,noti);
      }
      T w = tan(view/2)*2;
      for (int id = dp-1; id >= 0; --id) {
          while (*pause && !(*leave)) {
            std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
          }
        if (*leave) break;
        //if (numLight > 0) updateDepth(colours,id,W,H,px,py,dp,depth,numLight,threads,leave);
        if (numLight > 0) makeLight(vrams, data, matrix, n, dn, colours,solid, id, dp, W, H, px, py, numLight, threads, leave, pause);
        double fg = abs(getPoint3D(n,0,0,id+1,w).second - getPoint3D(n,0,0,id,w).second)*f->getFog();
        if (id < dp0) for (int i = px; i < W-px; ++i) {
            while (*pause && !(*leave)) {
              std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
            }
          if (*leave) break;
          //cout<<id<<" "<<colours[id]<<endl;
          bool b = addColours(H-2*py, colours[id][i]+4*py, temp_colours[i]+4*py, temp_d[i]+py, leave, pause, fg, id);
          noTransparent = b && noTransparent;
        }
        int id2 = id+numLight;
        int id0 = id-numLight-1;
        if (id0 >= 0) {
          if (colours[id0] == NULL) {
        if (id2 >= dp) throw RuntimeError(UNKNOWN_ERROR, "Something went wrong: id2 >= dp",ERROR_CONTENT);
        swap(colours[id0],colours[id2]);
        swap(solid[id0],solid[id2]);
        calcLayer(matrix,numLight,n,px,py,colours,solid,id0,threads,W,H,dp,leave,pause,status,vrams,noti);
        applyLayer(colours,solid,threads,id0,id+1,W,H,px,py,leave,pause);
          } else if (id2 < dp) {
        int empty = getMiddleOfLongest((void**)colours,0,id0);
        if (empty >= id0) empty = 0;
        if (colours[empty] == NULL) {
          swap(colours[empty],colours[id2]);
          swap(solid[empty],solid[id2]);
          calcLayer(matrix,numLight,n,px,py,colours,solid,empty,threads,W,H,dp,leave,pause,status,vrams,noti);
          applyLayer(colours,solid,threads,empty,id+1,W,H,px,py,leave,pause);
        }
          }
        }
      }
      /*if (blur0 != 0 || blur1 != 0 || blur2 != 0) {//Blur
        if (!noTransparent) {
          LOG<<"Warning: Blur is unavailable: transparent points found"<<endl;
        } else {
          //blurImage(temp_colours, temp_d, temp_colours2, W, H, px+numLight, py+numLight, blur0, blur1, blur2);
          //swap(temp_colours, temp_colours2);
        }
      }*/
      ulli x,y;
      for (int i = px; i < W0-px; ++i) {
        if (*leave) break;
        x = i + numLight;
        for (int j = py; j < H0-py; ++j) {
            while (*pause && !(*leave)) {
              std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
            }
          if (*leave) break;
          y = j + numLight;
          tex[i][j] = Colour(temp_colours[x][y*4], temp_colours[x][y*4+1], temp_colours[x][y*4+2], temp_colours[x][y*4+3]);
        }
      }
  } catch (ERROR_CODE e) {
    err = e;
  }
  
  for (int i = 0; i < dp; ++i) { 
    if (colours[i] != NULL) {
      for (int j = 0; j < W; ++j) delete [] colours[i][j];
      delete [] colours[i];
    }
    if (solid[i] != NULL) {
      for (int j = 0; j < W; ++j) delete [] solid[i][j];
      delete [] solid[i];
    }
  }
  delete [] solid;
  delete [] colours;
  
  for (int i = 0; i < W; ++i) {
    delete [] temp_colours[i];
    delete [] temp_colours2[i];
    delete [] depth[i];
    delete [] temp_d[i];
  }
  delete [] temp_colours;
  delete [] temp_colours2;
  delete [] depth;
  delete [] temp_d;
  
  for (int i = 0; i < threads; ++i) delete vrams[i];
  delete [] vrams;
  
  for (int i = 0; i < threads; ++i) {
    for (int j = 0; j < H; ++j) delete data[i][j];
    delete [] data[i];
  }
  delete [] data;
  delete [] dn;
  
  delete [] matrix;
  if (err != NONE) throw err;
}

template <class T> void Camera<T>::lightHelper(float ***colours, bool ***solid, int z0, int x0, int y0, int numLight, int dirx, int diry, int &h, int &d)
{
  h = 0;
  d = 1;
  int x = x0+dirx;
  int y = y0+diry;
  if (isSolid(colours[z0-1],x,y)) {
    h = 1;
    while (z0-h >= 0 && h < numLight && isSolid(colours[z0-h],x,y)) h++;
    h--;
  } else {
    if (isSolid(colours[z0],x,y)) {
      while (d < numLight && isSolid(colours[z0],x,y) && !isSolid(colours[z0-1],x,y)) {x += dirx; y += diry; d++;}
      if (d < numLight) {
	if (isSolid(colours[z0],x,y)) h = 1;
	else h = -1;
      }
    } else {
      h = -1;
      while (-h < numLight && !isSolid(colours[z0-h],x,y)) h--;
    }
  }
}

template <class T> void Camera<T>::makeLight(VRam **vrams, LightData<T> ***data, T *matrix, int n, int *dn, float ***colours, bool ***solid, int id, int dp, int W, int H, int px, int py, int numLight, int threads, bool *leave, bool *pause)
{
  if (id == 0 || id + numLight >= dp) return;
  
  vector<thread*> thr;
  error = NONE;
  int size = (W - 2*px - 2*numLight)/threads;
  if (size <= 0) size = 1;
  for (int i = 0; i < threads; ++i) {
    int from, to;
    from = size * i + px + numLight;
    to = from+size;
    if (to > W-px-numLight) to = W-px-numLight;
    if (to <= from) break;
    thr.push_back(new thread(&Camera<T>::makeLightPart,this,vrams[i],i,data[i],matrix, n,dn+i,colours,solid,id,dp,W,from,to,H,px,py,numLight,leave,pause));
  }
  for (int i = 0; i < thr.size(); ++i) {
    thr[i]->join();
    delete thr[i];
  }
  if (error != NONE) throw error;
}

template <class T> void Camera<T>::makeLightPart(VRam *vr, int thid, LightData<T> **data, T *matrix, int n, int *dn, float ***colours, bool ***solid, int id, int dp, int W, int a, int b, int H, int px, int py, int numLight, bool *leave, bool *pause)
{
  try {
	T w = tan(view/2)*2;
    //calcLight(vr,thid,data,matrix,*dn,colours,solid,leave,pause);
	for (int i = a; i < b; ++i) {
	  if (*leave) break;
	  for (int j = py+numLight; j < H-py-numLight; ++j) {
		  while (*pause && !(*leave)) {
			std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
		  }
		if (*leave) break;
		if (!isSolid(colours[id],i,j) || isSolid(colours[id-1],i,j)) continue;
		int th,lh,rh,bh;
		int td,ld,rd,bd;
		
		lightHelper(colours, solid, id, i, j, numLight, 1, 0, rh, rd);
		lightHelper(colours, solid, id, i, j, numLight,-1, 0, lh, ld);
		lightHelper(colours, solid, id, i, j, numLight, 0, 1, th, td);
		lightHelper(colours, solid, id, i, j, numLight, 0,-1, bh, bd);
		T a1,a2;
        a1 = ((T)rh-lh)/(rd+ld-1);
        a2 = ((T)th-bh)/(td+bd-1);

        /*if (thid == 2) {
            / *if (j == (py+numLight + H-py-numLight)/2 && id > numLight && id < dp-numLight) {
                cout<<rd<<" "<<rh<<endl;
                cout<<ld<<" "<<lh<<endl;
                cout<<a1<<endl;
                cout<<endl;
                colours[id][i][j*4+1] = 0;
                colours[id][i][j*4+2] = 1;
            }* /
            if (i == (b+a)/2+10 && j == (py+numLight + H-py-numLight)/2 && id > numLight && id < dp-numLight) {
                cout<<"---------"<<endl;
                for (int z0 = id-numLight+1; z0 < id+numLight; ++z0) {
                    for (int x0 = i-numLight+1; x0 < i+numLight; ++x0) {
                        if (z0 == id && x0 == i) {
                            cout<<"X";
                        } else if (isSolid(colours[z0],x0,j)) {
                            cout<<"#";
                        } else {
                            cout<<".";
                        }
                        colours[z0][x0][j*4] = 1;
                        colours[z0][x0][j*4+1] = 0;
                        colours[z0][x0][j*4+2] = 0;
                    }
                    cout<<endl;
                }
                cout<<rd<<" "<<rh<<endl;
                cout<<ld<<" "<<lh<<endl;
                cout<<a1<<endl;
                cout<<"---------"<<endl<<endl<<endl<<endl;
            }
        }*/

		//LOG<<th<<" "<<lh<<" "<<rh<<" "<<bh<<endl;
		//LOG<<td<<" "<<ld<<" "<<rd<<" "<<bd<<endl;
		//LOG<<a1<<" "<<a2<<endl;
		//LOG<<endl;
		T x1,z1,y2,z2;
		T x3,y3,z3;
		x1 = 1;
		z1 = -a1;
		y2 = 1;
		z2 = -a2;
		T l1,l2;
		l1 = sqrt(1 + z1*z1);
		l2 = sqrt(1 + z2*z2);
		x1 /= l1;
		z1 /= l1;
		y2 /= l2;
		z2 /= l2;
		//LOG<<x1<<" 0 "<<z1<<"   0 "<<y2<<" "<<z2<<endl;
		x3 = -z1 * y2;
		y3 = -x1 * z2;
		z3 =  x1 * y2;
		data[*dn]->id = id;
		data[*dn]->x = i;
		data[*dn]->y = j;
		//int id2 = id-1;
		//int x2 = i-x;
		//int y2 = j-y;
		pair<pair<T,T>, T> pp,pd;
		pp = getPoint3D(n, i-numLight ,j-numLight ,id ,w);
		data[*dn]->px = pp.first.first;
		data[*dn]->py = pp.first.second;
		data[*dn]->pz = pp.second;
		//LOG<<x3<<" "<<y3<<" "<<z3<<endl;
		//LOG<<endl;
		data[*dn]->dx = data[*dn]->px + x3;
		data[*dn]->dy = data[*dn]->py + y3;
		data[*dn]->dz = data[*dn]->pz + z3;
		/*T le = sqrt(data[*dn]->dx*data[*dn]->dx + data[*dn]->dy*data[*dn]->dy + data[*dn]->dz*data[*dn]->dz);
		data[*dn]->dx /= le;
		data[*dn]->dy /= le;
		data[*dn]->dz /= le;*/
		//cout<<data[*dn]->dx<<" "<<data[*dn]->dy<<" "<<data[*dn]->dz<<endl;
		/*data[*dn]->v[0] = -x;
		data[*dn]->v[1] = -y;
		data[*dn]->v[2] = -1;*/
		(*dn)++;
		if (*dn >= H) calcLight(vr,thid,data,matrix,*dn,colours,solid,leave,pause);
	  }
	}
	calcLight(vr,thid,data,matrix,*dn,colours,solid,leave,pause);
  } catch (ERROR_CODE e) {
	*leave = true;
    error = e;
  }
}

template <class T> void Camera<T>::calcLight(VRam *vr, int thid, LightData<T> **data, T *matrix, int &dn, float ***colours, bool ***solid, bool *leave, bool *pause)
{
  if (dn == 0) return;
  int dim = transform.getRows()-1;
  
  /*for (int i = 0; i < dn; ++i) {
    data[i]->pos[0] = data[i]->px;
    data[i]->dir[0] = data[i]->dx - data[i]->pos[0];
    data[i]->pos[1] = data[i]->py;
    data[i]->dir[1] = data[i]->dy - data[i]->pos[1];
    data[i]->pos[2] = data[i]->pz;
    data[i]->dir[2] = data[i]->dz - data[i]->pos[2];
    for (int j = 3; j < dim; ++j) data[i]->pos[j] = data[i]->dir[j] = 0;
  }*/
  
  for (int j = 0; j < dim; ++j) {
    register T mx = matrix[j*(dim+1)  ];
    register T my = matrix[j*(dim+1)+1];
    register T mz = matrix[j*(dim+1)+2];
    register T m  = matrix[(j+1)*(dim+1)-1];
    for (int i = 0; i < dn; ++i) {
      data[i]->pos[j] = mx * data[i]->px + my * data[i]->py + mz * data[i]->pz + m;
      data[i]->dir[j] = mx * data[i]->dx + my * data[i]->dy + mz * data[i]->dz + m;
      data[i]->dir[j] -= data[i]->pos[j];
    }
  }
  for (int i = 0; i < dn; ++i) {
    T l = 0;
    for (int j = 0; j < dim; ++j) l += data[i]->dir[j]*data[i]->dir[j];
    l = sqrt(l);
    for (int j = 0; j < dim; ++j) data[i]->dir[j] /= l;
  }
  //for (int i = 0; i < dn; ++i) LOG<<data[i]->dir[0]<<" "<<data[i]->dir[1]<<" "<<data[i]->dir[2]<<endl;
  f->shade(*vr,dim,thid,data,dn,colours,leave,pause);
  dn = 0;
}

template <class T> bool** Camera<T>::getPrevious(bool ***solid, int id)
{
  for (int i = id-1; i >= 0; --i) if (solid[i] != NULL) return solid[i];
  return solid[id];
}

template <class T> bool Camera<T>::isSolid(float **colours, int x, int y)
{
  return isColourSolid(colours[x][y*4+3]);
}

template <class T> void Camera<T>::applyLayer(float ***colours, bool ***solid, int threads, int id, int dp, int W, int H, int px, int py, bool *leave, bool *pause)
{
  bool **prev = getPrevious(solid,id);
  if (prev == solid[id]) prev = NULL;
  
  vector<thread*> thr;
  
  error = NONE;
  int size = (W-2*px)/threads;
  if (size == 0) size = 1;
  for (int i = 0; i < threads; ++i) {
    int from, to;
    from = size * i + px;
    to = from+size;
    if (to > W-px) to = W-px;
    if (to <= from) break;
    thr.push_back(new thread(&Camera<T>::applyPart, this,colours,solid,prev,id,dp,from,to,H,py,leave,pause));
  }
  for (int i = 0; i < thr.size(); ++i) {
    thr[i]->join();
    delete thr[i];
  }
  if (error != NONE) throw error;
}

template <class T> void Camera<T>::applyPart(float ***colours, bool ***solid, bool **prev, int id, int dp, int a, int b, int H, int py, bool *leave, bool *pause)
{
  try {
	for (int i = a; i < b; ++i) {
	  if (*leave) break;
	  for (int j = py; j < H-py; ++j) {
		  while (*pause && !(*leave)) {
			std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
		  }
		if (*leave) break;
		if (solid[id][i][j] && (prev == NULL || !prev[i][j])) {
	  int d = id+1;
	  while (d < dp) {
		if (colours[d] != NULL) {
		  if (solid[d][i][j]) break;
		  colours[d][i][j*4+3] = 1;
		  solid[d][i][j] = true;
		}
		d++;
	  }
		}
	  }
	}
  } catch (ERROR_CODE e) {
	*leave = true;
    error = e;
  }
}

template <class T> void Camera<T>::updateLayer(float **colours, bool **solid, int W, int H, int px, int py, int threads, int numLight, bool *leave, bool *pause)
{
  vector<thread*> thr;
  error = NONE;
  int size = (W-2*px)/threads;
  if (size == 0) size = 1;
  
  for (int i = 0; i < threads; ++i) {
    int from, to;
    from = size * i + px;
    to = from+size;
    if (to > W-px) to = W-px;
    if (to <= from) break;
    thr.push_back(new thread(&Camera<T>::updateLayerSet,this,colours,solid,numLight,from,to,H,py,leave,pause));
  }
  for (int i = 0; i < thr.size(); ++i) {
    thr[i]->join();
    delete thr[i];
  }
  thr.clear();
  
  for (int i = 0; i < threads; ++i) {
    int from, to;
    from = size * i + px;
    to = from+size;
    if (to > W-px) to = W-px;
    if (to <= from) break;
    thr.push_back(new thread(&Camera<T>::updateLayerCols,this,colours,solid,numLight,from,to,H,py,leave,pause));
  }
  for (int i = 0; i < thr.size(); ++i) {
    thr[i]->join();
    delete thr[i];
  }
  thr.clear();
  
  size = (H-2*py)/threads;
  if (size == 0) size = 1;
  for (int i = 0; i < threads; ++i) {
    int from, to;
    from = size * i + py;
    to = from+size;
    if (to > H-py) to = H-py;
    if (to <= from) break;
    thr.push_back(new thread(&Camera<T>::updateLayerRows,this,colours,solid,numLight,from,to,W,px,leave,pause));
  }
  for (int i = 0; i < thr.size(); ++i) {
    thr[i]->join();
    delete thr[i];
  }
  if (error != NONE) throw error;
}

template <class T> void Camera<T>::updateLayerSet(float **colours, bool **solid, int numLight, int a, int b, int H, int py, bool *leave, bool *pause)
{
  for (int i = a; i < b; ++i) {
      while (*pause && !(*leave)) {
        std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
      }
    if (*leave) break;
    for (int j = py; j < H-py; ++j) solid[i][j] = true;
  }
}

template <class T> void Camera<T>::updateLayerCols(float **colours, bool **solid, int numLight, int a, int b, int H, int py, bool *leave, bool *pause)
{
  for (int i = a; i < b; ++i) {
    int sum = numLight;
    while (*pause && !(*leave)) {
      std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
    }
    if (*leave) break;
    for (int j = py; j < H-py+numLight; ++j) {
      if (j >= H-py || isSolid(colours,i,j)) sum++;
      else sum = 0;
      if (sum < 2*numLight+1) {
	int m = j-numLight;
	if (m >= py) solid[i][m] = false;
      }
    }
  }
}

template <class T> void Camera<T>::updateLayerRows(float **colours, bool **solid, int numLight, int a, int b, int W, int px, bool *leave, bool *pause)
{
  for (int j = a; j < b; ++j) {
    int sum = numLight;
    while (*pause && !(*leave)) {
      std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
    }
    if (*leave) break;
    for (int i = px; i < W-px+numLight; ++i) {
      if (i >= W-px || isSolid(colours,i,j)) sum++;
      else sum = 0;
      if (sum < 2*numLight+1) {
	int m = i-numLight;
	if (m >= px) solid[m][j] = false;
      }
    }
  }
}

template <class T> void Camera<T>::calcLayer(T *matrix, int numLight, int n, int px, int py, float ***colours, bool ***solid, int id, int threads, int W, int H, int dp, bool *leave, bool *pause, double *status, VRam **vrams, INotifiable *noti)
{
  if (colours[id] == NULL) {
    colours[id] = new float*[W];
    solid[id] = new bool*[W];
    //cout<<"Create: "<<colours[id]<<endl;
    for (int i = 0; i < W; ++i) {
      colours[id][i] = new float[H*4];
      solid[id][i] = new bool[H];
      for (int j = py; j < H-py; ++j) {
        /*colours[id][i][j*4  ] = bg.r;
        colours[id][i][j*4+1] = bg.g;
        colours[id][i][j*4+2] = bg.b;
        colours[id][i][j*4+3] = bg.a;*/
        colours[id][i][j*4  ] = 0;
        colours[id][i][j*4+1] = 0;
        colours[id][i][j*4+2] = 0;
        colours[id][i][j*4+3] = 0;
      }
    }
  }
  error = NONE;
  bool **prev = getPrevious(solid,id);
  if (prev == solid[id]) prev = NULL;
  //if (prev == NULL) cout<<"prev: NULL"<<endl;
  //else cout<<"prev: "<<prev[px][py*4]<<" "<<prev[px][py*4+1]<<" "<<prev[px][py*4+2]<<" "<<prev[px][py*4+3]<<endl;
  vector<thread*> thr;
  int size = (W-2*px)/threads;
  if (size == 0) size = 1;
  for (int i = 0; i < threads; ++i) {
    int from, to;
    from = size * i + px;
    to = from+size;
    if (to > W-px) to = W-px;
    if (to <= from) break;
    thr.push_back(new thread(&Camera<T>::calcPart,this,matrix, numLight,n,py,colours[id],solid[id],prev,id,from,to,H,leave,pause,status,i,threads,dp,vrams[i],noti));
  }
  for (int i = 0; i < thr.size(); ++i) {
    thr[i]->join();
    delete thr[i];
  }
  if (error != NONE) throw error;
  updateLayer(colours[id], solid[id], W, H, px, py, threads, numLight, leave, pause);
}

template <class T> void Camera<T>::calcPart(T *matrix, int numLight, int n, int py, float **colours, bool **solid, bool **prev, int id, int from, int to, int H, bool *leave, bool *pause, double *status, int thread, int threads, int dp, VRam *vram, INotifiable *noti)
{
  int dim = transform.getRows()-1;
  T *vectors = new T[3*(H-2*py)];
  T *vectors_transformed = new T[dim*(H-2*py)];
  try {
	
	double st = 0;
	if (status == NULL) status = &st;
	
	pair<pair<T,T>, T> temp_p0,temp_p1,temp_p2;
	T w = tan(view/2)*2;
	double val = 1.0/(to-from)/threads/dp;
	for (int i = from; i < to; ++i) {
		while (*pause && !(*leave)) {
		  std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
		}
	  if (*leave) break;
	  
	  temp_p0 = getPoint3D(n, i-numLight,  py-numLight, id, w);
	  temp_p1 = getPoint3D(n, i+1-numLight,py-numLight, id, w);
	  temp_p2 = getPoint3D(n, i-numLight,py+1-numLight, id, w);
	  
	  getPoints<T>(temp_p0.first.first, temp_p0.first.second, temp_p0.second, temp_p1.first.first-temp_p0.first.first, temp_p2.first.second-temp_p0.first.second,1,H-2*py,vectors,leave,pause);
	  transformPoints<T>(dim, matrix, H-2*py, vectors, vectors_transformed,leave,pause);
	  
	  bool *p = NULL;
	  if (prev != NULL) p = prev[i]+py;
	  f->gets(*vram, dim, H-2*py, vectors_transformed, colours[i]+4*py, leave, pause, thread, p);
	  *status += val;
	  if (noti != NULL) noti->notify(*status);
	}
  } catch (ERROR_CODE e) {
	*leave = true;
    error = e;
  }
  
  
  delete [] vectors;
  delete [] vectors_transformed;
}

template <class T> void Camera<T>::setSpread(int n) {spread = n;}
template <class T> int Camera<T>::getSpread() {return spread;}
