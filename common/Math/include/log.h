#ifndef LOG_H
#define LOG_H

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <ctime>
#include <sstream>

#include "common/Helper/loggerbase.h"

#define LOG (*Log::instance)

typedef std::basic_ostream<char, std::char_traits<char> > CoutType;
typedef CoutType& (*StandardEndLine)(CoutType&);

using namespace std;

class StreamError: public exception
{
  virtual const char* what() const throw()
  {
    return "Cannot open file";
  }
};

class Log
{
  ostream *out;
  ofstream *file;
  ostream *out2;
  bool endL;
  ostream *debugs;
  LoggerBase *logger;
  LogModifier m;
public:
  static Log* instance;
  Log(const char*);
  Log(ostream*);
  Log(ostream*,const char*);
  Log(const char*,ostream*);
  Log(ostream*,ostream*);
  ~Log();
  void log(string);
  void continuesLog(string);
  void endline();
  void openDebug(ostream*);
  template <class T> void debug(const T);
  void setLogger(LoggerBase*);
  friend Log& operator<<(Log&, const LogModifier);
};

template <class T>
void Log::debug(const T s)
{
  if (debugs != NULL) (*debugs) << s << endl;
}

template <class T>
Log& operator<<(Log &log, const T x)
{
    stringstream ss;
    ss<<x;
    log.continuesLog(ss.str());
    log.debug(ss.str());
    return log;
}

Log& operator<<(Log&, StandardEndLine);

#endif
