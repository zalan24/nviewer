#pragma once

#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>

#include "info.h"
#include "config.h"
#include "function.h"
#include "render.h"
#include "common/Helper/sessionfile.h"

using namespace std;

class Session
{
  string name;
  Camera<RTYPE> *cam;
  Function<RTYPE> *func;
  SessionFile *file;
  //SessionConfig *cfg;
  MachineConfig *mcfg;
public:
  VRam *vram;
private:
  int dim;
  SessionFileData data;

  void setD();
  void updateStyles();
public:
  Session(string, int, MachineConfig*);
  Session(string, MachineConfig*);
  void loadScriptInPreview(string, INotifiable*, INotifiable*);
  void loadScriptInExport(string, INotifiable*, INotifiable*);
  void loadLastScript(INotifiable*, INotifiable*);
  void clearScript();
  ~Session();
  string getName();
  Function<RTYPE> *getFunction();
  void open(string, int);
  void open(string);
  void clear();
  SessionFileData *getData() {return &data;}
  Camera<RTYPE> *getCamera();
  MachineConfig *getMachineConfig();
  void reloadFunction();
  bool isScriptLoaded() const {return func != NULL && cam != NULL;}
  bool isThereAScript() const;
  int getDim() const {return dim;}
  SessionFile *getFile() const {return file;}
  void update() {setD();}
  bool scriptAssociated() const;
};
