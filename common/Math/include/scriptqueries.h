#pragma once

#include "custumfunctionclass.h"
#include "transformable.h"

#include "common/Helper/texture.h"

#define SESSION_QUERY string("Session")
#define CAMERA_QUERY string("Camera")

template <class T>
class Query_dim : public FunctionClass<T>
{
  int *dim;
public:
  Query_dim(int *d, VRam *vr) : FunctionClass<T>(SESSION_QUERY+".dim", *d, vr), dim(d) {FunctionClass<T>::setUp("",TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    return dim;
  }
  FunctionClass<T>* copy() {
    Query_dim<T> *r = (Query_dim<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Query_dim<T>));
    return new (r) Query_dim<T>(*this);
  }
  virtual int getType() const {return FunctionClass<T>::getType() | FUNCTIONTYPE_PARAMDEP;}
};

template <class T>
class Query_light : public FunctionClass<T>
{
  T *p;
public:
  Query_light(int dim, VRam *vr) : FunctionClass<T>(SESSION_QUERY+".light", dim, vr) {
    FunctionClass<T>::setUp("",TYPE_VECTOR_SIGN);
    p = (T*)vr->allocate(sizeof(T)*dim);
    for (int i = 0; i < dim; ++i) p[i] = 0;
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    return p;
  }
  FunctionClass<T>* copy() {
    Query_light<T> *r = (Query_light<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Query_light<T>));
    return new (r) Query_light<T>(*this);
  }
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    if (cam == NULL) return;
    LightDirection<T> *l = ((Transformable<T>*)cam)->getLight();
    for (int i = 0; i < FunctionClass<T>::getDim(); ++i) p[i] = l->get(i);
  }
  virtual int getType() const {return FunctionClass<T>::getType() | FUNCTIONTYPE_PARAMDEP;}
};

template <class T>
class Query_ambient : public FunctionClass<T>
{
  T a;
public:
  Query_ambient(int dim, VRam *vr) : FunctionClass<T>(SESSION_QUERY+".ambient", dim, vr) {
    FunctionClass<T>::setUp("",TYPE_VECTOR_SIGN);
    a = 0;
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    return &a;
  }
  FunctionClass<T>* copy() {
    Query_ambient<T> *r = (Query_ambient<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Query_ambient<T>));
    return new (r) Query_ambient<T>(*this);
  }
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    if (cam == NULL) return;
    a = ((Transformable<T>*)cam)->getAmbientLight();
  }
  virtual int getType() const {return FunctionClass<T>::getType() | FUNCTIONTYPE_PARAMDEP;}
};

template <class T>
class Query_campos : public FunctionClass<T>
{
  T *p;
public:
  Query_campos(int dim, VRam *vr) : FunctionClass<T>(CAMERA_QUERY+".pos", dim, vr) {
    FunctionClass<T>::setUp("",TYPE_VECTOR_SIGN);
    p = (T*)vr->allocate(sizeof(T)*dim);
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    return p;
  }
  FunctionClass<T>* copy() {
    Query_campos<T> *r = (Query_campos<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Query_campos<T>));
    return new (r) Query_campos<T>(*this);
  }
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    if (cam == NULL) return;
    Transformable<T> *transformable = (Transformable<T>*)cam;
    Matrix<T> m = transformable->getTransform();
    for (int i = 0; i < FunctionClass<T>::getDim(); ++i) p[i] = m.get(i,FunctionClass<T>::getDim());
  }
  virtual int getType() const {return FunctionClass<T>::getType() | FUNCTIONTYPE_PARAMDEP;}
};

template <class T>
class Query_camdir : public FunctionClass<T>
{
  T *p;
public:
  Query_camdir(int dim, VRam *vr) : FunctionClass<T>(CAMERA_QUERY+".dir", dim, vr) {
    FunctionClass<T>::setUp("",TYPE_VECTOR_SIGN);
    p = (T*)vr->allocate(sizeof(T)*dim);
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    return p;
  }
  FunctionClass<T>* copy() {
    Query_camdir<T> *r = (Query_camdir<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Query_camdir<T>));
    return new (r) Query_camdir<T>(*this);
  }
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    if (cam == NULL) return;
    Transformable<T> *transformable = (Transformable<T>*)cam;
    Matrix<T> m = transformable->getTransform();
    for (int i = 0; i < FunctionClass<T>::getDim(); ++i) p[i] = m.get(i,2);				//m * {0;0;2;0;...}
  }
  virtual int getType() const {return FunctionClass<T>::getType() | FUNCTIONTYPE_PARAMDEP;}
};

template <class T>
class Query_camtrans : public FunctionClass<T>
{
  T **p;
public:
  Query_camtrans(int dim, VRam *vr) : FunctionClass<T>(CAMERA_QUERY+".transform", dim, vr) {
    FunctionClass<T>::setUp("",TYPE_MATRIX_SIGN);
    p = (T**)vr->allocate(sizeof(T*)*dim);
    for (int i = 0; i < dim; ++i) p[i] = (T*)vr->allocate(sizeof(T)*dim);
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    return p;
  }
  FunctionClass<T>* copy() {
    Query_camtrans<T> *r = (Query_camtrans<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Query_camtrans<T>));
    return new (r) Query_camtrans<T>(*this);
  }
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    if (cam == NULL) return;
    Transformable<T> *transformable = (Transformable<T>*)cam;
    Matrix<T> m = transformable->getTransform();
    for (int i = 0; i < FunctionClass<T>::getDim(); ++i) for (int j = 0; j < FunctionClass<T>::getDim(); ++j) p[i][j] = m.get(j,i);
  }
  virtual int getType() const {return FunctionClass<T>::getType() | FUNCTIONTYPE_PARAMDEP;}
};

template <class T>
class Query_texture : public FunctionClass<T>
{
  Texture *texture;
public:
  Query_texture(int dim, VRam *vr, string name, Texture *texture) : FunctionClass<T>(name, dim, vr), texture(texture) {
    FunctionClass<T>::setUp("r,r",TYPE_COLOUR_SIGN);
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    T *x, *y;
    x = (T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
    y = (T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr,thread);
    int ix,iy;
    int w,h;
    w = texture->getX();
    h = texture->getY();
    ix = (w*(*x));
    iy = (h*(*y));
    ix %= w;
    iy %= h;
    return texture->getPointer(ix,iy);
  }
  FunctionClass<T>* copy() {
    Query_texture<T> *r = (Query_texture<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Query_texture<T>));
    return new (r) Query_texture<T>(*this);
  }
};
