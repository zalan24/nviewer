#pragma once

#include <iostream>
#include <math.h>

#include "vectorn.h"
#include "matrix.h"
#include "config.h"
#include "scriptparser.h"
#include "common/Helper/stylemanager.h"

#include "common/Helper/inotifiable.h"

using namespace std;

class InvalidDimension: public exception
{
  virtual const char* what() const throw()
  {
    return "Dim must be at least 3";
  }
};

template <class T>
class Function 
{
protected:
  int Dim;
  double fog;
  Colour background;
  void *cam;
  bool preview;
  int numThreads;
  LightDirection<T> *light;
  T ambientLight;
  float blur0,blur1,blur2;
  INotifiable *scriptNot;
  INotifiable *runtimeNot;
  ulli vrammax;
public:
  virtual ~Function() {}
  void resetVrammax() {vrammax = 0;}
  void setVRamNotifiers(INotifiable *script, INotifiable *runtime);
  float getBlur0() const {return blur0;}
  float getBlur1() const {return blur1;}
  float getBlur2() const {return blur2;}
  VRam *func_vram;
  virtual void gets(VRam&, int, int, T*, float*, bool*, bool*, int, bool*) = 0;
  virtual void setStyle(StyleSheet*);
  virtual void exportStyle(StyleSheet*);
  virtual void set(StyleSheet*);
  virtual void exportParam(StyleSheet*);
  double getFog() const;
  Colour getBackground() const;
  Function(int,double,VRam*,bool,int);
  int getDim();
  static Function<T>* getFunction(string, StyleSheet*, int, double, VRam*, bool, int);
  static void getFunctionList(vector<string>&);
  virtual string getStatus() {return string("Func VRam: ")+func_vram->getStatus();}
  void setCamera(void *c) {cam = c;}
  virtual void preCalc(VRam&,int) {}
  virtual void shade(VRam&, int, int, LightData<T>**, int, float***, bool*, bool*);
  LightDirection<T> *getLight() {return light;}
  T getAmbientLight() const {return ambientLight;}
  virtual void collectFiles(vector<string> &files) const {}
};

template <class T> void Function<T>::setVRamNotifiers(INotifiable *script, INotifiable *runtime)
{
    scriptNot = script;
    runtimeNot = runtime;
    scriptNot->notify((void*)func_vram);
}

template <class T> void Function<T>::shade(VRam &vr, int dim, int thid, LightData<T> **data, int dn, float ***colours, bool *leave, bool *pause)
{
  //register T x = light.getX();
  //register T y = light.getY();
  //register T z = light.getZ();
  for (int i = 0; i < dn; ++i) {
      while (*pause && !(*leave)) {
        std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
      }
    if (*leave) break;
    T dot = 0; //data[i].nx * x + data[i].ny * y + data[i].nz * z;
    for (int j = 0; j < dim; ++j) dot -= data[i]->dir[j] * light->get(j);
    //if (!(dot >= -1 && dot <= 1)) LOG<<"dot = "<<dot<<endl;
    T l = lerp(ambientLight, (T)1, ((T)1 - dot)/2);
    //if (!(l >= 0 && l <= 1)) LOG<<ambientLight<<" "<<dot<<" "<<((T)1 - dot)/2<<" "<<l<<endl;
    //LOG<<data[i]->dir[0]<<" "<<data[i]->dir[1]<<" "<<data[i]->dir[2]<<endl;
    //LOG<<dot<<" "<<l<<endl;
    colours[data[i]->id][data[i]->x][data[i]->y*4  ] *= l;
    colours[data[i]->id][data[i]->x][data[i]->y*4+1] *= l;
    colours[data[i]->id][data[i]->x][data[i]->y*4+2] *= l;
  }
}

template <class T> Function<T>::Function(int Dim, double fog, VRam *vr, bool preview, int numThreads) :
    Dim(Dim),
    fog(fog),
    func_vram(vr),
    cam(NULL),
    preview(preview),
    numThreads(numThreads),
    scriptNot(NULL),
    runtimeNot(NULL),
    vrammax(0) {
  if (Dim < 3) throw InvalidDimension();
  light = new LightDirection<T>(Dim);
}
template <class T> int Function<T>::getDim() {return Dim;}
template <class T> double Function<T>::getFog() const {return fog;}
template <class T> Colour Function<T>::getBackground() const {return background;}

template <class T>
void Function<T>::set(StyleSheet *)
{
}

template <class T>
void Function<T>::exportParam(StyleSheet *)
{
}

template <class T>
void Function<T>::setStyle(StyleSheet *ss)
{
    if (ss->isStyle("background")) background.parse(ss->getValue("background"));
    else background.parse("#000000ff");
    if (ss->isStyle("fog")) fog = matof(ss->getValue("fog").c_str());
    else fog = 1;
    if (ss->isStyle("light")) light->parse(ss->getValue("light"));
    else light->parse("0 0 1");
    if (ss->isStyle("ambient")) ambientLight = matof(ss->getValue("ambient").c_str());
    else ambientLight = 0.5;
    /*blur0 = matof(e->getAttribute("blur0","0").c_str());
    blur1 = matof(e->getAttribute("blur1","0").c_str());
    blur2 = matof(e->getAttribute("blur2","0").c_str());*/
}

template <class T>
void Function<T>::exportStyle(StyleSheet *ss)
{
    ss->clear();
    ss->addStyle("%colour", "background",background.print());
    ss->addStyleT("%float", "fog",fog);
    ss->addStyleT("%custom", "light",light->print());
    ss->addStyleT("%float", "ambient",ambientLight);
    /*ss->addStyleT("blur0",blur0);
    ss->addStyleT("blur1",blur1);
    ss->addStyleT("blur2",blur2);*/
}

template <class T>
class SquareGrid : public Function<T>
{
public:
  Colour colour;
  Colour colour2;
  T size;
  T space;
  void gets(VRam&, int, int, T*, float*, bool*, bool*, int, bool*);
  SquareGrid(int,VRam*,int);
  void setStyle(StyleSheet *ss);
  void exportStyle(StyleSheet*);
  void set(StyleSheet*);
  void exportParam(StyleSheet*);
};

template <class T> void SquareGrid<T>::set(StyleSheet *e) {
  Function<T>::set(e);
  space = matof(e->getValue("space","0.8").c_str());
  size = matof(e->getValue("size","1").c_str());
}

template <class T> void SquareGrid<T>::exportParam(StyleSheet *e) {
  Function<T>::exportParam(e);
  e->addStyleT("space",space);
  e->addStyleT("size",size);
}

template <class T> SquareGrid<T>::SquareGrid(int d, VRam *vr, int numThreads) : Function<T>(d,vr,false,numThreads), space(0.8), size(1) {}

template <class T>
void SquareGrid<T>::setStyle(StyleSheet *ss)
{
    Function<T>::setStyle(ss);
    if (ss->isStyle("colour1")) colour.parse(ss->getValue("colour1"));
    else colour.parse("#ff0000ff");
    if (ss->isStyle("colour2")) colour2.parse(ss->getValue("colour2"));
    else colour2.parse("#00ff00ff");
}

template <class T>
void SquareGrid<T>::exportStyle(StyleSheet *ss)
{
  Function<T>::exportStyle(ss);
  ss->addStyle("%colour", "colour1",colour.print());
  ss->addStyle("%colour", "colour2",colour2.print());
}

template <class T>
void SquareGrid<T>::gets(VRam &vr, int dim, int n, T* vectors, float * output, bool *leave, bool *pause, int thid, bool *prev)
{
  for (int i = 0; i < n; ++i) {
    T ss = size;
    if (dim > 3) ss *= vectors[i*dim+3] +1.0;
    T m = 0;
    for (int j = 0; j < dim; ++j) m += vectors[i*dim+j]*vectors[i*dim+j];
    Colour ret;
    T x = vectors[i*dim  ];
    T y = vectors[i*dim+1];
    T z = vectors[i*dim+2];
    T f = (y*y + z*z - x*x)/ss*ss;
    if (f < 0) f = 0;
    else if (f > 1) f = 1;
    x /= ss;
    y /= ss;
    z /= ss;
    x = abs(x);
    y = abs(y);
    z = abs(z);
    if (x > 1 || y > 1 || z > 1) ret.a = 0;
    else {
      T limit = space;
      T blue =  clamp01(vectors[i*dim+3]/ss);
      if (x > limit && y > limit ||
	  x > limit && z > limit ||
	  y > limit && z > limit) ret = Colour(lerp(colour.r,colour2.r,f),lerp(colour.g,colour2.g,f), lerp<float>(lerp(colour.b,colour2.b,f),1.0,blue), 1);
      else ret.a = 0;
    }
    
    if (ret.a == 0) ret = Colour(0,0,0,0);
    output[i*4  ] = ret.r;
    output[i*4+1] = ret.g;
    output[i*4+2] = ret.b;
    output[i*4+3] = ret.a;
    while (*pause && !(*leave)) {
      std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
    }
    if (*leave) break;
  }
}

template <class T>
class JuliaSet : public Function<T>
{
public:
  Colour colour1;
  Colour colour2;
  Colour colour3;
  pair<float,float> saturation;
  pair<float,float> brightness;
  pair<float,float> transparency;
  int iterations;
  T limit;
  void gets(VRam&, int, int, T*, float*, bool*, bool*, int, bool*);
  JuliaSet(int,VRam*,int);
  void setStyle(StyleSheet *ss);
  void exportStyle(StyleSheet*);
  void set(StyleSheet*);
  void exportParam(StyleSheet*);
};

template <class T> void JuliaSet<T>::set(StyleSheet *e) {
  Function<T>::set(e);
  iterations = atoi(e->getValue("iterations","20").c_str());
  limit = matof(e->getValue("limit","10").c_str());
}

template <class T> void JuliaSet<T>::exportParam(StyleSheet *e) {
  Function<T>::exportParam(e);
  e->addStyleT("%int", "iterations",iterations);
  e->addStyleT("%float", "limit", limit);
}

template <class T> JuliaSet<T>::JuliaSet(int d, VRam *vr, int numThreads) : Function<T>(d,vr,false,numThreads), saturation(make_pair(0.0f,0.0f)), brightness(make_pair(0.0f,0.0f)), transparency(make_pair(1.0f,0.0f)), iterations(20), limit(10) {}

template <class T>
void JuliaSet<T>::setStyle(StyleSheet *ss)
{
    Function<T>::setStyle(ss);
    if (ss->isStyle("colour1")) colour1.parse(ss->getValue("colour1"));
    else colour1.parse("#ff0000ff");
    if (ss->isStyle("colour2")) colour2.parse(ss->getValue("colour2"));
    else colour2.parse("#0000ffff");
    if (ss->isStyle("colour3")) colour3.parse(ss->getValue("colour3"));
    else colour3.parse("#0000ffff");
}

template <class T>
void JuliaSet<T>::exportStyle(StyleSheet *ss)
{
  Function<T>::exportStyle(ss);
  ss->addStyle("%colour", "colour1",colour1.print());
  ss->addStyle("%colour", "colour2",colour2.print());
  ss->addStyle("%colour", "colour3",colour3.print());
  ss->addStyleT("%float", "saturation0",saturation.first);
  ss->addStyleT("%float", "saturation1",saturation.second);
  ss->addStyleT("%float", "brightness0",brightness.first);
  ss->addStyleT("%float", "brightness1",brightness.second);
  ss->addStyleT("%float", "transparecny0",transparency.first);
  ss->addStyleT("%float", "transparecny1",transparency.second);
}

template <class T>
void JuliaSet<T>::gets(VRam &vr, int dim, int n, T* vectors, float * output, bool * leave, bool *pause, int thid, bool *prev)
{
  for (int i = 0; i < n; ++i) {
    complex<T> z,c;
    z = complex<T>(vectors[i*dim],sqrt(vectors[i*dim+1]*vectors[i*dim+1] + vectors[i*dim+2]*vectors[i*dim+2]));
    c = complex<T>(vectors[i*dim+3], vectors[i*dim+4]);
    
    int num;
    
    for (num = 0; num < iterations; ++num) {
      z = z*z + c;
      if (z.real()*z.real() + z.imag()*z.imag() > limit*limit) break;
    }
    float f = (float)num/iterations;
    f = 1.0f-f;
    
    Colour ret;
    if (num == iterations) {
      ret = colour1;
    }
    else {
      ret = lerp_col(colour2, colour3, f);
      ret = adjustBrightness(ret,lerp(brightness.first, brightness.second, f));
      ret = adjustSaturate(ret,lerp(saturation.first, saturation.second, f));
      ret = adjustTransparency(ret, lerp(transparency.first, transparency.second, f));
    }
    
    output[i*4  ] = ret.r;
    output[i*4+1] = ret.g;
    output[i*4+2] = ret.b;
    output[i*4+3] = ret.a;
    while (*pause && !(*leave)) {
      std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
    }
    if (*leave) break;
  }
}

template <class T>
class CustomFunction : public Function<T>
{
  vector<FunctionClass<T>*> functions;
  //vector<ParameterHolder<T>> parameters;
  //vector<ParameterHolder<T>> styles;
  FunctionClass<T>* main;
  FunctionClass<T>* shader;
  void clear();
  bool opened;
  void setParam(StyleSheet*);
  string laststatus;
  ScriptParser<T> *parser;
  string package;
  VRam styleRam;
public:
  void gets(VRam&, int, int, T*, float*, bool*, bool*, int, bool*);
  CustomFunction(int,VRam*,string,bool,int);
  CustomFunction(StyleSheet*,int,double,VRam*,string,bool,int);
  virtual ~CustomFunction();
  void setStyle(StyleSheet*);
  void exportStyle(StyleSheet*);
  void set(StyleSheet*);
  void exportParam(StyleSheet*);
  string getStatus() {return Function<T>::getStatus() + "\n" + laststatus;}
  virtual void preCalc(VRam&,int);
  virtual void shade(VRam&, int, int, LightData<T>**, int, float***, bool*, bool*);
  virtual void collectFiles(vector<string> &files) const {parser->collectFiles(files);}
};
template <class T> CustomFunction<T>::CustomFunction(int d, VRam *vr, string package, bool preview, int numThreads) : Function<T>(d,vr,preview,numThreads), opened(false), laststatus(""), parser(NULL), package(package), styleRam(vr->size()) {}
template <class T> CustomFunction<T>::~CustomFunction() {clear();}
template <class T> CustomFunction<T>::CustomFunction(StyleSheet *e, int dim, double fog, VRam *vr, string package, bool preview, int numThreads) : Function<T>(dim,fog,vr,preview,numThreads), opened(false), laststatus(""), parser(NULL), package(package), styleRam(vr->size()) {set(e);}

template <class T>
void CustomFunction<T>::setStyle(StyleSheet *ss)
{
    styleRam.clear();
  Function<T>::setStyle(ss);
  VRam temp(Function<T>::func_vram->size());
  for (int i = 0; i < parser->styles.size(); ++i) {
    if (!parser->styles[i].second->isKey("id")) throw CompileError(PARAM_NO_ID, "Style parameter doesn't have an id",ERROR_CONTENT);
    string sid = parser->styles[i].second->getAttribute("id","unknown_style");
    string value;
    if (ss->isStyle(sid)) value = ss->getValue(sid);
    else value = parser->styles[i].second->getAttribute("defaultvalue","");
    string type = parser->styles[i].second->getAttribute("valuetype","");
    pair<string, void*> val;

    val = parser->evaluate(&temp,&styleRam,value,Function<T>::numThreads);

    /*try {
      FunctionParseInput<T> pdata(NULL,NULL,NULL,0,"console",0,0,0,NULL,NULL,Function<T>::numThreads);
      CustomFunctionClass<T>* func = new CustomFunctionClass<T> ("-","->*","() " + value, Function<T>::getDim(), &temp, pdata);
      vector<Function_const_value<T>*> parameters;
      vector<pair<FunctionClass<T>*,string>> rootArgs;
      func->parse(FunctionParseInput<T>(NULL,NULL,&functions,Function<T>::getDim(), pdata.filename, pdata.line, pdata.col,0,NULL,NULL,Function<T>::numThreads),parameters,rootArgs);
      if (func->getOutConstructor()->getRoot()->toString() != type) throw CompileError(WRONG_TYPE_PATTERN, "Pattern has the wrong type", ERROR_CONTENT);
      val = func->calc(NULL,NULL,temp,0);
    } catch (int e) {
      throw CompileError(INVALID_PARAM, "Invalid parameter",ERROR_CONTENT);
    }*/
    parser->styles[i].s = value;
    //cout<<"value evaluated: "<<value<<endl;
    //cout<<val.first<<": "<<*((T*)val.second)<<endl;
    /*if (type == TYPE_BOOL_SIGN) *((bool*)parser->styles[i].first->value) = *((bool*)val);
    else if (type == TYPE_INT_SIGN) *((int*)parser->styles[i].first->value) = *((int*)val);
    else if (type == TYPE_REAL_SIGN) *((T*)parser->styles[i].first->value) = *((T*)val);
    else if (type == TYPE_COLOUR_SIGN) *((Colour*)parser->styles[i].first->value) = *((Colour*)val);
    else if (type == TYPE_CPLX_SIGN) *((complex<T>*)parser->styles[i].first->value) = *((complex<T>*)val);
    else if (type == TYPE_VECTOR_SIGN) for (int j = 0; j < Function<T>::getDim(); ++j) ((T*)parser->styles[i].first->value)[j] = ((T*)val)[j];
    else if (type == TYPE_MATRIX_SIGN) for (int j = 0; j < Function<T>::getDim(); ++j) for (int k = 0; k < Function<T>::getDim(); ++k) ((T**)parser->styles[i].first->value)[j][k] = ((T**)val)[j][k];*/
    //else if (type == TYPE_ARRAY_SIGN) *((pair<void*,void*>*)styles[i].first->value) = *((pair<void*,void*>*)val);
    if (type == "" || type == val.first) parser->styles[i].first->value = val.second;
    else throw CompileError(INVALID_PARAM, "Parameter/Style has the wrong type: " + sid + " (" + type + " expected, " + val.first + " found)",ERROR_CONTENT);
  }
}


template <class T> void CustomFunction<T>::exportStyle(StyleSheet *ss) {
  Function<T>::exportStyle(ss);
  for (int i = 0; i < parser->styles.size(); ++i) {
    if (!parser->styles[i].second->isKey("id")) throw CompileError(PARAM_NO_ID, "Style parameter doesn't have an id",ERROR_CONTENT);
    string type = parser->styles[i].second->getAttribute("valuetype","");
    ss->addStyle(type, parser->styles[i].second->getAttribute("id",""),parser->styles[i].s);
  }
}

template <class T> void CustomFunction<T>::exportParam(StyleSheet *ss) {
  Function<T>::exportParam(ss);
  for (int i = 0; i < parser->parameters.size(); ++i) {
    if (!parser->parameters[i].second->isKey("id")) throw CompileError(PARAM_NO_ID, "Parameter parameter doesn't have an id",ERROR_CONTENT);
    string type = parser->parameters[i].second->getAttribute("valuetype","");
    ss->addStyle(type, parser->parameters[i].second->getAttribute("id",""),parser->parameters[i].s);
  }
}


template <class T> void CustomFunction<T>::setParam(StyleSheet *ss) {
  VRam temp(Function<T>::func_vram->size());
  for (unsigned int i = 0; i < parser->parameters.size(); ++i) {
    if (!parser->parameters[i].second->isKey("id")) throw CompileError(PARAM_NO_ID, "Parameter doesn't have an id",ERROR_CONTENT);

    string sid = parser->parameters[i].second->getAttribute("id","unknown_style");
    string value;
    if (ss->isStyle(sid)) value = ss->getValue(sid);
    else value = parser->parameters[i].second->getAttribute("defaultvalue","");
    string type = parser->parameters[i].second->getAttribute("valuetype","");
    pair<string, void*> val;

    val = parser->evaluate(&temp,&styleRam,value,Function<T>::numThreads);

    parser->parameters[i].s = value;

    if (type == "" || type == val.first) parser->parameters[i].first->value = val.second;
    else throw CompileError(INVALID_PARAM, "Parameter/Style has the wrong type: " + sid + " (" + type + " expected, " + val.first + " found)",ERROR_CONTENT);

    /*string value = e->getValue(parser->parameters[i].second->getAttribute("id","unknown_style"),parser->parameters[i].second->getAttribute("defaultvalue",""));
    string type = parser->parameters[i].second->getAttribute("valuetype","");
    void *val;
    try {
      FunctionParseInput<T> pdata(NULL,NULL,NULL,0,"console",0,0,0,NULL,NULL,Function<T>::numThreads);
      CustomFunctionClass<T>* func = new CustomFunctionClass<T> ("-","->*","() " + value, Function<T>::getDim(), &temp, pdata, NULL);
      vector<Function_const_value<T>*> parameters;
      vector<pair<FunctionClass<T>*,string>> rootArgs;
      func->parse(FunctionParseInput<T>(NULL,NULL,&functions,Function<T>::getDim(),"",0,0,0,NULL,NULL,Function<T>::numThreads),parameters,rootArgs);
      if (func->getOutConstructor()->getRoot()->toString() != type) throw CompileError(WRONG_TYPE_PATTERN, "Pattern has the wrong type", ERROR_CONTENT);
      val = func->calc(NULL,NULL,temp,0);
    } catch (int e) {
      throw CompileError(INVALID_PARAM, "Invalid parameter",ERROR_CONTENT);
    }
    parser->parameters[i].s = value;
    if (type == TYPE_BOOL_SIGN) *((bool*)parser->parameters[i].first->value) = *((bool*)val);
    else if (type == TYPE_INT_SIGN) *((int*)parser->parameters[i].first->value) = *((int*)val);
    else if (type == TYPE_REAL_SIGN) *((T*)parser->parameters[i].first->value) = *((T*)val);
    else if (type == TYPE_COLOUR_SIGN) *((Colour*)parser->parameters[i].first->value) = *((Colour*)val);
    else if (type == TYPE_CPLX_SIGN) *((complex<T>*)parser->parameters[i].first->value) = *((complex<T>*)val);
    else if (type == TYPE_VECTOR_SIGN) for (int j = 0; j < Function<T>::getDim(); ++j) ((T*)parser->parameters[j].first->value)[i] = ((T*)val)[j];
    else if (type == TYPE_MATRIX_SIGN) for (int j = 0; j < Function<T>::getDim(); ++j) for (int k = 0; k < Function<T>::getDim(); ++k) ((T**)parser->parameters[i].first->value)[j][k] = ((T**)val)[j][k];
    else throw CompileError(INVALID_PARAM, "Invalid parameter",ERROR_CONTENT);*/
  }
}
template <class T> void CustomFunction<T>::clear()
{
  //Function<T>::func_vram->clear();
  if (parser != NULL) delete parser;
  parser = NULL;
}
template <class T> void CustomFunction<T>::set(StyleSheet *e)
{
  Function<T>::set(e);
  if (opened) {
    setParam(e);
    return;
  }
  opened = true;
  clear();
  parser = new ScriptParser<T>(package,Function<T>::func_vram,Function<T>::getDim(),Function<T>::preview,Function<T>::numThreads);
  main = parser->getMain();
  shader = parser->getShader();
  if (main == NULL) throw Exception(MISSING_MAIN, "'main' is not defined");
  LOG<<"Script parsed"<<endl;
  if (Function<T>::scriptNot != NULL) Function<T>::scriptNot->notify((void*)Function<T>::func_vram);
  setParam(e);
}

template <class T>
void CustomFunction<T>::preCalc(VRam &vram, int thid) {
  vram.setStart(0);
  vram.clear();
  parser->preCalc(vram,thid);
  vram.setStart(vram.getAllocated());
  main->updateCamera(NULL);
  main->updateCamera(Function<T>::cam);
  if (shader != NULL) {
    shader->updateCamera(NULL);
    shader->updateCamera(Function<T>::cam);
  }
}

template <class T>
void CustomFunction<T>::gets(VRam &vram, int dim, int n, T *vectors, float *output, bool *leave, bool *pause, int thid, bool *prev)
{
    //if (thid == 0) if (Function<T>::runtimeNot != NULL) Function<T>::runtimeNot->notify((void*)(&vram));
  //long long mx = 0;
  //int size = vram.size();
  for (int i = 0; i < n; ++i) {
    if (prev != NULL && prev[i]) {
      output[i*4+3] = 1.0;
      continue;
    }
    vram.clear();
    Colour *ret = (Colour*)main->calc(&vectors[i*dim], NULL, vram, thid);
    output[i*4  ] = ret->r;
    output[i*4+1] = ret->g;
    output[i*4+2] = ret->b;
    output[i*4+3] = ret->a;
    while (*pause && !(*leave)) {
      std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
    }
    if (*leave) break;
    //if (vram.getAllocated() > mx) mx = vram.getAllocated();
  }
    if (vram.getMax() > Function<T>::vrammax) {
        Function<T>::vrammax = vram.getMax();
        if (Function<T>::runtimeNot != NULL) Function<T>::runtimeNot->notify(&vram);
    }
}

template <class T> void CustomFunction<T>::shade(VRam &vram, int dim, int thid, LightData<T> **data, int dn, float ***colours, bool *leave, bool *pause)
{
  if (shader == NULL) Function<T>::shade(vram,dim,thid,data,dn,colours,leave,pause);
  else {
    void *dat[3];
    //T vec[dim];
    //for (int i = 3; i < dim; ++i) vec[i] = 0;
    float col[4];
    dat[0] = col;
    //dat[1] = vec;
    col[3] = 1.0;
    for (int i = 0; i < dn; ++i) {
      col[0] = colours[data[i]->id][data[i]->x][data[i]->y*4  ];
      col[1] = colours[data[i]->id][data[i]->x][data[i]->y*4+1];
      col[2] = colours[data[i]->id][data[i]->x][data[i]->y*4+2];
      //vec[0] = data[i].nx;
      //vec[1] = data[i].ny;
      //vec[2] = data[i].nz;
      dat[1] = data[i]->dir;
      dat[2] = data[i]->pos;
      vram.clear();
      Colour *ret = (Colour*)shader->calc(dat, NULL, vram, thid);
      colours[data[i]->id][data[i]->x][data[i]->y*4  ] = ret->r;
      colours[data[i]->id][data[i]->x][data[i]->y*4+1] = ret->g;
      colours[data[i]->id][data[i]->x][data[i]->y*4+2] = ret->b;
      //colours[data[i].id][data[i].x][data[i].y*4+3] = ret->a;
      while (*pause && !(*leave)) {
        std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
      }
      if (*leave) break;
    }
  }
}

template <class T> void Function<T>::getFunctionList(vector<string> &vec)
{
  vec.push_back("squarefunc");
  vec.push_back("julia");
  vec.push_back("<package.name>");
}

template <class T> Function<T>* Function<T>::getFunction(string filename, StyleSheet *e, int dim, double fog, VRam *vr, bool preview, int numThreads)
{
  /*if (name == "squarefunc") return new SquareGrid<T>(e,vr,numThreads);
  if (name == "julia") return new JuliaSet<T>(e,vr,numThreads);*/
  return new CustomFunction<T>(e,dim,fog,vr,filename,preview,numThreads);
}
