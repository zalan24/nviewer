#pragma once

const string FORMAT_NAME = "[a-zA-Z0-9_\\.]";
const string FORMAT_TYPE = "[0-9a-zA-Z\\(\\)\\,_\\[\\]: ]";
const string FORMAT_PACKAGE = "^[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)+$";
const string FORMAT_PARAMETER = "^"+FORMAT_TYPE+"+ "+FORMAT_NAME+"+( )*=( )*.+$";
const string FROMAT_DEFINITION_CONST = "^"+FORMAT_TYPE+"+ "+FORMAT_NAME+"+( )*$";
const string FROMAT_DEFINITION_FUNCTION = "^"+FORMAT_TYPE+"+( )*->( )*"+FORMAT_TYPE+"+( )*"+FORMAT_NAME+"+( )*\\([a-zA-Z0-9_\\(\\), ]+\\)$";
const string FORMAT_DIMENSIONS = "^\\[[0-9]+\\.\\.[0-9]*\\]$";
const string FORMAT_TEXTURE = "^[a-zA-Z0-9_]+:[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)+$";
const string FORMAT_TYPEDEF = "^"+FORMAT_NAME+"+="+FORMAT_TYPE+"+$";

const string FROMAT_DEFINITION_CONST_GENERIC = "^"+FORMAT_NAME+"+( )*$";
const string FROMAT_DEFINITION_FUNCTION_GENERIC = "^"+FORMAT_NAME+"+( )*\\([a-zA-Z0-9_\\(\\), ]+\\)$";

const string FORMAT_INT = "^(-)?[0-9]+$";
const string FORMAT_SESSIONAME = "^[0-9a-zA-Z_]+$";

const string FORMAT_NVWSCRIPT = "^.*\\.nvw$";

const string FORMAT_FILTER_BEGIN = "^.*";
const string FORMAT_FILTER_END = ".*$";
