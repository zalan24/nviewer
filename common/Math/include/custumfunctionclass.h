#pragma once

#include "tools.h"
#include "config.h"
#include "errors.h"

#include "common/Helper/vram.h"

#define TYPE_BOOL_NAME string("Bool")
#define TYPE_BOOL_SIGN string("B")
#define TYPE_INT_NAME string("Int")
#define TYPE_INT_SIGN string("I")
#define TYPE_REAL_NAME string("Real")
#define TYPE_REAL_SIGN string("R")
#define TYPE_CPLX_NAME string("Complex")
#define TYPE_CPLX_SIGN string("C")
#define TYPE_COLOUR_NAME string("Colour")
#define TYPE_COLOUR_SIGN string("U")
#define TYPE_VECTOR_NAME string("Vector")
#define TYPE_VECTOR_SIGN string("V")
#define TYPE_MATRIX_NAME string("Matrix")
#define TYPE_MATRIX_SIGN string("M")
#define TYPE_LIST_NAME string("Tuple")
#define TYPE_LIST_SIGN string("T")
#define TYPE_ARRAY_NAME string("[")
#define TYPE_ARRAY_SIGN string("A")
#define TYPE_FUNCTION_NAME string("Function")
#define TYPE_FUNCTION_SIGN string("F")


#define MATH_CLASS "Math"
#define ARRAY_CLASS "Array"

#define FUNCTIONTYPE_NONE 0
#define FUNCTIONTYPE_ARGDEP 1
#define FUNCTIONTYPE_PARAMDEP 2

template <class T> class FunctionClass;
template <class T> class CustomFunctionClass;
template <class T> class RootFunctionClass;
template <class T> class FunctionComposition;
template <class T> class Function_const_value;

/*class SyntacsError: public exception
{
  string text;
  string file;
  int col;
  int line;
public:
  SyntacsError(string text, string file, int line) throw() : text(text), file(file), line(line), col(0) {}
  SyntacsError(string text, string file, int line, int col) throw() : text(text), file(file), line(line), col(col) {}
  virtual const char* what() const throw()
  {
    stringstream ss;
    ss<<line;
    return (new string("Syntacs error: "+text + '\n' + "in: "+file+" at line "+ss.str()))->c_str();
  }
};

class CompileError: public exception
{
  string text;
  string file;
  int line;
  int col;
public:
  CompileError(string text, string file, int line) throw() : text(text), file(file), line(line), col(0) {}
  CompileError(string text, string file, int line, int col) throw() : text(text), file(file), line(line), col(col) {}
  virtual const char* what() const throw()
  {
    stringstream ss;
    ss<<line;
    return (new string("Compile error: "+text + '\n' + "in: "+file+" at line "+ss.str()))->c_str();
  }
};*/

template <class T> void* createCustomType(const string,int);
template <class T> void deleteCustomType(const string,void*);

template <class T>
struct FunctionParseInput
{
  VRam *temp;
  VRam *temp2;
  int dim;
  vector<FunctionClass<T>*> *functions;
  int line;
  int col;
  string filename;
  int depth;
  Element *rootElement;
  Element *ownElement;
  int threads;
  FunctionParseInput() : temp(NULL), temp2(NULL), functions(NULL), dim(3), line(1), col(1), filename(""), depth(-1), rootElement(NULL), ownElement(NULL), threads(0) {}
  FunctionParseInput(VRam *temp, VRam *temp2, vector<FunctionClass<T>*> *functions, int dim, string filename, int line, int col, int depth, Element *rootElement, Element *ownElement, int threads) : temp(temp), temp2(temp2), functions(functions), dim(dim), line(line), col(col), filename(filename), depth(depth), rootElement(rootElement), ownElement(ownElement), threads(threads) {}
};

template <class T>
class CustomType
{
public:
  virtual bool isGeneric() const = 0;
  virtual void* construct(VRam&,int) const = 0;
  //virtual void destruct(void*) const = 0;
  virtual string toString() = 0;
  //virtual void copy(void*,void*,int) const = 0;
  virtual void* copy(VRam&,void*,int) const = 0;
  virtual bool compare(void*, void*, int) const = 0;
  virtual string getNull(int length) const = 0;
};

template <class T>
class CustomTypeG : public CustomType<T>
{
public:
  bool isGeneric() const {return true;}
  string name;
  CustomTypeG (string name) : name(name) {}
  void* construct(VRam&, int) const {return NULL;}
  string toString() {return name;}
  //void copy(void * a, void * b, int length) const {}
  void* copy(VRam&, void*, int) const {return NULL;}
  bool compare(void*, void*, int) const {return false;}
  string getNull(int) const {return "0";}
};

template <class T>
class CustomTypeR : public CustomType<T>
{
public:
  bool isGeneric() const {return false;}
  void* construct(VRam &vr, int) const {return vr.allocate(sizeof(T));}
  //void destruct(void *data) const {delete ((T*)data);}
  string toString() {return TYPE_REAL_SIGN;}
  //void copy(void * a, void * b, int length) const {*((T*)a) = *((T*)b);}
  void* copy(VRam &vr,void *value, int) const {
    T *ret = (T*)vr.allocate(sizeof(T));
    *ret = *((T*)value);
    return ret;
  }
  bool compare(void *a, void *b, int) const {return *((T*)a) == *((T*)b);}
  string getNull(int) const {return "0.0";}
};

template <class T>
class CustomTypeB : public CustomType<T>
{
public:
  bool isGeneric() const {return false;}
  void* construct(VRam &vr, int) const {return vr.allocate(sizeof(bool));}
  //void destruct(void *data) const {delete ((bool*)data);}
  string toString() {return TYPE_BOOL_SIGN;}
  //void copy(void * a, void * b, int length) const {*((bool*)a) = *((bool*)b);}
  void* copy(VRam &vr,void *value, int) const {
    bool *ret = (bool*)vr.allocate(sizeof(bool));
    *ret = *((bool*)value);
    return ret;
  }
  bool compare(void *a, void *b, int) const {return *((bool*)a) == *((bool*)b);}
  string getNull(int) const {return "False";}
};

template <class T>
class CustomTypeI : public CustomType<T>
{
public:
  bool isGeneric() const {return false;}
  void* construct(VRam &vr, int) const {return vr.allocate(sizeof(int));}
  //void destruct(void *data) const {delete ((int*)data);}
  string toString() {return TYPE_INT_SIGN;}
  //void copy(void * a, void * b, int length) const {*((int*)a) = *((int*)b);}
  void* copy(VRam &vr,void *value, int) const {
    int *ret = (int*)vr.allocate(sizeof(int));
    *ret = *((int*)value);
    return ret;
  }
  bool compare(void *a, void *b, int) const {return *((int*)a) == *((int*)b);}
  string getNull(int) const {return "0";}
};

template <class T>
class CustomTypeC : public CustomType<T>
{
public:
  bool isGeneric() const {return false;}
  void* construct(VRam &vr, int) const {return vr.allocate(sizeof(complex<T>));}
  //void destruct(void *data) const {delete ((complex<T>*)data);}
  string toString() {return TYPE_CPLX_SIGN;}
  //void copy(void * a, void * b, int length) const {*((complex<T>*)a) = *((complex<T>*)b);}
  void* copy(VRam &vr,void *value, int) const {
    complex<T> *ret = (complex<T>*)vr.allocate(sizeof(complex<T>));
    *ret = *((complex<T>*)value);
    return ret;
  }
  bool compare(void *a, void *b, int) const {return *((complex<T>*)a) == *((complex<T>*)b);}
  string getNull(int) const {return "(0+i)";}
};

template <class T>
class CustomTypeU : public CustomType<T>
{
public:
  bool isGeneric() const {return false;}
  void* construct(VRam &vr, int) const {return vr.allocate(sizeof(Colour));}
  //void destruct(void *data) const {delete ((Colour*)data);}
  string toString() {return TYPE_COLOUR_SIGN;}
  //void copy(void * a, void * b, int length) const {*((Colour*)a) = *((Colour*)b);}
  void* copy(VRam &vr,void *value, int) const {
    Colour *ret = (Colour*)vr.allocate(sizeof(Colour));
    *ret = *((Colour*)value);
    return ret;
  }
  bool compare(void *a, void *b, int) const {return *((Colour*)a) == *((Colour*)b);}
  string getNull(int) const {return "(#00000000)";}
};

template <class T>
class CustomTypeV : public CustomType<T>
{
public:
  bool isGeneric() const {return false;}
  void* construct(VRam &vr, int len) const {return vr.allocate(sizeof(T)*len);}
  //void destruct(void *data) const {delete [] ((T*)data);}
  string toString() {return TYPE_VECTOR_SIGN;}
  /*void copy(void * av, void * b, int length) const {
    T* a = (T*)av;
    for (int i = 0; i < length; ++i) a[i] = ((T*)b)[i];
  }*/
  void* copy(VRam &vr,void *val, int length) const {
    T* value = (T*)val;
    T *ret = (T*)vr.allocate(sizeof(T)*length);
    for (int i = 0; i < length; ++i) ret[i] = value[i];
    return ret;
  }
  bool compare(void *a, void *b, int length) const {
    T *av = (T*)a;
    T *bv = (T*)b;
    for (int i = 0; i < length; ++i) if (av[i] != bv[i]) return false;
    return true;
  }
  string getNull(int length) const {
    string s = "";
    for (int i = 0; i < length; ++i) s += ",0.0";
    if (s.length() > 0) s = s.substr(1,-1);
    return "{"+s+"}";
  }
};

template <class T>
class CustomTypeM : public CustomType<T>
{
public:
  bool isGeneric() const {return false;}
  void* construct(VRam &vr, int len) const {
    T** ret = (T**)vr.allocate(sizeof(T*)*len);
    for (int i = 0; i < len; ++i) ret[i] = (T*)vr.allocate(sizeof(T)*len);
    return (void*)ret;
  }
  string toString() {return TYPE_MATRIX_SIGN;}
  /*void copy(void * av, void * b, int length) const {
    T** a = (T**)av;
    for (int i = 0; i < length; ++i) for (int j = 0; j < length; ++j) a[i][j] = ((T**)b)[i][j];
  }*/
  void* copy(VRam &vr,void *val, int length) const {
    T **value = (T**)val;
    T **ret = (T**)vr.allocate(sizeof(T*)*length);
    for (int i = 0; i < length; ++i) {
      ret[i] = (T*)vr.allocate(sizeof(T)*length);
      for (int j = 0; j < length; ++j) ret[i][j] = value[i][j];
    }
    return ret;
  }
  bool compare(void *a, void *b, int length) const {
    T **av = (T**)a;
    T **bv = (T**)b;
    for (int i = 0; i < length; ++i) for (int j = 0; j < length; ++j) if (av[i][j] != bv[i][j]) return false;
    return true;
  }
  string getNull(int length) const {
    string s = "";
    for (int i = 0; i < length; ++i) s += ",0.0";
    if (s.length() > 0) s = s.substr(1,-1);
    s = "{"+s+"}";
    string s2 = "";
    for (int i = 0; i < length; ++i) s2 += ","+s;
    if (s2.length() > 0) s2 = s2.substr(1,-1);
    return "{"+s2+"}";
  }
};

template <class T>
class CustomTypeList : public CustomType<T>
{
public:
  int length;
  vector<CustomType<T>*> types;
  bool isGeneric() const {
    for (unsigned int i = 0; i < types.size(); ++i) if (types[i]->isGeneric()) return true;
    return false;
  }
  void* construct(VRam &vr, int len) const {
    void **ret = (void**)vr.allocate(sizeof(void*)*length);
    for (int i = 0; i < length; ++i) ret[i] = types[i]->construct(vr, len);
    return ret;
  }
  /*void destruct(void *data) const {
    for (int i = 0; i < length; ++i) types[i]->destruct(((void**)data)[i]);
    delete [] (void**)data;
  }*/
  string toString() {
    stringstream ss;
    ss<<TYPE_LIST_SIGN<<"("<<types[0]->toString();
    for (int i = 1; i < length; ++i) ss<<","<<types[i]->toString();
    ss<<")";
    return ss.str();
  }
  /*void copy(void * x, void * y, int len) const {
    void **a = (void**)x;
    void **b = (void**)y;
    for (int i = 0; i < length; ++i) types[i]->copy(a[i], b[i], len);
  }*/
  void* copy(VRam &vr,void *val, int len) const {
    void **value = (void**)val;
    void **ret = (void**)vr.allocate(sizeof(void*)*length);
    for (int i = 0; i < length; ++i) ret[i] = types[i]->copy(vr,value[i],len);
    return ret;
  }
  bool compare(void *a, void *b, int len) const {
    void **av = (void**)a;
    void **bv = (void**)b;
    for (int i = 0; i < length; ++i) if (!types[i]->compare(av[i],bv[i],len)) return false;
    return true;
  }
  string getNull(int length) const {
    string s = "";
    for (int i = 0; i < length; ++i) s += ","+types[i]->getNull(length);
    if (s.length() > 0) s = s.substr(1,-1);
    return "("+s+")";
  }
};

template <class T>
class CustomTypeA : public CustomType<T>
{
public:
  CustomType<T>* type;
  bool isGeneric() const {
    return type->isGeneric();
  }
  void* construct(VRam &vr, int) const {
    pair<void*,void*> *ret = (pair<void*,void*>*)vr.allocate(sizeof(pair<void*,void*>));
    ret->first = NULL;
    ret->second = NULL;
    return ret;
  }
  string toString() {
    if (type == NULL) return TYPE_ARRAY_SIGN;
    return TYPE_ARRAY_SIGN+type->toString();
  }
  void* copy(VRam &vr, void *val, int length) const {
    pair<void*,void*> *value = (pair<void*,void*>*)val;
    pair<void*,void*> *ret = (pair<void*,void*>*)construct(vr,length);
    if (value->first == NULL) ret->first = NULL;
    else ret->first = type->copy(vr,value->first,length);
    if (value->second == NULL) ret->second = NULL;
    else ret->second = copy(vr,value->second,length);
    return ret;
  }
  bool compare(void *a, void *b, int len) const {
    pair<void*,void*> *av = (pair<void*,void*>*)a;
    pair<void*,void*> *bv = (pair<void*,void*>*)b;
    if (av->first == NULL && bv->first == NULL) return true;
    if (av->first == NULL || bv->first == NULL) return false;
    if (!type->compare(av->first, bv->first, len)) return false;
    if (av->second == NULL || bv->second == NULL) {
      if (av->second == NULL && bv->second == NULL) return true;
      return false;
    }
    return compare(av->second,bv->second,len);
  }
  string getNull(int) const {return "[]";}
};

template <class T>
class CustomTypeF : public CustomType<T>
{
public:
  CustomType<T>* type1;
  CustomType<T>* type2;		//f : type1 -> type2
  bool isGeneric() const {
    return type1->isGeneric() || type2->isGeneric();
  }
  void* construct(VRam &vr, int) const {
    return vr.allocate(sizeof(FunctionClass<T>*));
  }
  string toString() {
    return TYPE_FUNCTION_SIGN+"("+type1->toString()+":"+type2->toString()+")";
  }
  void* copy(VRam &vr,void *value, int) const {
    FunctionClass<T> **ret = (FunctionClass<T>**)vr.allocate(sizeof(FunctionClass<T>*));
    *ret = *((FunctionClass<T>**)value);
    return ret;
  }
  bool compare(void *a, void *b, int) const {return *((FunctionClass<T>**)a) == *((FunctionClass<T>**)b);}
  string getNull(int) const {return "@0";}
};

template <class T>
class CustomTypeConstructor
{
  CustomType<T> *root;
  int len;
  FunctionClass<T> *f;
public:
  bool isGeneric() {
    //if (root == NULL) cout<<f->getName()<<endl;
    return root->isGeneric();
  }
  static CustomType<T> *createType(VRam&,const string,FunctionParseInput<T>&);
  CustomTypeConstructor(VRam &vr, const string pattern, int len,FunctionParseInput<T> &data, FunctionClass<T> *f) : len(len), f(f) {
    root = createType(vr,pattern,data);
    /*if (root == NULL) {
      throw Exception("Invalid type (" + pattern + ")");
    }*/
  }
  //~CustomTypeConstructor() {if (root != NULL) delete root;}
  void* construct(VRam &vr) const {return root->construct(vr,len);}
  void destruct(void* data) const {/*root->destruct(data);*/}
  CustomType<T>* getRoot() const {return root;}
};

template <class T>
class CustomResult
{
public:
  void *data;
  CustomType<T> *constructor;
  void* getData() const {return data;}
  CustomType<T>* getConstructor() const {return constructor;}
  CustomResult(void *data, CustomType<T> *constructor) : data(data), constructor(constructor) {}
  //~CustomResult() {if (data != NULL && constructor != NULL) constructor->destruct(data);}
};

struct FunctionRawData
{
  string name;
  string content;
  FunctionRawData() : name(""), content("") {}
  FunctionRawData(string name, string content) : name(name), content(content) {}
};

template <class T>
class FunctionClass
{
protected:
  bool rootSet;
  bool rec;
  bool rootUpdated;
  bool generic;
  string name;
  string input;
  string output;
  string ooutput;
  int dim;
  int constant;
  bool constant_checked;
  vector<CustomTypeConstructor<T>*> inConstructor;
  CustomTypeConstructor<T> *outConstructor;
  void buildInConstructor(string input) {
    vector<string> parts;
    getParts(input,parts,',',0);
    for (unsigned int i = 0; i < parts.size(); ++i) {
      CustomTypeConstructor<T> *c = (CustomTypeConstructor<T>*)func_vram->allocate(sizeof(CustomTypeConstructor<T>));
      c = new (c) CustomTypeConstructor<T>(*func_vram,parts[i],dim,parse_data,this);
      inConstructor.push_back(c);
    }
  }
  void setUp(string i,string o) {
    input = i;
    ooutput = output = o;
    buildInConstructor(input);
    if (output != "*") {
      outConstructor = (CustomTypeConstructor<T>*)func_vram->allocate(sizeof(CustomTypeConstructor<T>));
      outConstructor = new (outConstructor) CustomTypeConstructor<T>(*func_vram,output,dim,parse_data,this);
    }
  }
  void setUp() {
    if (inConstructor.size() == 0) buildInConstructor(input);
    if (outConstructor == NULL) {
      outConstructor = (CustomTypeConstructor<T>*)func_vram->allocate(sizeof(CustomTypeConstructor<T>));
      outConstructor = new (outConstructor) CustomTypeConstructor<T>(*func_vram,output,dim,parse_data,this);
    }
  }
  VRam *func_vram;
  void *camera;
  RootFunctionClass<T> *rootFunction;
public:
  bool isRecursive() const {return rec;}
  void setRecursion(bool r) {rec = r;}
  bool amiconstant() const {return constant;}
  void *mark;
  bool topfunction;
  void setRootFunction_Manually(RootFunctionClass<T> *r) {rootFunction = r;}
  FunctionParseInput<T> parse_data;
  bool collapsed;
  void setGeneric() {generic = true;}
  static string getTypes(const string);
  static bool isTypeSign(string s)
  {
    if (s == TYPE_BOOL_SIGN || s == TYPE_INT_SIGN || s == TYPE_REAL_SIGN || s == TYPE_CPLX_SIGN || s == TYPE_COLOUR_SIGN || s == TYPE_VECTOR_SIGN || s == TYPE_MATRIX_SIGN) return true;
    return false; //Complex types don't count
  }
  static bool isNameAllowed(string s)
  {
    string allowed = "_.";
    for (int i = 0; i < s.length(); ++i) {
      if (s[i] >= 'a' && s[i] <= 'z') continue;
      if (s[i] >= 'A' && s[i] <= 'Z') continue;
      if (i > 0 && s[i] >= '0' && s[i] <= '9') continue;
      if (allowed.find(s[i]) != -1) continue;
      return false;
    }
    return true;
  }
  virtual bool isGeneric() const {return false;}
  static bool matchType(string have, string need) {
    if (have == need) return true;
    vector<string> hs,ns;
    getParts(have,hs,',',0);
    getParts(need,ns,',',0);
    if (hs.size() != ns.size()) return false;
    if (hs.size() > 1) {
      for (unsigned int i = 0; i < hs.size(); ++i) if (!matchType(hs[i],ns[i])) return false;
      return true;
    }
    if (have == TYPE_ARRAY_SIGN && need[0] == TYPE_ARRAY_SIGN[0]) return true;
    if (have[0] == TYPE_LIST_SIGN[0] && need[0] == TYPE_LIST_SIGN[0]) {
      return matchType(have.substr(2,have.length()-3),need.substr(2,need.length()-3));
    }
    return false;
  }
  VRam *getVram() const {return func_vram;}
  int getDim() const {return dim;}
  vector<FunctionClass<T>*> parameters;
  string getName() const {return name;}
  int getNumParameter() const {return inConstructor.size();}
  CustomTypeConstructor<T> *getInConstructor(int i) const {return inConstructor[i];}
  CustomTypeConstructor<T> *getOutConstructor() const {return outConstructor;}
  string getInput() const {return input;}
  string getOutput() const {return output;}
  FunctionClass(string name, int dim, VRam *vr) : name(name), dim(dim), func_vram(vr), parse_data(), outConstructor(NULL), collapsed(false), constant_checked(false), camera(NULL), rootFunction(NULL), rootSet(false), rec(false), rootUpdated(false), mark(NULL), topfunction(true), generic(false) {}
  FunctionClass(string name, int dim, VRam *vr, FunctionParseInput<T> &data) : name(name), dim(dim), func_vram(vr), parse_data(data), outConstructor(NULL), collapsed(false), constant_checked(false), camera(NULL), rootFunction(NULL), rootSet(false), rec(false), rootUpdated(false), mark(NULL), topfunction(true), generic(false) {}
  virtual void* calc(void*, void*, VRam&, int) = 0;
  virtual void collapseChildren(map<void*,FunctionClass<T>*>&) {}
  static FunctionClass<T>* collapse(FunctionClass<T>*, map<void*,FunctionClass<T>*>&);
  virtual FunctionClass<T>* copy() {return this;}
  void yournotconstant() {constant |= FUNCTIONTYPE_ARGDEP; constant_checked = true;}
  virtual void updateCamera(void *cam) {
    if (camera == cam) return;
    camera = cam;
    for (unsigned int i = 0; i < parameters.size(); ++i) parameters[i]->updateCamera(cam);
  }
  virtual void setRootFunction(RootFunctionClass<T> *root)
  {
    if (rootSet) return;
    rootSet = true;
    if (rootFunction == NULL) rootFunction = root;
    //cout<<"setting root function: "<<getName()<<endl;
    for (unsigned int i = 0; i < parameters.size(); ++i) parameters[i]->setRootFunction(root);
  }
  static void updateRoots(FunctionClass<T> *f, map<void*,FunctionClass<T>*> &fs)
  {
    //cout<<f->getName()<<endl;
    if (f->rootUpdated) return;
    f->rootUpdated = true;
    if (f->isGeneric()) throw Exception(UNKNOWN_ERROR,"Update root in generic method");
    RootFunctionClass<T> *rf = dynamic_cast<RootFunctionClass<T>*> (f);
    if (rf != NULL) rf->updateRoots(fs);
    for (int i = 0; i < f->getNumChildFunctions(); ++i) updateRoots(f->getChildFunction(i), fs);
  }
  static void checkTypes(FunctionClass<T> *f, void *mark)
  {
    if (f == NULL) return;
    if (f->mark == mark) return;
    f->mark = mark;
    CustomFunctionClass<T> *cfc = dynamic_cast<CustomFunctionClass<T>*> (f);
    if (cfc != NULL && !cfc->checkType()) {
      throw CompileError(TYPE_MISTMATCH, "Type mismatch", f->parse_data.filename, f->parse_data.line, f->parse_data.col);
    }
    for (int i = 0; i < f->getNumChildFunctions(); ++i) checkTypes(f->getChildFunction(i), mark);
  }
  virtual void* getData2(int thread) const {return rootFunction->getData2(thread);}
  virtual int getNumChildFunctions() {return parameters.size();}
  virtual FunctionClass<T>* getChildFunction(int i) {return parameters[i];}
  virtual void setChildFunction(int i, FunctionClass<T> *f) {parameters[i] = f;}
  static void calcRec(FunctionClass<T> *f, void *mark)
  {
    if (f == NULL) return;
    if (f->rec) return;
    void *m = f->mark;
    if (m == mark) f->rec = true;
    f->mark = mark;
    for (int i = 0; i < f->getNumChildFunctions(); ++i) {
      calcRec(f->getChildFunction(i),mark);
    }
    f->mark = m;
  }
  static int isConstant(FunctionClass<T> *f) {
    if (f == NULL) return FUNCTIONTYPE_NONE;
    if (f->isGeneric()) throw Exception(CONTS_ON_GEN,"Internal error: Unallowed operation on generic function");
    if (f->constant_checked) return f->constant;
    f->constant_checked = true;
    f->constant = f->getType();
    CustomFunctionClass<T> *cfc = dynamic_cast<CustomFunctionClass<T>*> (f);
    if (cfc != NULL) return f->constant = cfc->constant = isConstant(cfc->root);
    FunctionComposition<T> *fc = dynamic_cast<FunctionComposition<T>*> (f);
    if (fc != NULL) return fc->constant = isConstant(fc->g) | (isConstant(fc->f) & FUNCTIONTYPE_PARAMDEP);
    for (int i = 0; i < f->getNumChildFunctions(); ++i) f->constant |= isConstant(f->getChildFunction(i));
    return f->constant;
  }
  static FunctionClass<T> *constantiate(FunctionClass<T> *f, void *mark, VRam &temp, vector<FunctionClass<T>*> &paramdep) {
    if (f == NULL) return f;
    if (f->mark == mark) return f;
    f->mark = mark;
    
    
    if (f->isGeneric()) throw Exception(UNKNOWN_ERROR, "constantiate generic method");
    
    int type = isConstant(f);
    //cout<<f->getName()<<" "<<type<<endl;
    if (!(type & FUNCTIONTYPE_ARGDEP)) {
      if (type & FUNCTIONTYPE_PARAMDEP) {
	paramdep.push_back(f);
	return f;
      }
      Function_const_value<T> *ret = dynamic_cast<Function_const_value<T>*> (f);
      if (ret != NULL) return ret;
      temp.clear();
      void * value = f->calc(NULL,NULL,temp,0);
      value = f->getOutConstructor()->getRoot()->copy(*(f->func_vram), value, f->dim);
      ret = (Function_const_value<T>*)f->func_vram->allocate(sizeof(Function_const_value<T>));
      ret = new (ret) Function_const_value<T>("const",f->dim,f->func_vram,f->getOutConstructor()->getRoot()->toString(),value);
      return ret;
    }
    
    for (int i = 0; i < f->getNumChildFunctions(); ++i) f->setChildFunction(i, constantiate(f->getChildFunction(i), mark, temp, paramdep));
    return f;
  }
  static void replaceFunctions(FunctionClass<T> *f, vector<FunctionClass<T>*> &search, void *mark, vector<FunctionClass<T>*> &to) {
    if (f == NULL) return;
    if (f->mark != mark) {
      f->mark = mark;
      for (int i = 0; i < f->getNumChildFunctions(); ++i) {
	FunctionClass<T> *g = f->getChildFunction(i);
	int found = -1;
    for (unsigned int j = 0; j < search.size(); ++j) if (g == search[j]) {
	  found = j;
	  break;
	}
	if (found == -1) replaceFunctions(g, search, mark, to);
	else f->setChildFunction(i, to[found]);
      }
    }
  }
  virtual int getType() const {return FUNCTIONTYPE_NONE;}
  virtual string getRootArgsSignature() const {return "";}
  bool equals(FunctionClass<T> *f) const {
    return f->getName() == getName() && f->getInput() == getInput() && f->getRootArgsSignature() == getRootArgsSignature();
  }
  static void visit(FunctionClass<T> *f, void *mark) {
    if (f == NULL) return;
    if (f->mark == mark) return;
    f->mark = mark;
    for (int i = 0; i < f->getNumChildFunctions(); ++i) visit(f->getChildFunction(i),mark);
  }
};

template <class T>
class BranchFunctionClass : public FunctionClass<T>
{
public:
  BranchFunctionClass(string name, int dim, VRam *vram) : FunctionClass<T>(name,dim,vram) {}
  BranchFunctionClass(string name, int dim, VRam *vram, FunctionParseInput<T> &data) : FunctionClass<T>(name,dim,vram,data) {}
  virtual int getNumBranches() const = 0;
  virtual FunctionClass<T>* getBranch(int) const = 0;
  virtual bool isBranch(int) = 0;
};

template <class T>
FunctionClass<T>* FunctionClass<T>::collapse(FunctionClass<T> *f, map<void*,FunctionClass<T>*> &funcs)
{
  if (f == NULL) return f;
  //cout<<f->getName()<<endl;
  auto itr = funcs.find(f);
  if (itr != funcs.end()) return funcs[f];
  if (f->collapsed) return f;
  f->collapsed = true;
  CustomFunctionClass<T> *g = dynamic_cast<CustomFunctionClass<T>*>(f);
  if (g != NULL) {
    FunctionClass<T> *ret = g->root;
    funcs[g] = ret;
    for (int i = 0; i < ret->getNumChildFunctions(); ++i) ret->setChildFunction(i, collapse(ret->getChildFunction(i), funcs));
    //g->root = NULL;
    return collapse(ret, funcs);
  }
  for (int i = 0; i < f->getNumChildFunctions(); ++i) f->setChildFunction(i, collapse(f->getChildFunction(i), funcs));
  return f;
}

template <class T>
class Function_function : public FunctionClass<T>
{
  FunctionClass<T> *f;
  FunctionClass<T> *g;
public:
  Function_function(int dim, FunctionClass<T> *f, FunctionClass<T> *g, VRam *vr) : f(f), g(g), FunctionClass<T>("function", dim, vr) {
    CustomTypeF<T> *tp = dynamic_cast<CustomTypeF<T>*>(f->getOutConstructor()->getRoot());
    if (tp == NULL) throw exception();
    if (tp->type1->toString() != g->getOutput()) throw exception();
    FunctionClass<T>::setUp("",tp->type2->toString());
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    FunctionClass<T> **ff = (FunctionClass<T>**)f->calc(data,data2,vr,thread);
    return (*ff)->calc(g->calc(data,data2,vr,thread),NULL,vr,thread);
  }
  virtual int getType() const {return FunctionClass<T>::getType();}
  
  void collapseChildren(map<void*,FunctionClass<T>*> &fs) {
    f = FunctionClass<T>::collapse(f,fs);
    g = FunctionClass<T>::collapse(g,fs);
  }
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    f->updateCamera(cam);
    g->updateCamera(cam);
  }
  virtual void setRootFunction(RootFunctionClass<T> *root)
  {
    if (FunctionClass<T>::rootSet) return;
    FunctionClass<T>::setRootFunction(root);
    f->setRootFunction(root);
    g->setRootFunction(root);
  }
  virtual int getNumChildFunctions() {return FunctionClass<T>::getNumChildFunctions()+2;}
  virtual FunctionClass<T>* getChildFunction(int i) {
    if (i < FunctionClass<T>::getNumChildFunctions()) return FunctionClass<T>::getChildFunction(i);
    i -= FunctionClass<T>::getNumChildFunctions();
    if (i == 0) return f;
    return g;
  }
  virtual void setChildFunction(int i, FunctionClass<T> *f2) {
    if (i < FunctionClass<T>::getNumChildFunctions()) FunctionClass<T>::setChildFunction(i, f2);
    else {
      i -= FunctionClass<T>::getNumChildFunctions();
      if (i == 0) f = f2;
      else g = f2;
    }
  }
};

template <class T>
class Function_const : public FunctionClass<T>
{
public:
  Function_const(int dim, string type, VRam *vr) : FunctionClass<T>("const", dim, vr) {FunctionClass<T>::setUp("",type);}
  void* calc(void *data, void*, VRam&, int) {
    return data;
  }
  bool isConstant() {
    return false;
  }
  virtual int getType() const {return FunctionClass<T>::getType() | FUNCTIONTYPE_ARGDEP;}
};

template <class T>
class Function_const_value : public FunctionClass<T>
{
public:
  void *value;
  int type;
  Function_const_value(string name, int dim, VRam *vr, string type, void *value) : value(value), FunctionClass<T>(name, dim, vr) {
    Function_const_value<T>::type = FUNCTIONTYPE_NONE;
    FunctionClass<T>::setUp("",type);
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    return value;
  }
  virtual int getType() const {return FunctionClass<T>::getType() | type;}
};

template <class T>
class Function_const_value_from_array : public FunctionClass<T>
{
public:
  void **value;
  int index;
  Function_const_value_from_array(string name, int dim, VRam *vr, string type, void **value, int index) : FunctionClass<T>(name, dim, vr), value(value), index(index) {FunctionClass<T>::setUp("",type);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    return value[index];
  }
};

template <class T>
class Function_con_r : public FunctionClass<T>
{
  T value;
public:
  Function_con_r(int dim, VRam *vr, T x) : FunctionClass<T>(TYPE_REAL_NAME, dim, vr), value(x) {FunctionClass<T>::setUp("",TYPE_REAL_SIGN);}
  void* calc(void*, void*, VRam&, int) {
    return (void*)&value;
  }
};

template <class T>
class Function_con_b : public FunctionClass<T>
{
  bool value;
public:
  Function_con_b(int dim, VRam *vr, bool x) : FunctionClass<T>(TYPE_BOOL_NAME, dim, vr), value(x) {FunctionClass<T>::setUp("",TYPE_BOOL_SIGN);}
  void* calc(void*, void*, VRam&, int) {
    return (void*)&value;
  }
};

template <class T>
class Function_con_c : public FunctionClass<T>
{
  complex<T> value;
public:
  Function_con_c(int dim, VRam *vr, complex<T> x) : FunctionClass<T>(TYPE_CPLX_NAME, dim, vr), value(x) {FunctionClass<T>::setUp("",TYPE_CPLX_SIGN);}
  void* calc(void*, void*, VRam&, int) {
    return (void*)&value;
  }
};

template <class T>
class Function_con_u : public FunctionClass<T>
{
  Colour value;
public:
  Function_con_u(int dim, VRam *vr, Colour x) : FunctionClass<T>(TYPE_COLOUR_NAME, dim, vr), value(x) {FunctionClass<T>::setUp("",TYPE_COLOUR_SIGN);}
  void* calc(void*, void*, VRam&, int) {
    return (void*)&value;
  }
};

template <class T>
class Function_con_i : public FunctionClass<T>
{
  int value;
public:
  Function_con_i(int dim, VRam *vr, int x) : FunctionClass<T>(TYPE_INT_NAME, dim, vr), value(x) {FunctionClass<T>::setUp("",TYPE_INT_SIGN);}
  void* calc(void*, void*, VRam&, int) {
    return (void*)&value;
  }
};

template <class T>
class Function_con_v : public FunctionClass<T>
{
  T* value;
public:
  Function_con_v(int dim, VRam *vr, T* x) : FunctionClass<T>(TYPE_VECTOR_NAME, dim, vr), value(x) {FunctionClass<T>::setUp("",TYPE_VECTOR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    return (void*)value;
  }
};

template <class T>
class Function_gen_con_list : public FunctionClass<T>
{
  int length;
public:
  Function_gen_con_list(int dim, VRam *vr, FunctionClass<T> ** functions, int length) : FunctionClass<T>(TYPE_LIST_NAME, dim, vr), length(length) {
    for (int i = 0; i < length; ++i) FunctionClass<T>::parameters.push_back(functions[i]);
    stringstream ss;
    ss<<functions[0]->getOutConstructor()->getRoot()->toString();
    for (int i = 1; i < length; ++i) ss<<","<<functions[i]->getOutConstructor()->getRoot()->toString();
    FunctionClass<T>::setUp(ss.str(),TYPE_LIST_SIGN+"("+ss.str()+")");
  }
  Function_gen_con_list(int dim, VRam *vr, vector<CustomFunctionClass<T>*> &functions) : FunctionClass<T>(TYPE_LIST_NAME, dim, vr), length(functions.size()) {
    for (int i = 0; i < length; ++i) FunctionClass<T>::parameters.push_back(functions[i]);
    stringstream ss;
    ss<<functions[0]->getOutConstructor()->getRoot()->toString();
    for (int i = 1; i < length; ++i) ss<<","<<functions[i]->getOutConstructor()->getRoot()->toString();
    FunctionClass<T>::setUp(ss.str(),TYPE_LIST_SIGN+"("+ss.str()+")");
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    void **ret = (void**)FunctionClass<T>::getOutConstructor()->construct(vr);
    for (int i = 0; i < length; ++i) ret[i] = FunctionClass<T>::parameters[i]->calc(data,data2,vr,thread);
    return ret;
  }
};

template <class T>
class Function_get_v : public FunctionClass<T>
{
  FunctionClass<T> *lf,*rf;
  bool vector;
public:
  Function_get_v(int dim, VRam *vr, FunctionClass<T>* f1, FunctionClass<T>* f2, FunctionParseInput<T> &data) : FunctionClass<T>("[]", dim, vr, data), lf(f1), rf(f2) {
    vector = FunctionClass<T>::matchType(f1->getOutConstructor()->getRoot()->toString(), TYPE_VECTOR_SIGN);
    if (!vector && !FunctionClass<T>::matchType(f1->getOutConstructor()->getRoot()->toString(), TYPE_MATRIX_SIGN)) throw CompileError(INVALID_INDEXING, "The operator ["+TYPE_INT_SIGN+"] can only be applied to vectors and matrices",data.filename,data.line,data.col);
    if (!FunctionClass<T>::matchType(f2->getOutConstructor()->getRoot()->toString(), TYPE_INT_SIGN)) throw CompileError(INVALID_INDEX, "Only an integer can be index",data.filename,data.line,data.col);
    if (vector) FunctionClass<T>::setUp("",""+TYPE_REAL_SIGN);
    else FunctionClass<T>::setUp("",""+TYPE_VECTOR_SIGN);
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    if (vector) {
      T *v = (T*)lf->calc(data,data2,vr,thread);
      int *index = (int*)(rf->calc(data,data2,vr,thread));
      return &(v[*index]);
    } else {
      void **v = (void**)lf->calc(data,data2,vr,thread);
      int *index = (int*)(rf->calc(data,data2,vr,thread));
      return v[*index];
    }
    /*T* ret = (T*)vr.allocate(sizeof(T));
    *ret = v[*index];
    return ret;*/
  }
  void collapseChildren(map<void*,FunctionClass<T>*> &fs) {
    lf = FunctionClass<T>::collapse(lf,fs);
    rf = FunctionClass<T>::collapse(rf,fs);
  }
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    lf->updateCamera(cam);
    rf->updateCamera(cam);
  }
  virtual void setRootFunction(RootFunctionClass<T> *root)
  {
    if (FunctionClass<T>::rootSet) return;
    FunctionClass<T>::setRootFunction(root);
    lf->setRootFunction(root);
    rf->setRootFunction(root);
  }
  virtual int getNumChildFunctions() {return FunctionClass<T>::getNumChildFunctions()+2;}
  virtual FunctionClass<T>* getChildFunction(int i) {
    if (i < FunctionClass<T>::getNumChildFunctions()) return FunctionClass<T>::getChildFunction(i);
    i -= FunctionClass<T>::getNumChildFunctions();
    if (i == 0) return lf;
    return rf;
  }
  virtual void setChildFunction(int i, FunctionClass<T> *f) {
    if (i < FunctionClass<T>::getNumChildFunctions()) FunctionClass<T>::setChildFunction(i, f);
    else {
      i -= FunctionClass<T>::getNumChildFunctions();
      if (i == 0) lf = f;
      else rf = f;
    }
  }
};

template <class T>
class Function_constructor_v : public FunctionClass<T>
{
public:
  Function_constructor_v(int dim, VRam *vr) : FunctionClass<T>("Vector", dim, vr) {
    string s = TYPE_REAL_SIGN;
    for (int i = 1; i < dim; ++i) s += ","+TYPE_REAL_SIGN;
    FunctionClass<T>::setUp(s,TYPE_VECTOR_SIGN);
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    T* ret = (T*)FunctionClass<T>::getOutConstructor()->construct(vr);
    for (int i = 0; i < FunctionClass<T>::dim; ++i) ret[i] = *((T*)FunctionClass<T>::parameters[i]->calc(data,data2,vr,thread));
    return (void*)ret;
  }
};

template <class T>
class Function_constructor_m : public FunctionClass<T>
{
public:
  Function_constructor_m(int dim, VRam *vr) : FunctionClass<T>("Matrix", dim, vr) {
    string s = TYPE_VECTOR_SIGN;
    for (int i = 1; i < dim; ++i) s += ","+TYPE_VECTOR_SIGN;
    FunctionClass<T>::setUp(s,TYPE_MATRIX_SIGN);
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    T **ret = (T**)FunctionClass<T>::getOutConstructor()->construct(vr);
    for (int i = 0; i < FunctionClass<T>::dim; ++i) ret[i] = (T*)FunctionClass<T>::parameters[i]->calc(data,data2,vr,thread);
    return (void*)ret;
  }
};

template <class T>
class Function_constructor_a_empty : public FunctionClass<T>
{
public:
  Function_constructor_a_empty(int dim, VRam *vr) : FunctionClass<T>("Array", dim, vr) {FunctionClass<T>::setUp("",TYPE_ARRAY_SIGN);}
  void* calc(void*, void*, VRam &vr, int) {
    return FunctionClass<T>::getOutConstructor()->construct(vr);
  }
};

template <class T>
class Function_constructor_a : public FunctionClass<T>
{
public:
  Function_constructor_a(int dim, VRam *vr, string type) : FunctionClass<T>("Array", dim, vr) {FunctionClass<T>::setUp("",TYPE_ARRAY_SIGN+type);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    pair<void*,void*> *ret = (pair<void*,void*>*)FunctionClass<T>::getOutConstructor()->construct(vr);
    for (int i = FunctionClass<T>::parameters.size()-1; i >= 0; --i) {
      pair<void*,void*> *ret2 = (pair<void*,void*>*)FunctionClass<T>::getOutConstructor()->construct(vr);
      ret2->first = FunctionClass<T>::parameters[i]->calc(data,data2,vr,thread);
      ret2->second = ret;
      ret = ret2;
    }
    return ret;
  }
};

template <class T>
class Function_fetch_array : public FunctionClass<T>
{
  int index;
  FunctionClass<T> *root;
  FunctionClass<T> *dat_func;
public:
  Function_fetch_array(int dim, VRam *vr, CustomTypeList<T> *datatype, int index, Function_fetch_array<T> *root, FunctionClass<T> *dat_func, FunctionParseInput<T> &data) : dat_func(dat_func), root(root), index(index), FunctionClass<T>("Fetch", dim, vr, data) {
    if (index > (int)datatype->types.size()) throw CompileError(CANT_FETCH_ARRAY,"Could not fetch array",data.filename,data.line,data.col);
    if (dat_func != NULL && !FunctionClass<T>::matchType(dat_func->getOutConstructor()->getRoot()->toString(), datatype->toString())) throw CompileError(TYPE_MISTMATCH, "Type mismatch", data.filename,data.line,data.col);
    FunctionClass<T>::setUp(datatype->toString(),datatype->types[index]->toString());
  }
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    if (dat_func != NULL) data = dat_func->calc(data,data2,vr,thread);
    if (root != NULL) return ((void**)root->calc(data,data2,vr,thread))[index];
    return ((void**)data)[index];
  }
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    if (root != NULL) root->updateCamera(cam);
    if (dat_func != NULL) dat_func->updateCamera(cam);
  }
  virtual void setRootFunction(RootFunctionClass<T> *r)
  {
    if (FunctionClass<T>::rootSet) return;
    FunctionClass<T>::setRootFunction(r);
    if (root != NULL) root->setRootFunction(r);
    if (dat_func != NULL) dat_func->setRootFunction(r);
  }
  virtual int getNumChildFunctions() {
    int num = FunctionClass<T>::getNumChildFunctions();
    if (root != NULL) num++;
    if (dat_func != NULL) num++;
    return num;
  }
  virtual FunctionClass<T>* getChildFunction(int i) {
    if (i < FunctionClass<T>::getNumChildFunctions()) return FunctionClass<T>::getChildFunction(i);
    i -= FunctionClass<T>::getNumChildFunctions();
    if (root != NULL) i--;
    if (i == 0 && root != NULL) return root;
    else return dat_func;
  }
  virtual void setChildFunction(int i, FunctionClass<T> *f) {
    if (i < FunctionClass<T>::getNumChildFunctions()) FunctionClass<T>::setChildFunction(i, f);
    else {
      i -= FunctionClass<T>::getNumChildFunctions();
      if (root != NULL) i--;
      if (i == 0 && root != NULL) root = f;
      else dat_func = f;
    }
  }
};

template <class T>
class IfFunction : public BranchFunctionClass<T>
{
  FunctionClass<T> *condition;
  FunctionClass<T> *true_side;
  FunctionClass<T> *false_side;
public:
  IfFunction(FunctionClass<T> *condition, FunctionClass<T> *true_side, FunctionClass<T> *false_side, string in, string out, int dim, VRam *vr, FunctionParseInput<T> &data) : condition(condition), true_side(true_side), false_side(false_side), BranchFunctionClass<T>("if", dim, vr, data) {
    if (!FunctionClass<T>::matchType(condition->getOutConstructor()->getRoot()->toString(), TYPE_BOOL_SIGN)) throw CompileError(IVALID_IF_CONDITION,"The condition of the if statement must return a bool value", data.filename,data.line,data.col);
    if (!FunctionClass<T>::matchType(true_side->getOutConstructor()->getRoot()->toString(), out) ||
       !FunctionClass<T>::matchType(false_side->getOutConstructor()->getRoot()->toString(), out)) throw CompileError(IVALID_IF_RETURN_TYPE,"Invalid return type", data.filename,data.line,data.col);
    FunctionClass<T>::setUp(in,out);
  }
  void* calc(void *data, void *data2, VRam &vr, int thread)
  {
    bool * con = (bool*)condition->calc(data,data2,vr,thread);
    if (*con == true) return true_side->calc(data,data2,vr,thread);
    else return false_side->calc(data,data2,vr,thread);
  }
  virtual int getNumBranches() const {return 2;}
  virtual FunctionClass<T>* getBranch(int i) const {
    if (i == 0) return true_side;
    return false_side;
  }
  void collapseChildren(map<void*,FunctionClass<T>*> &fs)
  {
     condition = FunctionClass<T>::collapse(condition, fs);
     true_side = FunctionClass<T>::collapse(true_side, fs);
     false_side = FunctionClass<T>::collapse(false_side, fs);
  }
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    condition->updateCamera(cam);
    true_side->updateCamera(cam);
    false_side->updateCamera(cam);
  }
  virtual void setRootFunction(RootFunctionClass<T> *root)
  {
    if (FunctionClass<T>::rootSet) return;
    FunctionClass<T>::setRootFunction(root);
    condition->setRootFunction(root);
    true_side->setRootFunction(root);
    false_side->setRootFunction(root);
  }
  virtual int getNumChildFunctions() {return FunctionClass<T>::getNumChildFunctions()+3;}
  virtual FunctionClass<T>* getChildFunction(int i) {
    if (i < FunctionClass<T>::getNumChildFunctions()) return FunctionClass<T>::getChildFunction(i);
    i -= FunctionClass<T>::getNumChildFunctions();
    if (i == 0) return condition;
    if (i == 1) return true_side;
    return false_side;
  }
  virtual void setChildFunction(int i, FunctionClass<T> *f) {
    if (i < FunctionClass<T>::getNumChildFunctions()) FunctionClass<T>::setChildFunction(i, f);
    else {
      i -= FunctionClass<T>::getNumChildFunctions();
      if (i == 0) condition = f;
      else if (i == 1) true_side = f;
      else false_side = f;
    }
  }
  virtual bool isBranch(int i) {
    if (i < FunctionClass<T>::getNumChildFunctions()) return false;
    i -= FunctionClass<T>::getNumChildFunctions();
    if (i == 0) return false;
    return true;
  }
};

template <class T>
class ErrorFunction : public FunctionClass<T>
{
  string error;
public:
  ErrorFunction(int dim, VRam *vr, string error, FunctionParseInput<T> &data) : FunctionClass<T>("Error", dim, vr, data), error(error) {}
  void* calc(void*, void*, VRam&, int) {
    throw RuntimeError(UNHANDLED_PATTERN, error.c_str(), FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
  }
};

template <class T>
class Function_head_a : public FunctionClass<T>
{
public:
  Function_head_a(int dim, VRam *vr, string type, FunctionParseInput<T> &data) : FunctionClass<T>("Index", dim, vr, data) {FunctionClass<T>::setUp(type,type.substr(1,-1));}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    pair<void*,void*> *dat = (pair<void*,void*>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
    if (dat->first == NULL) throw RuntimeError(UNALLOWED_OPERATION_ON_EMPTY_ARRAY,"Tried to get the head of an empty array", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
    return dat->first;
  }
};

template <class T>
class Function_tail_a : public FunctionClass<T>
{
public:
  Function_tail_a(int dim, VRam *vr, string type, FunctionParseInput<T> &data) : FunctionClass<T>("Tail", dim, vr, data) {FunctionClass<T>::setUp(type,type);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    pair<void*,void*> *dat = (pair<void*,void*>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
    if (dat->first == NULL) throw RuntimeError(UNALLOWED_OPERATION_ON_EMPTY_ARRAY, "Tried to get the tail of an empty array", FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
    //cout<<data<<" "<<dat->second<<endl;
    return dat->second;
  }
};

template <class T>
struct FunctionConstructData
{
  string arguments;
  string input;
  vector<FunctionClass<T>*> parameters;
  FunctionClass<T>* param;
  const CustomFunctionClass<T>* main;
  const vector<pair<FunctionClass<T>*,string>> *rootArgs;
  vector<Function_const_value<T>*> *params;
  FunctionConstructData(FunctionClass<T> *param, const CustomFunctionClass<T> *main, const vector<pair<FunctionClass<T>*,string>> *rootArgs, vector<Function_const_value<T>*> *params) : arguments(""), input(""), param(param), main(main), rootArgs(rootArgs), params(params) {}
};

template <class T>
class PatternFunction;

template <class T>
class Pattern
{
public:
  virtual bool match(void*,void*,VRam&,int) const = 0;
  virtual int getNumFunctions() const {return 0;}
  virtual FunctionClass<T>* getFunction(int) const {return NULL;}
  virtual void setFunction(int, FunctionClass<T>*) {}
  static Pattern<T>* parse(VRam*, string, string, FunctionParseInput<T>, FunctionConstructData<T>&);
  static void* getValue(VRam *vram, string s, string out, FunctionParseInput<T> data) {
    VRam *vr = vram;
    if (data.temp != NULL) vr = data.temp;
    CustomFunctionClass<T>* func = (CustomFunctionClass<T>*) vr->allocate(sizeof(CustomFunctionClass<T>));
    func = new (func) CustomFunctionClass<T> ("-","->*","() " + s, data.dim, vr, data);
    vector<Function_const_value<T>*> parameters;
    vector<pair<FunctionClass<T>*,string>> rootArgs;
    func->parse(FunctionParseInput<T>(NULL,data.temp2,data.functions,data.dim,data.filename,data.line,data.col,data.depth,data.rootElement,data.ownElement,data.threads),parameters,rootArgs);
    if (!FunctionClass<T>::matchType(func->getOutConstructor()->getRoot()->toString(), out)) throw CompileError(WRONG_TYPE_PATTERN,"Pattern has the wrong type", ERROR_CONTENT);
    void * value = func->calc(NULL,NULL,*vram,0);
    return value;
  }
};

template <class T>
class Pattern_if : public Pattern<T>
{
  FunctionClass<T> *f;
public:
  Pattern_if(FunctionClass<T> *f, string type, string s, FunctionParseInput<T> &data) : f(f) {
    if (f->getOutput() != TYPE_BOOL_SIGN || f->getInput() != type) throw CompileError(IVALID_IF_CONDITION, "The statement ("+s+") must be the form of f : "+type+" -> Bool", data.filename, data.line, data.col);
  }
  bool match(void *data, void *data2, VRam &vram, int thread) const {
    return (*(bool*)f->calc(data,data2,vram,thread));
  }
  int getNumFunctions() const {return 1;}
  FunctionClass<T>* getFunction(int) const {return f;}
  void setFunction(int, FunctionClass<T> *nf) {f = nf;}
};

template <class T>
class Pattern_tautology : public Pattern<T>
{
public:
  bool match(void*, void*, VRam&, int) const {return true;}
};

template <class T>
class PatternBool : public Pattern<T>
{
public:
  static Pattern<T>* parse(VRam*, string, string, FunctionParseInput<T>);
};

template <class T>
class PatternBool_equality : public PatternBool<T>
{
  bool value;
public:
  PatternBool_equality(bool* value) : value(*value) {}
  bool match(void *data, void*, VRam&, int) const {return *((bool*)data) == value;}
};

template <class T>
class PatternInt : public Pattern<T>
{
public:
  static Pattern<T>* parse(VRam*, string, string, FunctionParseInput<T>);
};

template <class T>
class PatternInt_equality : public PatternInt<T>
{
  int value;
public:
  PatternInt_equality(int* value) : value(*value) {}
  bool match(void *data, void*, VRam&, int) const {return *((int*)data) == value;}
};


template <class T>
class PatternInt_intervall_down : public PatternInt<T>
{
protected:
  int value;
public:
  PatternInt_intervall_down(int* value) : value(*value) {}
};

template <class T>
class PatternInt_intervall_down_e : public PatternInt_intervall_down<T>
{
public:
  PatternInt_intervall_down_e(int* value) : PatternInt_intervall_down<T>(value) {}
  bool match(void *data, void*, VRam&, int) const {return PatternInt_intervall_down<T>::value <= *((int*)data);}
};

template <class T>
class PatternInt_intervall_down_t : public PatternInt_intervall_down<T>
{
public:
  PatternInt_intervall_down_t(int* value) : PatternInt_intervall_down<T>(value) {}
  bool match(void *data, void*, VRam&, int) const {return PatternInt_intervall_down<T>::value < *((int*)data);}
};

template <class T>
class PatternInt_intervall_up : public PatternInt<T>
{
protected:
  int value;
public:
  PatternInt_intervall_up(int* value) : value(*value) {}
};

template <class T>
class PatternInt_intervall_up_e : public PatternInt_intervall_up<T>
{
public:
  PatternInt_intervall_up_e(int* value) : PatternInt_intervall_up<T>(value) {}
  bool match(void *data, void*, VRam&, int) const {return *((int*)data) <= PatternInt_intervall_up<T>::value;}
};

template <class T>
class PatternInt_intervall_up_t : public PatternInt_intervall_up<T>
{
public:
  PatternInt_intervall_up_t(int* value) : PatternInt_intervall_up<T>(value) {}
  bool match(void *data, void*, VRam&, int) const {return *((int*)data) < PatternInt_intervall_up<T>::value;}
};

template <class T>
class PatternInt_intervall_full : public PatternInt<T>
{
  PatternInt_intervall_down<T> *down;
  PatternInt_intervall_up<T> *up;
public:
  PatternInt_intervall_full(PatternInt_intervall_down<T> *down, PatternInt_intervall_up<T> *up) : down(down), up(up) {}
  bool match(void *data, void *data2, VRam &vram, int thread) const {return down->match(data,data2,vram,thread) && up->match(data,data2,vram,thread);}
};


template <class T>
class PatternReal : public Pattern<T>
{
public:
  static Pattern<T>* parse(VRam*, string, string, FunctionParseInput<T>);
};

template <class T>
class PatternReal_equality : public PatternReal<T>
{
  T value;
public:
  PatternReal_equality(T* value) : value(*value) {}
  bool match(void *data, void*, VRam&, int) const {return *((T*)data) == value;}
};

template <class T>
class PatternReal_intervall_down : public PatternReal<T>
{
protected:
  T value;
public:
  PatternReal_intervall_down(T* value) : value(*value) {}
};

template <class T>
class PatternReal_intervall_down_e : public PatternReal_intervall_down<T>
{
public:
  PatternReal_intervall_down_e(T* value) : PatternReal_intervall_down<T>(value) {}
  bool match(void *data, void*, VRam&, int) const {return PatternReal_intervall_down<T>::value <= *((T*)data);}
};

template <class T>
class PatternReal_intervall_down_t : public PatternReal_intervall_down<T>
{
public:
  PatternReal_intervall_down_t(T* value) : PatternReal_intervall_down<T>(value) {}
  bool match(void *data, void*, VRam&, int) const {return PatternReal_intervall_down<T>::value < *((T*)data);}
};

template <class T>
class PatternReal_intervall_up : public PatternReal<T>
{
protected:
  T value;
public:
  PatternReal_intervall_up(T* value) : value(*value) {}
};

template <class T>
class PatternReal_intervall_up_e : public PatternReal_intervall_up<T>
{
public:
  PatternReal_intervall_up_e(T* value) : PatternReal_intervall_up<T>(value) {}
  bool match(void *data, void*, VRam&, int) const {return *((T*)data) <= PatternReal_intervall_up<T>::value;}
};

template <class T>
class PatternReal_intervall_up_t : public PatternReal_intervall_up<T>
{
public:
  PatternReal_intervall_up_t(T* value) : PatternReal_intervall_up<T>(value) {}
  bool match(void *data, void*, VRam&, int) const {return *((T*)data) < PatternReal_intervall_up<T>::value;}
};

template <class T>
class PatternReal_intervall_full : public PatternReal<T>
{
  PatternReal_intervall_down<T> *down;
  PatternReal_intervall_up<T> *up;
public:
  PatternReal_intervall_full(PatternReal_intervall_down<T> *down, PatternReal_intervall_up<T> *up) : down(down), up(up) {}
  bool match(void *data, void *data2, VRam &vram, int thread) const {return down->match(data,data2,vram,thread) && up->match(data,data2,vram,thread);}
};

template <class T>
class PatternComplex : public Pattern<T>
{
public:
  static Pattern<T>* parse(VRam*, string, string, FunctionParseInput<T>);
};

template <class T>
class PatternComplex_equality : public PatternComplex<T>
{
  complex<T> value;
public:
  PatternComplex_equality(complex<T>* value) : value(*value) {}
  bool match(void *data, void*, VRam&, int) const {return *((complex<T>*)data) == value;}
};

template <class T>
class PatternComplex_intervall_down : public PatternComplex<T>
{
protected:
  complex<T> value;
public:
  PatternComplex_intervall_down(complex<T>* value) : value(*value) {}
};

template <class T>
class PatternComplex_intervall_down_e : public PatternComplex_intervall_down<T>
{
public:
  PatternComplex_intervall_down_e(complex<T>* value) : PatternComplex_intervall_down<T>(value) {}
  bool match(void *data, void*, VRam&, int) const {return abs2(PatternComplex_intervall_down<T>::value) <= abs2(*((complex<T>*)data));}
};

template <class T>
class PatternComplex_intervall_down_t : public PatternComplex_intervall_down<T>
{
public:
  PatternComplex_intervall_down_t(complex<T>* value) : PatternComplex_intervall_down<T>(value) {}
  bool match(void *data, void*, VRam&, int) const {return abs2(PatternComplex_intervall_down<T>::value) < abs2(*((complex<T>*)data));}
};

template <class T>
class PatternComplex_intervall_up : public PatternComplex<T>
{
protected:
  complex<T> value;
public:
  PatternComplex_intervall_up(complex<T>* value) : value(*value) {}
};

template <class T>
class PatternComplex_intervall_up_e : public PatternComplex_intervall_up<T>
{
public:
  PatternComplex_intervall_up_e(complex<T>* value) : PatternComplex_intervall_up<T>(value) {}
  bool match(void *data, void*, VRam&, int) const {return abs2(*((complex<T>*)data)) <= abs2(PatternComplex_intervall_up<T>::value);}
};

template <class T>
class PatternComplex_intervall_up_t : public PatternComplex_intervall_up<T>
{
public:
  PatternComplex_intervall_up_t(complex<T>* value) : PatternComplex_intervall_up<T>(value) {}
  bool match(void *data, void*, VRam&, int) const {return abs2(*((complex<T>*)data)) < abs2(PatternComplex_intervall_up<T>::value);}
};

template <class T>
class PatternComplex_intervall_full : public PatternComplex<T>
{
  PatternComplex_intervall_down<T> *down;
  PatternComplex_intervall_up<T> *up;
public:
  PatternComplex_intervall_full(PatternComplex_intervall_down<T> *down, PatternComplex_intervall_up<T> *up) : down(down), up(up) {}
  bool match(void *data, void *data2, VRam &vram, int thread) const {return down->match(data,data2,vram,thread) && up->match(data,data2,vram,thread);}
};

template <class T>
class PatternColour : public Pattern<T>
{
public:
  static Pattern<T>* parse(VRam*, string, string, FunctionParseInput<T>);
};

template <class T>
class PatternColour_equality : public PatternColour<T>
{
  Colour value;
public:
  PatternColour_equality(Colour* value) : value(*value) {}
  bool match(void *data, void*, VRam&, int) const {return *((Colour*)data) == value;}
};

template <class T>
class PatternVector : public Pattern<T>
{
public:
  static Pattern<T>* parse(VRam*, string, string, FunctionParseInput<T>);
};

template <class T>
class PatternVector_equality : public PatternVector<T>
{
  T* value;
  int len;
public:
  PatternVector_equality(T* value, int len) : value(value), len(len) {}
  bool match(void *data, void*, VRam&, int) const {
    for (int i = 0; i < len; ++i) if (((T*)data)[i] != value[i]) return false;
    return true;
  }
};

template <class T>
class PatternMatrix : public Pattern<T>
{
public:
  static Pattern<T>* parse(VRam*, string, string, FunctionParseInput<T>);
};

template <class T>
class PatternMatrix_equality : public PatternMatrix<T>
{
  T** value;
  int len;
public:
  PatternMatrix_equality(T** value, int len) : value(value), len(len) {}
  bool match(void *data, void*, VRam&, int) const {
    for (int i = 0; i < len; ++i) for (int j = 0; j < len; ++j) if (((T**)data)[i][j] != value[i][j]) return false;
    return true;
  }
};

template <class T>
class PatternList : public Pattern<T>
{
  vector<Pattern<T>*> patterns;
public:
  Pattern<T>* parse(VRam*, string, string, FunctionParseInput<T>, FunctionConstructData<T>&);
  bool match(void *dat, void *data2, VRam &vram, int thread) const {
    void **data = (void**)dat;
    int len = patterns.size();
    for (int i = 0; i < len; ++i) if (!patterns[i]->match(data[i],data2,vram,thread)) return false;
    return true;
  }
};

template <class T>
class PatternArray : public Pattern<T>
{
public:
  static Pattern<T>* parse(VRam*, string, string, FunctionParseInput<T>, FunctionConstructData<T>&);
};

template <class T>
class PatternArray_empty : public PatternArray<T>
{
public:
  bool match(void *dat, void*, VRam&, int) const {
    pair<void*,void*> *data = (pair<void*,void*>*)dat;
    return data->first == NULL;
  }
};

template <class T>
class PatternArray_equality : public PatternArray<T>
{
  void* value;
  CustomType<T>* type;
  int len;
public:
  PatternArray_equality(void* value, CustomType<T>* type, int len) : value(value), type(type), len(len) {}
  bool match(void *data, void*, VRam&, int) const {return type->compare(data,value,len);}
};

template <class T>
class PatternArray_head_n_tail : public PatternArray<T>
{
public:
  PatternArray_head_n_tail() {}
  bool match(void *dat, void*, VRam&, int) const {
    pair<void*,void*> *data = (pair<void*,void*>*)dat;
    if (data->first == NULL) return false;
    return true;
  }
};

template <class T>
Pattern<T>* Pattern<T>::parse(VRam *vram, string s, string type, FunctionParseInput<T> data, FunctionConstructData<T> &fcd) {
  if (s == "*") {
    Pattern_tautology<T> *ret = (Pattern_tautology<T>*)vram->allocate(sizeof(Pattern_tautology<T>));
    ret = new (ret) Pattern_tautology<T>();
    return ret;
  }
  if (s.length() > 2 && s.substr(0,2) == "if") {
    CustomFunctionClass<T> *f = (CustomFunctionClass<T>*)vram->allocate(sizeof(CustomFunctionClass<T>));
    f = new (f) CustomFunctionClass<T> ("-",type+"->"+TYPE_BOOL_SIGN,"("+fcd.main->getArguments()+") " + s.substr(3,s.length()-4),fcd.main->getDim(), fcd.main->getVram(), data);
    for (unsigned int j = 0; j < fcd.main->parameters.size(); ++j) f->parameters.push_back(fcd.main->parameters[j]);
    f->parse(data,*fcd.params,*fcd.rootArgs);
    
    Pattern_if<T> *p = new ((Pattern_if<T>*)vram->allocate(sizeof(Pattern_if<T>))) Pattern_if<T>(f, type, s, data);
    return p;
  }
  if (type == TYPE_BOOL_SIGN) return PatternBool<T>::parse(vram, s, type, data);
  if (type == TYPE_INT_SIGN) return PatternInt<T>::parse(vram, s, type, data);
  if (type == TYPE_REAL_SIGN) return PatternReal<T>::parse(vram, s, type, data);
  if (type == TYPE_CPLX_SIGN) return PatternComplex<T>::parse(vram, s, type, data);
  if (type == TYPE_COLOUR_SIGN) return PatternColour<T>::parse(vram, s, type, data);
  if (type == TYPE_VECTOR_SIGN) return PatternVector<T>::parse(vram, s, type, data);
  if (type == TYPE_MATRIX_SIGN) return PatternMatrix<T>::parse(vram, s, type, data);
  if (type[0] == TYPE_LIST_SIGN[0]) {
    PatternList<T> *ret = (PatternList<T>*)vram->allocate(sizeof(PatternList<T>));
    ret = new (ret) PatternList<T>();
    ret->parse(vram, s, type, data, fcd);
    return ret;
  }
  if (FunctionClass<T>::matchType(TYPE_ARRAY_SIGN, type)) return PatternArray<T>::parse(vram, s, type, data, fcd);
  throw CompileError(INVALID_TYPE_PATTERN, "Invalid pattern", ERROR_CONTENT);
}

template <class T>
Pattern<T>* PatternBool<T>::parse(VRam *vram, string s, string type, FunctionParseInput<T> data) {
  PatternBool_equality<T> *ret = (PatternBool_equality<T>*)vram->allocate(sizeof(PatternBool_equality<T>));
  return new (ret) PatternBool_equality<T>((bool*)Pattern<T>::getValue(vram, s,type,data));
}

template <class T>
Pattern<T>* PatternInt<T>::parse(VRam *vram, string s, string type, FunctionParseInput<T> data) {
  if (s.length() > 2 && (s[0] == ']' || s[0] == '[') && (s[s.length()-1] == ']' || s[s.length()-1] == '[')) {
    vector<string> parts;
    getParts(s,parts,';',0);
    if (parts.size() != 2) throw CompileError(INVALID_INTERVALL, "Invalid intervallum", ERROR_CONTENT);
    PatternInt_intervall_down<T> *down = NULL;
    PatternInt_intervall_up<T> *up = NULL;
    string downs = parts[0];
    string ups = parts[1];
    if (downs != "") {
      if (s[0] == '[') {
	PatternInt_intervall_down_e<T> *dw = (PatternInt_intervall_down_e<T>*)vram->allocate(sizeof(PatternInt_intervall_down_e<T>));
	down = new (dw) PatternInt_intervall_down_e<T>((int*)Pattern<T>::getValue(vram,downs,type,data));
      } else {
	PatternInt_intervall_down_t<T> *dw = (PatternInt_intervall_down_t<T>*)vram->allocate(sizeof(PatternInt_intervall_down_t<T>));
	down = new (dw) PatternInt_intervall_down_t<T>((int*)Pattern<T>::getValue(vram,downs,type,data));
      }
    }
    if (ups != "") {
      if (s[s.length()-1] == ']') {
	PatternInt_intervall_up_e<T> *u = (PatternInt_intervall_up_e<T>*)vram->allocate(sizeof(PatternInt_intervall_up_e<T>));
	up = new (u) PatternInt_intervall_up_e<T>((int*)Pattern<T>::getValue(vram,ups,type,data));
      } else {
	PatternInt_intervall_up_t<T> *u = (PatternInt_intervall_up_t<T>*)vram->allocate(sizeof(PatternInt_intervall_up_t<T>));
	up = new (u) PatternInt_intervall_up_t<T>((int*)Pattern<T>::getValue(vram,ups,type,data));
      }
    }
    if (up == NULL && down == NULL) throw CompileError(UNKNOWN_ERROR, "Unknown error", ERROR_CONTENT);
    if (up == NULL) return down;
    if (down == NULL) return up;
    PatternInt_intervall_full<T> *r = (PatternInt_intervall_full<T>*)vram->allocate(sizeof(PatternInt_intervall_full<T>));
    return new (r) PatternInt_intervall_full<T>(down,up);
  }
  PatternInt_equality<T> *r = (PatternInt_equality<T>*)vram->allocate(sizeof(PatternInt_equality<T>));
  return new (r) PatternInt_equality<T>((int*)Pattern<T>::getValue(vram, s,type,data));
}

template <class T>
Pattern<T>* PatternReal<T>::parse(VRam *vram, string s, string type, FunctionParseInput<T> data) {
  if (s.length() > 2 && (s[0] == ']' || s[0] == '[') && (s[s.length()-1] == ']' || s[s.length()-1] == '[')) {
    vector<string> parts;
    //cout<<s<<"--- asd"<<endl;
    getParts(s,parts,';',1);
    if (parts.size() != 2) throw CompileError(INVALID_INTERVALL,"Invalid intervall", ERROR_CONTENT);
    PatternReal_intervall_down<T> *down = NULL;
    PatternReal_intervall_up<T> *up = NULL;
    string downs = parts[0];
    string ups = parts[1];
    if (downs != "") {
      if (s[0] == '[') {
	PatternReal_intervall_down_e<T> *dw = (PatternReal_intervall_down_e<T>*)vram->allocate(sizeof(PatternReal_intervall_down_e<T>));
	down = new (dw) PatternReal_intervall_down_e<T>((T*)Pattern<T>::getValue(vram,downs,type,data));
      } else {
	PatternReal_intervall_down_t<T> *dw = (PatternReal_intervall_down_t<T>*)vram->allocate(sizeof(PatternReal_intervall_down_t<T>));
	down = new (dw) PatternReal_intervall_down_t<T>((T*)Pattern<T>::getValue(vram,downs,type,data));
      }
    }
    if (ups != "") {
      if (s[s.length()-1] == ']') {
	PatternReal_intervall_up_e<T> * u = (PatternReal_intervall_up_e<T>*)vram->allocate(sizeof(PatternReal_intervall_up_e<T>));
	up = new (u) PatternReal_intervall_up_e<T>((T*)Pattern<T>::getValue(vram,ups,type,data));
      } else {
	PatternReal_intervall_up_t<T> * u = (PatternReal_intervall_up_t<T>*)vram->allocate(sizeof(PatternReal_intervall_up_t<T>));
	up = new (u) PatternReal_intervall_up_t<T>((T*)Pattern<T>::getValue(vram,ups,type,data));
      }
    }
    if (up == NULL && down == NULL) throw CompileError(UNKNOWN_ERROR,"Unknown error", ERROR_CONTENT);
    if (up == NULL) return down;
    if (down == NULL) return up;
    PatternReal_intervall_full<T> *r = (PatternReal_intervall_full<T>*)vram->allocate(sizeof(PatternReal_intervall_full<T>));
    return new (r) PatternReal_intervall_full<T>(down,up);
  }
  PatternReal_equality<T> *r = (PatternReal_equality<T>*)vram->allocate(sizeof(PatternReal_equality<T>));
  return new (r) PatternReal_equality<T>((T*)Pattern<T>::getValue(vram, s,type,data));
}

template <class T>
Pattern<T>* PatternComplex<T>::parse(VRam *vram, string s, string type, FunctionParseInput<T> data) {
  if (s.length() > 2 && (s[0] == ']' || s[0] == '[') && (s[s.length()-1] == ']' || s[s.length()-1] == '[')) {
    vector<string> parts;
    getParts(s,parts,';',0);
    if (parts.size() != 2) throw CompileError(INVALID_INTERVALL, "Invalid intervallum", ERROR_CONTENT);
    PatternComplex_intervall_down<T> *down = NULL;
    PatternComplex_intervall_up<T> *up = NULL;
    string downs = parts[0];
    string ups = parts[1];
    if (downs != "") {
      if (s[0] == '[') {
	PatternComplex_intervall_down_e<T> *dw = (PatternComplex_intervall_down_e<T>*)vram->allocate(sizeof(PatternComplex_intervall_down_e<T>));
	down = new (dw) PatternComplex_intervall_down_e<T>((complex<T>*)Pattern<T>::getValue(vram,downs,type,data));
      } else {
	PatternComplex_intervall_down_t<T> *dw = (PatternComplex_intervall_down_t<T>*)vram->allocate(sizeof(PatternComplex_intervall_down_t<T>));
	down = new (dw) PatternComplex_intervall_down_t<T>((complex<T>*)Pattern<T>::getValue(vram,downs,type,data));
      }
    }
    if (ups != "") {
      if (s[s.length()-1] == ']') {
	PatternComplex_intervall_up_e<T> *u = (PatternComplex_intervall_up_e<T>*)vram->allocate(sizeof(PatternComplex_intervall_up_e<T>));
	up = new (u) PatternComplex_intervall_up_e<T>((complex<T>*)Pattern<T>::getValue(vram,ups,type,data));
      } else {
	PatternComplex_intervall_up_t<T> *u = (PatternComplex_intervall_up_t<T>*)vram->allocate(sizeof(PatternComplex_intervall_up_t<T>));
	up = new (u) PatternComplex_intervall_up_t<T>((complex<T>*)Pattern<T>::getValue(vram,ups,type,data));
      }
    }
    if (up == NULL && down == NULL) throw CompileError(UNKNOWN_ERROR,"Unknown error", ERROR_CONTENT);
    if (up == NULL) return down;
    if (down == NULL) return up;
    PatternComplex_intervall_full<T> *r = (PatternComplex_intervall_full<T>*)vram->allocate(sizeof(PatternComplex_intervall_full<T>));
    return new (r) PatternComplex_intervall_full<T>(down,up);
  }
  PatternComplex_equality<T> *r = (PatternComplex_equality<T>*)vram->allocate(sizeof(PatternComplex_equality<T>));
  return new (r) PatternComplex_equality<T>((complex<T>*)Pattern<T>::getValue(vram, s,type,data));
}

template <class T>
Pattern<T>* PatternColour<T>::parse(VRam *vram, string s, string type, FunctionParseInput<T> data) {
  PatternColour_equality<T> *r = (PatternColour_equality<T>*)vram->allocate(sizeof(PatternColour_equality<T>));
  return new (r) PatternColour_equality<T>((Colour*)Pattern<T>::getValue(vram, s,type,data));
}

template <class T>
Pattern<T>* PatternVector<T>::parse(VRam *vram, string s, string type, FunctionParseInput<T> data) {
  PatternVector_equality<T> *r = (PatternVector_equality<T>*)vram->allocate(sizeof(PatternVector_equality<T>));
  return new (r) PatternVector_equality<T>((T*)Pattern<T>::getValue(vram, s,type,data),data.dim);
}

template <class T>
Pattern<T>* PatternMatrix<T>::parse(VRam *vram, string s, string type, FunctionParseInput<T> data) {
  PatternMatrix_equality<T> *r = (PatternMatrix_equality<T>*)vram->allocate(sizeof(PatternMatrix_equality<T>));
  return new (r) PatternMatrix_equality<T>((T**)Pattern<T>::getValue(vram, s,type,data),data.dim);
}

template <class T>
Pattern<T>* PatternList<T>::parse(VRam *vram, string s, string type, FunctionParseInput<T> data, FunctionConstructData<T> &fcd) {
  if (s[0] != '(' || s[s.length()-1] != ')') throw SyntacsError(INVALID_LIST_PATTERN, "Invalid list pattern", data.filename,data.line,data.col);
  vector<string> parts;
  getParts(s,parts,',',1);
  vector<string> types;
  getParts(type,types,',',1);
  if (types.size() != parts.size()) throw SyntacsError(WRONG_SUBPATTERNS, "Wrong number of subpattenrs", data.filename,data.line,data.col);
  for (unsigned int i = 0; i < parts.size(); ++i) {
    patterns.push_back(Pattern<T>::parse(vram,parts[i],types[i],data,fcd));
    for (unsigned int j = 0; j < fcd.parameters.size(); ++j) {
      //Function_fetch_array(int dim, VRam *vr, CustomTypeList<T> *datatype, int index, Function_fetch_array<T> *root, FunctionClass<T> *dat_func, FunctionParseInput<T> &data)
      CustomType<T> *tp = CustomTypeConstructor<T>::createType(*vram, type, data);
      CustomTypeList<T> *listtype = dynamic_cast<CustomTypeList<T>*> (tp);
      if (listtype == NULL) throw CompileError(INVALID_LIST_TYPE, "Invalud list type",data.filename,data.line,data.col);
      fcd.parameters[j]->parameters[0] = new ((Function_fetch_array<T>*)vram->allocate(sizeof(Function_fetch_array<T>))) Function_fetch_array<T>(data.dim,vram,listtype,i,NULL,fcd.parameters[j]->parameters[0],data);
    }
  }
}

template <class T>
Pattern<T>* PatternArray<T>::parse(VRam *vram, string s, string type, FunctionParseInput<T> data, FunctionConstructData<T> &fcd) {
  if (s == "[]") {
    PatternArray_empty<T> *r = (PatternArray_empty<T>*)vram->allocate(sizeof(PatternArray_empty<T>));
    return new (r) PatternArray_empty<T>();
  }
  if ((int)s.find("?") != -1) throw CompileError(CANNOT_PARSE, "Cannot parse: "+s, ERROR_CONTENT);
  vector<string> parts;
  getParts(replace(s,"::","?"),parts,'?',0);
  if (parts.size() > 2) throw CompileError(INVALID_HEAD_TAIL_PATTERN, "Invalid head::tail pattern", ERROR_CONTENT);
  if (parts.size() == 2) {
    for (unsigned int i = 0; i < parts.size()-1; ++i) {
      Function_head_a<T> *_af = (Function_head_a<T>*)vram->allocate(sizeof(Function_head_a<T>));
      Function_head_a<T> *af = new (_af) Function_head_a<T>(data.dim,vram,type,data);
      if (fcd.arguments != "") fcd.arguments += ",";
      if (fcd.input != "") fcd.input += ",";
      fcd.input += type.substr(1,-1);
      fcd.arguments += parts[i];
      fcd.parameters.push_back(af);
      af->parameters.push_back(fcd.param);
      //f->argument_functions.push_back(make_pair(af,parts[i]));
    }
    Function_tail_a<T> *_af = (Function_tail_a<T>*)vram->allocate(sizeof(Function_tail_a<T>));
    Function_tail_a<T> *af = new (_af) Function_tail_a<T>(data.dim,vram,type,data);
    if (fcd.arguments != "") fcd.arguments += ",";
    if (fcd.input != "") fcd.input += ",";
    fcd.input += type.substr(1,-1);
    fcd.arguments += parts[parts.size()-1];
    fcd.parameters.push_back(af);
    af->parameters.push_back(fcd.param);
    //f->argument_functions.push_back(make_pair(af,parts[parts.size()-1]));
    PatternArray_head_n_tail<T> *r = (PatternArray_head_n_tail<T>*)vram->allocate(sizeof(PatternArray_head_n_tail<T>));
    return new (r) PatternArray_head_n_tail<T>();
  }
  PatternArray_equality<T> *r = (PatternArray_equality<T>*)vram->allocate(sizeof(PatternArray_equality<T>));
  return new (r) PatternArray_equality<T>(Pattern<T>::getValue(vram, s,type,data), CustomTypeConstructor<T>::createType(*vram,type,data), data.dim);
}

template <class T>
class PatternFunction : public BranchFunctionClass<T>
{
  vector<pair<Pattern<T>*,FunctionClass<T>*>> patterns;
  FunctionClass<T> *default_func;
public:
  PatternFunction(int dim, VRam *vr, string in, string out, vector<pair<Pattern<T>*,CustomFunctionClass<T>*>> &pttrs, FunctionClass<T>* default_func, FunctionParseInput<T>, vector<Function_const_value<T>*>&, FunctionClass<T> *param, FunctionParseInput<T> &pdata) : BranchFunctionClass<T>("pattern", dim, vr, pdata), default_func(default_func) {
    FunctionClass<T>::parameters.push_back(param);
    for (unsigned int i = 0; i < pttrs.size(); ++i) patterns.push_back(make_pair(pttrs[i].first, pttrs[i].second));
    FunctionClass<T>::setUp(in,out);
  }
  void* calc(void *data, void *data2, VRam &vr, int thread)
  {
    void *dat = FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread);
    for (unsigned int i = 0; i < patterns.size(); ++i) if (patterns[i].first->match(dat,data2,vr,thread)) return patterns[i].second->calc(data,data2,vr,thread);
    return default_func->calc(data,data2,vr,thread);
  }
  virtual int getNumBranches() const {return patterns.size()+1;}
  virtual FunctionClass<T>* getBranch(int i) const {
    if (i <= (int)patterns.size()) return patterns[i].second;
    return default_func;
  }
  void collapseChildren(map<void*,FunctionClass<T>*> &fs)
  {
    for (unsigned int i = 0; i < patterns.size(); ++i) patterns[i].second = FunctionClass<T>::collapse(patterns[i].second, fs);
    default_func = FunctionClass<T>::collapse(default_func, fs);
    for (unsigned int i = 0; i < patterns.size(); ++i) {
        for (int j = 0; j < patterns[i].first->getNumFunctions(); ++j) {
            patterns[i].first->setFunction(j, FunctionClass<T>::collapse(patterns[i].first->getFunction(j) ,fs));
        }
    }
  }
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    default_func->updateCamera(cam);
    for (unsigned int i = 0; i < patterns.size() /*&& FunctionClass<T>::constant*/; ++i) patterns[i].second->updateCamera(cam);
  }
  virtual void setRootFunction(RootFunctionClass<T> *root)
  {
    if (FunctionClass<T>::rootSet) return;
    FunctionClass<T>::setRootFunction(root);
    default_func->setRootFunction(root);
    for (unsigned int i = 0; i < patterns.size() /*&& FunctionClass<T>::constant*/; ++i) patterns[i].second->setRootFunction(root);
  }
  virtual int getNumChildFunctions() {
    int sum = 0;
    for (unsigned int i = 0; i < patterns.size(); ++i) sum += patterns[i].first->getNumFunctions();
    return FunctionClass<T>::getNumChildFunctions()+1+patterns.size()+sum;
  }
  virtual FunctionClass<T>* getChildFunction(int i) {
    if (i < FunctionClass<T>::getNumChildFunctions()) return FunctionClass<T>::getChildFunction(i);
    i -= FunctionClass<T>::getNumChildFunctions();
    if (i == 0) return default_func;
    i--;
    if (i < (int)patterns.size()) return patterns[i].second;
    i -= patterns.size();
    for (unsigned int j = 0; j < patterns.size(); ++j) {
        int s = patterns[j].first->getNumFunctions();
        if (s <= i) i -= s;
        else {
            return patterns[j].first->getFunction(i);
        }
    }
  }
  virtual void setChildFunction(int i, FunctionClass<T> *f) {
    if (i < FunctionClass<T>::getNumChildFunctions()) FunctionClass<T>::setChildFunction(i, f);
    else {
      i -= FunctionClass<T>::getNumChildFunctions();
      if (i == 0) default_func = f;
      else {
        i--;
        if (i < (int)patterns.size()) patterns[i].second = f;
        else {
            i -= patterns.size();
            for (unsigned int j = 0; j < patterns.size(); ++j) {
                int s = patterns[i].first->getNumFunctions();
                if (s <= i) i -= s;
                else {
                    patterns[j].first->setFunction(i, f);
                    break;
                }
            }
        }
      }
    }
  }
  virtual bool isBranch(int i) {
    if (i < FunctionClass<T>::getNumChildFunctions()) return false;
    return true;
  }
};

template <class T>
class FunctionComposition : public FunctionClass<T>
{
public:
  FunctionClass<T> *f,*g;	//f(g(x))
  FunctionComposition(int dim, VRam *vr, FunctionClass<T>* f, FunctionClass<T>* g, FunctionParseInput<T> &data) : FunctionClass<T>("composition", dim, vr, data), f(f), g(g) {
    if (!FunctionClass<T>::matchType(g->getOutput(),f->getInput())) throw CompileError(TYPE_MISTMATCH,"Type mismatch", data.filename, data.line, data.col);
    FunctionClass<T>::setUp(g->getInput(),f->getOutput());
  }
  virtual void* calc(void *data, void *data2, VRam &vr, int thread)
  {
    return f->calc(g->calc(data,data2,vr,thread),data2,vr,thread);
  }
  void collapseChildren(map<void*,FunctionClass<T>*> &fs)
  {
    f = FunctionClass<T>::collapse(f, fs);
    g = FunctionClass<T>::collapse(g, fs);
  }
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    g->updateCamera(cam);
    f->updateCamera(cam);
  }
  virtual void setRootFunction(RootFunctionClass<T> *root)
  {
    if (FunctionClass<T>::rootSet) return;
    FunctionClass<T>::setRootFunction(root);
    g->setRootFunction(root);
    f->setRootFunction(root);
  }
  virtual int getNumChildFunctions() {return FunctionClass<T>::getNumChildFunctions()+2;}
  virtual FunctionClass<T>* getChildFunction(int i) {
    if (i < FunctionClass<T>::getNumChildFunctions()) return FunctionClass<T>::getChildFunction(i);
    i -= FunctionClass<T>::getNumChildFunctions();
    if (i == 0) return g;
    return f;
  }
  virtual void setChildFunction(int i, FunctionClass<T> *h) {
    if (i < FunctionClass<T>::getNumChildFunctions()) FunctionClass<T>::setChildFunction(i, h);
    else {
      i -= FunctionClass<T>::getNumChildFunctions();
      if (i == 0) g = h;
      else f = h;
    }
  }
};

template <class T>
class FunctionComposition_leveldown : public FunctionComposition<T>
{
public:
  FunctionComposition_leveldown(int dim, VRam *vr, FunctionClass<T>* f, FunctionClass<T>* g, FunctionParseInput<T> &data) : FunctionComposition<T>(dim, vr, f, g, data) {}
  virtual void* calc(void *data, void *data2, VRam &vr, int thread)
  {
    return FunctionComposition<T>::f->calc(FunctionComposition<T>::g->calc(data,data2,vr,thread),data,vr,thread);
  }
};

template <class T>
class FunctionComposition_levelup : public FunctionComposition<T>
{
public:
  FunctionComposition_levelup(int dim, VRam *vr, FunctionClass<T>* f, FunctionClass<T>* g, FunctionParseInput<T> &data) : FunctionComposition<T>(dim, vr, f, g, data) {}
  virtual void* calc(void *data, void *data2, VRam &vr, int thread)
  {
    return FunctionComposition<T>::f->calc(FunctionComposition<T>::g->calc(data,data2,vr,thread),FunctionComposition<T>::f->getData2(thread),vr,thread);
  }
};

template <class T>
class FunctionComposition_levelupdown : public FunctionComposition<T>
{
  FunctionClass<T> *neightbour;
public:
  FunctionComposition_levelupdown(int dim, VRam *vr, FunctionClass<T>* f, FunctionClass<T>* g, FunctionClass<T> *neightbour, FunctionParseInput<T> &data) : FunctionComposition<T>(dim, vr, f, g, data), neightbour(neightbour) {
      throw CompileError(UNSUPPORTED_FEATURE, "You cannot call subfunctions that belong to other parent and are not above the current function", data.filename, data.line, data.col);
  }
  virtual void* calc(void *data, void *data2, VRam &vr, int thread)
  {
    return FunctionComposition<T>::f->calc(FunctionComposition<T>::g->calc(data,data2,vr,thread),neightbour->getData2(thread),vr,thread);
  }
};

template <class T>
class Function_addarray : public FunctionClass<T>
{
public:
  Function_addarray(int dim, VRam *vr, string type) : FunctionClass<T>("::", dim, vr) {FunctionClass<T>::setUp(type+","+TYPE_ARRAY_SIGN+type,TYPE_ARRAY_SIGN+type);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    int size = FunctionClass<T>::parameters.size();
    pair<void*,void*> *ret = (pair<void*,void*>*)FunctionClass<T>::parameters[size-1]->calc(data,data2,vr,thread);
    for (int i = size-2; i >= 0; --i) {
      pair<void*,void*> *ret2 = (pair<void*,void*>*)FunctionClass<T>::outConstructor->construct(vr);
      ret2->first = FunctionClass<T>::parameters[i]->calc(data,data2,vr,thread);
      ret2->second = ret;
      ret = ret2;
    }
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_addarray<T> *r = (Function_addarray<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_addarray<T>));
    return new (r) Function_addarray<T>(*this);
  }
};

template <class T>
class Function_root_extractor : public FunctionClass<T>
{
public:
  RootFunctionClass<T> *root;
  int index;
  Function_root_extractor(string name, int dim, VRam *vr, string output, RootFunctionClass<T> *root, int index) : index(index), root(root), FunctionClass<T>(name, dim, vr) {FunctionClass<T>::setUp("",output);}
  void* calc(void*, void*, VRam&, int thread) {
    return root->extract(index,thread);
  }
  int getType() const {return FunctionClass<T>::getType() | FUNCTIONTYPE_ARGDEP;}
};

template <class T>
class RootFunctionClass : public FunctionClass<T>
{
protected:
  void ***memory;
  void ***memory2;
  int *memory_index;
  int *memory_index2;
  int memory_size;
public:
  vector<FunctionClass<T>*> functions;
  FunctionClass<T> *root;
  RootFunctionClass(int,VRam*,FunctionClass<T>*);
  void* calc(void*, void*, VRam&, int);
  void collapseChildren(map<void*,FunctionClass<T>*> &fs) {root = FunctionClass<T>::collapse(root, fs);}
  FunctionClass<T>* copy() {throw Exception(INVALID_COPY, "RootFunctionClass::copy() should no be used");}
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    root->updateCamera(cam);
  }
  virtual void setRootFunction(RootFunctionClass<T> *r)
  {
    if (FunctionClass<T>::rootSet) return;
    FunctionClass<T>::setRootFunction(r);
    root->setRootFunction(this);
  }
  virtual void* getData2(int thread) const {
    return memory2[thread][memory_index2[thread]-1];
  }
  virtual int getNumChildFunctions() {return FunctionClass<T>::getNumChildFunctions()+1;}
  virtual FunctionClass<T>* getChildFunction(int i) {
    if (i < FunctionClass<T>::getNumChildFunctions()) return FunctionClass<T>::getChildFunction(i);
    return root;
  }
  virtual void setChildFunction(int i, FunctionClass<T> *f) {
    if (i < FunctionClass<T>::getNumChildFunctions()) FunctionClass<T>::setChildFunction(i, f);
    else root = f;
  }
  virtual void updateRoots(map<void*,FunctionClass<T>*> &fs)
  {
    if (memory_size != 0) return;
    int size = functions.size();
    for (int i = 0; i < size; ++i) if (fs.find(functions[i]) != fs.end()) functions[i] = fs[functions[i]];
    if (size > 0) {
      sort(functions.begin(), functions.end());			//sort by memory addresse
      auto it = unique (functions.begin(), functions.end());		//erase identical functions
      functions.resize(std::distance(functions.begin(),it) );
    }
    size = functions.size();
    
    pair<int,int> *t = new pair<int,int>[size];
    countFunctions(this,functions,this,t);
    vector<FunctionClass<T>*> branched;
    for (int i = 0; i < size; ++i) {
      //cout<<" "<<functions[i]->getName()<<": ("<<t[i].first<<", "<<t[i].second<<")"<<endl;
      if (t[i].second == 0 || t[i].first <= 1) {
	if (t[i].first > 1) {
	  branched.push_back(functions[i]);
	  //cout<<"branched:  "<<functions[i]->getName()<<": ("<<t[i].first<<", "<<t[i].second<<")  ("<<functions[i]<<")"<<endl;
	}
	//else cout<<"off:  "<<functions[i]->getName()<<": ("<<t[i].first<<", "<<t[i].second<<")  ("<<functions[i]<<")"<<endl;
	t[i] = t[size-1];
	functions[i] = functions[size-1];
	functions.resize(--size);
	--i;
	continue;
      }
    }
    //for (int i = 0; i < size; ++i) cout<<" "<<functions[i]->getName()<<": ("<<t[i].first<<", "<<t[i].second<<")  ("<<functions[i]<<")"<<endl;
    delete [] t;
    if (size > 0) {
      bool **access = new bool*[size];
      //cout<<endl;
      bool *visited = new bool[size];
      for (int i = 0; i < size; ++i) {
	access[i] = new bool[size];
	for (int j = 0; j < size; ++j) access[i][j] = false;
	getAccess(functions[i], functions, this, functions[i], access[i], 0);
	//for (int j = 0; j < size; ++j) cout<<access[i][j];
	//cout<<endl;
      }
      int error = -1;
      for (int i = 0; i < size; ++i) {
	for (int j = 0; j < size; ++j) visited[j] = false;
	if (searchCircle(i,access,visited,size)) error = i;
      }
      if (error == -1) {
	vector<pair<pair<FunctionClass<T>*,bool*>,int>> points;
	vector<pair<pair<FunctionClass<T>*,bool*>,int>> outpoints;
	int *nums = new int[size];
	for (int i = 0; i < size; ++i) nums[i] = 0;
	for (int i = 0; i < size; ++i) for (int j = 0; j < size; ++j) if (access[i][j]) nums[i]++;
	//for (int j = 0; j < size; ++j) cout<<nums[j]<<endl;
	for (int i = 0; i < size; ++i) points.push_back(make_pair(make_pair(functions[i],access[i]),nums[i]));
    int n = 0;
	while (points[n].second != 0) ++n;				//No circle => existence
	topoSort(n, points, outpoints, size);
    if ((int)outpoints.size() != size) error = n;
	else {
	  //cout<<endl;
	  for (int i = 0; i < size; ++i) {
	    functions[i] = outpoints[i].first.first;
	    access[i] = outpoints[i].first.second;
	    //for (int j = 0; j < size; ++j) cout<<access[i][j];
	    //cout<<"    : "<<functions[i]->getName()<<endl;
	    vector<FunctionClass<T>*> swaps;
	    vector<Function_root_extractor<T>*> extractors;
	    for (int j = 0; j < i; ++j) if (access[i][j]) {
	      swaps.push_back(functions[j]);
	      int index = i-j;
	      extractors.push_back(new ((Function_root_extractor<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_root_extractor<T>))) Function_root_extractor<T>(functions[j]->getName() + "_precalced", FunctionClass<T>::getDim(), FunctionClass<T>::getVram(), functions[j]->getOutput(), this, index));
	    }
	    if (extractors.size() > 0) {
	      //cout<<"replaced: "<<functions[i]->getName()<<endl;
	      replace(functions[i], swaps, extractors[0], extractors);
	      //cout<<"done"<<endl;
	    }
	  }
	  //cout<<endl;
	}
      }
      for (int i = 0; i < size; ++i) delete [] access[i];
      delete [] access;
      delete [] visited;
      if (error != -1) {
	stringstream ss;
	ss<<"Invalid recursion ("<<error<<")";
	throw CompileError(INVALID_RECURSION, ss.str(), functions[error]->parse_data.filename, functions[error]->parse_data.line, functions[error]->parse_data.col);
      }
      
      vector<Function_root_extractor<T>*> extractors;
      for (int i = 0; i < size; ++i) {
	int index = size-i;
	extractors.push_back(new ((Function_root_extractor<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_root_extractor<T>))) Function_root_extractor<T>(functions[i]->getName() + "_precalced", FunctionClass<T>::getDim(), FunctionClass<T>::getVram(), functions[i]->getOutput(), this, index));
      }
      replace(this, functions, extractors[0], extractors);
    }
    if (branched.size() > 0) plantBranches(this, branched, &memory_size, fs);
    
    
    memory_size = functions.size();
    int db = 1;
    if (FunctionClass<T>::rec) db = atoi(MachineConfig::instance->getApp()->getAttribute("reclimit",DEFAULT_RECMEMORY).c_str());
    memory_size *= db;
    memory_index = (int*)FunctionClass<T>::func_vram->allocate(sizeof(int)*FunctionClass<T>::parse_data.threads);
    memory_index2 = (int*)FunctionClass<T>::func_vram->allocate(sizeof(int)*FunctionClass<T>::parse_data.threads);
    memory = (void***)FunctionClass<T>::func_vram->allocate(sizeof(void*)*FunctionClass<T>::parse_data.threads);
    memory2 = (void***)FunctionClass<T>::func_vram->allocate(sizeof(void*)*FunctionClass<T>::parse_data.threads);
    for (int i = 0; i < FunctionClass<T>::parse_data.threads; ++i) {
      memory[i] = (void**)FunctionClass<T>::func_vram->allocate(sizeof(void*)*memory_size);
      memory2[i] = (void**)FunctionClass<T>::func_vram->allocate(sizeof(void*)*db);
    }
  }
  void plantBranches(FunctionClass<T>* f, vector<FunctionClass<T>*> &funcs, void *mark, map<void*,FunctionClass<T>*> &fs) {
    if (f == NULL) return;
    if (f->mark == mark) return;
    f->mark = mark;
    BranchFunctionClass<T> *branch = dynamic_cast<BranchFunctionClass<T>*>(f);
    if (branch != NULL) {
      for (int i = 0; i < branch->getNumChildFunctions(); ++i) {
	if (!branch->isBranch(i)) plantBranches(branch->getChildFunction(i),funcs,mark,fs);
	else {
	  FunctionClass<T> *br = branch->getChildFunction(i);
	  RootFunctionClass<T> *rt = new ((RootFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(RootFunctionClass<T>))) RootFunctionClass<T>(FunctionClass<T>::getDim(), FunctionClass<T>::func_vram, br);
      rt->setRecursion(FunctionClass<T>::rec);
      for (unsigned int j = 0; j < funcs.size(); ++j) rt->functions.push_back(funcs[j]);
	  rt->updateRoots(fs);
	  if (rt->functions.size() > 0) branch->setChildFunction(i, rt);				//If no need for rt, leave it in the vram
	}
      }
    } else for (int i = 0; i < f->getNumChildFunctions(); ++i) plantBranches(f->getChildFunction(i),funcs,mark,fs);
  }
  void topoSort(int n, vector<pair<pair<FunctionClass<T>*,bool*>,int>> &points, vector<pair<pair<FunctionClass<T>*,bool*>,int>> &outpoints, int size) {
    outpoints.push_back(points[n]);
    points[n].second = -1;
    for (int i = 0; i < size; ++i) if (points[i].first.second[n]) points[i].second--;
    for (int i = 0; i < size; ++i) if (points[i].second == 0) topoSort(i, points, outpoints, size);
  }
  bool searchCircle(int x, bool **t, bool *visited, int size) {
    if (visited[x]) return true;
    visited[x] = true;
    for (int i = 0; i < size; ++i) if (t[x][i]) if (searchCircle(i,t,visited,size)) return true;
    return false;
  }
  void getAccess(FunctionClass<T> *f, vector<FunctionClass<T>*> &search, FunctionClass<T> *main, void *mark, bool *t, int depth) {
    if (f == NULL || f == main) return;
    int found = -1;
    if (depth > 0) {
      for (unsigned int i = 0; i < search.size(); ++i) if (f == search[i]) {
	found = i;
	break;
      }
    }
    if (found != -1) t[found] = true;
    else if (f->mark != mark) {
      f->mark = mark;
      for (int i = 0; i < f->getNumChildFunctions(); ++i) getAccess(f->getChildFunction(i), search, main, mark, t, depth+1);
    }
  }
  void countFunctions(FunctionClass<T> *f, vector<FunctionClass<T>*> &search, void *mark, pair<int,int> *t) {		//<global,local>
    if (f == NULL) return;
    pair<int,int> *t2 = new pair<int,int>[search.size()];
    pair<int,int> *t3 = new pair<int,int>[search.size()];
    pair<int,int> *t4 = new pair<int,int>[search.size()];
    //cout<<"     call on: "<<f->getName()<<" ("<<f->getNumChildFunctions()<<")"<<endl;
    /*int found = -1;
    for (unsigned int i = 0; i < search.size(); ++i) if (f == search[i]) {
      found = i;
      break;
    }*/
    if (f->mark != mark) {
      f->mark = mark;
      BranchFunctionClass<T> *branch = dynamic_cast<BranchFunctionClass<T>*>(f);
      if (branch != NULL) {
	for (int i = 0; i < branch->getNumChildFunctions(); ++i) {
	  if (!branch->isBranch(i)) countFunctions(branch->getChildFunction(i), search, mark, t2);
	  else {
	    countFunctions(branch->getChildFunction(i), search, mark, t3);
        for (unsigned int j = 0; j < search.size(); ++j) {
	      if (t3[j].first > t4[j].first) t4[j] = t3[j];
	      t3[j] = make_pair(0,0);
	    }
	  }
	}
    for (unsigned int i = 0; i < search.size(); ++i) {
	  t2[i].second = 0;
	  t2[i].first += t4[i].first;
	}
      } else for (int i = 0; i < f->getNumChildFunctions(); ++i) countFunctions(f->getChildFunction(i), search, mark, t2);
      //f->mark = NULL;
    }
    for (unsigned int i = 0; i < search.size(); ++i) {
      t[i].first += t2[i].first;
      t[i].second += t2[i].second;
      if (f == search[i]) {
	t[i].first++;
	t[i].second++;
      }
    }
    delete [] t2;
    delete [] t3;
    delete [] t4;
  }
  void replace(FunctionClass<T> *f, vector<FunctionClass<T>*> &search, void *mark, vector<Function_root_extractor<T>*> &extractors) {		//<global,local>
    if (f == NULL) return;
    //cout<<"     call on: "<<f->getName()<<" ("<<f->getNumChildFunctions()<<")"<<endl;
    if (f->mark != mark) {
      f->mark = mark;
      for (int i = 0; i < f->getNumChildFunctions(); ++i) {
	FunctionClass<T> *g = f->getChildFunction(i);
	int found = -1;
    for (unsigned int j = 0; j < search.size(); ++j) if (g == search[j]) {
	  found = j;
	  break;
	}
	if (found == -1) replace(g, search, mark, extractors);
	else f->setChildFunction(i, extractors[found]);
      }
    }
  }
  void* extract(int index, int thread) {
    //cout<<memory_index<<"-"<<index<<endl;
    return memory[thread][memory_index[thread]-index];
  }
};

template <class T>
RootFunctionClass<T>::RootFunctionClass(int dim, VRam *vram, FunctionClass<T> *root) : FunctionClass<T>(root->getName(),dim,vram), root(root), memory_index(0), memory_index2(0)
{
  FunctionClass<T>::setUp(root->getInput(),root->getOutput());
  FunctionClass<T>::parse_data = root->parse_data;
}

template <class T>
void* RootFunctionClass<T>::calc(void *data, void *data2, VRam &vr, int thread)
{
  int start = memory_index[thread];
  if (memory_index[thread] + functions.size() > memory_size) {
    stringstream ss;
    ss<<"Recursion limit exceeded ("<<memory_index[thread]<<"+"<<(functions.size())<<" / "<<memory_size<<")";
    throw RuntimeError(RECURSION_LIMIT_EXCEEDED, ss.str(), FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line, FunctionClass<T>::parse_data.col);
  }
  memory2[thread][memory_index2[thread]++] = data2;
  //cout<<FunctionClass<T>::getName()<<endl;
  //cout<<"Recursion limit exceeded ("<<memory_index<<"+"<<(functions.size()+1)<<" / "<<memory_size<<")"<<endl;
  //cout<<"calc: "<<endl;
  for (int i = 0; i < functions.size(); ++i) {
    memory[thread][memory_index[thread]] = functions[i]->calc(NULL,data,vr,thread);
    memory_index[thread]++;
  }
  //cout<<"done"<<endl;
  void *ret = root->calc(data,data2,vr,thread);
  memory_index[thread] = start;
  memory_index2[thread]--;
  //cout<<"root: "<<ret<<endl;
  return ret;
}

template <class T>
class TurnFunctionClass : public FunctionClass<T>
{
public:
  FunctionClass<T> *root;
  TurnFunctionClass(int dim, VRam *vram, FunctionClass<T> *root) : FunctionClass<T>("-",dim,vram), root(root) {FunctionClass<T>::setUp(root->getInput(),root->getOutput());}
  void* calc(void *data, void *data2, VRam &vr, int thread)
  {
    /*T *v1 = (T*)data;
    T *v2 = (T*)data2;
    if (v1 != NULL) LOG<<"v1 = "<<v1[0]<<" "<<v1[1]<<" "<<v1[2]<<endl;
    if (v2 != NULL) LOG<<"v2 = "<<v2[0]<<" "<<v2[1]<<" "<<v2[2]<<endl;*/
    return root->calc(data2,data,vr,thread);
  }
  void collapseChildren(map<void*,FunctionClass<T>*> &fs) {root = FunctionClass<T>::collapse(root, fs);}
  FunctionClass<T>* copy() {throw Exception(INVALID_COPY, "TurnFunctionClass::copy() should no be used");}
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    root->updateCamera(cam);
  }
  virtual void setRootFunction(RootFunctionClass<T> *r)
  {
    if (FunctionClass<T>::rootSet) return;
    FunctionClass<T>::setRootFunction(r);
    root->setRootFunction(r);
  }
  virtual int getNumChildFunctions() {return FunctionClass<T>::getNumChildFunctions()+1;}
  virtual FunctionClass<T>* getChildFunction(int i) {
    if (i < FunctionClass<T>::getNumChildFunctions()) return FunctionClass<T>::getChildFunction(i);
    return root;
  }
  virtual void setChildFunction(int i, FunctionClass<T> *f) {
    if (i < FunctionClass<T>::getNumChildFunctions()) FunctionClass<T>::setChildFunction(i, f);
    else root = f;
  }
};

template <class T>
class LevelFunctionClass : public FunctionClass<T>
{
public:
  FunctionClass<T> *root;
  LevelFunctionClass(int dim, VRam *vram, FunctionClass<T> *root) : FunctionClass<T>("-",dim,vram), root(root) {FunctionClass<T>::setUp(root->getInput(),root->getOutput());}
  virtual void* calc(void *data, void *data2, VRam &vr, int thread) = 0;
  void collapseChildren(map<void*,FunctionClass<T>*> &fs) {root = FunctionClass<T>::collapse(root, fs);}
  FunctionClass<T>* copy() {throw Exception(INVALID_COPY, "LevelFunctionClass::copy() should no be used");}
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    root->updateCamera(cam);
  }
  virtual void setRootFunction(RootFunctionClass<T> *r)
  {
    if (FunctionClass<T>::rootSet) return;
    FunctionClass<T>::setRootFunction(r);
    root->setRootFunction(r);
  }
  virtual int getNumChildFunctions() {return FunctionClass<T>::getNumChildFunctions()+1;}
  virtual FunctionClass<T>* getChildFunction(int i) {
    if (i < FunctionClass<T>::getNumChildFunctions()) return FunctionClass<T>::getChildFunction(i);
    return root;
  }
  virtual void setChildFunction(int i, FunctionClass<T> *f) {
    if (i < FunctionClass<T>::getNumChildFunctions()) FunctionClass<T>::setChildFunction(i, f);
    else root = f;
  }
};

template <class T>
class LevelFunctionClass_down : public LevelFunctionClass<T>
{
public:
  LevelFunctionClass_down(int dim, VRam *vram, FunctionClass<T> *root) : LevelFunctionClass<T>(dim,vram,root) {}
  void* calc(void *data, void*, VRam &vr, int thread) {
//    T *v = (T*)data;
    return LevelFunctionClass<T>::root->calc(NULL,data,vr,thread);
  }
};

template <class T>
class LevelFunctionClass_up : public LevelFunctionClass<T>
{
public:
  LevelFunctionClass_up(int dim, VRam *vram, FunctionClass<T> *root) : LevelFunctionClass<T>(dim,vram,root) {}
  void* calc(void*, void*, VRam &vr, int thread) {
    return LevelFunctionClass<T>::root->calc(NULL,LevelFunctionClass<T>::root->getData2(thread),vr,thread);
  }
};

template <class T>
class LevelFunctionClass_updown : public LevelFunctionClass<T>
{
  FunctionClass<T> *neightbour;
public:
  LevelFunctionClass_updown(int dim, VRam *vram, FunctionClass<T> *root, FunctionClass<T> *neightbour) : LevelFunctionClass<T>(dim,vram,root), neightbour(neightbour) {}
  void* calc(void*, void*, VRam &vr, int thread) {
    return LevelFunctionClass<T>::root->calc(NULL,neightbour->getData2(thread),vr,thread);
  }
};

template <class T>
class Function_negate_b : public FunctionClass<T>
{
public:
  Function_negate_b(int dim, VRam *vr) : FunctionClass<T>("!", dim, vr) {FunctionClass<T>::setUp(TYPE_BOOL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr, int thread) {
    bool * ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = !(*((bool*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr,thread))));
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_negate_b<T> *r = (Function_negate_b<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_negate_b<T>));
    return new (r) Function_negate_b<T>(*this);
  }
};

template <class T>
class CustomFunctionClass : public FunctionClass<T>
{
  string func;
  string arguments;
  string rootArguments;
  vector<FunctionClass<T>*> *build_functions;
  vector<CustomFunctionClass<T>*> genericVersions;
  FunctionClass<T>* parseContent(const string, FunctionParseInput<T>, vector<Function_const_value<T>*>&,const vector<pair<FunctionClass<T>*,string>>&, string) const;
  void parseArgumentFunctions(const string,CustomType<T>*,Function_fetch_array<T>*,FunctionClass<T>*,FunctionParseInput<T>&);
  static string getGeneric(string,string,FunctionParseInput<T>&);
  CustomFunctionClass<T>* generateFromGeneric(string);
  static string operations[];
  bool parsed;
  bool argumentInBrackets;
  RootFunctionClass<T> *toBuild;
  string getType(const string) const;
  pair<string,FunctionClass<T>*> getFunctionSignature(const string, string, string, FunctionParseInput<T>, vector<Function_const_value<T>*>&, const vector<pair<FunctionClass<T>*,string>>&, vector<pair<string,FunctionClass<T>*>>*,int&) const;
  FunctionClass<T>* getFunctionFromSignature(const string, string, FunctionParseInput<T>, vector<Function_const_value<T>*>&,const vector<pair<FunctionClass<T>*,string>>&, vector<FunctionClass<T>*>&,string) const;
  string getContent(const string) const;
  static string getTypeFromGeneric(string, string, map<string,string>&);
  string getRootArgsSignature() const {return rootArguments;}
  static string getRootArgsSignature(const vector<pair<FunctionClass<T>*,string>>&);
  bool canCallFunction(CustomFunctionClass<T>*,FunctionParseInput<T>&) const;
public:
  void setBuildFunctions(vector<FunctionClass<T>*> *b) {build_functions = b;}
  vector<FunctionClass<T>*> *getBuildFunctions() const {return build_functions;}
  string getRootArguments() const {return rootArguments;}
  CustomFunctionClass<T> *instantiate(const string, const string, FunctionParseInput<T>&, vector<Function_const_value<T>*>&, 
  const vector<pair<FunctionClass<T>*,string>>&, bool parse=true);
  CustomFunctionClass<T> * parseInstance(CustomFunctionClass<T> *ret, FunctionParseInput<T> &data, vector<Function_const_value<T>*> &parameters,
                                         const vector<pair<FunctionClass<T>*,string>> &rootArgs);
  string getFunc() const {return func;}
  string getArguments() const {return arguments;}
  int getNumGenericVersions() const {return genericVersions.size();}
  CustomFunctionClass<T>* getGenericVersion(int i) const {return genericVersions[i];}
  void setGenericVersion(int i, CustomFunctionClass<T> *f) {genericVersions[i] = f;}
  void addGenericVersion(CustomFunctionClass<T> *f) {genericVersions.push_back(f);}
  void eraseGenericVersion(CustomFunctionClass<T> *f) {
    for (unsigned int i = 0; i < genericVersions.size(); ++i) if (genericVersions[i] == f) {
      genericVersions[i] = genericVersions[genericVersions.size()-1];
      genericVersions.resize(genericVersions.size()-1);
    }
  }
  bool isGeneric() const {
    if (FunctionClass<T>::generic) return true;
    if (FunctionClass<T>::getOutConstructor() != NULL && FunctionClass<T>::getOutConstructor()->isGeneric()) return true;
    if (FunctionClass<T>::getInput() != "") if (FunctionClass<T>::getInConstructor(0)->isGeneric()) return true;
    return false;
  }
  FunctionClass<T>* findByElement(CustomFunctionClass<T>*) const;
  bool isParsed() const {return parsed;}
  vector<pair<FunctionClass<T>*,string>> argument_functions;
  FunctionClass<T> *root;
  CustomFunctionClass(string,string,string,int,VRam*,RootFunctionClass<T>*);
  CustomFunctionClass(string,string,string,int,VRam*,FunctionParseInput<T>&);
  CustomFunctionClass(string,string,string,int,VRam*,FunctionParseInput<T>&,bool);
  pair<string,ERROR_CODE> preParse(FunctionParseInput<T>,vector<Function_const_value<T>*>&,const vector<pair<FunctionClass<T>*,string>>&);
  void parse(FunctionParseInput<T>,vector<Function_const_value<T>*>&,const vector<pair<FunctionClass<T>*,string>>&);
  void* calc(void *data, void *data2, VRam &vr, int thread);
  void collapseChildren(map<void*,FunctionClass<T>*> &fs);
  FunctionClass<T>* copy() {
    CustomFunctionClass<T> *r = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
    return new (r) CustomFunctionClass<T>(*this);
  }
  virtual void updateCamera(void *cam) {
    if (FunctionClass<T>::camera == cam) return;
    FunctionClass<T>::updateCamera(cam);
    root->updateCamera(cam);
  }
  virtual void setRootFunction(RootFunctionClass<T> *r)
  {
    if (FunctionClass<T>::rootSet) return;
    FunctionClass<T>::setRootFunction(r);
    if (root == NULL) {
      stringstream ss;
      ss<<"Missing root ("<<FunctionClass<T>::getName()<<")";
      throw CompileError(MISSING_ROOT, ss.str(), FunctionClass<T>::parse_data.filename, FunctionClass<T>::parse_data.line, FunctionClass<T>::parse_data.col);
    }
    root->setRootFunction(r);
  }
  virtual int getNumChildFunctions() {return FunctionClass<T>::getNumChildFunctions()+1;}
  virtual FunctionClass<T>* getChildFunction(int i) {
    if (i < FunctionClass<T>::getNumChildFunctions()) return FunctionClass<T>::getChildFunction(i);
    return root;
  }
  virtual void setChildFunction(int i, FunctionClass<T> *f) {
    if (i < FunctionClass<T>::getNumChildFunctions()) FunctionClass<T>::setChildFunction(i, f);
    else root = f;
  }
  bool checkType()
  {
    if (root->getOutput() == FunctionClass<T>::getOutput()) return true;
    return false;
  }
};

template <class T> string CustomFunctionClass<T>::getContent(const string s) const
{
  return s;		//TODO
  int n = s.find(")(");
  //cout<<s<<" "<<n<<endl;
  if (n == -1) return s;
  int x,y;
  int ins1,ins2;
  ins1 = ins2 = 0;
  x = n; y = n+1;
  while (x >= 0) {
    if (s[x] == ')') ins1++;
    else if (s[x] == '(') ins1--;
    if (ins1 == 0) break;
    x--;
  }
  while (y < (int)s.length()) {
    if (s[y] == '(') ins2++;
    else if (s[y] == ')') ins2--;
    if (ins2 == 0) break;
    y++;
  }
  if (ins1 != 0 || ins2 != 0) return s;
  if (x == 0 && y+1 == (int)s.length()) return s;
  if (x > 0 && y+1 < (int)s.length() && s[x-1] == '(' && s[y+1] == ')') return s;
  string s2 = getContent(s.substr(0,x)) + "(" + s.substr(x,y-x) + ")" + getContent(s.substr(y,-1));
  LOG.debug(s+" -> "+s2);
  //cout<<s<<" -> "<<s2<<endl;
  return s2;
}

template <class T> string CustomFunctionClass<T>::operations[] = {"'or'","'and'","'xor'","=>",">=","<=",">","<","!=","=","++","+","-","*","/","%","^",""};

template <class T> FunctionClass<T>* CustomFunctionClass<T>::findByElement(CustomFunctionClass<T> *f) const
{
  //cout<<FunctionClass<T>::parse_data.rootElement->toString()<<endl;
  //cout<<"caller: "<<FunctionClass<T>::getName()<<"  search: "<<f->getName()<<endl;
  Element *e = FunctionClass<T>::parse_data.ownElement;
  //cout<<"-----------------------------------------------\n\n\n\n\n\n"<<e->toString()<<endl;
  if (e == NULL) throw Exception(UNKNOWN_ERROR, "findByElement() started at NULL");
  e = e->root;
  CustomFunctionClass<T> *g, *g2;
  CustomFunctionClass<T> *last = f;
  while (e != NULL) {
    void *tag = e->getTag();
    if (tag != NULL) {
      g = (CustomFunctionClass<T>*)tag;
      if (!g->isGeneric()) {
          if (g == f) return f;
      } else for (int i = 0; i < g->getNumGenericVersions(); ++i) if (g->getGenericVersion(i) == f) return f;
      for (int i = 0; i < e->getChildCount(); ++i) {
	Element *e2 = e->getChild(i);
	void *tag = e2->getTag();
	if (tag == NULL) continue;
	g2 = (CustomFunctionClass<T>*)tag;
	bool foundFunction = false;
    if (g2->isGeneric()) {
	  for (int j = 0; j < g2->getNumGenericVersions(); ++j) if (g2->getGenericVersion(j) == f) {
	    foundFunction = true;
	    break;
	  }
	} else if (g2 == f) foundFunction = true;
	if (foundFunction) {
	  void **mark = (void**)this;
	  if (FunctionClass<T>::mark == mark) mark++;	//Make sure mark is not used already, (this) is never a RootFunctionClass
      if (!last->isGeneric()) return last;
	  for (int j = 0; j < last->getNumGenericVersions(); ++j) {
	    CustomFunctionClass<T> *l = last->getGenericVersion(j);
	    if (l->getInput() == g2->getRootArgsSignature()) {		//Found a match, need to check if this functions calls (this)
	      FunctionClass<T>::visit(l,NULL);		//Reset
	      FunctionClass<T>::visit(l,mark);
	      if (FunctionClass<T>::mark == mark) return l;
	    }
	  }
	}
      }
      last = g;
    }
    e = e->root;
  }
  throw Exception(UNKNOWN_ERROR,"findByElement() could not find a function");
}

template <class T> CustomFunctionClass<T>* CustomFunctionClass<T>::generateFromGeneric(string gen)
{
  string type = "";
  int num = FunctionClass<T>::getNumParameter();
  if (num > 0) type = FunctionClass<T>::getInConstructor(0)->getRoot()->toString();
  for (int i = 1; i < num; ++i) type = type + ","+FunctionClass<T>::getInConstructor(i)->getRoot()->toString();
  type = type+"->"+FunctionClass<T>::getOutConstructor()->getRoot()->toString();
  type = replace(type,"'",gen);
  //cout<<"Generating: "<<FunctionClass<T>::name<<" "<<func<<" : "<<type<<endl;
  CustomFunctionClass<T> *r = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
  CustomFunctionClass<T>* ret = new (r) CustomFunctionClass<T>(FunctionClass<T>::name,type,func,FunctionClass<T>::dim, FunctionClass<T>::func_vram,FunctionClass<T>::parse_data);
  return ret;
}

template <class T> string CustomFunctionClass<T>::getGeneric(string type, string expected, FunctionParseInput<T> &data)
{
  
  if (type == "'") return expected;
  if (type == "") return "";
  vector<string> root_t;
  vector<string> root_e;
  getParts(type,root_t,',',0);
  getParts(expected,root_e,',',0);
  return "";
  if (root_t.size() != 1) {
    string ret = "";
    for (int i = 0; i < root_t.size(); ++i) {
      ret = getGeneric(root_t[i],root_e[i],data);
      if (ret != "") break;
    }
    if (ret == "") return "";
    //cout<<type.length()<<" "<<expected.length()<<endl;
    //cout<<"'"<<type<<"' '"<<expected<<"'"<<endl;
    type = replace(type,"'",ret);
    //cout<<"'"<<type<<"' '"<<expected<<"'"<<endl;
    if (FunctionClass<T>::matchType(type, expected)) return ret;
    return "";
  }
  
  if (type[0] == TYPE_ARRAY_SIGN[0] && expected[0] == TYPE_ARRAY_SIGN[0]) {
    string ret = getGeneric(type.substr(1,-1), expected.substr(1,-1), data);
    if (ret == "") return "";
    type = replace(type,"'",ret);
    if (FunctionClass<T>::matchType(type, expected)) return ret;
    return "";
  }
  
  if (type[0] != TYPE_LIST_SIGN[0]) return "";
  if (expected[0] != TYPE_LIST_SIGN[0]) return "";
  vector<string> parts_t;
  vector<string> parts_e;
  getParts(type,parts_t,',',1);
  getParts(expected,parts_e,',',1);
  if (parts_t.size() != parts_e.size()) throw CompileError(CANNOT_PARSE_GENERIC, "Could not parse generic function",data.filename,data.line,data.col);
  string ret = "";
  for (int i = 0; i < parts_t.size(); ++i) {
    ret = getGeneric(parts_t[i],parts_e[i],data);
    if (ret != "") break;
  }
  if (ret == "") return "";
  type = replace(type,"'",ret);
  if (FunctionClass<T>::matchType(type, expected)) return ret;
  return "";
}

template <class T>
CustomFunctionClass<T>::CustomFunctionClass(string name, string input, string output, int dim, VRam *vr, RootFunctionClass<T> *rootf) : FunctionClass<T>(name, dim, vr), func(""), parsed(true), argumentInBrackets(false), toBuild(rootf), rootArguments(""), build_functions(NULL)
{
  root = rootf;
  FunctionClass<T>::parse_data = root->parse_data;
  FunctionClass<T>::setUp(FunctionClass<T>::getTypes(input), FunctionClass<T>::getTypes(output));
}

template <class T> CustomFunctionClass<T>::CustomFunctionClass(string name, string type, string func, int dim, VRam *vr, FunctionParseInput<T> &data) : FunctionClass<T>(name, dim, vr, data), func(func), parsed(false), argumentInBrackets(false), toBuild(NULL), rootArguments(""), build_functions(NULL)
{
  int n = type.find("->");
  string i,o;
  i = type.substr(0,n);
  o = type.substr(n+2,-1);
  FunctionClass<T>::setUp(FunctionClass<T>::getTypes(i), FunctionClass<T>::getTypes(o));
}

template <class T> CustomFunctionClass<T>::CustomFunctionClass(string name, string type, string func, int dim, VRam *vr, FunctionParseInput<T> &data, bool customfunc) : FunctionClass<T>(name, dim, vr, data), func(func), parsed(false), argumentInBrackets(false), toBuild(NULL), rootArguments(""), build_functions(NULL)
{
  int n = type.find("->");
  string i,o;
  i = type.substr(0,n);
  o = type.substr(n+2,-1);
  if (customfunc) {
    vector<string> parts;
    getParts(i,parts,',',0);
    if (parts.size() > 1) {
      i = TYPE_LIST_SIGN+"("+i+")";
      argumentInBrackets = true;
    }
  }
  FunctionClass<T>::setUp(FunctionClass<T>::getTypes(i), FunctionClass<T>::getTypes(o));
}

template <class T> void CustomFunctionClass<T>::collapseChildren(map<void*,FunctionClass<T>*> &fs) {
  root = FunctionClass<T>::collapse(root, fs);
}

template <class T> void* CustomFunctionClass<T>::calc(void *data, void *data2, VRam &vr, int thread)			//Only executed during build time
{
  //cout<<FunctionClass<T>::getName()<<endl;
  //throw RuntimeError("Parsing error (an instance of CustomFunctionClass is used)",FunctionClass<T>::parse_data.filename,FunctionClass<T>::parse_data.line,FunctionClass<T>::parse_data.col);
  return root->calc(data,data2,vr,thread);
}

template <class T> string CustomFunctionClass<T>::getType(const string s) const {
  vector<string> parts;
  getParts(s,parts,',',0);
  if (parts.size() > 1) return TYPE_LIST_SIGN+"("+s+")";
  return s;
}

template <class T> void CustomFunctionClass<T>::parseArgumentFunctions(const string tp, CustomType<T> *datatype, Function_fetch_array<T> *fetched, FunctionClass<T> *param_f, FunctionParseInput<T> &data)
{
  //cout<<"Argument: "<<tp<<endl;
  if (tp == "") throw SyntacsError(MISSING_ARG_NAME, "Missing argument name",data.filename,data.line,data.col);
  if (tp == "_") return;
  int first = -1;
  int inside = 0;
  int numout = 0;
  //int last = -1;
  //int comma = -1;
  for (unsigned int i = 0; i < tp.length(); ++i) {
    if (tp[i] == '(') {
      inside++;
      if (first == -1) first = i;
    } else if (tp[i] == ')') {
      //last = i;
      inside--;
    } else if (tp[i] == ',' && inside == 1) {
      //comma = i;
    }
    if (inside == 0) numout++;
  }
  if (inside != 0) {
    if (inside > 0) throw SyntacsError(EXPECTED_C, "')' expected",data.filename,data.line,data.col);
    throw SyntacsError(EXPECTED_C, "'(' expected",data.filename,data.line,data.col);
  }
  if (first != 0) {
    if (param_f != NULL) argument_functions.push_back(make_pair(param_f,tp.substr(0,first)));
    else argument_functions.push_back(make_pair(fetched,tp.substr(0,first)));
  }
  if (first != -1) {
    CustomTypeList<T> *a = dynamic_cast<CustomTypeList<T>*>(datatype);
    if (a == NULL) throw CompileError(INVALID_ARG_TYPE, "Invalid argument type",data.filename,data.line,data.col);
    inside = 0;
    int n = 0;
    int last = 0;
    for (unsigned int i = 0; i < tp.length(); ++i) {
      if (tp[i] == '(') {
	inside++;
    if (inside == 1) last = i;
      } else if (tp[i] == ')') {
	inside--;
      }
      if ((tp[i] == ',' && inside == 1) || (tp[i] == ')' && inside == 0)) {
    if ((unsigned int)n >= a->types.size()) throw CompileError(WRONG_ARG_TYPE, "Wrong argument type",data.filename,data.line,data.col);
	
	Function_fetch_array<T> *ft = (Function_fetch_array<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_fetch_array<T>));
	Function_fetch_array<T> *ftch = new (ft) Function_fetch_array<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram,(CustomTypeList<T>*)datatype,n,fetched,param_f,data);
	
	parseArgumentFunctions(tp.substr(last+1,i-(last+1)),a->types[n++],ftch,NULL,data);
	last = i;
      }
    }
    if ((unsigned int)n < a->types.size()) throw CompileError(WRONG_ARG_TYPE, "Wrong argument type",data.filename,data.line,data.col);
  }
}

template <class T> string CustomFunctionClass<T>::getTypeFromGeneric(string type, string input, map<string,string> &types)			//pl: T(T(Real,*),Int) T(x,Int,z)
{
  if (type == "" || input == "") return "";
  if (!FunctionClass<T>::isTypeSign(type) && type[0] != TYPE_ARRAY_SIGN[0] && type[0] != TYPE_LIST_SIGN[0] && type[0] != TYPE_FUNCTION_SIGN[0]) return input;
  if (FunctionClass<T>::isTypeSign(input)) {
    if (type != input) return "";
  }
  if (input[0] == TYPE_ARRAY_SIGN[0]) {
    if (type[0] != TYPE_ARRAY_SIGN[0]) return "";
    string ret2 = getTypeFromGeneric(type.substr(1,-1),input.substr(1,-1),types);
    if (ret2 == "") return "";
    return TYPE_ARRAY_SIGN+ret2;
  }
  if (input[0] == TYPE_LIST_SIGN[0]) {
    if (type[0] != TYPE_LIST_SIGN[0]) return "";
    string ret = "";
    vector<string> tparts;
    getParts(type,tparts,',',1);
    vector<string> iparts;
    getParts(input,iparts,',',1);
    if (tparts.size() != iparts.size()) return "";
    if (tparts.size() < 2) return "";
    for (unsigned int i = 0; i < tparts.size(); ++i) {
      string ret2 = getTypeFromGeneric(tparts[i],iparts[i],types);
      if (ret2 == "") return "";
      ret += ","+ret2;
    }
    if (ret.length() > 0) ret = ret.substr(1,-1);
    return TYPE_LIST_SIGN + "(" + ret + ")";
  }
  if (input[0] == TYPE_FUNCTION_SIGN[0]) {
    if (type[0] != TYPE_FUNCTION_SIGN[0]) return "";
    vector<string> tparts,iparts;
    getParts(type,tparts,':',1);
    getParts(input,iparts,':',1);
    if (tparts.size() != 2 || iparts.size() != 2) return "";
    string r1,r2;
    r1 = getTypeFromGeneric(tparts[0],iparts[0],types);
    r2 = getTypeFromGeneric(tparts[1],iparts[1],types);
    if (r1 != "" || r2 != "") return "";
    return TYPE_FUNCTION_SIGN+"("+r1+":"+r2+")";
  }
  //Otherwise include is generic
  if (input == "*") return type;
  if (types[input] == "") types[input] = type;
  return types[input];
}

template <class T> CustomFunctionClass<T> *CustomFunctionClass<T>::instantiate(const string input, const string output, FunctionParseInput<T> &data, vector<Function_const_value<T>*> &parameters, 
  const vector<pair<FunctionClass<T>*,string>> &rootArgs, bool parse)
{
  CustomFunctionClass<T> *ret = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
  
  //cout<<"Instantiating: "<<FunctionClass<T>::getName()<<" depth: "<<FunctionClass<T>::parse_data.depth<<endl;
  
  if (getFunc() == "") {		//Cfc -> RootFunctionClass -> Cfc
    
    RootFunctionClass<T> *ort = dynamic_cast<RootFunctionClass<T>*> (root);
    
    CustomFunctionClass<T> *f = dynamic_cast<CustomFunctionClass<T>*> (ort->root);
    //CustomFunctionClass<T> *retf = f->instantiate(input,output,FunctionClass<T>::parse_data,parameters,rootArgs);
    //HIBA;
    CustomFunctionClass<T> *retf = f->instantiate(input,output,data,parameters,rootArgs,false);

    RootFunctionClass<T> *rootf = new ((RootFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(RootFunctionClass<T>))) RootFunctionClass<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram, retf);
    ret = new (ret) CustomFunctionClass<T>(retf->getName(), retf->getInput(), retf->getOutput(), FunctionClass<T>::dim, FunctionClass<T>::func_vram, rootf);
    addGenericVersion(ret);
    retf = parseInstance(retf, data, parameters, rootArgs);
    if (retf == NULL) {
        return NULL;
    }
    if (retf->isGeneric()) {
        ret->setGeneric();
        eraseGenericVersion(ret);
        return NULL;
    }
    ret->setRootFunction_Manually(rootf);
    retf->setRootFunction_Manually(rootf);
    return ret;
  } else {
  
    map<string,string> types;
    
    
    string inputtype = "";
    if (FunctionClass<T>::getInput() != "") {
      inputtype = getTypeFromGeneric(input,FunctionClass<T>::getInput(),types);
      if (inputtype == "") return NULL;
    }
    string outputtype = getTypeFromGeneric(output,FunctionClass<T>::getOutput(),types);
    if (outputtype == "") outputtype = output;
    if (outputtype == "") outputtype = "*";
    
    //cout<<endl;
    //cout<<"i: "<<inputtype<<" ("<<input<<", "<<FunctionClass<T>::getInput()<<")"<<endl;
    //cout<<"o: "<<outputtype<<" ("<<output<<", "<<FunctionClass<T>::getOutput()<<")"<<endl;
    LOG.debug("instantiate: "+FunctionClass<T>::getName()+": "+inputtype+"->"+outputtype+" = "+getArguments() + getFunc());
    //cout<<"instantiate: "<<FunctionClass<T>::getName()<<": "<<inputtype+"->"+outputtype<<" = "<<getArguments() + getFunc()<<endl;
    ret = new (ret) CustomFunctionClass<T> (FunctionClass<T>::getName(),inputtype+"->"+outputtype,getArguments() + getFunc(),FunctionClass<T>::dim, FunctionClass<T>::func_vram, FunctionClass<T>::parse_data);
    //for (int j = 0; j < FunctionClass<T>::parameters.size(); ++j) ret->parameters.push_back(FunctionClass<T>::parameters[j]);
    Function_const<T> *inputf = new ((Function_const<T>*)FunctionClass<T>::getVram()->allocate(sizeof(Function_const<T>))) Function_const<T>(FunctionClass<T>::dim,inputtype,FunctionClass<T>::func_vram);
    ret->parameters.push_back(inputf);
    //cout<<" *** Parsing *** : "<<getFunc()<<endl;
    //cout<<"Done: "<<getFunc()<<endl;
    //cout<<"result: "<<ret->getOutput()<<" "<<ret->isGeneric()<<endl;
    pair<string,ERROR_CODE> er = ret->preParse(data,parameters,rootArgs);
    if (er.second != NONE) throw CompileError(er.second, er.first, data.filename, data.line, data.col);
    ret->setBuildFunctions(build_functions);
    addGenericVersion(ret);
    if (parse) return parseInstance(ret, data, parameters, rootArgs);
    else return ret;
  }
}

template <class T> CustomFunctionClass<T>* CustomFunctionClass<T>::parseInstance(CustomFunctionClass<T> *ret, FunctionParseInput<T> &data, vector<Function_const_value<T>*> &parameters,
                                                                                 const vector<pair<FunctionClass<T>*,string>> &rootArgs )
{
    ret->parse(data,parameters,rootArgs);
    if (ret->isGeneric()) {
      eraseGenericVersion(ret);
      return NULL;
    }
    return ret;
}

template <class T> string CustomFunctionClass<T>::getRootArgsSignature(const vector<pair<FunctionClass<T>*,string>> &rootArgs)
{
  string ret = "";
  for (unsigned int i = 0; i < rootArgs.size(); ++i) ret += "," + rootArgs[i].first->getOutput();
  if (ret.length() > 0) ret = ret.substr(1,-1);
  if (rootArgs.size() > 1) ret = "(" + ret + ")";
  return ret;
}

template <class T> pair<string,ERROR_CODE> CustomFunctionClass<T>::preParse(FunctionParseInput<T> data, vector<Function_const_value<T>*> &parameters, const vector<pair<FunctionClass<T>*,string>> &rootArgs)
{
  pair<string,ERROR_CODE> ret("",NONE);
  if (toBuild != NULL) {
    CustomFunctionClass<T> *f = dynamic_cast<CustomFunctionClass<T>*>(toBuild->root);
    if (f == NULL) throw Exception(UNKNOWN_ERROR,"RootFunction has a non-CustomFunction as root");
    f->preParse(data,parameters,rootArgs);
    FunctionClass<T>::output = f->getOutput();
    return ret;
  }
  if (parsed) return ret;
  if (isGeneric()) return ret;
  int inside = 0;
  int n = 0;
  int start = 0;
  while (n < (int)func.length()) {
    if (func[n] == '(') {
      if (inside == 0) start = n+1;
      inside++;
    } else if (func[n] == ')') {
      inside--;
      if (inside == 0) break;
    }
    n++;
  }
  string require = "";
  if (FunctionClass<T>::output != "*") require = FunctionClass<T>::output;
  
  
  string tp = noWhiteSpaces(func.substr(start,n-start));
  n++;
  while (n < (int)func.length() && iswspace(func[n])) n++;
  if (n == (int)func.length()) throw CompileError(MISSING_FUNCTION_BODY, "Missing function body",data.filename, data.line, data.col);
  string content = noWhiteSpaces(func.substr(n,-1));
  arguments = tp;
  if (argumentInBrackets) arguments = "("+arguments+")";
  vector<string> parts;
  getParts(content,parts,'|',0);
  if (parts.size() <= 1) return ret;
  vector<string> v;
  for (int i = 0; i < (int)parts.size(); ++i) {
    string s = parts[i];
    if (s.length() > 2 && s.substr(0,2) == "if") {
      inside = 0;
      n = 2;
      while (n < (int)s.length()) {
	if (s[n] == '(') inside++;
	else if (s[n] == ')') {
	  inside--;
	  if (inside == 0) break;
	}
	n++;
      }
      if (n >= (int)s.length()) throw SyntacsError(INVALID_PATTERN, "Invalid if-type pattern",data.filename, data.line, data.col);
      s = s.substr(n+1,-1);
    } else if ((n = s.find("->")) != -1) s = s.substr(n+2,-1);
    v.push_back(s);
  }
  sort(v.begin(), v.end(), complexityRelation);
  for (unsigned int i = 0; i < v.size(); ++i) {
    LOG.debug("preParsing pattern: "+v[i]);
    try {
      CustomFunctionClass<T> *r = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
      string out = FunctionClass<T>::getOutput();
      if ((out == "" || out == "*") && require != "") out = require;
      r = new (r) CustomFunctionClass<T> ("-",FunctionClass<T>::getInput()+"->"+out,"("+arguments+") " + v[i],FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
      for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) r->parameters.push_back(FunctionClass<T>::parameters[j]);
      r->parse(data,parameters,rootArgs);
      FunctionClass<T>::output = r->getOutput();
      return make_pair("",NONE);
    } catch (ERROR_CODE e) {
      if (ret.second == NONE) ret = make_pair(Error::ERROR_STRING,e);
      stringstream ss;
      ss<<"preParsing: could not parse "<<v[i]<<" ("<<ret.first<<", "<<ret.second<<")";
      LOG.debug(ss.str());
    }
  }
  return ret;
}

template <class T> void CustomFunctionClass<T>::parse(FunctionParseInput<T> data, vector<Function_const_value<T>*> &parameters, const vector<pair<FunctionClass<T>*,string>> &rootArgs)
{
  if (toBuild != NULL) {
    CustomFunctionClass<T> *f = dynamic_cast<CustomFunctionClass<T>*>(toBuild->root);
    if (f == NULL) throw Exception(UNKNOWN_ERROR,"RootFunction has a non-CustomFunction as root");
    for (unsigned int i = 0; i < FunctionClass<T>::parameters.size(); ++i) f->parameters.push_back(FunctionClass<T>::parameters[i]);
    f->parse(data,parameters,rootArgs);
    return;
  }
  if (parsed) return;
  parsed = true;
  rootArguments = getRootArgsSignature(rootArgs);
  if (isGeneric()) {
    build_functions = new ((vector<FunctionClass<T>*>*)FunctionClass<T>::func_vram->allocate(sizeof(vector<FunctionClass<T>*>))) vector<FunctionClass<T>*>();
    for (unsigned int i = 0; i < data.functions->size(); ++i) {
        FunctionClass<T>* f = (*data.functions)[i];
        build_functions->push_back(f);
    }
    return;
  }
  int inside = 0;
  unsigned int n = 0;
  int start = 0;
  while (n < func.length()) {
    if (func[n] == '(') {
      if (inside == 0) start = n+1;
      inside++;
    } else if (func[n] == ')') {
      inside--;
      if (inside == 0) break;
    }
    n++;
  }
  string tp = noWhiteSpaces(func.substr(start,n-start));
  n++;
  while (n < func.length() && iswspace(func[n])) n++;
  if (n == func.length()) throw CompileError(MISSING_FUNCTION_BODY, "Missing function body",data.filename, data.line, data.col);
  string content = func.substr(n,-1);
  arguments = tp;
  if (argumentInBrackets) arguments = "("+arguments+")";
  vector<string> param;
  getParts(arguments,param,',',0);
  if (param.size() != (unsigned int)FunctionClass<T>::getNumParameter()) throw CompileError(PARAMETER_TYPE_MISMATCH, "Function parameters don't match the input type",data.filename, data.line, data.col);
  //if (FunctionClass<T>::parameters.size() == 0) cout<<FunctionClass<T>::getName()<<" no args"<<endl;
  if ((int)FunctionClass<T>::parameters.size() != 0) for (unsigned int i = 0; i < param.size(); ++i) parseArgumentFunctions(param[i],FunctionClass<T>::getInConstructor(i)->getRoot(),NULL,FunctionClass<T>::parameters[i],data);
  string require = "";
  if (FunctionClass<T>::output != "*") require = FunctionClass<T>::output;
  //LOG<<"depth: "<<data.depth<<" "<<FunctionClass<T>::parse_data.depth<<endl;
  data.depth = FunctionClass<T>::parse_data.depth;
  data.filename = FunctionClass<T>::parse_data.filename;
  data.line = FunctionClass<T>::parse_data.line;
  data.col = FunctionClass<T>::parse_data.col;
  root = parseContent(noWhiteSpaces(content),data,parameters,rootArgs,require);
  string outp = "-";
  if (root->getOutConstructor() != NULL) outp = root->getOutConstructor()->getRoot()->toString();
  //if (root->getOutConstructor() == NULL || root->getOutConstructor()->getRoot()->toString() != outp) {
  if (FunctionClass<T>::ooutput != outp) {
    FunctionClass<T>::outConstructor = NULL;
    FunctionClass<T>::output = outp;
    FunctionClass<T>::setUp();
  }
}

template <class T> pair<string,FunctionClass<T>*> CustomFunctionClass<T>::getFunctionSignature(const string input, string arguments, string content, FunctionParseInput<T> data, vector<Function_const_value<T>*> &parameters, const vector<pair<FunctionClass<T>*,string>> &rootArgs, vector<pair<string,FunctionClass<T>*>> *params,int &err) const
{
  //LOG.debug("Cont: "+content);
  vector<string> parts;
  getParts(content,parts,',',0);
  if (parts.size() > 1) {
    pair<string,FunctionClass<T>*> r = getFunctionSignature(input,arguments,parts[0],data,parameters,rootArgs,NULL,err);
    if (params != NULL) params->push_back(make_pair(parts[0],r.second));
    string ret = r.first;
    for (unsigned int i = 1; i < parts.size(); ++i) {
      r = getFunctionSignature(input,arguments,parts[i],data,parameters,rootArgs,NULL,err);
      if (params != NULL) params->push_back(make_pair(parts[i],r.second));
      ret += "," + r.first;
    }
    return make_pair(ret,(FunctionClass<T>*)NULL);
  }
  parts.clear();
  if (content.length() > 2 && getNumAtDepth(content,0) == 0 && content[0] == '(' && content[content.length()-1] == ')') getParts(content,parts,',',1);
  if (parts.size() > 1) {
    vector<FunctionClass<T>*> v;
    pair<string,FunctionClass<T>*> r;
    r = getFunctionSignature(input,arguments,parts[0],data,parameters,rootArgs,NULL,err);
    if (r.second != NULL) v.push_back(r.second);
    string ret = TYPE_LIST_SIGN+"(" + r.first;
    for (unsigned int i = 1; i < parts.size(); ++i) {
      r = getFunctionSignature(input,arguments,parts[i],data,parameters,rootArgs, NULL,err);
      ret += "," + r.first;
      if (v.size() > 0) {
	if (r.second != NULL) v.push_back(r.second);
	else v.clear();
      }
    }
    ret += ")";
    
    if (v.size() > 0) {
      FunctionClass<T> **mfunctions = (FunctionClass<T>**)FunctionClass<T>::func_vram->allocate(sizeof(FunctionClass<T>*)*v.size());
      mfunctions = new (mfunctions) FunctionClass<T>*[v.size()];
      for (unsigned int i = 0; i < v.size(); ++i) mfunctions[i] = v[i];
      Function_gen_con_list<T> *rt = (Function_gen_con_list<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gen_con_list<T>));
      rt = new (rt) Function_gen_con_list<T>(FunctionClass<T>::dim,FunctionClass<T>::func_vram,mfunctions,v.size());
      if (params != NULL) params->push_back(make_pair(content,rt));
      return make_pair(ret,rt);
    }
    if (params != NULL) params->push_back(make_pair(content,(FunctionClass<T>*)NULL));
    return make_pair(ret,(FunctionClass<T>*)NULL);
  }
  try {
    CustomFunctionClass<T>* f = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
    f = new (f) CustomFunctionClass<T> ("-",FunctionClass<T>::input+"->*","("+arguments+") " + content,FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
    //cout<<"Trying: "<<FunctionClass<T>::input<<" : "<<"("+arguments+") " + content<<endl;
    for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) f->parameters.push_back(FunctionClass<T>::parameters[j]);
    f->parse(data,parameters,rootArgs);
    if (params != NULL) params->push_back(make_pair(content,f));
    return make_pair(f->getOutput(),f);
  } catch (ERROR_CODE e) {
    err = e;
    stringstream ss;
    ss<<"Could not build: "<<content<<". Error: "<<Error::ERROR_STRING<<" code: "<<e<<endl;
    LOG.debug(ss.str());
    if (params != NULL) params->push_back(make_pair(content,(FunctionClass<T>*)NULL));
    throw e;
    //return make_pair("*",(FunctionClass<T>*)NULL);
  }
}

template <class T> FunctionClass<T>* CustomFunctionClass<T>::getFunctionFromSignature(const string name, string signature, FunctionParseInput<T> data, vector<Function_const_value<T>*> &parameters, const vector<pair<FunctionClass<T>*,string>> &rootArgs, vector<FunctionClass<T>*> &functions, string require) const
{
  FunctionClass<T> *ret = NULL;
  string sign = signature;
  signature = replace(signature, "\\", "");
  signature = replace(signature, "(", "\\(");
  signature = replace(signature, ")", "\\)");
  signature = replace(signature, "[", "\\[");
  signature = replace(signature, "]", "\\]");
  signature = replace(signature, "*", ".*");
  signature = noWhiteSpaces(signature);
  //if (name == "level") cout<<name<<" "<<signature<<endl;
  //cout<<name<<endl;
  for (unsigned int i = 0; i < functions.size(); ++i) {
    //if (functions[i]->getName() == "level") cout<<" *** "<<functions[i]->getInput()<<" "<<signature<<" "<<i<<" "<<functions[i]<<endl;
    if (functions[i] == ret || functions[i]->getName() != name || functions[i]->isGeneric()) continue;
    if (match(functions[i]->getInput(), signature)) {
      if (ret != NULL) throw CompileError(MULTIPLE_DEFINITION1, "Multiple definition of (1): "+name+"("+sign+")", data.filename, data.line, data.col);
      ret = functions[i];
    }
  }
  for (unsigned int i = 0; i < functions.size(); ++i) {
    if (functions[i] == ret || functions[i]->getName() != name || !functions[i]->isGeneric()) continue;
    CustomFunctionClass<T> *cfc = dynamic_cast<CustomFunctionClass<T>*> (functions[i]);
    if (cfc == NULL) continue;
    //LOG<<cfc<<" "<<cfc->getFunc()<<endl;
    for (int j = 0; j < cfc->getNumGenericVersions(); ++j) {
      CustomFunctionClass<T> *f = cfc->getGenericVersion(j);
      if (f == ret) continue;
      if (match(f->getInput(), signature) && canCallFunction(f,data)) {
	if (ret != NULL) throw CompileError(MULTIPLE_DEFINITION2, "Multiple definition of (2): "+name+"("+sign+")", data.filename, data.line, data.col);
	ret = f;
      }
    }
    if (ret == NULL) {
      CustomFunctionClass<T> *f = NULL;
      //LOG<<FunctionClass<T>::getName()<<" "<<name<<" "<<FunctionClass<T>::parse_data.depth << " " << cfc->parse_data.depth<<endl;
      if (cfc->topfunction) f = cfc->instantiate(sign, require, data, parameters, rootArgs);
      else if (FunctionClass<T>::parse_data.depth+1 == cfc->parse_data.depth) f = cfc->instantiate(sign, require, data, parameters,argument_functions); // Level down
      else if (FunctionClass<T>::parse_data.depth == cfc->parse_data.depth) f = cfc->instantiate(sign, require, data, parameters, rootArgs);		//neightbour
      else {
	Element *e1, *e2;
	int lvl1,lvl2;
	lvl1 = FunctionClass<T>::parse_data.depth;
	lvl2 = cfc->parse_data.depth;
	e1 = FunctionClass<T>::parse_data.ownElement;
	e2 = cfc->parse_data.ownElement;
	while (e1 != NULL && e2 != NULL && e1 != e2) {
	  if (lvl1 > lvl2) {
	    lvl1--;
	    e1 = e1->root;
	  } else if (lvl1 < lvl2) {
	    lvl2--;
	    e2 = e2->root;
	  } else /* if (lvl1 == lvl2 && e1 != e2) */ {
	    lvl1--;
	    e1 = e1->root;
	    lvl2--;
	    e2 = e2->root;
	  }
	}
	
	Element *root = e2->root;	//not a topfunction -> root != NULL
	CustomFunctionClass<T> *tf = (CustomFunctionClass<T>*)root->getTag();
	CustomFunctionClass<T> *callerFunction = NULL;
	void *mark = FunctionClass<T>::parse_data.ownElement;
	if (!tf->isGeneric()) {
	  FunctionClass<T>::visit(tf,NULL);		//Reset
	  FunctionClass<T>::visit(tf,mark);
	  if (FunctionClass<T>::mark == mark) callerFunction = tf;
	} else {
	  for (int j = 0; j < tf->getNumGenericVersions(); ++j) {
	    CustomFunctionClass<T> *v = tf->getGenericVersion(j);
	    FunctionClass<T>::visit(v,NULL);		//Reset
	    FunctionClass<T>::visit(v,mark);
	    if (FunctionClass<T>::mark == mark) {
	      callerFunction = v;
	      break;
	    }
	  }
	}
	if (callerFunction != NULL) {					//Level up || Level updown
	    f = cfc->instantiate(sign, require, data, parameters, callerFunction->argument_functions);
	}
      }
      if (f != NULL) {
	if (match(f->getInput(), signature) /* && canCallFunction(f) */) {
	  if (ret != NULL) throw CompileError(MULTIPLE_DEFINITION3, "Multiple definition of (3): "+name+"("+sign+")", data.filename, data.line, data.col);
	  ret = f;
	}
      }
    }
  }
  
  return ret;
}


template <class T> bool CustomFunctionClass<T>::canCallFunction(CustomFunctionClass<T> *cfc, FunctionParseInput<T>&) const
{
  if (cfc->topfunction) return true;
  if (FunctionClass<T>::parse_data.depth+1 == cfc->parse_data.depth) {		//Level down
    if (FunctionClass<T>::getInput() == cfc->getRootArguments()) return true;
    return false;
  } else if (FunctionClass<T>::parse_data.depth > cfc->parse_data.depth && cfc->parse_data.depth > 1) {
    FunctionClass<T> *neightbour = NULL;
    try {
      neightbour = findByElement(cfc);
    } catch (ERROR_CODE e) {
      return false;
    }
    if (neightbour == cfc) return true;			//Level up: this->data2 is used
    else {						//level updown
      //neightbour is the root
      //neightbour is only non-NULL, if it calls (this) and has cfc as a helperfunction-version
      return true;
    }
    return false;
  }
  return true;
}

template <class T> FunctionClass<T>* CustomFunctionClass<T>::parseContent(const string s, FunctionParseInput<T> data, vector<Function_const_value<T>*> &parameters, const vector<pair<FunctionClass<T>*,string>> &rootArgs, string require) const
{
  if (s == "") throw Exception(EMPTY_DEF,"Empty function definition");
  if (build_functions != NULL) data.functions = build_functions;
  
  int numnotnull = 0;
  int inside = 0;
  int comma = 0;
  int first = -1;
  int last = -1;
  int numpattern = 0;
  
  if ((int)FunctionClass<T>::input.find("'") != -1) return NULL;
  string s2 = getContent(s);
  
  stringstream depths;
  depths<<FunctionClass<T>::parse_data.depth;
  if (data.temp != NULL) LOG.debug("Parsing: "+s+" required: "+require+" depth: "+depths.str()); else LOG.debug("Parsing: "+s+" required: "+require+" depth: "+depths.str());
  if (s2 != s) return parseContent(s2,data,parameters,rootArgs,require);
  
  
  if (s[0] == '@') {
    string function, type1,type2;
    int n = s.find('<');
    if (n == -1) {
      function = s.substr(1,-1);
      type1 = type2 = "";
      if (require != "") {
	//cout<<"required: "<<require<<endl;
	if (require[0] != TYPE_FUNCTION_SIGN[0]) throw CompileError(TYPE_MISTMATCH, "Type mismatch", data.filename, data.line, data.col);
	vector<string> tp;
	getParts(require,tp,':',1);
	if (tp.size() != 2) {
	  stringstream ss;
	  ss<<"Invalid function type: "<<require<<endl;
	  throw CompileError(INVALID_FUNCTION_TYPE, ss.str(), data.filename, data.line, data.col);
	}
	type1 = tp[0];
	type2 = tp[1];
      }
    } else {
      function = s.substr(1,n-1);
      string tp = s.substr(n+1,s.length()-function.length()-3);
      vector<string> parts;
      getParts(tp,parts,':',0);
      if (parts.size() != 2) {
	stringstream ss;
	ss << "A invalid function: " << s; 
	throw SyntacsError(INVALID_FUNCTION, ss.str(), data.filename, data.line, data.col);
      }
      type1 = FunctionClass<T>::getTypes(parts[0]);
      type2 = FunctionClass<T>::getTypes(parts[1]);
    }
    while (function.length() > 2 && function[0] == '(' && function[function.length()-1] == ')') function = function.substr(1,function.length()-2);
    FunctionClass<T> *found = NULL;
    for (int i = data.functions->size()-1; i >= 0; --i) {
      if (type2 == "") {
	if ((*data.functions)[i]->getName() == function) {
	  if (found != NULL) {
	    stringstream ss;
	    ss << "Please specify the function's type (@("<<function<<")<T:T>)";
	    throw CompileError(FUNCTION_TYPE_REQUIRED, ss.str(), data.filename, data.line, data.col);
	  }
	  found = (*data.functions)[i];
	}
      } else {
	CustomFunctionClass<T> *cfc = dynamic_cast<CustomFunctionClass<T>*> ((*data.functions)[i]);
	//if ((*data.functions)[i]->getName() == function) cout<<type1<<"="<<(*data.functions)[i]->getInput()<<" "<<(*data.functions)[i]->getOutput()<<"="<<type2<<endl;
	string ftp = getType((*data.functions)[i]->getInput());
	if ((*data.functions)[i]->getName() == function && (FunctionClass<T>::matchType(type1, ftp) || cfc != NULL && FunctionClass<T>::matchType(TYPE_LIST_SIGN+"("+type1+")", ftp)) && FunctionClass<T>::matchType((*data.functions)[i]->getOutput(), type2)) {
	  if (found != NULL) {
	    stringstream ss;
	    ss << "Overloaded function: " << function << " : " << type1<<"->"<<type2;
	    throw CompileError(OVERLOADED_FUNCTION, ss.str(), data.filename, data.line, data.col);
	  }
	  found = (*data.functions)[i];
	}
      }
    }
    if (found == NULL) throw CompileError(UNDEFINED_FUNCTION, "Undefined function: "+function, data.filename, data.line, data.col);
    type1 = found->getInput();
    type2 = found->getOutput();
    CustomFunctionClass<T> *cfc = dynamic_cast<CustomFunctionClass<T>*>(found);
    if (cfc == NULL) {
      cfc = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
      string args = "";
      string content = function + "(";
      vector<string> argsp;
      getParts(type1,argsp,',',0);
      args += "arg0";
      content += "arg0";
      for (unsigned int i = 1; i < argsp.size(); ++i) {
	stringstream argname;
	argname << "arg"<<i;
	args += ","+argname.str();
	content += ","+argname.str();
      }
      content += ")";
      if (argsp.size() > 1) {
	type1 = TYPE_LIST_SIGN + "(" + type1 + ")";
	args = "("+args+")";
      }
      //cout<<(type1+"->"+type2+" ("+args+") " + content)<<endl;
      cfc = new (cfc) CustomFunctionClass<T> (function,type1+"->"+type2,"("+args+") " + content,FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
      Function_const<T> *inputf = new Function_const<T>(FunctionClass<T>::dim,cfc->getInput(),FunctionClass<T>::func_vram);
      cfc->parameters.push_back(inputf);
      vector<pair<FunctionClass<T>*,string>> rA;
      cfc->parse(data,parameters,rA);
    }
    if (!cfc->topfunction) throw CompileError(HELPER_FUNCTION_AS_ARG, "Helper functions cannot be used as arguments", data.filename, data.line, data.col);
    FunctionClass<T>::calcRec(cfc,NULL);			//calcRec double -> set functions to recursive
    FunctionClass<T>::calcRec(cfc,NULL);
    string type = TYPE_FUNCTION_SIGN+"("+cfc->getInput()+":"+cfc->getOutput()+")";
    void **value = (void**)FunctionClass<T>::func_vram->allocate(sizeof(FunctionClass<T>*));
    *value = cfc;
    Function_const_value<T> *ret = new ((Function_const_value<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_const_value<T>))) Function_const_value<T>(cfc->getName(), FunctionClass<T>::getDim(), FunctionClass<T>::func_vram, type, value);
    return ret;
  }

  for (unsigned int i = 0; i < s.length(); ++i) {
    if (isOpening(s[i])) {
      if (inside == 0 && first == -1 && s[i] == '(') first = i;
      inside++;
    } else if (isClosing(s[i])) {
      inside--;
      if (inside == 0 && s[i] == ')') last = i;
    }
    else if (inside == 0) {
      if (s[i] == ',') comma++;
      if (s[i] == '|') numpattern++;
    }
    if (inside == 0) numnotnull++;
  }
  if (first != -1 && numnotnull <= 1) {
    if (s.length() <= 2) throw CompileError(HELPER_FUNCTION_AS_ARG, "Cannot resolve '"+s+"'",data.filename, data.line, data.col);
    return parseContent(s.substr(1,s.length()-2),data,parameters,rootArgs,require);
  }
  if (inside != 0) {
    if (inside > 0) throw SyntacsError(EXPECTED_C, "Closing bracket expected",data.filename, data.line, data.col);
    throw SyntacsError(EXPECTED_C, "Opening bracket expected",data.filename, data.line, data.col);
  }
  if (numpattern > 0) {
    if (FunctionClass<T>::parameters.size() != 1) throw CompileError(PATTERNED_FUNCTION_MULTIPLE_ARGS, "Pattern function can have only one argument", data.filename, data.line, data.col);
    vector<string> parts;
    getParts(s,parts,'|',0);
    vector<pair<Pattern<T>*,CustomFunctionClass<T>*>> patterns;
    FunctionClass<T> *deffunction = NULL;
    //cout<<parts.size()<<endl;
    //for (int i = 0; i < parts.size(); ++i) cout<<parts[i]<<endl;
    
    /*for (int i = 0; i < pttrs.size(); ++i) {
      Pattern<T> *p = Pattern<T>::parse(vr, pttrs[i].first, in, data, pttrs[i].second, this);
      pttrs[i].second->parse(data,parameters);
      patterns.push_back(make_pair(p, pttrs[i].second));
    }*/
    string output = require;
    for (unsigned int i = 0; i < parts.size(); ++i) {
      string pttr,fnc;
      int n = -1;
      int inside2 = 0;
      for (unsigned int j = 0; j < parts[i].length(); ++j) {
	if (isOpening(parts[i][j])) inside2++;
	else if (isClosing(parts[i][j])) inside2--;
	else if (inside2 == 0 && parts[i][j] == '-' && j+1 < parts[i].length() && parts[i][j+1] == '>') {
	  n = j;
	  break;
	}
      }
      int len = 2;
      if (n == -1) {
	if (parts[i].length() > 2 && parts[i].substr(0,2) == "if") {
	  inside = 0;
	  n = 0;
      while (n < (int)parts[i].length()) {
	    if (parts[i][n] == '(') inside++;
	    else if (parts[i][n] == ')') {
	      inside--;
	      if (inside == 0) {
		len = 0;
		n++;
		break;
	      }
	    }
	    ++n;
	  }
      if (n == (int)parts[i].length()) n = -1;
	}
      }
      if (n == -1) {
	pttr = "";
	fnc = parts[i];
      } else {
	pttr = parts[i].substr(0,n);
	fnc = parts[i].substr(n+len,-1);
      }
      //LOG<<"asd: "<<pttr<<" "<<fnc<<endl;
      //r->parse(data,parameters);
      if (n == -1) {
	if (i+1 == parts.size()) {
	  CustomFunctionClass<T> *r = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
	  string out = FunctionClass<T>::getOutput();
	  if ((out == "" || out == "*") && require != "") out = require;
	  r = new (r) CustomFunctionClass<T> ("-",FunctionClass<T>::getInput()+"->"+out,"("+arguments+") " + fnc,FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
      for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) r->parameters.push_back(FunctionClass<T>::parameters[j]);
	  r->parse(data,parameters,rootArgs);
	  deffunction = r;
	  if (output == "") output = r->getOutput();
	  else if (output != r->getOutput()) throw CompileError(PATTERN_WITH_DIFFEREN_TYPES, "Patterns have different types", data.filename, data.line, data.col);
	}
	else throw CompileError(INVALID_PATTERN, "Invalid pattern", data.filename, data.line, data.col);
      } else {
	FunctionConstructData<T> dat(FunctionClass<T>::parameters[0],this,&rootArgs,&parameters);
	Pattern<T> *p = Pattern<T>::parse(FunctionClass<T>::func_vram, pttr, FunctionClass<T>::getInput(), data, dat);
	string args = arguments;
	string inp = FunctionClass<T>::getInput();
	if (dat.arguments != "") {
	  if (args != "") args += ",";
	  args += dat.arguments;
	  if (inp != "") inp += ",";
	  inp += dat.input;
	}
	CustomFunctionClass<T> *r = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
	string out = FunctionClass<T>::getOutput();
	  if ((out == "" || out == "*") && require != "") out = require;
	r = new (r) CustomFunctionClass<T> ("-",inp+"->"+out,"("+args+") " + fnc,FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
    for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) r->parameters.push_back(FunctionClass<T>::parameters[j]);
    for (unsigned int j = 0; j < dat.parameters.size(); ++j) r->parameters.push_back(dat.parameters[j]);
	r->parse(data,parameters,rootArgs);
	patterns.push_back(make_pair(p,r));
	if (output == "") output = r->getOutput();
	else if (output != r->getOutput()) throw CompileError(PATTERN_WITH_DIFFEREN_TYPES, "Patterns have different types", data.filename, data.line, data.col);
      }
    }
    if (deffunction == NULL) {
      deffunction = (ErrorFunction<T>*)FunctionClass<T>::func_vram->allocate(sizeof(ErrorFunction<T>));
      deffunction = new (deffunction) ErrorFunction<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram,"Unhandled pattern",data);
    }
    PatternFunction<T> *ret = (PatternFunction<T>*)FunctionClass<T>::func_vram->allocate(sizeof(PatternFunction<T>));
    if (output == "") throw Exception(UNKNOWN_ERROR, "Unknown error: pattern/output = \"\"");
    ret = new (ret) PatternFunction<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram, FunctionClass<T>::getInput(), output, patterns, deffunction, data, parameters, FunctionClass<T>::parameters[0], data);
    //ret->parameters.push_back(FunctionClass<T>::parameters[0]);
    return ret;
  }
  if (comma > 0) {
    vector<string> parts;
    int prev = 0;
    for (unsigned int i = 0; i < s.length(); ++i) {
      if (isOpening(s[i])) inside++;
      else if (isClosing(s[i])) inside--;
      else if (inside == 0 && s[i] == ',') {
	parts.push_back(s.substr(prev,i-prev));
	prev = i+1;
      }
    }
    if (prev < (int)s.length()) parts.push_back(s.substr(prev,-1));
    if (parts.size() < 2) throw CompileError(CANNOT_RESOLVE, "Cannot resolve '"+s+"'",data.filename, data.line, data.col);
    int length = parts.size();
    FunctionClass<T> **mfunctions = (FunctionClass<T>**)FunctionClass<T>::func_vram->allocate(sizeof(FunctionClass<T>*)*length);
    mfunctions = new (mfunctions) FunctionClass<T>*[length];
    CustomFunctionClass<T>* r;
    vector<string> reqs;
    if (require != "") {
      getParts(require,reqs,',',1);
      if ((int)reqs.size() != length) throw CompileError(TYPE_MISTMATCH, "Type mismatch '"+s+"'",data.filename, data.line, data.col);
    }
    for (int i = 0; i < length; ++i) {
      string req = "*";
      if (require != "") req = reqs[i];
      r = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
      r = new (r) CustomFunctionClass<T> ("-",FunctionClass<T>::getInput()+"->"+req,"("+arguments+") " + parts[i],FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
      for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) r->parameters.push_back(FunctionClass<T>::parameters[j]);
      r->parse(data,parameters,rootArgs);
      mfunctions[i] = r;
    }
    Function_gen_con_list<T> *rt = (Function_gen_con_list<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gen_con_list<T>));
    rt = new (rt) Function_gen_con_list<T>(FunctionClass<T>::dim,FunctionClass<T>::func_vram,mfunctions,length);
    return rt;
  }
  
  inside = 0;
  bool firstop = false;
  for (int j = 0; CustomFunctionClass<T>::operations[j] != ""; ++j) {
    string op = CustomFunctionClass<T>::operations[j];
    for (unsigned int i = 0; i < s.length(); ++i) {
      if (isOpening(s[i])) inside++;
      else if (isClosing(s[i])) inside--;
      else if (inside == 0) {
    if (i+(int)op.length() < (int)s.length() && s.substr(i,op.length()) == op) {
	  if (i == 0) firstop = true;
      if (i > 0) {
          int len = op.length();
          if (op.length() >= 2 && op[0] == '\'' && op[op.length()-1] == '\'') op = op.substr(1,op.length()-2); //'or' -> or
          return parseContent(op+"("+s.substr(0,i)+","+s.substr(i+len,-1)+")",data,parameters,rootArgs,require);
      }
	  else i += op.length()-1;
	}
      }
    }
    if (firstop) break;
  }
  if (s[0] == '!') {
    CustomFunctionClass<T> *f = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
    f = new (f) CustomFunctionClass<T> ("-",FunctionClass<T>::getInput()+"->"+TYPE_BOOL_SIGN,"("+arguments+") " + s.substr(1,-1),FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
    for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) f->parameters.push_back(FunctionClass<T>::parameters[j]);
    f->parse(data,parameters,rootArgs);
    Function_negate_b<T> *r = (Function_negate_b<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_negate_b<T>));
    r = new (r) Function_negate_b<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram);
    r->parameters.push_back(f);
    return r;
  } else if (s[0] == '-' && s.length() >= 2 && s[1] != '(') {
    /*CustomFunctionClass<T> *f = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
    string s2 = "(0-1)*(" + s.substr(1,-1) + ")";
    //cout<<s2<<endl;
    f = new (f) CustomFunctionClass<T> ("-",FunctionClass<T>::getInput()+"->"+require,"("+arguments+") " + s2,FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
    for (int j = 0; j < FunctionClass<T>::parameters.size(); ++j) f->parameters.push_back(FunctionClass<T>::parameters[j]);
    f->parse(data,parameters,rootArgs);
    return f;*/
    return parseContent("-("+s.substr(1,-1)+")",data,parameters,rootArgs,require);
  }
  
  
  inside = s[0] == '(' ? 1 : 0;
  int nn = 1;
  while (nn < (int)s.length()) {
    if (inside == 0 && s[nn] == '(') break;
    if (s[nn] == '(') inside++;
    else if (s[nn] == ')') inside--;
    nn++;
  }
  string funcname = "";
  if (nn != -1) funcname = s.substr(0,nn);
  //cout<<"asd: "<<s<<endl;
  /*if (nn < s.length()) {
    try {
      string s2,cont;
      s2 = s.substr(0,nn);
      cont = s.substr(nn+1,s.length()-(nn+2));
      //cout<<s2<<" "<<cont<<endl;
      //cout<<f->getOutput()<<endl;
      vector<string> parts;
      getParts(cont,parts,',',0);
      if (parts.size() > 1) cont = "("+cont+")";
      else if (parts.size() == 0) throw Exception(UNKNOWN_ERROR,"parts.size() = 0");
      CustomFunctionClass<T> *g = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
      //cout<<"bulding: ("+arguments+") " + cont<<endl;
      g = new (g) CustomFunctionClass<T> ("-",FunctionClass<T>::getInput()+"->*","("+arguments+") " + cont,FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
      for (int j = 0; j < FunctionClass<T>::parameters.size(); ++j) g->parameters.push_back(FunctionClass<T>::parameters[j]);
      g->parse(data,parameters,rootArgs);
      
      string funcreq = "*";
      if (require != "") funcreq = TYPE_FUNCTION_SIGN + "(" + g->getOutput() + ":" + require + ")";
      CustomFunctionClass<T> *f = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
      f = new (f) CustomFunctionClass<T> ("-",FunctionClass<T>::getInput()+"->"+funcreq,"("+arguments+") " + s2,FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
      for (int j = 0; j < FunctionClass<T>::parameters.size(); ++j) f->parameters.push_back(FunctionClass<T>::parameters[j]);
      f->parse(data,parameters,rootArgs);
      
      if (f->getOutput()[0] == TYPE_FUNCTION_SIGN[0]) {
	Function_function<T> *ret = new ((Function_function<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_function<T>))) Function_function<T>(FunctionClass<T>::getDim(), f, g, FunctionClass<T>::func_vram);
	return ret;
      }
    } catch (ERROR_CODE e) {
      //cout<<e.what()<<endl;
    }
  }*/
  //cout<<"asd2: "<<s<<endl;
  
  
  
 
  vector<string> arrayparts;
  getParts(s,arrayparts,"::",0);
  if (arrayparts.size() > 1) {
    vector<FunctionClass<T>*> params;
    string req = "*";
    if (require != "") req = require.substr(TYPE_ARRAY_SIGN.length(),-1);			//[a] -> [a -> a
    for (unsigned int i = 0; i < arrayparts.size(); ++i) {
      CustomFunctionClass<T>* f = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
      f = new (f) CustomFunctionClass<T> ("-",FunctionClass<T>::input+"->"+req,"("+arguments+") " + arrayparts[i],FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
      for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) f->parameters.push_back(FunctionClass<T>::parameters[j]);
      f->parse(data,parameters,rootArgs);
      params.push_back(f);
    }
    string tp = params[0]->getOutput();
    for (unsigned int i = 1; i < params.size(); ++i) if (tp != params[i]->getOutput()) CompileError(INVALID_ELENT_TO_ARRAY, "Invalid use of ::", data.filename, data.line, data.col);
    string atp = params[params.size()-1]->getOutput();
    if (atp[0] != TYPE_ARRAY_SIGN[0] || atp.length() <= 1 || atp.substr(1,-1) != tp) CompileError(INVALID_ELENT_TO_ARRAY, "Invalid use of ::", data.filename, data.line, data.col);
    Function_addarray<T> *ret = (Function_addarray<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_addarray<T>));
    ret = new (ret) Function_addarray<T>(FunctionClass<T>::getDim(),FunctionClass<T>::func_vram, tp);
    for (unsigned int i = 0; i < params.size(); ++i) ret->parameters.push_back(params[i]);
    return ret;
  }
  if (first != -1) { 
    //LOG.debug(" *** Building Function *** "+s);
    if (first == 0) throw CompileError(CANNOT_RESOLVE, "Cannot resolve '"+s+"'",data.filename, data.line, data.col);
    string fnc = s.substr(0,first);
    string arg = s.substr(first+1,last-first-1);
    if (arg == "") throw CompileError(FUNCTION_NO_ARGS, "Functions with no arguments are not allowed", data.filename, data.line, data.col);
    
    //cout<<"Getting function"<<endl;
    vector<pair<string,FunctionClass<T>*>> params;
    int err = 0;
    pair<string,FunctionClass<T>*> sg_func = getFunctionSignature(FunctionClass<T>::input, arguments, arg, data, parameters, rootArgs, &params, err);
    //if (err != 0 && err != UNDEFINED_REFERENCE) throw err;
    string signature = sg_func.first;
    //LOG<<"signature: "<<signature<<endl;
    FunctionClass<T> *found = getFunctionFromSignature(fnc, signature, data, parameters, rootArgs, *data.functions,require);
    //if (found == NULL) cout<<"not found: "<<fnc<<" "<<signature<<endl;
    //else cout<<"found: "<<fnc<<" "<<signature<<endl;
    if (found != NULL) {
      vector<string> inputs;
      getParts(found->getInput(), inputs, ',', 0);
      for (unsigned int i = 0; i < params.size(); ++i) {
	FunctionClass<T>* f2 = params[i].second;
	if (f2 == NULL) {
	  //cout<<"Building: "<<param_string[i]<<" "<<inputs[i]<<endl;
	  CustomFunctionClass<T> *f = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
	  f = new (f) CustomFunctionClass<T> ("-",FunctionClass<T>::input+"->"+inputs[i],"("+arguments+") " + params[i].first,FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
      for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) f->parameters.push_back(FunctionClass<T>::parameters[j]);
	  f->parse(data,parameters,rootArgs);
	  params[i].second = f;
	}
      }
      CustomFunctionClass<T> *cfc = dynamic_cast<CustomFunctionClass<T>*> (found);
      if (cfc != NULL) {
	if (cfc->getInput() == "") throw CompileError(FUNCTION_NO_ARGS, "Functions with no arguments are not allowed", data.filename, data.line, data.col);
	//cout<<data.depth<<" "<<cfc->parse_data.depth<<endl;
	if (FunctionClass<T>::parse_data.depth+1 == cfc->parse_data.depth) {
	  FunctionComposition_leveldown<T> *fg = (FunctionComposition_leveldown<T>*)FunctionClass<T>::func_vram->allocate(sizeof(FunctionComposition_leveldown<T>));
	  fg = new (fg) FunctionComposition_leveldown<T>(FunctionClass<T>::getDim(),FunctionClass<T>::func_vram,cfc,params[0].second,data);
	  //LOG.debug(" *** Finish 1 *** "+s);
	  return fg;
	} else if (FunctionClass<T>::parse_data.depth > cfc->parse_data.depth && cfc->parse_data.depth > 1) {
	  FunctionClass<T> *neightbour = findByElement(cfc);
	  if (neightbour == cfc) {
	    FunctionComposition_levelup<T> *fg = (FunctionComposition_levelup<T>*)FunctionClass<T>::func_vram->allocate(sizeof(FunctionComposition_levelup<T>));
	    fg = new (fg) FunctionComposition_levelup<T>(FunctionClass<T>::getDim(),FunctionClass<T>::func_vram,cfc,params[0].second,data);
	    //LOG.debug(" *** Finish 2 *** "+s);
	    return fg;
	  } else {
	    FunctionComposition_levelupdown<T> *fg = (FunctionComposition_levelupdown<T>*)FunctionClass<T>::func_vram->allocate(sizeof(FunctionComposition_levelupdown<T>));
	    fg = new (fg) FunctionComposition_levelupdown<T>(FunctionClass<T>::getDim(),FunctionClass<T>::func_vram,cfc,params[0].second,neightbour,data);
	    //LOG.debug(" *** Finish 3 *** "+s);
	    return fg;
	  }
	} else {
	  FunctionComposition<T> *fg = (FunctionComposition<T>*)FunctionClass<T>::func_vram->allocate(sizeof(FunctionComposition<T>));
	  fg = new (fg) FunctionComposition<T>(FunctionClass<T>::getDim(),FunctionClass<T>::func_vram,cfc,params[0].second,data);
	  //LOG.debug(" *** Finish 4 *** "+s);
	  return fg;
	}
      } else {
	FunctionClass<T> *ret = found->copy();
	ret->parameters.clear();
    for (unsigned int j = 0; j < params.size(); ++j) ret->parameters.push_back(params[j].second);
	//LOG.debug(" *** Finish 5 *** "+s);
	return ret;
      }
    } else /* found = NULL */ if (sg_func.second != NULL) {
      try {
            //LOG<<"Hiba itt"<<endl;
	string funcreq = "*";
	if (require != "") funcreq = TYPE_FUNCTION_SIGN + "(" + sg_func.second->getOutput() + ":" + require + ")";
	CustomFunctionClass<T> *f = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
	f = new (f) CustomFunctionClass<T> ("-",FunctionClass<T>::getInput()+"->"+funcreq,"("+arguments+") " + funcname,FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
    for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) f->parameters.push_back(FunctionClass<T>::parameters[j]);
	f->parse(data,parameters,rootArgs);
	
	if (f->getOutput()[0] == TYPE_FUNCTION_SIGN[0]) {
	  Function_function<T> *ret = new ((Function_function<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_function<T>))) Function_function<T>(FunctionClass<T>::getDim(), f, sg_func.second, FunctionClass<T>::func_vram);
	  return ret;
	}
      } catch (ERROR_CODE e) {
	// Just go on
      }
    }
  }
  if (s == "[]") {
    Function_constructor_a_empty<T> *r = (Function_constructor_a_empty<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constructor_a_empty<T>));
    return new (r) Function_constructor_a_empty<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram);
  } else if (s[0] == '[' && s[s.length()-1] == ']') {
    vector<string> parts;
    getParts(s.substr(1,s.length()-2),parts,';',0);
    string type = "";
    Function_constructor_a<T> *ret = NULL;
    string req = "*";
    if (require != "") req = require.substr(TYPE_ARRAY_SIGN.length(),-1);			//[a] -> [a -> a
    for (unsigned int i = 0; i < parts.size(); ++i) {
      CustomFunctionClass<T>* f = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
      f = new (f) CustomFunctionClass<T> ("-",FunctionClass<T>::input+"->"+req,"("+arguments+") " + parts[i],FunctionClass<T>::dim, FunctionClass<T>::func_vram,data);
      for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) f->parameters.push_back(FunctionClass<T>::parameters[j]);
      f->parse(data,parameters,rootArgs);
      if (type == "") {
	type = f->getOutput();
	ret = (Function_constructor_a<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constructor_a<T>));
	ret = new (ret) Function_constructor_a<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram, type);
      }
      else if (!FunctionClass<T>::matchType(f->getOutput(), type)) throw CompileError(ARRAY_MULTIPLE_TYPES, "An array can hold only one type of elements", data.filename, data.line, data.col);
      ret->parameters.push_back(f);
    }
    if (ret == NULL) throw CompileError(UNKNOWN_ERROR, "Unknown error", ERROR_CONTENT);
    return ret;
  }
  if (isBoolean(s)) {
    Function_con_b<T> *r = (Function_con_b<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_con_b<T>));
    return new (r) Function_con_b<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram,s[0] == 't'||s[0] == 'T');
  }
  if (isInteger(s)) {
    if (require == TYPE_REAL_SIGN) return parseContent(s +".0",data,parameters,rootArgs,require);
    if (require == TYPE_CPLX_SIGN) return parseContent(s +".0+0.0*i",data,parameters,rootArgs,require);
    
    Function_con_i<T> *r = (Function_con_i<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_con_i<T>));
    return new (r) Function_con_i<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram,atoi(s.c_str()));;
  }
  if (isT(s)) {
    if (require == TYPE_CPLX_SIGN) return parseContent(s +"+0.0*i",data,parameters,rootArgs,require);
    Function_con_r<T> *r = (Function_con_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_con_r<T>));
    T val = matof(s.c_str());
    return new (r) Function_con_r<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram,val);
  }
  if (isColour(s)) {
    Colour c;
    c.parse(s);
    Function_con_u<T> *r = (Function_con_u<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_con_u<T>));
    return new (r) Function_con_u<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram,c);
  }
  if (s == "i" || s == string(MATH_CLASS)+".i") {
    Function_con_c<T> *r = (Function_con_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_con_c<T>));
    return new (r) Function_con_c<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram,complex<T>(0,1));
  }
  if (s.length() >= 2 && s[0] == '{' && s[s.length()-1] == '}') {
    //Function_get_v
    vector<string> parts;
    getParts(s.substr(1,s.length()-2),parts,',',0);
    if ((int)parts.size() != FunctionClass<T>::getDim()) throw CompileError(WRONG_NUM_COMP, "Wrong number of components in a vector/matrix", data.filename, data.line, data.col);
    vector<FunctionClass<T>*> components;
    int numv, numr;
    numv = numr = 0;
    string req = "*";
    if (require != "") {
      if (require == TYPE_VECTOR_SIGN) req = TYPE_REAL_SIGN;
      else if (require == TYPE_MATRIX_SIGN) req = TYPE_VECTOR_SIGN;
    }
    for (unsigned int i = 0; i < parts.size(); ++i) {
      CustomFunctionClass<T>* f = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
      f = new (f) CustomFunctionClass<T> ("-",FunctionClass<T>::input+"->"+req,"("+arguments+") " + parts[i],FunctionClass<T>::dim, FunctionClass<T>::func_vram,data);
      for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) f->parameters.push_back(FunctionClass<T>::parameters[j]);
      f->parse(data,parameters,rootArgs);
      if (FunctionClass<T>::matchType(f->getOutput(), TYPE_REAL_SIGN)) numr++;
      else if (FunctionClass<T>::matchType(f->getOutput(), TYPE_VECTOR_SIGN)) numv++;
      if (numr != i+1 && numv != i+1) throw CompileError(INVALID_COMP_TYPE, "The components of a matrix/vector must be the type of Vector/Real", data.filename, data.line, data.col);
      components.push_back(f);
    }
    if (numr > 0) {
      Function_constructor_v<T> *ret = (Function_constructor_v<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constructor_v<T>));
      ret = new (ret) Function_constructor_v<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram);
      for (unsigned int j = 0; j < components.size(); ++j) ret->parameters.push_back(components[j]);
      return ret;
    } else {
      Function_constructor_m<T> *ret = (Function_constructor_m<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constructor_m<T>));
      ret = new (ret) Function_constructor_m<T>(FunctionClass<T>::dim, FunctionClass<T>::func_vram);
      for (unsigned int j = 0; j < components.size(); ++j) ret->parameters.push_back(components[j]);
      return ret;
    }
  }
  for (unsigned int i = 0; i < argument_functions.size(); ++i) {
    if (argument_functions[i].second == s) {
      if (FunctionClass<T>::getOutConstructor() != NULL && !FunctionClass<T>::matchType(argument_functions[i].first->getOutConstructor()->getRoot()->toString(), FunctionClass<T>::getOutConstructor()->getRoot()->toString())) {
	stringstream ss;
	ss<<"(found: "<<argument_functions[i].first->getOutConstructor()->getRoot()->toString() << " expected: " << FunctionClass<T>::getOutConstructor()->getRoot()->toString()<<")";
	throw CompileError(TYPE_MISTMATCH, "Data type mismatch "+ss.str(),data.filename, data.line, data.col);
      }
      return argument_functions[i].first;
    }
  }
  for (unsigned int i = 0; i < rootArgs.size(); ++i) {
    if (rootArgs[i].second == s) {
      if (FunctionClass<T>::getOutConstructor() != NULL && !FunctionClass<T>::matchType(rootArgs[i].first->getOutConstructor()->getRoot()->toString(), FunctionClass<T>::getOutConstructor()->getRoot()->toString())) {
	stringstream ss;
	ss<<"(found: "<<rootArgs[i].first->getOutConstructor()->getRoot()->toString() << " expected: " << FunctionClass<T>::getOutConstructor()->getRoot()->toString()<<")";
	throw CompileError(TYPE_MISTMATCH, "Data type mismatch "+ss.str(),data.filename, data.line, data.col);
      }
      TurnFunctionClass<T> *turn = new ((TurnFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(TurnFunctionClass<T>))) TurnFunctionClass<T>(FunctionClass<T>::getDim(),FunctionClass<T>::getVram(),rootArgs[i].first);
      return turn;
    }
  }
  if (s[s.length()-1] == ']') {
    int inside = 0;
    int last = 0;
    for (int i = s.length()-1; i >= 0; --i) {
      if (s[i] == ']') inside++;
      else if (s[i] == '[') {
	inside--;
	if (inside == 0) {
	  last = i;
	  break;
	}
      }
    }
    if (inside != 0) {
      if (inside > 0) throw SyntacsError(EXPECTED_C, "'[' expected", data.filename, data.line, data.col);
      throw SyntacsError(EXPECTED_C, "']' expected", data.filename, data.line, data.col);
    }
    if (last == 0) throw CompileError(MISSING_VALUE, "Missing value before operator []", data.filename, data.line, data.col);
    if (s.length()-last-2 <= 0) throw CompileError(MISSING_VALUE, "Missing index from []", data.filename, data.line, data.col);
    string vc = s.substr(0,last);
    string index = s.substr(last+1,s.length()-last-2);
    //cout<<vc<<" "<<index<<endl;
    string req = "*";
    if (require != "") {
      if (require == TYPE_VECTOR_SIGN) req = TYPE_REAL_SIGN;
      else if (require == TYPE_MATRIX_SIGN) req = TYPE_VECTOR_SIGN;
    }
    CustomFunctionClass<T> *vc_f = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
    vc_f = new (vc_f) CustomFunctionClass<T> ("-",FunctionClass<T>::getInput()+"->"+req,"("+arguments+") " + vc,FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
    for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) vc_f->parameters.push_back(FunctionClass<T>::parameters[j]);
    vc_f->parse(data,parameters,rootArgs);
    CustomFunctionClass<T> *index_f = (CustomFunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(CustomFunctionClass<T>));
    index_f = new (index_f) CustomFunctionClass<T> ("-",FunctionClass<T>::getInput()+"->"+TYPE_INT_SIGN,"("+arguments+") " + index,FunctionClass<T>::dim, FunctionClass<T>::func_vram, data);
    for (unsigned int j = 0; j < FunctionClass<T>::parameters.size(); ++j) index_f->parameters.push_back(FunctionClass<T>::parameters[j]);
    index_f->parse(data,parameters,rootArgs);
    FunctionClass<T> *f = (FunctionClass<T>*)FunctionClass<T>::func_vram->allocate(sizeof(FunctionClass<T>));
    f = new (f) Function_get_v<T>(FunctionClass<T>::dim,FunctionClass<T>::func_vram,vc_f,index_f,data);
    return f;
  }
  FunctionClass<T> *found_const = NULL;
  for (int i = data.functions->size()-1; i >= 0; --i) {
      //LOG<<s<<" asdasd "<<(*data.functions)[i]->getName()<<"  "<<(*data.functions)[i]->getInput()<<endl;
    CustomFunctionClass<T> *cfc = dynamic_cast<CustomFunctionClass<T>*> ((*data.functions)[i]);
    if (cfc == found_const && cfc != NULL) continue;
    //LOG<<"asd"<<endl;
    if ((*data.functions)[i]->getName() == s && (*data.functions)[i]->getInput() == "") {
      //cout<<data.depth<<" "<<cfc->parse_data.depth<<" (2)"<<endl;
      if (cfc != NULL) {
	
    if (cfc->isGeneric()) {
	  int index = 0;
	  while (index < cfc->getNumGenericVersions() && !canCallFunction(cfc->getGenericVersion(index),data)) index++;
	  if (cfc->getNumGenericVersions() == index) {
	    
	    
	    
	    CustomFunctionClass<T> *cfc2 = dynamic_cast<CustomFunctionClass<T>*> (getFunctionFromSignature(cfc->getName(), cfc->getInput(), data, parameters, rootArgs, *data.functions, require));		//Must be CustomFunctionClass<T>*
	    
	    
	    
	    //CustomFunctionClass<T> *cfc2 = cfc->instantiate("", require, data, parameters, rootArgs);
	    if (cfc2 != NULL) cfc = cfc2;
	    //else cout<<"NULL"<<endl;
	  } else cfc = cfc->getGenericVersion(index);
	}
	
	if (cfc->isGeneric()) continue;
	
	FunctionClass<T> * cfc2 = cfc;
	if (FunctionClass<T>::parse_data.depth+1 == cfc->parse_data.depth) {
	  cfc2 = new ((LevelFunctionClass_down<T>*)FunctionClass<T>::func_vram->allocate(sizeof(LevelFunctionClass_down<T>))) LevelFunctionClass_down<T>(FunctionClass<T>::getDim(), FunctionClass<T>::func_vram, cfc);
	} else if (FunctionClass<T>::parse_data.depth > cfc->parse_data.depth && cfc->parse_data.depth > 1) {
	  FunctionClass<T> *neightbour = findByElement(cfc);
	  if (neightbour == cfc2) {
	    throw CompileError(NO_EXIT_CONDITION, "Unallowed recursion (no exit condition)", data.filename, data.line, data.col);
	    //cfc2 = new ((LevelFunctionClass_up<T>*)FunctionClass<T>::func_vram->allocate(sizeof(LevelFunctionClass_up<T>))) LevelFunctionClass_up<T>(FunctionClass<T>::getDim(), FunctionClass<T>::func_vram, cfc);
	  } else {
	    cfc2 = new ((LevelFunctionClass_updown<T>*)FunctionClass<T>::func_vram->allocate(sizeof(LevelFunctionClass_updown<T>))) LevelFunctionClass_updown<T>(FunctionClass<T>::getDim(), FunctionClass<T>::func_vram, cfc, neightbour);
	  }
	}
	if (found_const != NULL) throw CompileError(MULTIPLE_DEFINITION, "Multiple definition of "+s, data.filename, data.line, data.col);
	found_const = cfc2;
      } else {
	FunctionClass<T> *ret = (*data.functions)[i]->copy();
	if (found_const != NULL) throw CompileError(MULTIPLE_DEFINITION, "Multiple definition of "+s, data.filename, data.line, data.col);
	found_const = ret;
      }
    }
  }
  if (found_const != NULL) return found_const;
  for (unsigned int i = 0; i < parameters.size(); ++i) if (parameters[i]->getName() == s) {return parameters[i];}
  throw CompileError(UNDEFINED_REFERENCE, "Undefined reference to '"+s+"'", data.filename, data.line, data.col);
}

template <class T> string FunctionClass<T>::getTypes(const string s)
{
  string ret = s;//replace(s,","," ");
  ret = replace(ret,TYPE_BOOL_NAME,TYPE_BOOL_SIGN);
  ret = replace(ret,TYPE_INT_NAME,TYPE_INT_SIGN);
  ret = replace(ret,TYPE_REAL_NAME,TYPE_REAL_SIGN);
  ret = replace(ret,TYPE_CPLX_NAME,TYPE_CPLX_SIGN);
  ret = replace(ret,TYPE_COLOUR_NAME,TYPE_COLOUR_SIGN);
  ret = replace(ret,TYPE_VECTOR_NAME,TYPE_VECTOR_SIGN);
  ret = replace(ret,TYPE_MATRIX_NAME,TYPE_MATRIX_SIGN);
  ret = replace(ret,TYPE_LIST_NAME,TYPE_LIST_SIGN);
  ret = replace(ret,TYPE_ARRAY_NAME,TYPE_ARRAY_SIGN);
  ret = replace(ret,"]","");
  ret = replace(ret,TYPE_FUNCTION_NAME,TYPE_FUNCTION_SIGN);
  ret = noWhiteSpaces(ret);
  if (ret.length() > 0) {
    if (ret[0] == '(') ret = TYPE_LIST_SIGN + ret;
    ret = replace(ret, "((", "("+TYPE_LIST_SIGN+"(");
  }
  return ret;
}

template <class T> CustomType<T>* CustomTypeConstructor<T>::createType(VRam &vr, const string pattern, FunctionParseInput<T> &data)
{
  //cout<<pattern<<endl;
  if (pattern.length() == 0) return NULL;
  //if (pattern[0] == '(') return createType(vr, TYPE_LIST_SIGN + pattern, data);
  if (pattern[0] == TYPE_REAL_SIGN[0]) return new ((CustomTypeR<T>*)vr.allocate(sizeof(CustomTypeR<T>))) CustomTypeR<T>();
  if (pattern[0] == TYPE_INT_SIGN[0]) return new ((CustomTypeI<T>*)vr.allocate(sizeof(CustomTypeI<T>))) CustomTypeI<T>();
  if (pattern[0] == TYPE_BOOL_SIGN[0]) return new ((CustomTypeB<T>*)vr.allocate(sizeof(CustomTypeB<T>))) CustomTypeB<T>();
  if (pattern[0] == TYPE_CPLX_SIGN[0]) return new ((CustomTypeC<T>*)vr.allocate(sizeof(CustomTypeC<T>))) CustomTypeC<T>();
  if (pattern[0] == TYPE_COLOUR_SIGN[0]) return new ((CustomTypeU<T>*)vr.allocate(sizeof(CustomTypeU<T>))) CustomTypeU<T>();
  if (pattern[0] == TYPE_VECTOR_SIGN[0]) return new ((CustomTypeV<T>*)vr.allocate(sizeof(CustomTypeV<T>))) CustomTypeV<T>();
  if (pattern[0] == TYPE_MATRIX_SIGN[0]) return new ((CustomTypeM<T>*)vr.allocate(sizeof(CustomTypeM<T>))) CustomTypeM<T>();
  if (pattern[0] == TYPE_ARRAY_SIGN[0]) {
    CustomType<T> *type = NULL;
    if (pattern.length() > 1) type = CustomTypeConstructor<T>::createType(vr, pattern.substr(1,-1), data);
    CustomTypeA<T> *ret = new ((CustomTypeA<T>*)vr.allocate(sizeof(CustomTypeA<T>))) CustomTypeA<T>();
    ret->type = type;
    return ret;
  }
  if (pattern[0] == TYPE_LIST_SIGN[0]) {
    if (pattern.length() == 1) throw SyntacsError(SINNGLE_TUPLE, "A tuple type cannot stand alone",data.filename,data.line,data.col);
    if (pattern[1] != '(') throw SyntacsError(EXPECTED_C, "'(' expected after tuple",data.filename,data.line,data.col);
    int inside = 1;
    int n = 2;
    int comma = 1;
    CustomTypeList<T> *ret = new ((CustomTypeList<T>*)vr.allocate(sizeof(CustomTypeList<T>))) CustomTypeList<T>();
    ret->length = 0;
    while (n < (int)pattern.length()) {
      if ((pattern[n] == ')' || pattern[n] == ',') && inside == 1) {
	ret->length++;
	CustomType<T> *type;
	type = createType(vr, pattern.substr(comma+1,n-comma-1), data);
	ret->types.push_back(type);
      }
      if (pattern[n] == ')') {
	inside--;
	if (inside == 0) break;
      } else if (pattern[n] == '(') inside++;
      else if (inside == 1 && pattern[n] == ',') comma = n;
      n++;
    }
    if (inside != 0) {
      if (inside > 0) throw SyntacsError(EXPECTED_C, "')' expected",data.filename,data.line,data.col);
      throw SyntacsError(EXPECTED_C, "'(' expected",data.filename,data.line,data.col);
    }
    if (ret->length == 0) throw SyntacsError(EMPTY_TUPLE, "An tuple cannot be empty",data.filename,data.line,data.col);
    return ret;
  }
  if (pattern[0] == TYPE_FUNCTION_SIGN[0]) {
    //cout<<pattern<<endl;
    if (pattern.length() == 1) throw SyntacsError(FUNCTION_FORMAT, "A function type must be in the form of function(A:B)",data.filename,data.line,data.col);
    if (pattern[1] != '(') throw SyntacsError(FUNCTION_FORMAT, "A function type must be in the form of function(A:B)",data.filename,data.line,data.col);
    CustomTypeF<T> *ret = new ((CustomTypeF<T>*)vr.allocate(sizeof(CustomTypeF<T>))) CustomTypeF<T>();
    string s = pattern.substr(2,pattern.length()-3);
    vector<string> parts;
    getParts(s,parts,':',0);
    if (parts.size() != 2) throw SyntacsError(FUNCTION_FORMAT, "A function type must be in the form of function(A:B). Constant functions are not allowed here.",data.filename,data.line,data.col);
    vector<string> parts1;
    getParts(parts[0],parts1,',',0);
    vector<string> parts2;
    getParts(parts[1],parts2,',',0);
    if (parts1.size() != 1 || parts2.size() != 1) throw SyntacsError(FUNCTION_FORMAT, "A function type must be in the form of function(A:B)",data.filename,data.line,data.col);
    ret->type1 = createType(vr, parts[0], data);
    ret->type2 = createType(vr, parts[1], data);
    return ret;
  }
  if (!(pattern[0] >= 'A' && pattern[0] <= 'Z')) return new ((CustomTypeG<T>*)vr.allocate(sizeof(CustomTypeG<T>))) CustomTypeG<T>(pattern);
  return NULL;
}
