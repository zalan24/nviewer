#ifndef MATRIX_H
#define MATRIX_H

#include "common/Math/include/vectorn.h"
#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

class MatrixOutOfRange: public exception
{
  virtual const char* what() const throw()
  {
    return "Matrix coloumn is out of range";
  }
};
class InvalidMatrixSize: public exception
{
  virtual const char* what() const throw()
  {
    return "Invalid matrix size";
  }
};
class InvalidMatrixOperation: public exception
{
  virtual const char* what() const throw()
  {
    return "Operation cannot be done on matrices of different size";
  }
};
class InvalidMatrixD: public exception
{
  virtual const char* what() const throw()
  {
    return "The matrix has to be the type of NxN to enable determinant";
  }
};

typedef long double Real;

template<class T>
class Matrix
{
  int N,M;
  VectorN<T> **v;
public:
  int getRows() const;
  int getColoumns() const;
  VectorN<T>& get(int) const;
  T get(int,int) const;
  void set(int,int,T);
  Matrix(int,int);
  Matrix(const Matrix<T>&);
  Matrix<T>& operator+=(const Matrix<T>&);
  Matrix<T>& operator-=(const Matrix<T>&);
  Matrix<T>& operator*=(const T);
  Matrix<T>& operator/=(const T);
  Matrix<T>& operator=(const Matrix<T>&);
  Matrix<T> operator-();
  Matrix<T> Trans();
  Matrix<T> operator*(const T);
  Real D();
  static Matrix<T> zero(const int, const int);
  static Matrix<T> Identity(const int);
  static Matrix<T> subIdentity(const int, const int);
  static Matrix<T> Scaling(const int, VectorN<T>&);
  static Matrix<T> Translation(const int, VectorN<T>&);
  static Matrix<T> Scaling(const int, VectorN<T>);
  static Matrix<T> Translation(const int, VectorN<T>);
  static Matrix<T> Rotation(const int, Real, int, int);
  ~Matrix();
};

template<class T> Matrix<T> operator+(const Matrix<T>&, const Matrix<T>&);
template<class T> Matrix<T> operator-(const Matrix<T>&, const Matrix<T>&);
template<class T> Matrix<T> operator*(const Matrix<T>&, const Matrix<T>&);
template<class T> VectorN<T> operator*(const Matrix<T>&, const VectorN<T>&);

template <class T> Matrix<T> Matrix<T>::zero(const int N, const int M)
{
  return Matrix<T>(N,M);
}

template <class T> Matrix<T> Matrix<T>::Identity(const int N)
{
  Matrix<T> m(N,N);
  for (int i = 0; i < N; ++i) for (int j = 0; j < N; ++j) m.set(i,j,0);
  for (int i = 0; i < N; ++i) m.set(i,i,1);
  return m;
}

template <class T> Matrix<T> Matrix<T>::subIdentity(const int N, const int i)
{
  Matrix<T> m(N,N);
  m.set(i,i,1);
  return m;
}

template <class T> Matrix<T> Matrix<T>::Scaling(const int N, VectorN<T> vec)
{
  Matrix<T> m = Identity(N+1);
  for (int i = 0; i < vec.size(); ++i) m.set(i,i,vec.get(i));
  return m;
}

template <class T> Matrix<T> Matrix<T>::Translation(const int N, VectorN<T> vec)
{
  Matrix<T> m = Identity(N+1);
  for (int i = 0; i < vec.size(); ++i) m.set(i,N,vec.get(i));
  return m;
}

template <class T> Matrix<T> Matrix<T>::Scaling(const int N, VectorN<T> &vec)
{
  Matrix<T> m = Identity(N+1);
  for (int i = 0; i < vec.size(); ++i) m.set(i,i,vec.get(i));
  return m;
}

template <class T> Matrix<T> Matrix<T>::Translation(const int N, VectorN<T> &vec)
{
  Matrix<T> m = Identity(N+1);
  for (int i = 0; i < vec.size(); ++i) m.set(i,N,vec.get(i));
  return m;
}

template <class T> Matrix<T> Matrix<T>::Rotation(const int N, Real angle, int x, int y)
{
  Matrix<T> m = Identity(N+1);
  Real cs = cos(angle);
  Real sn = sin(angle);
  m.set(x,x,cs);
  m.set(y,y,cs);
  m.set(x,y,-sn);
  m.set(y,x,sn);
  return m;
}

template<class T> Matrix<T>::Matrix(int N,int M) : N(N), M(M)
{
  if (N < 0 || M < 0) throw InvalidMatrixSize();
  v = new VectorN<T>*[M];
  for (int i = 0; i < M; ++i) v[i] = new VectorN<T>(N);
}

template<class T> Matrix<T>::Matrix(const Matrix<T> &m) : N(m.getRows()), M(m.getColoumns())
{
  v = new VectorN<T>*[M];
  for (int i = 0; i < M; ++i) v[i] = new VectorN<T>(m.get(i));
}

template<class T> Matrix<T>::~Matrix()
{
  for (int i = 0; i < M; ++i) delete v[i];
  delete [] v;
}

template<class T> int Matrix<T>::getRows() const
{
  return N;
}
template<class T> int Matrix<T>::getColoumns() const
{
  return M;
}

template<class T> VectorN<T>& Matrix<T>::get(int i) const
{
  if (i < 0 || i >= M) throw MatrixOutOfRange();
  return *v[i];
}

template<class T> T Matrix<T>::get(int j, int i) const
{
  if (i < 0 || i >= M || j < 0 || j >= N) throw MatrixOutOfRange();
  return v[i]->get(j);
}

template<class T> void Matrix<T>::set(int j, int i, T p)
{
  if (i < 0 || i >= M || j < 0 || j >= N) throw MatrixOutOfRange();
  (*v[i])[j] = p;
}

template <class T> ostream& operator<<(ostream& os, const Matrix<T> &m)
{
  os<<"(";
  for (int i = 0; i < m.getColoumns(); ++i) {
    if (i > 0) os<<", ";
    os << m.get(i);
  }
  os<<")";
  return os;
}

template <class T> Matrix<T>& Matrix<T>::operator+=(const Matrix<T> &m)
{
  if (getRows() != m.getRows() || getColoumns() != m.getColoumns()) throw InvalidMatrixOperation();
  for (int i = 0; i < getRows(); ++i) for (int j = 0; j < getColoumns(); ++j) (*v[j])[i] += m.get(i,j);
  return *this;
}
template <class T> Matrix<T>& Matrix<T>::operator-=(const Matrix<T> &m)
{
  if (getRows() != m.getRows() || getColoumns() != m.getColoumns()) throw InvalidMatrixOperation();
  for (int i = 0; i < getRows(); ++i) for (int j = 0; j < getColoumns(); ++j) (*v[j])[i] -= m.get(i,j);
  return *this;
}
template <class T> Matrix<T>& Matrix<T>::operator*=(const T p)
{
  for (int i = 0; i < getRows(); ++i) for (int j = 0; j < getColoumns(); ++j) (*v[j])[i] *= p;
  return *this;
}
template <class T> Matrix<T>& Matrix<T>::operator/=(const T p)
{
  for (int i = 0; i < getRows(); ++i) for (int j = 0; j < getColoumns(); ++j) (*v[j])[i] /= p;
  return *this;
}
template <class T> Matrix<T>& Matrix<T>::operator=(const Matrix<T> &m)
{
  if (getRows() != m.getRows() || getColoumns() != m.getColoumns()) throw InvalidMatrixOperation();
  for (int i = 0; i < getRows(); ++i) for (int j = 0; j < getColoumns(); ++j) (*v[j])[i] = m.get(i,j);
  return *this;
}
template <class T> Matrix<T> Matrix<T>::operator-()
{
  Matrix<T> m(getRows(), getColoumns());
  for (int i = 0; i < getRows(); ++i) for (int j = 0; j < getColoumns(); ++j) m.set(i,j, -get(i,j));
  return m;
}
template <class T> Matrix<T> Matrix<T>::Trans()
{
  Matrix<T> m(getColoumns(), getRows());
  for (int i = 0; i < getRows(); ++i) for (int j = 0; j < getColoumns(); ++j) m.set(j,i, get(i,j));
  return m;
}
template <class T> Real Matrix<T>::D()
{
  if (getColoumns() != getRows()) throw InvalidMatrixD();
  if (getRows() == 0) return 0;
  if (getRows() == 1) return get(0,0);
  if (getRows() == 2) return get(0,0)*get(1,1) - get(0,1)*get(1,0);
  Real sum = 0;
  Matrix<T> m(getRows()-1, getRows()-1);
  for (int i = 0; i < getRows(); ++i) {
    Real d = get(0,i);
    if (i%2 == 1) d*=-1;
    for (int r = 1; r < getRows(); ++r) {
      for (int cc = 0; cc < getColoumns(); ++cc) {
	int c = cc;
	if (c == i) continue;
	if (c > i) c--;
	m.set(r-1,c,get(r,cc));
      }
    }
    sum += d*m.D();
  }
  return sum;
}
template<class T> Matrix<T> operator+(const Matrix<T> &m1, const Matrix<T> &m2)
{
  if (m1.getRows() != m2.getRows() || m1.getColoumns() != m2.getColoumns()) throw InvalidMatrixOperation();
  Matrix<T> m(m1.getRows(), m1.getColoumns());
  for (int i = 0; i < m.getRows(); ++i) for (int j = 0; j < m.getColoumns(); ++j) m.set(i,j, m1.get(i,j) + m2.get(i,j));
  return m;
}
template<class T> Matrix<T> operator-(const Matrix<T> &m1, const Matrix<T> &m2)
{
  if (m1.getRows() != m2.getRows() || m1.getColoumns() != m2.getColoumns()) throw InvalidMatrixOperation();
  Matrix<T> m(m1.getRows(), m1.getColoumns());
  for (int i = 0; i < m.getRows(); ++i) for (int j = 0; j < m.getColoumns(); ++j) m.set(i,j, m1.get(i,j) - m2.get(i,j));
  return m;
}
template<class T> Matrix<T> operator*(const Matrix<T> &m1, const Matrix<T> &m2)
{
  if (m1.getColoumns() != m2.getRows()) throw InvalidMatrixOperation();
  Matrix<T> m(m1.getRows(), m2.getColoumns());
  T sum;
  for (int i = 0; i < m.getRows(); ++i) for (int j = 0; j < m.getColoumns(); ++j) {
    sum = 0;
    for (int k = 0; k < m1.getColoumns(); ++k) sum += m1.get(i, k)*m2.get(k,j);
    m.set(i,j,sum);
  }
  return m;
}
template<class T> VectorN<T> operator*(const Matrix<T> &m1, const VectorN<T> &v)
{
  if (m1.getColoumns() != v.size() && m1.getColoumns() != v.size()+1) throw InvalidMatrixOperation();
  VectorN<T> ret(v.size());
  T sum;
  for (int i = 0; i < v.size(); ++i) {
    sum = 0;
    for (int k = 0; k < m1.getColoumns(); ++k) sum += m1.get(i, k)*v.get(k);
    ret[i] = sum;
  }
  return ret;
}
template<class T> Matrix<T> Matrix<T>::operator*(const T p)
{
  Matrix<T> m(*this);
  m *= p;
  return m;
}


#endif //MATRIX_H
