#pragma once

#define ERROR_CONTENT __FILE__,__LINE__

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <sstream>
#include <map>
#include <vector>
#include <limits> 
#include <thread>
#include <dirent.h>
#include <complex>
#include <ratio>
#include <chrono>
#include <regex>

#include "common/Math/include/info.h"
#include "common/Math/include/errors.h"
#include "common/Math/include/formats.h"

#define PRECISION 21
#define DIVISION_CHARACTER ((char)1)
#define SLEEP_FOR 500


typedef unsigned long long int ulli;
typedef long long int lli;
typedef unsigned int uint;
typedef struct stat Stat;
typedef std::chrono::duration<int,std::milli> milliseconds_type;

using namespace std;

class InvalidArgument: public exception
{
  virtual const char* what() const throw()
  {
    return "Invalid argument found in the configuration";
  }
};

class Colour
{
public:
  float r,g,b,a;
  Colour() : r(0), g(0), b(0), a(0) {}
  Colour(float r, float g, float b, float a) : r(r), g(g), b(b), a(a) {}
  string print();
  void parse(const string);
};

template <class T>
class LightData
{
  int dim;
public:
  LightData(int dim) : dim(dim) {
    dir = new T[dim];
    pos = new T[dim];
  }
  ~LightData()
  {
    delete [] dir;
    delete [] pos;
  }
  int id,x,y;
  T *dir;
  T *pos;
  T px,py,pz;
  T dx,dy,dz;
};

bool operator==(const Colour&, const Colour&);

string getHex(int);
int fromHex(const string);

double Radian(double);

template <class T> string to_string(T x)
{
  stringstream ss;
  ss<<x;
  return x.str();
}

template <class T, class T2> T lerp(T a, T b, T2 f)
{
  return (b-a)*f + a;
}

template <class T> T mabs(T a)
{
  if (a < 0) return -a;
  return a;
}

template <class T> T clamp01(T a)
{
  if (a < 0) return 0;
  if (a > 1) return 1;
  return a;
}

template <class T> string writeValue(T &x)
{
  stringstream ss;
  ss.precision(PRECISION);
  ss<<x;
  return ss.str();
}

template <class T> void readValue(const string s, T &x)
{
  stringstream ss;
  ss.precision(PRECISION);
  ss<<s;
  ss>>x;
}

template <class T> T abs2(const complex<T> &c) {
  return c.imag() * c.imag() + c.real() * c.real();
}

template <class T> T pow_ai(T a, int b)
{
  if (b == 0) return a/a;
  if (b == 1) return a;
  T r = pow_ai(a,b/2);
  if (b % 2 == 0) return r*r;
  return r*r*a;
}

template <class T> bool operator >= (const complex<T> &c1, const complex<T> &c2) {
  return c1.real()*c1.real() + c1.imag()*c1.imag() >= c2.real()*c2.real() + c2.imag()*c2.imag(); }
template <class T> bool operator > (const complex<T> &c1, const complex<T> &c2) {
  return c1.real()*c1.real() + c1.imag()*c1.imag() > c2.real()*c2.real() + c2.imag()*c2.imag(); }
template <class T> bool operator <= (const complex<T> &c1, const complex<T> &c2) {
  return c1.real()*c1.real() + c1.imag()*c1.imag() <= c2.real()*c2.real() + c2.imag()*c2.imag(); }
template <class T> bool operator < (const complex<T> &c1, const complex<T> &c2) {
  return c1.real()*c1.real() + c1.imag()*c1.imag() < c2.real()*c2.real() + c2.imag()*c2.imag(); }

template <class T> T pow_aa(T a, T b)
{  
  return pow(a,b);
}

template <class T> T root(T A, int m, int acc)
{
  T a = 1;
  for (int i = 0; i < acc; ++i) a = (A/pow_ai(a,m-1) + a*(m-1)) / m;
  return a;
}

void readAxisData(string, vector<pair<int,double>>&);
bool isTextCharacter(unsigned char);
void splitString(const string&,vector<string>&);
void splitString(const string&,const char,vector<string>&);
pair<string, string> getKeyValue(const string&);
string getElementOfMap(map<string,string>&, const string, const string);
int getGetStringWidth(void*,string);
pair<double,double> getLimit(int,int);
void getFolders(string,vector<string>&);
bool testFile(string);
void removeFolder(const char*);
Colour lerp_col(Colour, Colour, float);
Colour adjustSaturate(Colour, float);
Colour adjustBrightness(Colour, float);
Colour adjustTransparency(Colour, float);
string noWhiteSpaces(string);
string replace(string,string,string);
bool isBoolean(const string);
bool isInteger(const string);
bool isT(const string);
bool isColour(const string);
void getParts(const string, vector<string>&, char, int);
void getParts(const string, vector<string>&, const string, int);
bool isOpening(const char);
bool isClosing(const char);
bool match(const string, const string);
string getGenericTypeFromArgs(string, int&, const string);
int getNumAtDepth(const string,const int);
bool complexityRelation(const string&, const string&);
bool isLetter(char);
int fact(int);
int getMiddleOfLongest(void**,int,int);
bool isColourSolid(float);
int readInt(const string);
uint getDate();
uint dateToSec(uint t);
uint dateToMin(uint t);
uint dateToHour(uint t);
uint dateToDay(uint t);
uint dateToMonth(uint t);
uint dateToYear(uint t);
string dateToString(uint t);
float matof(const char cs[]);

template <class T>
class LightDirection
{
  T *v;
  T *tv;
  int dim;
public:
  LightDirection(int dim) : dim(dim) {
    v = new T[dim];
    tv = new T[dim];
  }
  ~LightDirection() {
    delete [] v;
    delete [] tv;
  }
  void normalize() {
    T l = 0; //x*x + y*y + z*z;
    for (int i = 0; i < dim; ++i) l += v[i] * v[i];
    if (l != 1) {
      l = sqrt(l);
      for (int i = 0; i < dim; ++i) v[i] /= l;
    }
  }
  void transform(T *matrix, int dim)
  {
    for (int j = 0; j < dim; ++j) {
      tv[j] = 0;
      for (int i = 0; i < dim; ++i) tv[j] += matrix[j*(dim+1) + i] * v[i];
    }
  }
  T get(int i) const {return v[i];}
  T getTransformed(int i) const {return tv[i];}
  void set(int i, T val) {v[i] = val;}
  string print() const {
    stringstream ss;
    ss<<v[0];
    for (int i = 1; i < dim; ++i) ss << " " << v[i];
    return ss.str();
  }
  void parse(string s) {
    vector<string> parts;
    getParts(s,parts,' ',0);
    if (parts.size() > (unsigned int)dim) throw SyntacsError(INVALID_VECTOR,"Invalid direction: it should be in the format <X> <Y> <Z> <W> ...",ERROR_CONTENT);
    for (unsigned int i = 0; i < parts.size(); ++i) set(i,matof(parts[i].c_str()));
    for (unsigned int i = parts.size(); i < (unsigned int)dim; ++i) set(i,0);
    normalize();
  }
};
