#include <vector>

#include "matrix.h"

using namespace std;

template <class T>
class OptimizedMatrix
{
public:
  vector<int> *info;
  Matrix<T> *matrix;
  OptimizedMatrix(Matrix<T>&);
  ~OptimizedMatrix();
};

template <class T> OptimizedMatrix<T>::OptimizedMatrix(Matrix<T> &m)
{
  matrix = &m;
  info = new vector<int>[m.getRows()];
  for (int i = 0; i < m.getRows(); ++i) {
    for (int j = 0; j < m.getColoumns(); ++j) {
      if (m.get(i,j) != 0) {
	info[i].push_back(j);
      }
    }
  }
}

template<class T> VectorN<T> operator*(const OptimizedMatrix<T>&, const VectorN<T>&);

template <class T> OptimizedMatrix<T>::~OptimizedMatrix()
{
  delete [] info;
}

template<class T> VectorN<T> operator*(const OptimizedMatrix<T> &m, const VectorN<T> &v)
{
  VectorN<T> ret = VectorN<T>::zero(v.size());
  for (int i = 0; i < v.size(); ++i) {
    for (int j = 0; j < m.info[i].size(); ++j) ret[i] += m.matrix->get(i,m.info[i][j])*v.get(m.info[i][j]);
  }
  return ret;
}

