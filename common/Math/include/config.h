#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <map>

#include "common/Math/include/tools.h"

#define DEFAULT_WIDTH "512"
#define DEFAULT_HEIGHT "512"
#define DEFAULT_THREADS "1"
#define DEFAULT_VRAM "131072"
#define DEFAULT_FVRAM "1048576"
#define DEFAULT_RECMEMORY "131072"
#define DEFAULT_OCCLUSION "16"
#define DEFAULT_LIGHTING "8"
#define DEFAULT_SDENSITY "16"
#define DEFAULT_SDEPTH "16"
#define DEFAULT_SNUMD "24"

#define DEFAULT_FUNCTIONNAME "squarefunc"

#define DEFAULT_CONSOLE 167

#define ROTATION_SPEED 1
//1024*1024
#define DEFAULT_MAX_ARRAY_SIZE 1048576

#define MACHINE_CONFIG (*MachineConfig::instance)

//------------------------
//       -KeyBoard-
//------------------------

using namespace std;

class UnexpectedTag: public exception
{
  virtual const char* what() const throw()
  {
    return "Unexpected tag in the config";
  }
};

class InvalidClosingTag: public exception
{
  virtual const char* what() const throw()
  {
    return "Invalid closing tag";
  }
};

enum OpenType
{
    OPEN = 1,
    CREATE = 2
};

OpenType operator |(OpenType a, OpenType b);
OpenType operator &(OpenType a, OpenType b);

class Element
{
  string type;
  string id;
  vector<Element*> children;
  map<string,string> attributes;
  void *tag;
public:
  Element *root;
  Element();
  Element(const string);
  virtual ~Element();
  void setTag(void*);
  void *getTag() const;
  string toString() const;
  string toString(string) const;
  static Element* fromString(string);
  static Element* createElementByName(const string, const string);
  virtual string getType() const;
  int getChildCount() const;
  Element *getChild(int) const;
  string getAttribute(const string, const string);
  void setAttribute(const string, const string);
  template <class T> void setAttributeT(const string, const T);
  bool isKey(const string);
  void addChild(Element*);
  void removeChild(Element*);
  string getId() const;
  Element *findObjectOfType(const string);
  Element *findObjectWithId(const string);
  void getAttributes(vector<pair<string, string>>&);
};


template <class T> void Element::setAttributeT(const string key, const T x)
{
  setAttribute(key,writeValue(x));
}

class KeyConfig : public Element {
public:
  KeyConfig();
  KeyConfig(const string);
  string getType() const;
};

class KeyBoardConfig : public Element {
public:
  KeyBoardConfig();
  KeyBoardConfig(const string);
  string getType() const;
};

class Window : public Element {
public:
  Window();
  Window(const string);
  string getType() const;
};

class App : public Element {
public:
  App();
  App(const string);
  string getType() const;
};

class Process : public Element {
public:
  Process();
  Process(const string);
  string getType() const;
};

class FunctionElement : public Element {
public:
  FunctionElement();
  FunctionElement(const string);
  string getType() const;
};

class CustomFunctionElement : public Element {
public:
  CustomFunctionElement();
  CustomFunctionElement(const string);
  string getType() const;
};

class CameraElement : public Element {
public:
  CameraElement();
  CameraElement(const string);
  string getType() const;
};

class HolderElement : public Element {
public:
  HolderElement();
  HolderElement(const string);
  string getType() const;
};

class StyleElement : public Element {
public:
  StyleElement();
  StyleElement(const string);
  string getType() const;
};

class CustomParameterElement : public Element {
public:
  CustomParameterElement();
  CustomParameterElement(const string);
  string getType() const;
};

class Config
{
protected:
  string filename;
public:
  Element *root;
  void clear();
  Config(const char *file, OpenType type = OPEN | CREATE);
  void WriteConfig(const char*) const;
  void WriteConfig() const;
  ~Config();
};

class MachineConfig : public Config
{
protected:
  App *app;
  Window *window;
  KeyBoardConfig *keyboard;
  Process *process;
public:
  static MachineConfig *instance;
  App* getApp() const;
  Window* getWindow() const;
  KeyBoardConfig* getKeyBoard() const;
  Process* getProcess() const;
  MachineConfig(const char *file, OpenType type = OPEN | CREATE);
  void setDefault();
};

class SessionConfig : public Config
{
protected:
  FunctionElement *func;
public:
  SessionConfig(const char *file, OpenType type = OPEN | CREATE);
  void setDefault();
  FunctionElement* getFunc();
};

class ListConfig : public Config
{
protected:
  HolderElement *holder;
public:
  HolderElement* getHolder();
  ListConfig(const char *file, OpenType type = OPEN | CREATE);
  void setDefault();
};
