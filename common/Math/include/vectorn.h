#pragma once

#include <iostream>
#include <fstream>
#include <math.h>
#include <stdarg.h>
#include "tools.h"

using namespace std;

typedef long double Real;

class WrongVectorSize: public exception
{
  virtual const char* what() const throw()
  {
    return "Operations on vectors of different dimensions are not allowed";
  }
};

class DivideBy0: public exception
{
  virtual const char* what() const throw()
  {
    return "Cannot divide by 0";
  }
};

class VectorOutOfRange: public exception
{
  virtual const char* what() const throw()
  {
    return "Vector index is out of range";
  }
};

template <class T>
class VectorN
{
  T *t;
  int N;
  Real sqrM;
  Real M;
public:
  int size() const;
  T get(int) const;
  VectorN(const VectorN<T>&);
  VectorN(int);
  VectorN(int,int);
  VectorN(initializer_list<T>);
  ~VectorN();
  VectorN<T>& operator+=(const VectorN<T>&);
  VectorN<T>& operator-=(const VectorN<T>&);
  VectorN<T>& operator*=(T);
  VectorN<T>& operator/=(T);
  VectorN<T> operator*(T);
  VectorN<T> operator/(T);
  VectorN<T> operator-();
  T operator*(const VectorN<T>&);
  T& operator[](int);
  VectorN<T>& operator=(const VectorN<T>&);
  VectorN<T>& operator=(initializer_list<T>);
  Real magnitude();
  Real sqrMagnitude();
  void normalize();
  VectorN<T> normalized();
  static VectorN<T> zero(const int);
  static VectorN<T> forward();
  static VectorN<T> identity(const int, const int);
  Real distance(const VectorN<T>&);
};

template <class T> bool operator==(const VectorN<T>&,const VectorN<T>&);
template <class T> bool operator!=(const VectorN<T>&,const VectorN<T>&);
template <class T> bool operator<=(const VectorN<T>&,const VectorN<T>&);
template <class T> bool operator>=(const VectorN<T>&,const VectorN<T>&);
template <class T> bool operator<(const VectorN<T>&,const VectorN<T>&);
template <class T> bool operator>(const VectorN<T>&,const VectorN<T>&);

template <class T> VectorN<T> operator+(const VectorN<T>&,const VectorN<T>&);
template <class T> VectorN<T> operator-(const VectorN<T>&,const VectorN<T>&);



template <class T> int VectorN<T>::size() const
{
  return N;
}

template <class T> T VectorN<T>::get(int i) const
{
  if (i > N || i < 0) throw VectorOutOfRange();
  if (i == N) return 1;
  return t[i];
}

template <class T> VectorN<T> VectorN<T>::zero(const int N)
{
  return VectorN<T>(N,0);
}

template <class T> VectorN<T> VectorN<T>::forward()
{
  VectorN<T> v(3,0);
  v[2] = 1;
  return v;
}

template <class T> VectorN<T> VectorN<T>::identity(const int N, const int i)
{
  VectorN<T> v(N,0);
  v[i] = 1;
  return v;
}

template <class T> Real VectorN<T>::magnitude()
{
  if (M != -1) return M;
  return M = sqrt(sqrMagnitude());
}
template <class T> Real VectorN<T>::sqrMagnitude()
{
  if (sqrM != -1) return sqrM;
  sqrM = 0;
  for (int i = 0; i < N; ++i) sqrM += t[i]*t[i];
  return sqrM;
}

template <class T> Real VectorN<T>::distance(const VectorN<T> &v2)
{
  Real sum = 0;
  for (int i = 0; i < N; ++i) sum += (t[i]-v2.get(i))*(t[i]-v2.get(i));
  return sqrt(sum);
}

template <class T> void VectorN<T>::normalize()
{
  *this /= magnitude();
  M = sqrM = 1;
}

template <class T> VectorN<T> VectorN<T>::normalized()
{
  VectorN<T> v(*this / magnitude());
  return v;
}

template <class T> VectorN<T>& VectorN<T>::operator=(const VectorN<T> &v)
{
  if (N != v.size()) throw WrongVectorSize();
  for (int i = 0; i < N; ++i) t[i] = v.t[i];
  M = sqrM = -1;
  return *this;
}

template <class T> VectorN<T>& VectorN<T>::operator=(initializer_list<T> v)
{
  if (N != v.size()) throw WrongVectorSize();
  //for (int i = 0; i < N; ++i) t[i] = v.get(i);
  int n = 0;
  for (auto i = v.begin(); i != v.end(); i++) t[n++] = *i;
  M = sqrM = -1;
  return *this;
}

template <class T> VectorN<T>::VectorN(int N) : N(N)
{
  t = new T[N];
  M = sqrM = 0;
}

template <class T> VectorN<T>::VectorN(int N, int a) : N(N)
{
  t = new T[N];
  for (int i = 0; i < N; ++i) t[i] = a;
  M = sqrM = -1;
}

template <class T> VectorN<T>::VectorN(const VectorN<T> &v) : N(v.size())
{
  t = new T[N];
  for (int i = 0; i < N; ++i) t[i] = v.t[i];
  M = sqrM = -1;
}

template <class T> VectorN<T>::VectorN(initializer_list<T> v)
{
  N = v.size();
  t = new T[N];
  int n = 0;
  for (auto i = v.begin(); i != v.end(); i++) t[n++] = *i;
  M = sqrM = -1;
}


template <class T> VectorN<T>::~VectorN()
{
  delete [] t;
}

template <class T> VectorN<T>& VectorN<T>::operator+=(const VectorN &v2)
{
  if (N != v2.size()) throw WrongVectorSize();
  for (int i = 0; i < N; ++i) t[i] += v2.get(i);
  M = sqrM = -1;
  return *this;
}

template <class T> VectorN<T>& VectorN<T>::operator-=(const VectorN &v2)
{
  if (N != v2.size()) throw WrongVectorSize();
  for (int i = 0; i < N; ++i) t[i] -= v2.get(i);
  M = sqrM = -1;
  return *this;
}

template <class T> VectorN<T>& VectorN<T>::operator*=(T p)
{
  for (int i = 0; i < N; ++i) t[i] *= p;
  if (M != -1) {
    M *= p;
    sqrM = M*M;
  }
  return *this;
}

template <class T> VectorN<T>& VectorN<T>::operator/=(T p)
{
  if (p == 0) throw DivideBy0();
  for (int i = 0; i < N; ++i) t[i] /= p;
  if (M != -1) {
    M /= p;
    sqrM = M*M;
  }
  return *this;
}

template <class T> VectorN<T> VectorN<T>::operator*(T p)
{
  VectorN<T> v(*this);
  for (int i = 0; i < N; ++i) v[i] *= p;
  return v;
}

template <class T> VectorN<T> VectorN<T>::operator/(T p)
{
  VectorN<T> v(*this);
  for (int i = 0; i < N; ++i) v[i] /= p;
  return v;
}

template <class T> T& VectorN<T>::operator[](int i)
{
  if (i >= N || i < 0) throw VectorOutOfRange();
  M = sqrM = -1;
  return t[i];
}

template <class T> T VectorN<T>::operator*(const VectorN<T> &v)
{
  if (N != v.size()) throw WrongVectorSize();
  T sum = 0;
  for (int i = 0; i < N; ++i) sum += t[i]*v.t[i];
  return sum;
}

template <class T> VectorN<T> VectorN<T>::operator-()
{
  VectorN<T> v(N);
  for (int i = 0; i < N; ++i) v[i] = -t[i];
  return v;
}

template <class T> ostream& operator<<(ostream& os, const VectorN<T> &obj)
{
  os<<"(";
  if (obj.size() > 0) os<<obj.get(0);
  for (int i = 1; i < obj.size(); ++i) os<<", "<<obj.get(i);
  os<<")";
  return os;
}


template <class T> bool operator==(const VectorN<T> &v1,const VectorN<T> &v2)
{
  if (v1.size() != v2.size()) throw WrongVectorSize();
  for (int i = 0; i < v1.size(); ++i) if (v1.get(i) != v2.get(i)) return false;
  return true;
}
template <class T> bool operator!=(const VectorN<T> &v1,const VectorN<T> &v2)
{
  return !(v1==v2);
}
template <class T> bool operator<=(const VectorN<T> &v1,const VectorN<T> &v2)
{
  if (v1.size() != v2.size()) throw WrongVectorSize();
  return v1.sqrMagnitude() <= v2.sqrMagnitude();
}
template <class T> bool operator>=(const VectorN<T> &v1,const VectorN<T> &v2)
{
  if (v1.size() != v2.size()) throw WrongVectorSize();
  return v1.sqrMagnitude() >= v2.sqrMagnitude();
}
template <class T> bool operator<(const VectorN<T> &v1,const VectorN<T> &v2)
{
  if (v1.size() != v2.size()) throw WrongVectorSize();
  return v1.sqrMagnitude() < v2.sqrMagnitude();
}
template <class T> bool operator>(const VectorN<T> &v1,const VectorN<T> &v2)
{
  if (v1.size() != v2.size()) throw WrongVectorSize();
  return v1.sqrMagnitude() > v2.sqrMagnitude();
}
template <class T> VectorN<T> operator+(const VectorN<T> &v1,const VectorN<T> &v2)
{
  if (v1.size() != v2.size()) throw WrongVectorSize();
  VectorN<T> v(v1.size());
  for (int i = 0; i < v1.size(); ++i) v[i] = v1.get(i) + v2.get(i);
  return v;
}
template <class T> VectorN<T> operator-(const VectorN<T> &v1,const VectorN<T> &v2)
{
  if (v1.size() != v2.size()) throw WrongVectorSize();
  VectorN<T> v(v1.size());
  for (int i = 0; i < v1.size(); ++i) v[i] = v1.get(i) - v2.get(i);
  return v;
}
