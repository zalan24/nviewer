#include "common/Math/include/image.h"

namespace Image {


  unsigned short int X = 0,Y = 0;
  RGB **t = NULL;
  RGB Back_colour = RGB(255,255,255);
  
  void setT(int x, int y, RGB c)
  {
    t[x][y] = c;
  }
  
  void CloseGeometryImage()
  {
      if (t != NULL) {
	  for (int i = 0; i < X; ++i) delete [] t[i];
	  delete [] t;
      }
      t = NULL;
  }
  
  void InitGeometryImage( int x, int y )
  {
    if (x == X && y == Y) return;
    if (t != NULL) CloseGeometryImage();
    X = x;
    Y = y;
    t = new RGB*[X];
    for (int i = 0; i < X; ++i) t[i] = new RGB[Y];
  }

  void DrawBackGround(RGB c = Back_colour)
  {
      Back_colour = c;
      for (int i = 0; i < X; ++i) for (int j = 0; j < Y; ++j) t[i][j] = c;
  }

  void WriteBitmap(const char *filename)
  {
      FILE *f;
      unsigned char *img = NULL;
      int filesize = 54 + 3*X*Y;
      img = new unsigned char[3*X*Y];
      for (int i = 0; i < 3*X*Y; ++i) img[i] = 0;
      for(int i=0; i<X; i++) {
	  for(int j=0; j<Y; j++) {
	      img[(i+j*X)*3+2] = (unsigned char)(t[i][j].r);
	      img[(i+j*X)*3+1] = (unsigned char)(t[i][j].g);
	      img[(i+j*X)*3+0] = (unsigned char)(t[i][j].b);
	  }
      }

      unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
      unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
      unsigned char bmppad[3] = {0,0,0};

      bmpfileheader[ 2] = (unsigned char)(filesize    );
      bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
      bmpfileheader[ 4] = (unsigned char)(filesize>>16);
      bmpfileheader[ 5] = (unsigned char)(filesize>>24);

      bmpinfoheader[ 4] = (unsigned char)(       X    );
      bmpinfoheader[ 5] = (unsigned char)(       X>> 8);
      bmpinfoheader[ 6] = (unsigned char)(       X>>16);
      bmpinfoheader[ 7] = (unsigned char)(       X>>24);
      bmpinfoheader[ 8] = (unsigned char)(       Y    );
      bmpinfoheader[ 9] = (unsigned char)(       Y>> 8);
      bmpinfoheader[10] = (unsigned char)(       Y>>16);
      bmpinfoheader[11] = (unsigned char)(       Y>>24);

      f = fopen(filename,"wb");
      fwrite(bmpfileheader,1,14,f);
      fwrite(bmpinfoheader,1,40,f);
      for(int i=0; i<Y; i++)
      {
	  fwrite(img+(X*(Y-i-1)*3),3,X,f);
	  fwrite(bmppad,1,(4-(X*3)%4)%4,f);
      }
      fclose(f);
  }

};
