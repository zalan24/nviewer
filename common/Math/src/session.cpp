#include "common/Math/include/session.h"

Session::Session(string file, int dim, MachineConfig *mcfg) : name(file), cam(NULL), func(NULL), file(NULL), mcfg(mcfg), vram(NULL), dim(dim)
{
    open(file, dim);
}

Session::Session(string file, MachineConfig *mcfg) : name(file), cam(NULL), func(NULL), file(NULL), mcfg(mcfg), vram(NULL), dim(0)
{
    open(file);
}

Session::~Session()
{
    clear();
}

string Session::getName() {return name;}

Function<RTYPE> *Session::getFunction() {return func;}

void Session::clear()
{
  if (vram != NULL) delete vram;
  if (file != NULL) {
      updateStyles();
      file->save(data);
      delete file;
  }
  if (func != NULL) delete func;
  if (cam != NULL) delete cam;
  file = NULL;
  func = NULL;
  cam = NULL;
  vram = NULL;
}

void Session::open(string name)
{
    clear();

    LOG<<"Opening session: "<<name<<endl;
    bool isFile = false;
    if (name.length() > 4) isFile = name.substr(name.length()-4,-1) == ".cfg";
    //if (!isFile) mkdir(name.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    string filename = name;
    if (!isFile) filename = name + "/session.cfg";
    //cfg = new SessionConfig(filename.c_str(), OPEN);

    //dim = readInt(cfg->getFunc()->getAttribute("dim",""));

    file = new SessionFile;
    if (!file->load(filename, data)) throw Exception(CANNOT_OPEN_FILE, "Could not open session file: "+name);
    dim = data.dim;

    int len = readInt(mcfg->getApp()->getAttribute("functionvram",DEFAULT_FVRAM));
    vram = new VRam(len);
}

void Session::open(string name, int dim)
{
    clear();

    LOG<<"Opening session: "<<name<<endl;
    bool isFile = false;
    if (name.length() > 4) isFile = name.substr(name.length()-4,-1) == ".cfg";
    if (!isFile) mkdir(name.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    string filename = name;
    if (!isFile) filename = name + "/session.cfg";
    //cfg = new SessionConfig(filename.c_str(), CREATE);
    //cfg->getFunc()->setAttributeT("dim",dim);

    file = new SessionFile;
    data.dim = dim;
    if (!file->save(filename, data)) throw Exception(FILE_NOT_FOUND, "Could not open session file: "+name);

    int len = readInt(mcfg->getApp()->getAttribute("functionvram",DEFAULT_FVRAM));
    vram = new VRam(len);
  
}


Camera< RTYPE >* Session::getCamera() {return cam;}

MachineConfig* Session::getMachineConfig() {return mcfg;}

void Session::reloadFunction()
{
  //func->set(cfg->getFunc());
}

void Session::loadScriptInPreview(string filename, INotifiable *script, INotifiable *runtime)
{
    data.script = filename;
    clearScript();
    int numThreads = readInt(mcfg->getApp()->getAttribute("threads",DEFAULT_THREADS));
    LOG<<filename<<endl;
    StyleSheet ss("param");
    //load stylesheet
    func = Function<RTYPE>::getFunction (filename, &ss, dim, 1.0, vram, true, numThreads);
    func->setVRamNotifiers(script,runtime);
    func->setStyle(&data.style);
    func->set(&data.param);
    cam = new Camera<RTYPE> (func, mcfg);
    //cfg->getFunc()->setAttribute("script",filename);
    file->save(data);
    //cfg->WriteConfig();
    setD();
}

void Session::loadScriptInExport(string filename, INotifiable *, INotifiable *)
{
    clearScript();
    int numThreads = readInt(mcfg->getApp()->getAttribute("threads",DEFAULT_THREADS));
    LOG<<filename<<endl;
    StyleSheet ss("param");
    //load stylesheet
	LOG<<"Laading script"<<endl;
    func = Function<RTYPE>::getFunction (filename, &ss, dim, 1.0, vram, false, numThreads);
//     func->setVRamNotifiers(script,runtime);
    func->setStyle(&data.style);
    func->set(&data.param);
	LOG<<"Creating camera"<<endl;
    cam = new Camera<RTYPE> (func, mcfg);
    //cfg->getFunc()->setAttribute("script",filename);
    data.script = filename;
    file->save(data);
    //cfg->WriteConfig();
    setD();
	LOG<<"Script ready"<<endl;
}

void Session::setD()
{
    int den,dep,numd;
    den = readInt(mcfg->getApp()->getAttribute("density",""));
    dep = readInt(mcfg->getApp()->getAttribute("depth",""));
    numd = readInt(mcfg->getApp()->getAttribute("numd",""));
    cam->setD(den,dep,numd);
}

void Session::loadLastScript(INotifiable *script, INotifiable *runtime)
{
    string filename = data.script;
    if (filename != "") loadScriptInPreview(filename, script, runtime);
}

bool Session::scriptAssociated() const
{
    string filename = data.script;
    return filename != "";
}

bool Session::isThereAScript() const
{
    string filename = data.script;
    return filename != "";
}

void Session::clearScript()
{
    if (func != NULL) delete func;
    if (cam != NULL) delete cam;
    func = NULL;
    cam = NULL;
}

void Session::updateStyles()
{
    if (func == NULL) return;
    data.param.clear();
    data.style.clear();
    func->exportParam(&data.param);
    func->exportStyle(&data.style);
}



