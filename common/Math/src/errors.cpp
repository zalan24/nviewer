#include "common/Math/include/errors.h"

string Error::ERROR_STRING = "";

ERROR_CODE SyntacsError(ERROR_CODE e, string text, string file, int line, int) {
  stringstream ss;
  ss<<line;
  Error::ERROR_STRING = "Syntacs error: "+text + '\n' + "in: "+file+" at line "+ss.str();
  return e;
}
ERROR_CODE SyntacsError(ERROR_CODE e, string text, string file, int line) {return SyntacsError(e,text,file,line,0);}

ERROR_CODE CompileError(ERROR_CODE e, string text, string file, int line, int) {
  stringstream ss;
  ss<<line;
  Error::ERROR_STRING = "Compile error: "+text + '\n' + "in: "+file+" at line "+ss.str();
  return e;
}
ERROR_CODE CompileError(ERROR_CODE e, string text, string file, int line) {return CompileError(e,text,file,line,0);}

ERROR_CODE RuntimeError(ERROR_CODE e, string text, string file, int line, int) {
  stringstream ss;
  ss<<line;
  Error::ERROR_STRING = "Runtime error: "+text + '\n' + "in: "+file+" at line "+ss.str();
  return e;
}
ERROR_CODE RuntimeError(ERROR_CODE e, string text, string file, int line) {return RuntimeError(e,text,file,line,0);}

ERROR_CODE Exception(ERROR_CODE e, string text)
{
  Error::ERROR_STRING = text;
  return e;
}
