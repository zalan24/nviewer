#include "common/Math/include/config.h"

OpenType operator |(OpenType a, OpenType b)
{return static_cast<OpenType>(static_cast<int>(a) | static_cast<int>(b));}

OpenType operator &(OpenType a, OpenType b)
{return static_cast<OpenType>(static_cast<int>(a) & static_cast<int>(b));}

Config::Config(const char *filename, OpenType type) : filename(filename), root(NULL)
{
    ifstream in(filename);
    if (in) {
        string s = "", s2;
        while (in>>s2) {
            s += s2 + " ";
        }
        in.close();
        root = Element::fromString(s);
        if (!(type & OPEN)) {
            clear();
            throw Exception(CANNOT_OPEN_FILE, string("File already exists: ")+string(filename));
        }
    } else {
        clear();
        if (!(type & CREATE)) throw Exception(CANNOT_OPEN_FILE, string("Cannot open file: ")+string(filename));
    }
}

void Config::WriteConfig() const
{
    WriteConfig(filename.c_str());
}


void Config::WriteConfig(const char *filename) const
{
  if (root == NULL) return;
  string outs = root->toString();
  if (outs == "") return;
  ofstream out(filename);
  if (out) {
    out<<outs;
    out.close();
  }
}


MachineConfig *MachineConfig::instance = NULL;

MachineConfig::MachineConfig(const char *filename, OpenType type) : Config(filename, type)
{
  if (instance == NULL) instance = this;
  LOG<<"Loading config file: "<<filename<<endl;
  if (root != NULL) {
    if (root->getType() != "app") throw UnexpectedTag();
    app = dynamic_cast<App*> (root);
  } else app = NULL;
  setDefault();
  window = dynamic_cast<Window*> (app->findObjectOfType("window"));
  keyboard = dynamic_cast<KeyBoardConfig*> (app->findObjectOfType("keyboard"));
  process = dynamic_cast<Process*> (app->findObjectOfType("process"));
}


App* MachineConfig::getApp() const {return app;}
Window* MachineConfig::getWindow() const {return window;}
KeyBoardConfig* MachineConfig::getKeyBoard() const {return keyboard;}
Process* MachineConfig::getProcess() const {return process;}

void MachineConfig::setDefault()
{
  if (app == NULL) app = new App(string("threads=\"")+DEFAULT_THREADS+"\"");
  else {
    if (!app->isKey("threads")) app->setAttribute("threads", DEFAULT_THREADS);
    if (!app->isKey("vram")) app->setAttribute("vram", DEFAULT_VRAM);
    if (!app->isKey("functionvram")) app->setAttribute("functionvram", DEFAULT_FVRAM);
    if (!app->isKey("reclimit")) app->setAttribute("reclimit", DEFAULT_RECMEMORY);
    if (!app->isKey("occlusion")) app->setAttribute("occlusion", DEFAULT_OCCLUSION);
    if (!app->isKey("lighting")) app->setAttribute("lighting", DEFAULT_LIGHTING);
  }
  Window * window;
  Element * e = app->findObjectOfType("window");
  if (e == NULL) {
    window = new Window(string("width=\"")+DEFAULT_WIDTH+"\" height=\""+DEFAULT_HEIGHT+"\"");
    app->addChild(window);
  } else {
    if (e->getType() != "window") throw UnexpectedTag();
    window = dynamic_cast<Window*> (e);
    if (!window->isKey("width")) window->setAttribute("width", DEFAULT_WIDTH);
    if (!window->isKey("height")) window->setAttribute("height", DEFAULT_HEIGHT);
  }
  KeyBoardConfig *keyboard;
  e = app->findObjectOfType("keyboard");
  if (e == NULL) {
    keyboard = new KeyBoardConfig();
    window->addChild(keyboard);
  } else {
    if (e->getType() != "keyboard") throw UnexpectedTag();
    keyboard = dynamic_cast<KeyBoardConfig*> (e);
  }
  vector<KeyConfig*> keys;
  /*keys.push_back(new KeyConfig("id=\"kc_left\" value=\""+to_string((int)DEFAULT_LEFT)+"\""));
  keys.push_back(new KeyConfig("id=\"kc_right\" value=\""+to_string((int)DEFAULT_RIGHT)+"\""));
  keys.push_back(new KeyConfig("id=\"kc_zoomin\" value=\""+to_string((int)DEFAULT_ZOOMIN)+"\""));
  keys.push_back(new KeyConfig("id=\"kc_zoomout\" value=\""+to_string((int)DEFAULT_ZOOMOUT)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_forward\" value=\""+to_string((int)DEFAULT_FORWARD)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_backward\" value=\""+to_string((int)DEFAULT_BACKWARD)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_scalein\" value=\""+to_string((int)DEFAULT_SCALEIN)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_scaleout\" value=\""+to_string((int)DEFAULT_SCALEOUT)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis1\" value=\""+to_string((int)DEFAULT_AXIS1)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis2\" value=\""+to_string((int)DEFAULT_AXIS2)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis3\" value=\""+to_string((int)DEFAULT_AXIS3)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis4\" value=\""+to_string((int)DEFAULT_AXIS4)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis5\" value=\""+to_string((int)DEFAULT_AXIS5)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis6\" value=\""+to_string((int)DEFAULT_AXIS6)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis7\" value=\""+to_string((int)DEFAULT_AXIS7)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis8\" value=\""+to_string((int)DEFAULT_AXIS8)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis9\" value=\""+to_string((int)DEFAULT_AXIS9)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis10\" value=\""+to_string((int)DEFAULT_AXIS10)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis11\" value=\""+to_string((int)DEFAULT_AXIS11)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis12\" value=\""+to_string((int)DEFAULT_AXIS12)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_axis13\" value=\""+to_string((int)DEFAULT_AXIS13)+"\""));
  keys.push_back(new KeyConfig( "id=\"kc_console\" value=\""+to_string((int)DEFAULT_CONSOLE)+"\""));*/
  for (unsigned int i = 0; i < keys.size(); ++i) {
    Element * e = keyboard->findObjectWithId(keys[i]->getId());
    if (e != NULL) {
      if (e->getType() == "key") {
	KeyConfig *key = dynamic_cast<KeyConfig*> (e);
	if (!key->isKey("value")) key->setAttribute("value", "0");
      } else throw UnexpectedTag();
      delete keys[i];
    } else keyboard->addChild(keys[i]);
  }
  e = app->findObjectOfType("process");
  Process *proc;
  if (e == NULL) {
    stringstream ss;
    ss<<DEFAULT_MAX_ARRAY_SIZE;
    proc = new Process(string("max_array_size=\"")+ss.str()+"\"");
    app->addChild(proc);
  } else {
    if (e->getType() != "process") throw UnexpectedTag();
    proc = dynamic_cast<Process*> (e);
    stringstream ss;
    ss<<DEFAULT_MAX_ARRAY_SIZE;
    if (!proc->isKey("max_array_size")) proc->setAttribute("max_array_size", ss.str());
  }
  root = app;
  WriteConfig();
}

void Config::clear()
{
  delete root;
  root = NULL;
}

Config::~Config()
{
  WriteConfig(filename.c_str());
  clear();
}

App::App() : Element()
{
}

App::App(const string args) : Element(args)
{
}

string App::getType() const
{
  return "app";
}

Process::Process() : Element()
{
}

Process::Process(const string args) : Element(args)
{
}

string Process::getType() const
{
  return "process";
}

KeyConfig::KeyConfig() : Element()
{
}

KeyConfig::KeyConfig(const string args) : Element(args)
{
}

string KeyConfig::getType() const
{
  return "key";
}

CameraElement::CameraElement() : Element()
{
}

CameraElement::CameraElement(const string args) : Element(args)
{
}

string CameraElement::getType() const
{
  return "camera";
}

KeyBoardConfig::KeyBoardConfig() : Element()
{
}

KeyBoardConfig::KeyBoardConfig(const string args) : Element(args)
{
}

string KeyBoardConfig::getType() const
{
  return "keyboard";
}

Window::Window() : Element()
{
}
Window::Window(const string args) : Element(args)
{
}

string Window::getType() const
{
  return "window";
}

HolderElement::HolderElement() : Element()
{
}
HolderElement::HolderElement(const string args) : Element(args)
{
}

string HolderElement::getType() const
{
  return "holder";
}

CustomParameterElement::CustomParameterElement() : Element()
{
}
CustomParameterElement::CustomParameterElement(const string args) : Element(args)
{
}

string CustomParameterElement::getType() const
{
  return "parameter";
}

StyleElement::StyleElement() : Element()
{
}
StyleElement::StyleElement(const string args) : Element(args)
{
}

string StyleElement::getType() const
{
  return "style";
}

FunctionElement::FunctionElement() : Element()
{
}
FunctionElement::FunctionElement(const string args) : Element(args)
{
}

string FunctionElement::getType() const
{
  return "function";
}

CustomFunctionElement::CustomFunctionElement() : Element()
{
}
CustomFunctionElement::CustomFunctionElement(const string args) : Element(args)
{
}

string CustomFunctionElement::getType() const
{
  return "customfunction";
}

string Element::getId() const {return id;}

Element::Element() : tag(NULL), root(NULL)
{
  id = "";
  type = "unknown";
}
Element::Element(const string args) : tag(NULL), root(NULL)
{
  type = "unknown";
  vector<string> args_V;
  if (args.length() > 0) {
    splitString(args, args_V);
    for (unsigned int i = 0; i < args_V.size(); ++i) {
      pair<string, string> e = getKeyValue(args_V[i]);
      attributes[e.first] = e.second;
    }
  }
  id = getAttribute("id", "-");
}

Element::~Element()
{
  for (int i = 0; i < getChildCount(); ++i) delete getChild(i);
}

void* Element::getTag() const {return tag;}
void Element::setTag(void *t) {tag = t;}


int Element::getChildCount() const
{
  return children.size();
}
Element *Element::getChild(int i) const
{
  return children[i];
}
void Element::addChild(Element *e)
{
  e->root = this;
  children.push_back(e);
}

void Element::removeChild(Element *e)
{
  if (children.size() == 0) return;
  auto itr = children.begin();
  while (itr != children.end()) {
    if (*itr == e) {
      children.erase(itr);
      return;
    }
    ++itr;
  }
}


void Element::setAttribute(const string key, const string value)
{
  if (key == "id") id = value;
  attributes[key] = value;
}

bool Element::isKey(const string key)
{
  auto it = attributes.find(key);
  if (it == attributes.end()) return false;
  return true;
}

string Element::getAttribute(const string key, const string def)
{
  string ret = def;
  auto it = attributes.find(key);
  if (it != attributes.end()) ret = (*it).second;
  else setAttribute(key, def);
  return ret;
}

string Element::toString() const
{
  return toString("");
}

string Element::getType() const
{
  return type;
}


string Element::toString(string d) const
{
  string ret = d+"<"+getType();
  for (auto i : attributes) {
    //cout<<i.first<<endl;
    ret += "\n\t"+d+i.first+"=\""+i.second+"\"";
  }
  ret += ">\n";
  for (int i = 0; i < getChildCount(); ++i) ret += getChild(i)->toString(d+"\t");
  ret += d+"</"+getType()+">\n";
  return ret;
}

void Element::getAttributes(vector<pair<string,string>> &vec)
{
  for (auto i : attributes) vec.push_back(i);
}

Element* Element::createElementByName(const string s, const string attr)
{
  if (s == "app") return new App(attr);
  if (s == "window") return new Window(attr);
  if (s == "keyboard") return new KeyBoardConfig(attr);
  if (s == "key") return new KeyConfig(attr);
  if (s == "process") return new Process(attr);
  if (s == "function") return new FunctionElement(attr);
  if (s == "camera") return new CameraElement(attr);
  if (s == "holder") return new HolderElement(attr);
  if (s == "style") return new StyleElement(attr);
  if (s == "customfunction") return new CustomFunctionElement(attr);
  if (s == "parameter") return new CustomParameterElement(attr);
  return NULL;
}

Element* Element::fromString(string s)
{
  Element *ret = NULL;
  bool inside = false;
  string stack[1000];
  int stackid[1000];
  int st = 0;
  int begin = 0;
  bool end;
  string name,attr;
  //cout<<"------"<<endl;
  //cout<<s<<endl;
  for (unsigned int i = 0; i < s.length();) {
    if (s[i] == '\"') inside = !inside;
    if (inside) continue;
    if (s[i] == '<') {
      begin = i;
      i++;
      if (i >= s.length()) continue;
      if (s[i] == '/') {
	end = true;
	i++;
      } else end = false;
      if (i >= s.length()) continue;
      name = "";
      while (i < s.length() && ((!iswspace(s[i]) && s[i] != '>') || inside)) {
	if (s[i] == '\"') inside = !inside;
	name += s[i++];
      }
      attr = "";
      while (i < s.length() && (s[i] != '>' || inside)) {
	if (s[i] == '\"') inside = !inside;
	attr += s[i++];
      }
      if (end) {
	--st;
	//cout<<"end: "<<stack[st]<<" "<<name<<endl;
	if (stack[st] != name) throw InvalidClosingTag();
	if (st == 1) ret->addChild(fromString(s.substr(stackid[st],i+1-stackid[st])));
      } else {
	if (st == 0) {
	  ret = createElementByName(name,attr);
	}
	//cout<<"begin: "<<name<<endl;
	stackid[st] = begin;
	stack[st] = name;
	st++;
      }
    } else i++;
  }
  //cout<<"done"<<endl;
  return ret;
}

Element *Element::findObjectOfType(const string s)
{
  if (s == getType()) return this;
  for (int i = 0; i < getChildCount(); ++i) {
    Element *e = getChild(i)->findObjectOfType(s);
    if (e != NULL) return e;
  }
  return NULL;
}

Element *Element::findObjectWithId(const string s)
{
  if (s == getId()) return this;
  for (int i = 0; i < getChildCount(); ++i) {
    Element *e = getChild(i)->findObjectWithId(s);
    if (e != NULL) return e;
  }
  return NULL;
}


SessionConfig::SessionConfig(const char *filename, OpenType type) : Config(filename, type), func(NULL)
{
  LOG<<"Loading session config file: "<<filename<<endl;
  if (root != NULL) {
    if (root->getType() != "function") throw UnexpectedTag();
    func = dynamic_cast<FunctionElement*> (root);
  }
  setDefault();
}

void SessionConfig::setDefault()
{
  if (func == NULL) func = new FunctionElement("dim=\"3\"");
  else {
    //if (!func->isKey("functionname")) func->setAttribute("functionname", DEFAULT_FUNCTIONNAME);
    if (!func->isKey("dim")) func->setAttribute("dim", "3");
  }
  root = func;
  WriteConfig();
}

FunctionElement* SessionConfig::getFunc() {return func;}

ListConfig::ListConfig(const char *filename, OpenType type) : Config(filename, type), holder(NULL)
{
  LOG<<"Loading list config file: "<<filename<<endl;
  setDefault();
}

void ListConfig::setDefault()
{
  if (root != NULL) {
    if (root->getType() != "holder") throw UnexpectedTag();
    holder = dynamic_cast<HolderElement*> (root);
  } else holder = new HolderElement("");
  root = holder;
  WriteConfig();
}

HolderElement* ListConfig::getHolder() {return holder;}

