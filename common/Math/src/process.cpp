#include "common/Math/include/process.h"


bool addColours(int n, float * in, float *bg, int *d, bool *leave, bool *pause, double distance, int id)
{
  bool ret = true;
  for (int i = 0; i < n; ++i) {
    if (ret && isColourSolid(in[i*4+3])) {
      d[i] = id;
    } else if (in[i*4+3] != 0) ret = false;
    double a = in[4*i+3];
    double x,y;
    x = a*distance;
    y = (1.0-a);
    double mx = x+y;
    x /= mx;
    y /= mx;
    a = x;
    bg[i*4  ] = lerp(bg[4*i  ], in[i*4  ], a);
    bg[i*4+1] = lerp(bg[4*i+1], in[i*4+1], a);
    bg[i*4+2] = lerp(bg[4*i+2], in[i*4+2], a);
    bg[i*4+3] = 1;
    while (*pause && !(*leave)) {
      std::this_thread::sleep_for(std::chrono::milliseconds(SLEEP_FOR));
    }
    if (*leave) break;
  }
  return ret;
}
