#include "common/Math/include/tools.h"

bool operator==(const Colour &c1, const Colour &c2)
{
  return c1.r == c2.r && c1.g == c2.g && c1.b == c2.b && c1.a == c2.a;
}

void Colour::parse(const string s)
{
  r = (float)fromHex(s.substr(1,2))/255;
  g = (float)fromHex(s.substr(3,2))/255;
  b = (float)fromHex(s.substr(5,2))/255;
  a = (float)fromHex(s.substr(7,2))/255;
}

string Colour::print()
{
  string sa,sr,sg,sb;
  sa = getHex(a*255);
  sr = getHex(r*255);
  sg = getHex(g*255);
  sb = getHex(b*255);
  if (sa.length() == 1) sa = "0" + sa;
  if (sr.length() == 1) sr = "0" + sr;
  if (sg.length() == 1) sg = "0" + sg;
  if (sb.length() == 1) sb = "0" + sb;
  return "#"+sr+sg+sb+sa;
}

string getHex(int n)
{
  string s = "0123456789abcdef";
  string ret = "";
  while (n > 0) {
    ret = s[n%16] + ret;
    n /= 16;
  }
  if (ret == "") ret = "0";
  return ret;
}

int fromHex(const string s)
{
  string c = "0123456789abcdef";
  int ret = 0;
  for (unsigned int i = 0; i < s.length(); ++i) ret = ret*16 + c.find(s[i]);
  return ret;
}



double Radian(double degree)
{
  return degree*M_PI/180;
}

bool isTextCharacter(unsigned char c)
{
  string s = "abcdefghijklmnopqrstuvwxyzABCEDFGHIJKLMNOPQRSTUVWXYZ0123456789_.:;,?<>#&@{}[]$/'\"+!%=()-* ";
  if ((int)s.find(c) == -1) return false;
  return true;
}

void splitString(const string &s,vector<string> &vec)
{
  unsigned int i;
  bool inside = false;
  unsigned int begin = 0;
  for (i = 0; i < s.length(); ++i) {
    if (s[i] == '"') inside = !inside;
    if (!inside && iswspace(s[i])) {
      if (begin < i) vec.push_back(s.substr(begin,i-begin));
      begin = i+1;
    }
  }
  if (begin <= s.length()-1) vec.push_back(s.substr(begin,-1));
}

void splitString(const string &s, const char c,vector<string> &vec)
{
  unsigned int i;
  bool inside = false;
  unsigned int begin = 0;
  for (i = 0; i < s.length(); ++i) {
    if (s[i] == '"') inside = !inside;
    if (!inside && s[i] == c) {
      if (begin < i) vec.push_back(s.substr(begin,i-begin));
      begin = i+1;
    }
  }
  if (begin <= s.length()-1) vec.push_back(s.substr(begin,-1));
}

pair<string, string> getKeyValue(const string &s)
{
  int n = s.find("=");
  if (n == 0) throw InvalidArgument();
  string key = s.substr(0,n);
  string value = s.substr(n+2,s.length()-(n+3));
  return make_pair(key,value);
}

string getElementOfMap(map<string,string> &map, const string key, const string def)
{
  auto it = map.find(key);
  if (it == map.end()) return def;
  return (*it).second;
}

void readAxisData(string s, vector<pair<int, double>> &vec)
{
  string s1,s2;
  int n1,n2;
  n1 = s.find('(');
  n2 = s.find(')');
  if (n1 >= n2 || n1 == -1) {
    s2 = s;
    s1 = "";
  } else {
    s1 = s.substr(n1+1,n2-n1-1);
    s2 = s.substr(n2+1,-1);
  }
  vector<string> values;
  splitString(s2,values);
  if (s1 == "") {
    for (unsigned int i = 0; i < values.size(); ++i) vec.push_back(make_pair(i,matof(values[i].c_str())));
    return;
  }
  vector<string> ax;
  splitString(s1, ax);
  if (ax.size() < values.size()) return;
  for (unsigned int i = 0; i < ax.size(); ++i) vec.push_back(make_pair(atoi(ax[i].c_str())-1, matof(values[i].c_str())));
}


pair<double,double> getLimit(int X, int Y)
{
  if (X > Y) return make_pair(1.0,(double)Y/X);
  if (X < Y) return make_pair((double)X/Y,1.0);
  return make_pair(1.0,1.0);
}

void getFolders(string dr, vector<string> &vec)
{
  DIR *dir = opendir(dr.c_str());
  struct dirent *entry = readdir(dir);
  while (entry != NULL)
  {
    if (entry->d_type == DT_DIR) vec.push_back(string(entry->d_name));
    entry = readdir(dir);
  }
  closedir(dir);
}

bool testFile(string file)
{
  ifstream in(file.c_str());
  if (in.good()) {
    in.close();
    return true;
  }
  in.close();
  return false;
}

void removeFolder(const char *path)
{
  struct dirent *entry = NULL;
  DIR *dir = NULL;
  dir = opendir(path);
  while((entry = readdir(dir)))
  {   
    DIR *sub_dir = NULL;
    FILE *file = NULL;
    char abs_path[100] = {0};
    if(*(entry->d_name) != '.')
    {   
      sprintf(abs_path, "%s/%s", path, entry->d_name);
      if((sub_dir = opendir(abs_path)))
      {   
	closedir(sub_dir);
	removeFolder(abs_path);
      }   
      else 
      {   
    if((file = fopen(abs_path, "r")))
	{   
	  fclose(file);
	  remove(abs_path);
	}   
      }   
    }   
  }   
  remove(path);
}


Colour lerp_col(Colour colour1, Colour colour2, float f)
{
  return Colour(lerp(colour1.r,colour2.r,f), lerp(colour1.g,colour2.g,f), lerp(colour1.b,colour2.b,f), lerp(colour1.a,colour2.a,f));
}

Colour adjustBrightness(Colour c, float f)
{
  return Colour(c.r*f, c.g*f, c.b*f, c.a);
}

Colour adjustTransparency(Colour c, float f)
{
  return Colour(c.r,c.g,c.b,c.a*f);
}

Colour adjustSaturate(Colour c, float f)
{
  return Colour(1.0-(1.0-c.r)*(1.0-f), 1.0-(1.0-c.g)*(1.0-f), 1.0-(1.0-c.b)*(1.0-f), c.a);
}

string noWhiteSpaces(string s)
{
  string ret = "";
  for (unsigned int i = 0; i < s.length(); ++i) if (!iswspace(s[i])) ret += s[i];
  return ret;
}


string replace(string s1, string s2, string s3)
{
  string ret = "";
  for (unsigned int i = 0; i < s1.length(); ++i) {
    if (s1.length()-i < s2.length()) {
      ret += s1.substr(i,-1);
      break;
    } else if (s1.substr(i,s2.length()) == s2) {
      ret += s3;
      i += s2.length();
      i--;
      continue;
    }
    ret += s1[i];
  }
  return ret;
}

bool isBoolean(const string s)
{
  if (s == "true" || s == "false" || s == "TRUE" || s == "FALSE" || s == "True" || s == "False") return true;
  return false;
}

bool isT(const string s)
{
  if (s.length() == 0) return false;
  unsigned int n = 0;
  unsigned int nump = 0;
  if (s[0] == '-') n++;
  if (n == s.length()) return false;
  for (unsigned int i = n; i < s.length(); ++i) {
    if (s[i] == '.') nump++;
    else if (s[i] < '0' || s[i] > '9') return false;
  }
  if (nump > 1) return false;
  if (nump + n == s.length()) return false;
  if (s[n] == '.' || s[s.length()-1] == '.') return false;
  return true;
}


bool isColour(const string s)
{
  if (s.length() != 1+4*2) return false;
  if (s[0] != '#') return false;
  for (unsigned int i = 1; i < s.length(); ++i) if (!((s[i] <= '9' && s[i] >= '0') || (s[i] <= 'f' && s[i] >= 'a'))) return false;
  return true;
}

bool isInteger(const string s)
{
  if (s.length() == 0) return false;
  for (unsigned int i = 0; i < s.length(); ++i) if (s[i] < '0' || s[i] > '9') return false;
  return true;
}


void getParts(const string s, vector<string> &vec, char div, int depth)
{
  int inside = 0;
  int last = -1;
  if (s.length() > 0 && s[0] == div) last = 0;
  for (unsigned int i = last+1; i < s.length(); ++i) {
    if (isOpening(s[i])) {
      inside++;
      if (inside == depth) last = i;
    }
    if ((s[i] == div || isClosing(s[i]) || i+1 == s.length()) && inside == depth) {
      string s2;
      if (s[i] == div || isClosing(s[i])) s2 = s.substr(last+1,i-(last+1));
      else s2 = s.substr(last+1,i-last);
      if (s2 != "") {
	vec.push_back(s2);
	//cout<<"asd: "<<s2<<endl;
      }
      last = i;
    }
    if (isClosing(s[i])) {
      inside--;
      if (inside == 0 && depth == 0 && i+1 == s.length()) {
	string s2 = s.substr(last+1,-1);
	if (s2 != "") vec.push_back(s2);
      }
    }
  }
}

void getParts(const string s, vector<string> &vec, const string div, int depth)
{
  string s2 = replace(s,div,string("")+DIVISION_CHARACTER);
  getParts(s2,vec,DIVISION_CHARACTER,depth);
}

bool isOpening(const char c) {return c == '(' || c == '{' || c == '[';}
bool isClosing(const char c) {return c == ')' || c == '}' || c == ']';}
bool match(const string s, const string reg) {return std::regex_match(s,std::regex(reg));}



string getGenericTypeFromArgs(string args, int &n, const string b)
{
  if (args.length() == 0) return "";
  if (args[args.length()-1] == ')') {
    while (args.length() > 0 && args[0] != '(') args = args.substr(1,-1);
    if (args.size() <= 2) return "";
    return "("+getGenericTypeFromArgs(args.substr(1,args.size()-2),n,b)+")";
  }
  vector<string> parts;
  getParts(args,parts,',',0);
  string ret = "";
  for (unsigned int i = 0; i < parts.size(); ++i) {
    stringstream ss;
    ss<<"type_"<<b<<n++;
    ret += ","+ss.str();
  }
  if (ret.length() > 0) ret = ret.substr(1,-1);
  return ret;
}

int getNumAtDepth(const string s, const int d)
{
  int inside = 0;
  int ret = 0;
  for (unsigned int i = 0; i < s.length(); ++i) {
    if (s[i] == '(') inside++;
    else if (s[i] == ')') inside--;
    else if (inside == d) ret++;
  }
  return ret;
}

bool complexityRelation(const string &s1, const string &s2)
{
  int db1,db2;
  db1 = db2 = 0;
  for (unsigned int i = 0; i < s1.length(); ++i) if (s1[i] == '(') db1++;
  for (unsigned int i = 0; i < s2.length(); ++i) if (s2[i] == '(') db2++;
  return db1 <= db2;
}

bool isLetter(char c)
{
  if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) return true;
  return false;
}

int fact(int n)
{
  int ret = 1;
  while (n > 1) ret *= n--;
  return ret;
}

int getMiddleOfLongest(void **t, int a, int b)
{
  int length=0;
  int start=a;
  int mx=a;
  int val=0;
  for (int i = a; i < b; ++i) {
    if (t[i] == NULL) {
      length++;
      if (length > val) {
	val = length;
	mx = start;
      }
    } else {
      start = i+1;
      length = 0;
    }
  }
  if (mx < b) return mx+val/2;
  return a;
}

bool isColourSolid(float f)
{
  return f >= 0.999;
}

int readInt(const string s)
{
    if (match(s,FORMAT_INT)) {
        return atoi(s.c_str());
    } else {
        throw Exception(INVALID_INT, "Invalid Int format");
    }
}

uint getDate()
{
    time_t t = time(0);
    struct tm * now = localtime( & t );
    uint y = now->tm_year;
    uint m = now->tm_mon;
    uint d = now->tm_mday;
    uint h = now->tm_hour;
    uint min = now->tm_min;
    uint sec = now->tm_sec;
    return sec + 60 * (min + 60 * (h + 24 * (d + 32*(m + 13 * y))));
}

uint dateToSec(uint t)
{
    return t % 60;
}

uint dateToMin(uint t)
{
    return (t / 60) % 60;
}

uint dateToHour(uint t)
{
    return (t / 60 / 60) % 24;
}

uint dateToDay(uint t)
{
    return (t / 60 / 60 / 24) % 32;
}

uint dateToMonth(uint t)
{
    return ((t / 60 / 60 / 24 / 32) % 13) + 1;
}

uint dateToYear(uint t)
{
    return (t / 60 / 60 / 24 / 32 / 13) + 1900;
}

string dateToString(uint t)
{
    int y,m,d,h,min,s;
    y = dateToYear(t);
    m = dateToMonth(t);
    d = dateToDay(t);
    h = dateToHour(t);
    min = dateToMin(t);
    s = dateToSec(t);
    stringstream ss;
    ss<<y<<".";
    if (m < 10) ss<<"0";
    ss<<m<<".";
    if (d < 10) ss<<"0";
    ss<<d<<". ";
    if (h < 10) ss<<"0";
    ss<<h<<":";
    if (min < 10) ss<<"0";
    ss<<min<<":";
    if (s < 10) ss<<"0";
    ss<<s;
    return ss.str();
}

float matof(const char cs[])
{
    /*string s(cs);
    int comma = s.find(',');
    if (comma != -1) s[comma] = '.';
    return atof(s.c_str());*/
    std::istringstream istr(cs);
    float ret;
    istr>>ret;
    return ret;
}

/*
void blurImage(float **scl, int **d, float **tcl, int W, int H, int px, int py, float blur0, float blur1, float blur2)
{
  for (int i = px; i < W-px; ++i) {
    for (int j = py; py < H-py; ++j) {
      tcl[i][j*4  ] = 0;
      tcl[i][j*4+1] = 0;
      tcl[i][j*4+2] = 0;
      tcl[i][j*4+3] = 0;
    }
  }
}


void gaussBlur (float **scl, float **tcl, int W, int H, float r)
    double rs = ceil(r * 2.57);     // significant radius
    for(var i=0; i<H; i++)
        for(var j=0; j<W; j++) {
            var val = 0, wsum = 0;
            for(var iy = i-rs; iy<i+rs+1; iy++)
                for(var ix = j-rs; ix<j+rs+1; ix++) {
                    var x = min(w-1, max(0, ix));
                    var y = min(h-1, max(0, iy));
                    var dsq = (ix-j)*(ix-j)+(iy-i)*(iy-i);
                    var wght = Math.exp( -dsq / (2*r*r) ) / (Math.PI*2*r*r);
                    val += scl[x][y] * wght;  wsum += wght;
                }
            tcl[i*w+j] = Math.round(val/wsum);            
        }
}*/
