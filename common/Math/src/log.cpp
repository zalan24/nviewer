#include "common/Math/include/log.h"

Log* Log::instance = NULL;

void Log::setLogger(LoggerBase *l)
{
    logger = l;
}

Log::Log(const char *filename): out2(NULL), endL(true), debugs(NULL), logger(NULL), m(NORMAL)
{
    file = new ofstream(filename);
    out = file;
    if (!(*out)) throw StreamError();
    log("Initalize log(const char*)");
    instance = this;
}

Log::Log(ostream *out2, const char *filename) : out2(out2), endL(true), debugs(NULL), logger(NULL), m(NORMAL)
{
    if (!(*out2)) throw StreamError();
    file = new ofstream(filename);
    out = file;
    if (!(*out)) throw StreamError();
    log("Initalize log(ostream*, const char*)");
    instance = this;
}

Log::Log(const char *filename, ostream *out2) : Log(out2,filename) {}

Log::Log(ostream *out) : out(out), file(NULL), out2(NULL), endL(true), logger(NULL), m(NORMAL)
{
    if (!(*out)) throw StreamError();
    log("Initalize log(const char*, ostream*)");
    instance = this;
}

Log::Log(ostream *out, ostream *out2) : out(out), file(NULL), out2(out2), endL(true), debugs(NULL), logger(NULL), m(NORMAL)
{
    if (!(*out)) throw StreamError();
    if (!(*out2)) throw StreamError();
    log("Initalize log(ostream*, ostream*)");
    instance = this;
}

Log::~Log()
{
    log("Close log");
    if (file != NULL) {
        file->close();
        delete file;
    }
}

Log& operator<<(Log &log, const LogModifier m)
{
  log.m = m;
  return log;
}

void Log::openDebug(ostream *s)
{
    debugs = s;
}

const string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);
    return buf;
}

void Log::log(string s)
{
    if (!endL) {
        (*out)<<endl;
        if (logger != NULL) logger->logLoaded(m);
        if (out2 != NULL) (*out2)<<endl;
    }
    endL = true;
    if (logger != NULL) logger->loadText(s);
    string time = currentDateTime();
    s = time + ": " + s;
    (*out)<<s<<endl;
    if (out2 != NULL) (*out2)<<s<<endl;
}

void Log::continuesLog(string s)
{
    if (logger != NULL) logger->loadText(s);
    if (endL) {
        string time = currentDateTime();
        s = time + ": " + s;
    }
    (*out)<<s;
    if (out2 != NULL) (*out2)<<s;
    if (s[s.length()-1] == '\n') endL = true;
    else endL = false;
}

void Log::endline()
{
    (*out)<<endl;
    if (logger != NULL) logger->logLoaded(m);
    if (out2 != NULL) (*out2)<<endl;
    endL = true;
}


Log& operator<<(Log &log, StandardEndLine)
{
    log.endline();
    return log;
}
