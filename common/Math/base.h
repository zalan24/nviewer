#ifndef BASE_H
#define BASE_H

#include <iostream>
#include <sstream>
#include <vector>

#include "common/Math/include/matrix.h"
#include "common/Math/include/vectorn.h"

using namespace std;

template <class T>
class Base
{
    vector<VectorN<T>*> vectors;
    bool mirrored;
public:
    Base();
    ~Base();
    void clear();
    void setDefault(int dim);
    void normalize();
    void orthogonate(int fixed = 0);
    Matrix<T> toRotation() const;
    void setVector(int i, VectorN<T> v, bool needTransform = false);
    int size() const {return vectors.size();}
    VectorN<T> get(int i) const {return *vectors[i];}
    string toString() const;
    void pointMirror();
    void transform(Matrix<T> m);
};

template <class T> void Base<T>::transform(Matrix<T> m)
{
    for (unsigned int i = 0; i < vectors.size(); ++i) *vectors[i] = m * *vectors[i];
}

template <class T> string Base<T>::toString() const
{
    stringstream ss;
    for (int i = 0; i < size(); ++i) ss<<get(i)<<endl;
    return ss.str();
}

template <class T> Base<T>::Base() : mirrored(false)
{

}

template <class T> void Base<T>::pointMirror()
{
    mirrored = !mirrored;
    for (int i = 0; i < vectors.size(); ++i) *vectors[i] *= -1;
}

template <class T> void Base<T>::setVector(int ind, VectorN<T> v, bool needTransform)
{
    v.normalize();
    if (needTransform) {
        T dotm, dot;
        int index = -1;
        for (unsigned int i = 0; i < vectors.size(); ++i) {
            if (ind == (int)i) continue;
            dot = *vectors[i] * v;
            if (index == -1 || mabs(dot) > mabs(dotm)) {
                dotm = dot;
                index = i;
            }
        }
        if (index != ind) {
            Matrix<T> rot = Matrix<T>::Identity(v.size());      //Rotation matrix around (index;ind) 90
            rot.set(index,index,0);
            rot.set(ind,ind,0);
            rot.set(index,ind,1);
            rot.set(ind,index,-1);
            while (mabs(*vectors[ind] * v) < mabs(*vectors[index] * v)) transform(rot);
        }
        if (*vectors[ind] * v < 0) {
            Matrix<T> rot = Matrix<T>::Identity(v.size());      //Rotation matrix around (index;ind) 180
            rot.set(index,index,-1);
            rot.set(ind,ind,-1);
            int i = 0;
            if (i == ind) i++;
            transform(rot);
        }
    }
    *vectors[ind] = v;
}

template <class T> Matrix<T> Base<T>::toRotation() const
{
    Matrix<T> m(vectors[0]->size(), vectors.size());
    for (unsigned int i = 0; i < vectors.size(); ++i) {
        for (int j = 0; j < vectors[i]->size(); ++j) {
            m.set(j,i,(*vectors[i])[j]);
        }
    }
    return m;
}

template <class T> void Base<T>::orthogonate(int fixed)
{
    if (fixed > 0) swap(vectors[fixed], vectors[0]);
    for (unsigned int i = 0; i < vectors.size(); ++i) {
        for (unsigned int j = 0; j < i; ++j) {
            T dot = *(vectors[i]) * *(vectors[j]);      //vector[j] is normalized
            *(vectors[i]) -= *(vectors[j]) * dot;
        }
        vectors[i]->normalize();
    }
    if (fixed > 0) swap(vectors[fixed], vectors[0]);
}

template <class T> void Base<T>::normalize()
{
    for (int i = 0; i < vectors.size(); ++i) vectors[i]->normalize();
}

template <class T> void Base<T>::setDefault(int dim)
{
    clear();
    for (int i = 0; i < dim; ++i) {
        vectors.push_back(new VectorN<T>(VectorN<T>::identity(dim, i)));
    }
}

template <class T> void Base<T>::clear()
{
    for (unsigned int i = 0; i < vectors.size(); ++i) delete vectors[i];
}

template <class T> Base<T>::~Base()
{
    clear();
}

#endif // BASE_H
