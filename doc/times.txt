test_1_0 (cube; 1 thread; transparent) : 50s
test_1_1 (cube; 1 thread; solid) : 47s
box_test_0 (box; 1 thread; transparent) : 120s
box_test_1 (box; 1 thread; solid) : 60s
box (box 512x512x512; 2 threads) : 2207s
box (box 1024x768x1024; 4 threads) : 1697s
cube_sphere (FullHDx1024; 4 threads) : 1735s
box (box HDx1440; 4 threads; iterations: 12) : 5516s
fig9 (fig9 HDx1; 4 threads; iterations: 12) : 249s
