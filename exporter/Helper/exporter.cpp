#include "exporter.h"

Exporter::Exporter (int argc, char* argv[]) :
  sceneManager(NULL),
  styleManager(NULL),
  cfg(NULL),
  session(NULL),
  thr(NULL),
  callback("")
{
  loadCfg();
  filename = string(argv[1]);
  session_name = string(argv[2]);
  images = string(argv[3]);
  /*filename = "export2.zip";
  session_name = "test_session/";
  images = "images/";*/
  if (session_name.length() == 0) session_name = "./";
  if (session_name[session_name.length()-1] != '/') session_name += '/';
  if (images.length() == 0) images = "./";
  if (images[images.length()-1] != '/') images += '/';
  
  //styleManager = new StyleManager();
  string datafile = "data.nvwexport";
  
  PM.unzipDir(session_name, filename);
  PM.mkdir(images);
  path = session_name+TEMP_FILE;
  scriptPath = path + "Scripts/";
  
  ifstream in(path+datafile);
  if (!in.is_open()) {
	throw Exception(ERROR_CODE::CANNOT_OPEN_FILE, "Export file does not exist");
  }
  
  getline(in, mainscript);
  getline(in, stylefile);
  getline(in, scenefile);
  getline(in, sessionfile);
  string s;
  while (getline(in, s)) {
	string sep = " : ";
	int m = s.find(sep);
	string scene, style;
	scene = s.substr(0, m);
	style = s.substr(m+sep.length(), -1);
	exports.push_back(make_pair(scene, style));
  }
  cfg->getApp()->setAttribute("callback", "");
  
  setupSettings();
}

void Exporter::setupSettings()
{
  settings["lighting"] = OptionData(cfg->getApp(), "lighting", "Level of lighting");
  settings["occlusion"] = OptionData(cfg->getApp(), "occlusion", "Level of optimization for CPU on the cost of RAM");
  settings["reclimit"] = OptionData(cfg->getApp(), "reclimit", "Recursion limit for functions");
  settings["threads"] = OptionData(cfg->getApp(), "threads", "Number of threads to use for the calculation");
  settings["vram"] = OptionData(cfg->getApp(), "vram", "Size of the virtual RAM accessed by the functions");
  settings["functionvram"] = OptionData(cfg->getApp(), "functionvram", "Size of virtual RAM that loads the functions");
  settings["depth"] = OptionData(cfg->getApp(), "depth", "Depth of the exported images");
  settings["width"] = OptionData(cfg->getWindow(), "width", "Width of the exported images");
  settings["height"] = OptionData(cfg->getWindow(), "height", "Height of the exported images");
  settings["callback"] = OptionData(cfg->getApp(), "callback", "Command to execute after exporting");
}

Exporter::~Exporter()
{
  LOG<<"Closing exporter"<<endl;
  clean();
  if (cfg != NULL) delete cfg;
  PM.deleteDir(session_name);
}

void Exporter::pauseExport()
{
  LOG<<"Paused"<<endl;
  pause = true;
  pause_started = chrono::system_clock::now();
}

void Exporter::resumeExport()
{
  LOG<<"Resumed"<<endl;
  pause = false;
  chrono::time_point<chrono::system_clock> now = chrono::system_clock::now();
  pause_all += now - pause_started;
  pause_image += now - pause_started;
}

void Exporter::stopExport()
{
  LOG<<"Stopping..."<<endl;
  leave = true;
  pause = false;
  while (thr != NULL) sleep(1);
}



void Exporter::loadCfg()
{
  if (cfg != NULL) {
	delete cfg;
	cfg = NULL;
  }
  cfg = new MachineConfig("nviewer.conf");
  checkMachineConfig(cfg, MAXDISPLAYRES, false);
}

void Exporter::loadSession(string filename)
{
  session = new Session(filename.c_str(),cfg);
}

void Exporter::loadScript(string filename)
{
  session->loadScriptInExport(filename, NULL, NULL);
}

bool Exporter::promt()
{
  cout<<PROMT<<" ";
  string s;
  getline(cin,s);
  while (s.length() > 0 && iswspace(s[s.length()-1])) s = s.substr(0,s.length()-1);
  if (s == "help") {
	showHelp();
  } else if (s == "exit") {
	if (thr == NULL) {
	  LOG<<"Exiting"<<endl;
	  return false;
	} else {
	  cout<<"Export is still running"<<endl;
	}
  } else if (s == "export-pause") {
	if (thr == NULL) {
	  cout<<"Export has not started yet"<<endl;
	} else if (pause) {
	  cout<<"Export already paused"<<endl;
	} else {
	  pauseExport();
	}
  } else if (s == "export-resume") {
	if (thr == NULL) {
	  cout<<"Export has not started yet"<<endl;
	} else if (!pause) {
	  cout<<"Export not paused"<<endl;
	} else {
	  resumeExport();
	}
  } else if (s == "export-stop") {
	if (thr == NULL) {
	  cout<<"Export has not started yet"<<endl;
	} else {
	  stopExport();
	}
  } else if (s == "export") {
	if (thr != NULL) {
	  cout<<"Export already running"<<endl;
	} else {
	  exportPrecalc();
	  thr = new thread(&Exporter::startExporting, this);
	  thr->detach();
	}
  } else if (match(s, "get( .+)?")) {
	if (s == "get") {
	  map<string, OptionData>::iterator itr;
	  for (itr = settings.begin(); itr != settings.end(); ++itr) {
		showCommandHelp((*itr).first, "", (*itr).second.text);
	  }
	} else {
	  int spc = s.find(' ');
	  string id = s.substr(spc+1, -1);
	  map<string, OptionData>::iterator itr = settings.find(id);
	  if (itr == settings.end()) {
		cout<<"No such option found"<<endl;
		cout<<"Type get for more information"<<endl;
	  } else {
		OptionData *o = &(*itr).second;
		if (!o->element->isKey(o->attribute)) {
		  throw Exception(UNKNOWN_ERROR, "System settings: missing value. Try restarting the program.");
		}
		cout<<id<<"="<<o->element->getAttribute(o->attribute, "")<<endl;;
	  }
	}
  } else if (s == "set" || match(s, "set .+")) {
	if (thr != NULL) {
	  cout<<"Settings cannot be changes while the export is running"<<endl;
	  return true;
	}
	if (!match(s, "set [a-z]+( .+|.*)")) {
	  cout<<"Invalid arguments"<<endl;
	} else {
	  int spc = s.find(' ');
	  string idval = s.substr(spc+1, -1);
	  int eq = idval.find(' ');
	  string id;
	  string val;
	  if (eq == -1) {
		id = idval;
		val = "";
	  } else {
		id = idval.substr(0, eq);
		val = idval.substr(eq+1, -1);
	  }
	  map<string, OptionData>::iterator itr = settings.find(id);
	  if (itr == settings.end()) {
		cout<<"No such option found"<<endl;
		cout<<"Type get for more information"<<endl;
	  } else {
		OptionData *o = &(*itr).second;
		if (!o->element->isKey(o->attribute)) {
		  throw Exception(UNKNOWN_ERROR, "System settings: missing value. Try restarting the program.");
		}
		string def = o->element->getAttribute(o->attribute, "");
		try {
		  o->element->setAttribute(id,val);
		  checkMachineConfig(cfg, MAXDISPLAYRES, false);
		} catch (ERROR_CODE e) {
		  LOG<<ERROR<<Error::ERROR_STRING<<NORMAL<<endl;
		  o->element->setAttribute(id,def);
		}
	  }
	}
  } else if (s == "status") {
	if (thr == NULL) {
	  cout<<"Export has not started yet"<<endl;
	} else {
	  chrono::time_point<chrono::system_clock> now;
	  now = chrono::system_clock::now();
	  chrono::duration<double> t_all = chrono::system_clock::now() - start_whole;
	  chrono::duration<double> t_image = chrono::system_clock::now() - start_image;
	  t_all -= pause_all;
	  t_image -= pause_image;
	  
	  cout<<"Image: "<<export_count+1<<" / "<<exports.size()<<endl;
	  cout<<"Current: "<<double(int(state*10000))/100<<"%\t\t"<<"Estimated time left: "<<getTime(t_image, state)<<endl;
	  double allstate = (state + export_count) / exports.size();
	  cout<<"All: "<<double(int(allstate*10000))/100<<"%\t\t"<<"Estimated time left: "<<getTime(t_all, allstate)<<endl;
	}
  } else {
	cout<<"Unknown command"<<endl;
	cout<<"Type help for more information"<<endl;
  }
  return true;
}

string Exporter::getTime(chrono::duration<double> t, double state)
{
  stringstream ss;
  if (state < 0.001) return "-";
  long long int sec = int(t.count()/state - t.count());
  int min = 0;
  int h = 0;
  int day = 0;
  int year = 0;
  if (sec >= 60) {
	min = sec/60;
	sec = sec%60;
	if (min >= 60) {
	  h = min/60;
	  min = min%60;
	  if (h >= 24) {
		day = h/24;
		h = h%24;
		if (day >= 365) {
		  year = day / 365;
		  day = day%365;
		}
	  }
	}
  }
  if (year > 0) ss<<year<<"y ";
  if (day > 0) ss<<day<<"d ";
  if (h > 0) ss<<h<<"h ";
  if (min > 0) ss<<min<<"m ";
  ss<<sec<<"s";
  return ss.str();
}


void Exporter::showHelp()
{
  showCommandHelp("help", "", "Show help");
  showCommandHelp("exit", "", "Exit without exporting");
  showCommandHelp("export", "", "Start exporting");
  showCommandHelp("status", "", "Status of exporting images");
  showCommandHelp("get", "[id]", "Show settings");
  showCommandHelp("set", "<id> <value>", "Set settings");
  showCommandHelp("status", "", "Show export status");
  showCommandHelp("export-pause", "", "Pause export");
  showCommandHelp("export-resume", "", "Resume export");
  showCommandHelp("export-stop", "", "Stop export");
}

void Exporter::showCommandHelp(string command, string args, string text)
{
  stringstream ss;
  ss<<"  "<<command<<"  ";
  ss<<args<<" ";
  string s = ss.str();
  while (s.length() < HELPSIZE) s += " ";
  cout<<s<<text<<endl;
}

void Exporter::clean()
{
  if (styleManager != NULL) delete styleManager;
  if (sceneManager != NULL) delete sceneManager;
  if (session != NULL) delete session;
  styleManager = NULL;
  sceneManager = NULL;
  session = NULL;
}


void Exporter::exportPrecalc()
{
  cfg->WriteConfig();
  clean();
  loadSession(path+sessionfile);
  loadScript(scriptPath+mainscript);
  styleManager = new StyleManager(path+stylefile);
  sceneManager = new SceneManager(path+scenefile, session->getDim());
  
  
  if (!session->isThereAScript()) {
	LOG<<ERROR<<"Could not load script"<<endl<<NORMAL;
	return;
  }
  cam = session->getCamera();
  if (cam == NULL) {
	LOG<<ERROR<<"Could not load camera"<<endl<<NORMAL;
	return;
  }
  
  LOG<<"Export running in the background"<<endl;
  LOG<<"Type status for more detailes"<<endl;
  width = readInt(cfg->getWindow()->getAttribute("width", ""));
  height = readInt(cfg->getWindow()->getAttribute("height", ""));
  threads = readInt(cfg->getApp()->getAttribute("threads", ""));
  stringstream ss;
  ss<<max(width,height);
  cfg->getApp()->setAttribute("density", ss.str());
  cfg->getApp()->setAttribute("numd", "1");
  session->update();
}


void Exporter::startExporting()
{
  pause_all = chrono::duration<double>::zero();
  leave = false;
  pause = false;
  start_whole = chrono::system_clock::now();
  for (int i = 0; i < exports.size(); ++i) {
	pause_image = chrono::duration<double>::zero();
	start_image = chrono::system_clock::now();
	export_count = i;
	string scene = exports[i].first;
	  string style = exports[i].second;
	string imagename = scene+"_"+style;
	try {
	  //cout<<i+1<<"/"<<exports.size()<<endl;
	  if (!sceneManager->loadRecord(scene, cam)) {
		LOG<<"Error while exporting: "<<images+imagename<<endl;
		LOG<<ERROR<<"Could not load scene:"<<scene<<endl<<NORMAL;
		continue;
	  }
	  if (!styleManager->isRecord(style)) {
		LOG<<"Error while exporting: "<<images+imagename<<endl;
		LOG<<ERROR<<"Could not load style: "<<style<<endl<<NORMAL;
		continue;
	  }
	  StyleSheet *ss = styleManager->getStyleSheet(style);
	  cam->setStyleSheet(ss);
	  session->getFunction()->resetVrammax();
	  Texture text(width, height);
	  state = 0;
	  int numd = cam->getNumD();
	  pair<double,double> limit = getLimit(text.getX(), text.getY());
	  cam->calc(numd-1, threads, &leave, &pause, &state, limit.first, limit.second, NULL);
	  cam->render(text, &leave, &pause, limit.first, limit.second);
	  export_texture(images+imagename, text);
	} catch (ERROR_CODE e) {
	  LOG<<"Error while exporting: "<<images+imagename<<endl;
	  LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
	}
  }
  if (!leave) {
	cout<<"\r";
	delete promt_thr;
	promt_thr = NULL;
	LOG<<" *** Completed ***"<<endl;
	string command = cfg->getApp()->getAttribute("callback", "");
	if (command != "") {
	  system(command.c_str());
	}
  }
  delete thr;
  thr = NULL;
}

void Exporter::showPromt()
{
  while (promt());
  delete promt_thr;
  promt_thr = NULL;
}


int Exporter::run()
{
  promt_thr = new thread(&Exporter::showPromt, this);
  promt_thr->detach();
  while (promt_thr != NULL) {
	sleep(1);
  }
  return 0;
}


