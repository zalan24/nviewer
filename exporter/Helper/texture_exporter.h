#include "texture.h"

#define PNG_SKIP_SETJMP_CHECK

#include <png.h>

void export_texture(string filename, Texture &texture);