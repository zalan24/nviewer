#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <thread>
#include <chrono>
#include <sstream>

#include "common/Helper/pythonmanager.h"
#include "common/Helper/stylemanager.h"
#include "common/Helper/scenemanager.h"
#include "common/Helper/configcheck.h"
#include "common/Math/include/session.h"
#include "exporter/Helper/texture_exporter.h"

using namespace std;

#define MAXDISPLAYRES 32768
#define PROMT "> "
#define HELPSIZE 30

typedef std::chrono::duration<int,std::milli> milliseconds_type;

struct OptionData
{
  string attribute;
  Element *element;
  string text;
  OptionData() : element(NULL), attribute(""), text("") {}
  OptionData(Element *element, string attribute, string text) : attribute(attribute), element(element), text(text) {}
};

class Exporter
{
  bool leave;
  bool pause;
  double state;
  int height;
  int width;
  int threads;
  Camera<RTYPE> *cam;
  
  
  string filename;
  string session_name;
  string mainscript;
  string scenefile;
  string stylefile;
  string sessionfile;
  string path;
  string scriptPath;
  string images;
  StyleManager *styleManager;
  SceneManager *sceneManager;
  MachineConfig *cfg;
  Session *session;
  map<string, OptionData> settings;
  vector<pair<string, string> > exports;
  thread *thr;
  thread *promt_thr;
  string callback;
  int export_count;
  chrono::time_point<chrono::system_clock> start_whole;
  chrono::time_point<chrono::system_clock> start_image;
  chrono::time_point<chrono::system_clock> pause_started;
  chrono::duration<double> pause_all;
  chrono::duration<double> pause_image;
  
  void loadCfg();
  void loadSession(string filename);
  void loadScript(string filename);
  void showHelp();
  void showCommandHelp(string command, string args, string text);
  void startExporting();
  void setupSettings();
  void clean();
  void showPromt();
  void exportPrecalc();
  void pauseExport();
  void stopExport();
  void resumeExport();
  string getTime(chrono::duration<double> t, double state);
public:
  Exporter(int argc, char *argv[]);
  ~Exporter();
  
  bool promt();
  int run();
};