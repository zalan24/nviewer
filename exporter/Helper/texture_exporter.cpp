#include "texture_exporter.h"

void export_texture(string filename, Texture &texture)
{
  int X = texture.getX();
  int Y = texture.getY();
  string suffix = ".png";
  if (filename.length() <= suffix.length() || filename.substr(filename.length()-suffix.length(),-1) != suffix) filename += suffix;
  FILE *fp = fopen(filename.c_str(), "wb");
  if (!fp) throw Exception(IO_ERROR1, "Unable to export file (cannot open file)");
  png_voidp user_error_ptr;
  png_error_ptr user_error_fn, user_warning_fn;
  png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, (png_voidp)user_error_ptr,user_error_fn, user_warning_fn);
  if (!png_ptr) throw Exception(IO_ERROR2, "Unable to export file (png_ptr)");
  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr) {
    png_destroy_write_struct(&png_ptr,(png_infopp)NULL);
    throw Exception(IO_ERROR3, "Unable to export file (info_tr)");
  }
  if (setjmp(png_jmpbuf(png_ptr))) {
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(fp);
    throw Exception(IO_ERROR4, "Unable to export file (error while writing)");
  }
  png_set_filter(png_ptr, 0, PNG_FILTER_NONE);
  png_init_io(png_ptr, fp);
  int colour_type = PNG_COLOR_TYPE_RGB;
  png_set_IHDR(png_ptr, info_ptr, X, Y, 8, colour_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

  //png_set_gAMA(png_ptr, info_ptr, PNG_INFO_gAMA);
  //png_set_sRGB(png_ptr, info_ptr, srgb_intent);
  int num = 1;
  png_text text[1];

  char *key = new char[string("Software").length()];
  strcpy(key, "Software");
  char *message = new char[string(NAME_SIGNATURE).length()];
  strcpy(message, NAME_SIGNATURE);
  text[0].compression = PNG_TEXT_COMPRESSION_NONE;
  text[0].key = key;
  text[0].text = message;
  text[0].text_length = string(NAME_SIGNATURE).length();

  /*char *key2 = new char[string("E-mail").length()];
  strcpy(key2, "E-mail");
  char *message2 = new char[string("vegvarizalan@gmail.com").length()];
  strcpy(message2, "vegvarizalan@gmail.com");
  text[1].compression = PNG_TEXT_COMPRESSION_NONE;
  text[1].key = key2;
  text[1].text = message2;
  text[1].text_length = string("vegvarizalan@gmail.com").length();*/

  png_set_text(png_ptr, info_ptr, text, num);

  png_byte *row_pointers[Y];
  int pixel_size = (colour_type == PNG_COLOR_TYPE_RGB_ALPHA) ? 4 : 3;
  for (int j = 0; j < Y; ++j) {
    row_pointers[Y-j-1] = new png_byte[X*pixel_size];
    for (int i = 0; i < X; ++i) {
      row_pointers[Y-j-1][i*pixel_size] = texture.get(i,j).r*255;
      row_pointers[Y-j-1][i*pixel_size+1] = texture.get(i,j).g*255;
      row_pointers[Y-j-1][i*pixel_size+2] = texture.get(i,j).b*255;
      if (pixel_size == 4) row_pointers[Y-j-1][i*pixel_size+3] = texture.get(i,j).a*255;
    }
  }

  png_set_rows(png_ptr, info_ptr, row_pointers);
  png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);


  /*png_write_info(png_ptr, info_ptr);
  png_write_image(png_ptr, row_pointers);
  png_write_end(png_ptr, info_ptr);*/


  //png_write_image(png_ptr, row_pointers);

  png_destroy_write_struct(&png_ptr, &info_ptr);

  delete [] key;
  delete [] message;
  for (int j = 0; j < Y; ++j) delete [] row_pointers[j];
  fclose(fp);
}