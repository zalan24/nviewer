#include <iostream>
#include "exporter/Helper/exporter.h"
#include "common/Math/include/info.h"

using namespace std;

int main(int argc, char *argv[])
{ 
  if (argc < 4) {
	cout<<"Usage: "<<endl;
	cout<<argv[0]<<" <file> <sessionname> <path_to_images>"<<endl;
	return 0;
  }
  Log log("log.txt", &std::cout);
  LOG<<"Loading pyhton"<<endl;
#if DEBUG_ENABLED
  ofstream debugout("debug.txt");
  if (debugout.is_open()) {
	log.openDebug(&debugout);
	LOG<<"-- Debug mode"<<endl;
  } else LOG<<WARNING<<"Cannot open log debug file"<<endl<<NORMAL;
#endif
  PythonManager pythonManager(argv[0]);
  
  try {
	Exporter e(argc, argv);
	return e.run();
  } catch (ERROR_CODE e) {
	LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
  }
  
  return 0;
}
