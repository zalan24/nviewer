#include "cfgsetter.h"

CfgSetter::CfgSetter(Element *e, QGridLayout *layout, int row, QString label, QString disc, std::string key, int min, int max, int mult) :
    e(e),
    key(key),
    mult(mult)
{
    QLabel *lab = new QLabel(label);
    QLabel *d = new QLabel(disc);
    d->setWordWrap(true);
    box = new QSpinBox();
    int val = readInt(e->getAttribute(key,"0")) / mult;
    box->setMinimum(min);
    box->setMaximum(max);
    box->setValue(val);
    layout->addWidget(lab,row,0);
    layout->addWidget(box,row,1);
    layout->addWidget(d,row,2);
    lab->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    d->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

}

void CfgSetter::save()
{
    std::stringstream ss;
    ss<<((ulli)box->value()*(ulli)mult);
    e->setAttribute(key,ss.str());
}

QSpinBox* CfgSetter::getBox() const {return box;}

