#ifndef CALCTHREAD_H
#define CALCTHREAD_H

#include <QThread>
#include "common/Math/include/render.h"
#include "common/Helper/texture.h"
#include "common/Helper/inotifiable.h"

class CalcThread : public QThread, INotifiable
{
    Q_OBJECT
public:
    CalcThread();
    ~CalcThread();

    void setUp(int w, int h, Camera<RTYPE> *cam, int threads);
    void stopCalc();
    Texture *getTexture() const;
    void next();
    void previous();
    void pauseCalc();
    void resumeCalc();
    ERROR_CODE popError() {ERROR_CODE e = error; error = NONE; return e;}

signals:
    void draw();
    void onFail(QString s);
    void calcUpdate(int);

private:
    Texture *text;
    Camera<RTYPE> *cam;
    int threads;
    bool leave;
    bool pause;
    int widht;
    int height;
    int lastP;
    int moveTo;
    int index;
    ERROR_CODE error;

    void run();
    void notify(double state);
};

#endif // CALCTHREAD_H
