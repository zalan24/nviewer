#ifndef CFGSETTER_H
#define CFGSETTER_H

#include <iostream>
#include <sstream>
#include <QGridLayout>
#include <QLabel>
#include <QSpinBox>

#include "common/Math/include/config.h"

class CfgSetter
{
public:
    CfgSetter(Element *e, QGridLayout *layout, int row, QString label, QString disc, std::string key, int min, int max, int mult = 1);

    void save();
    QSpinBox *getBox() const;

private:
    Element *e;
    std::string key;
    int mult;
    QSpinBox *box;
};

#endif // CFGSETTER_H
