#include "logger.h"

#include <iostream>
#include <QScrollBar>

Logger::Logger(QTextBrowser *l, int maxlines) : console(l), maxlen(maxlines), begin(0), buffer("")
{
}

void Logger::log(QString line)
{
    bool bottom = console->verticalScrollBar()->value() >= console->verticalScrollBar()->maximum() - 10;
    if (lines.size() < maxlen) {
        lines.append(line);
        //QString s = console->toHtml();
        //std::cout<<s.toStdString()<<std::endl;
        console->append(line);
    } else {
        lines[begin] = line;
        begin = (begin+1)%maxlen;
        console->setText("");
        for (int i = 0; i < maxlen; ++i) console->append(lines[(i+begin)%maxlen]);
    }
    if (bottom) console->verticalScrollBar()->setValue(console->verticalScrollBar()->maximum());
}

void Logger::logWarning(QString line)
{
    line = "<span style=\" font-weight: bold; color:#ff9000;\">Warning: "+line+"</span>";
    log(line);
}

void Logger::logError(QString line)
{
    line = "<span style=\" font-weight: bold; color:#ff4020;\">Error: "+line+"</span>";
    log(line);
}

void Logger::loadText(string s)
{
    buffer += s;
}

void Logger::logLoaded(LogModifier m)
{
    if (buffer != "") {
        QString s = QString::fromStdString(buffer);
        switch (m) {
        case NORMAL:
            log(s);
            break;
        case WARNING:
            logWarning(s);
            break;
        case ERROR:
            logError(s);
            break;
        }
        buffer = "";
    }
}


Log& operator<<(Log &log, QString s)
{
    log<<s.toStdString();
    return log;
}
