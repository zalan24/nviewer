#include "calcthread.h"

CalcThread::CalcThread() : text(NULL), lastP(0), moveTo(0), index(0), error(NONE)
{
    leave = false;
    pause = false;
}

CalcThread::~CalcThread()
{
    if (text != NULL) delete text;
}

Texture* CalcThread::getTexture() const
{
    return text;
}

void CalcThread::pauseCalc()
{
    pause = true;
}

void CalcThread::resumeCalc()
{
    pause = false;
}

void CalcThread::setUp(int w, int h, Camera<RTYPE> *c, int th)
{
    if (text != NULL) delete text;
    text = new Texture(w,h);
    cam = c;
    threads = th;
}

void CalcThread::stopCalc()
{
    leave = true;
    moveTo = 0;
    pause = false;
}

void CalcThread::notify(double state)
{
    int p = state*100;
    if (lastP != p) {
        lastP = p;
        calcUpdate(p);
    }
}

void CalcThread::next()
{
    int numd = cam->getNumD();
    if (index+1 >= numd) return;
    moveTo = 1;
    leave = true;
}

void CalcThread::previous()
{
    if (index <= 1) return;
    moveTo = -1;
    leave = true;
}

void CalcThread::run()
{
    double state = 0;
    try {
        leave = false;
        pause = false;
        int numd = cam->getNumD();
        pair<double,double> limit = getLimit(text->getX(), text->getY());
        for (index = 1; index < numd; ++index) {
            calcUpdate(0);
            lastP = 0;
            //cout<<index<<"..."<<endl;
            cam->calc(index,threads,&leave,&pause,&state,limit.first,limit.second,this);
            cam->render(*text,&leave,&pause,limit.first,limit.second);
            if (moveTo != 0) {
                if (index+moveTo >= 1 && index+moveTo < numd) index += moveTo;
                moveTo = 0;
                leave = false;
                index--;
                continue;
            }
            if (leave) break;
            draw();
            calcUpdate(100);
        }
    } catch (ERROR_CODE e) {
        onFail(QString::fromStdString(Error::ERROR_STRING));
    }
}
