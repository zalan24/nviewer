#include "manager.h"

#include <fstream>

Manager::Manager(Logger *logger, DisplayWidget *display, QMenuData menudata, VRamWidget *scriptVRam, VRamWidget *runtimeVRam, CameraController *camc, QMainWindow *window) :
    logger(logger),
    display(display),
    log("log.txt", &std::cout),
    cfg(NULL),
    loaded(false),
    dialog(NULL),
    window(window),
    settings(NULL),
    nsession(NULL),
    session(NULL),
    filedialog(NULL),
    File_Open_Recent(menudata.File_Open_Recent),
    Scene_Open_Recent(menudata.Scene_Open_Recent),
    Style_Open_Recent(menudata.Style_Open_Recent),
    isDisplaying(true),
    runtimeVRam(runtimeVRam),
    scriptVRam(scriptVRam),
    vectorDialog(NULL),
    camdialog(NULL),
    dial(NULL),
    camController(camc),
    movestate(0),
    axis1(-1),
    axis2(-1),
    sceneManager(NULL),
    closeLoadSceneDialog(false),
    styleManager(NULL)
{
    connect(&calcThread,SIGNAL(draw()), this, SLOT(drawTexture()));
    connect(&calcThread,SIGNAL(onFail(QString)), this, SLOT(onDisplayFail(QString)));
    connect(&calcThread,SIGNAL(calcUpdate(int)), this, SLOT(calcUpdate(int)));
    connect(camController,SIGNAL(updateCamera(RTYPE,RTYPE)),this,SLOT(receiveCamData(RTYPE, RTYPE)));
    connect(camController,SIGNAL(updateMatrix(Matrix<RTYPE>)),this,SLOT(receiveTransform(Matrix<RTYPE>)));
    connect(&movetimer,SIGNAL(timeout()),this,SLOT(updateMoveTimer()));
    log.setLogger(logger);
    debugout.open("debug.txt");
    if (debugout.is_open()) {
        log.openDebug(&debugout);
    } else LOG<<WARNING<<"Cannot open log debug file"<<endl<<NORMAL;
    LOG<<"### Starting manager ###"<<endl;
    load();
}

void Manager::translate()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    if (dial != NULL) delete dial;
    dial = NULL;
    std::vector<RTYPE> vec(session->getFunction()->getDim());
    for (unsigned int i = 0; i < vec.size(); ++i) vec[i] = 0;
    VectorLocalDialog *vd;
    dial = vd = new VectorLocalDialog(vec, true);
    vd->setModal(true);
    vd->setWindowTitle("Translate");
    vd->show();
    connect(vd, SIGNAL(sendData(std::vector<RTYPE>, bool)), this, SLOT(receiveCameraTranslate(std::vector<RTYPE>, bool)));
}

void Manager::receiveCameraTranslate(VectorN<RTYPE> pos, bool local, bool aroundFocus, bool keepDir)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    Camera<RTYPE> *cam = session->getCamera();

    try {
        stopCalc();


        if (local) {
            Matrix<RTYPE> m = cam->getTransform();

            Matrix<RTYPE> m2 = Matrix<RTYPE>::Identity(m.getRows());
            for (int i = 0; i < m2.getColoumns(); ++i) {
                for (int j = 0; j < m2.getRows(); ++j) {
                    m2.set(j,i,m.get(j,i));
                }
            }
            pos = m2*pos;
        }
        double focus = cam->getFocus();
        VectorN<RTYPE> fpoint = cam->getDirection() * focus + cam->getPosition();
        if (aroundFocus) {
            cam->translate(pos);
            focus -= cam->getDirection() * pos;
            double mn = std::numeric_limits<RTYPE>::min();
            if (focus < mn) focus = mn;
            cam->setFocus(focus);
        } else {
            cam->translate(pos);
        }
        if (keepDir && cam->getPosition() != fpoint) {
            cam->setDirection(fpoint - cam->getPosition());
        }

        setUpCalcThread();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        session->clearScript();
    }
}

void Manager::receiveCameraTranslate(std::vector<RTYPE> vec, bool local)
{
    VectorN<RTYPE> pos(vec.size());
    for (unsigned int i = 0; i < vec.size(); ++i) pos[i] = vec[i];
    receiveCameraTranslate(pos,local,false,false);
}

void Manager::rotate()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    if (dial != NULL) delete dial;
    dial = NULL;
    int dim = session->getFunction()->getDim();
    RotationDialog *rd;
    dial = rd = new RotationDialog(dim, camController->isAroundFocus());
    rd->setModal(true);
    rd->setWindowTitle("Rotate");
    rd->show();
    connect(rd, SIGNAL(sendData(double,int,int,bool,bool)), this, SLOT(receiveCameraRotation(double,int,int,bool,bool)));
}

void Manager::receiveCameraRotation(double angle, int x, int y, bool local, bool focus)
{
    if (angle == 0) return;
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    Camera<RTYPE> *cam = session->getCamera();

    if (x == y) {
        LOG<<ERROR<<"Cannot rotate around 1 axis. Please select two."<<endl<<NORMAL;
        return;
    }


    try {
        stopCalc();
        if (focus) {
            cam->translate(cam->getDirection() * cam->getFocus());
            cam->rotate(angle,x,y,local);
            cam->translate(-cam->getDirection() * cam->getFocus());
        } else cam->rotate(angle,x,y,local);
        setUpCalcThread();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        session->clearScript();
    }
}


void Manager::setCamPosition()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    if (vectorDialog != NULL) delete vectorDialog;
    vectorDialog = NULL;
    std::vector<RTYPE> vec(session->getFunction()->getDim());
    Camera<RTYPE> *cam = session->getCamera();
    Matrix<RTYPE> m = cam->getTransform();
    for (int i = 0; i < m.getRows()-1; ++i) vec[i] = m.get(i, m.getColoumns()-1);
    vectorDialog = new VectorDialog(vec);
    vectorDialog->setModal(true);
    vectorDialog->setWindowTitle("Position");
    vectorDialog->show();
    connect(vectorDialog, SIGNAL(sendVector(std::vector<RTYPE>)), this, SLOT(receiveCameraPos(std::vector<RTYPE>)));
}

void Manager::receiveCameraPos(std::vector<RTYPE> vec)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    Camera<RTYPE> *cam = session->getCamera();
    VectorN<RTYPE> pos(vec.size());
    for (unsigned int i = 0; i < vec.size(); ++i) pos[i] = vec[i];

    try {
        stopCalc();
        cam->setPositionV(pos);
        setUpCalcThread();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        session->clearScript();
    }
}

void Manager::setCamDirection()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    if (vectorDialog != NULL) delete vectorDialog;
    vectorDialog = NULL;
    std::vector<RTYPE> vec(session->getFunction()->getDim());
    Camera<RTYPE> *cam = session->getCamera();

    Matrix<RTYPE> m = cam->getTransform();
    for (int i = 0; i < m.getRows(); ++i) m.set(i,m.getColoumns()-1,0);
    VectorN<RTYPE> trans = m*VectorN<RTYPE>::identity(session->getFunction()->getDim(), 2);
    for (unsigned int i = 0; i < vec.size(); ++i) vec[i] = trans[i];

    vectorDialog = new VectorDialog(vec);
    vectorDialog->setModal(true);
    vectorDialog->setWindowTitle("Direction");
    vectorDialog->show();
    connect(vectorDialog, SIGNAL(sendVector(std::vector<RTYPE>)), this, SLOT(receiveCameraDir(std::vector<RTYPE>)));
}

void Manager::receiveCameraDir(std::vector<RTYPE> vec)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    Camera<RTYPE> *cam = session->getCamera();
    VectorN<RTYPE> dir(vec.size());
    for (unsigned int i = 0; i < vec.size(); ++i) dir[i] = vec[i];
    if (dir.sqrMagnitude() < 0.00001) {
        LOG<<ERROR<<"The magnitude of the direction cannot be 0 (or nearly 0)"<<endl<<NORMAL;
        return;
    }

    try {
        stopCalc();
        //cam->setPositionV(pos);

        cam->setDirection(dir);

        setUpCalcThread();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        session->clearScript();
    }
}

void Manager::setCamScale()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    if (vectorDialog != NULL) delete vectorDialog;
    vectorDialog = NULL;
    std::vector<RTYPE> vec(session->getFunction()->getDim());
    for (unsigned int i = 0; i < vec.size(); ++i) vec[i] = 1;

    vectorDialog = new VectorDialog(vec);
    vectorDialog->setModal(true);
    vectorDialog->setWindowTitle("Scale");
    vectorDialog->show();
    connect(vectorDialog, SIGNAL(sendVector(std::vector<RTYPE>)), this, SLOT(receiveCameraScale(std::vector<RTYPE>)));
}

void Manager::receiveCameraScale(std::vector<RTYPE> vec)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    Camera<RTYPE> *cam = session->getCamera();
    VectorN<RTYPE> scale(vec.size()+1);
    for (unsigned int i = 0; i < vec.size(); ++i) scale[i] = vec[i];
    scale[vec.size()] = 1;

    try {
        stopCalc();

        Matrix<RTYPE> m = cam->getTransform();

        Matrix<RTYPE> m2 = Matrix<RTYPE>::Identity(m.getRows());
        for (int i = 0; i < m2.getRows(); ++i) m2.set(i,i,scale[i]);
        m2 = m*m2;
        for (int i = 0; i < m2.getColoumns(); ++i) {
            for (int j = 0; j < m2.getRows(); ++j) {
                m.set(j,i,m2.get(j,i));
            }
        }


        cam->setTransform(m);


        setUpCalcThread();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        session->clearScript();
    }
}

void Manager::receiveTransform(Matrix<RTYPE> m)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    Camera<RTYPE> *cam = session->getCamera();

    try {
        stopCalc();
        cam->setTransform(m);
        setUpCalcThread();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        session->clearScript();
    }
}

void Manager::receiveCamData(RTYPE scale, RTYPE focus)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    Camera<RTYPE> *cam = session->getCamera();

    try {
        stopCalc();
        cam->setFocus(focus);
        cam->setScale(scale);
        setUpCalcThread();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        session->clearScript();
    }
}

void Manager::setTransform()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    if (dial != NULL) delete dial;
    dial = NULL;
    Camera<RTYPE> *cam = session->getCamera();

    TransformDialog *dialt;
    dial = dialt = new TransformDialog(cam->getTransform());
    dialt->setModal(true);
    dialt->setWindowTitle("Transform");
    dialt->show();
    connect(dialt, SIGNAL(sendData(Matrix<RTYPE>)), this, SLOT(receiveTransform(Matrix<RTYPE>)));
}

void Manager::setCamView()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    if (camdialog != NULL) delete camdialog;
    camdialog = NULL;
    camdialog = new CameraViewDialog(session->getCamera());
    camdialog->setWindowTitle("View");
    camdialog->setModal(true);
    camdialog->show();
    connect(camdialog, SIGNAL(sendData(RTYPE,RTYPE,RTYPE)), this, SLOT(receiveViewData(RTYPE,RTYPE,RTYPE)));
}

void Manager::receiveViewData(RTYPE s, RTYPE f, RTYPE a)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    Camera<RTYPE> *cam = session->getCamera();

    try {
        stopCalc();
        cam->setScale(s);
        cam->setFocus(f);
        cam->setView(a);
        setUpCalcThread();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        session->clearScript();
    }
}

void Manager::addRecentSession(QString s)
{
    int index = -1;
    for (int i = 0; i < recentsessions.size(); ++i) if (recentsessions[i] == s) {
        index = i;
        break;
    }
    if (index != -1) {
        for (int i = index; i+1 < recentsessions.size(); ++i) recentsessions[i] = recentsessions[i+1];
        recentsessions[recentsessions.size()-1] = s;
    }
    else if (recentsessions.size() < MAX_RECENT_SESSIONS) recentsessions.push_back(s);
    else {
        for (int i = 0; i+1 < recentsessions.size(); ++i) recentsessions[i] = recentsessions[i+1];
        recentsessions[recentsessions.size()-1] = s;
    }
}

void Manager::addRecentCollection(QString s)
{
    int index = -1;
    for (int i = 0; i < recentcollection.size(); ++i) if (recentcollection[i] == s) {
        index = i;
        break;
    }
    if (index != -1) {
        for (int i = index; i+1 < recentcollection.size(); ++i) recentcollection[i] = recentcollection[i+1];
        recentcollection[recentcollection.size()-1] = s;
    }
    else if (recentcollection.size() < MAX_RECENT_SESSIONS) recentcollection.push_back(s);
    else {
        for (int i = 0; i+1 < recentcollection.size(); ++i) recentcollection[i] = recentcollection[i+1];
        recentcollection[recentcollection.size()-1] = s;
    }
}


void Manager::pauseDisplay()
{
    if (calcThread.isRunning()) calcThread.pauseCalc();
}

void Manager::resumeDisplay()
{
    if (calcThread.isRunning()) calcThread.resumeCalc();
}

void Manager::nextStep()
{
    if (calcThread.isRunning()) calcThread.next();
}

void Manager::prevStep()
{
    if (calcThread.isRunning()) calcThread.previous();
}

void Manager::updateRecentCollectionMenu() const
{
    //stringstream ss;
    //ss<<recentcollection.size();
    SessionFileData *data = session->getData();
    //session->getData()->getFunc()->setAttribute("numrecentcollections",ss.str());
    data->recentcollections.resize(recentcollection.size());
    Scene_Open_Recent->clear();
    QSignalMapper* signalMapper = new QSignalMapper (window);
    if (recentcollection.size() == 0) Scene_Open_Recent->addAction("-");
    for (int i = recentcollection.size()-1; i >= 0; --i) {
        //stringstream ss2;
        //ss2<<i;
        //session->getConfig()->getFunc()->setAttribute("recentcollections_"+ss2.str(),recentcollection[i].toStdString());
        data->recentcollections[i] = recentcollection[i].toStdString();

        QAction *act = Scene_Open_Recent->addAction(recentcollection[i]);
        connect (act, SIGNAL(triggered()), signalMapper, SLOT(map()));
        signalMapper->setMapping (act, i);
    }
    connect (signalMapper, SIGNAL(mapped(int)), this, SLOT(receiveCollectionFile(int)));
}

void Manager::updateRecentStyleMenu() const
{
    //stringstream ss;
    //ss<<recentstyles.size();
    SessionFileData *data = session->getData();
    //session->getConfig()->getFunc()->setAttribute("numrecentstyles",ss.str());
    data->recentstyles.resize(recentstyles.size());
    Style_Open_Recent->clear();
    QSignalMapper* signalMapper = new QSignalMapper (window);
    if (recentstyles.size() == 0) Style_Open_Recent->addAction("-");
    for (int i = recentstyles.size()-1; i >= 0; --i) {
        //stringstream ss2;
        //ss2<<i;
        //session->getConfig()->getFunc()->setAttribute("recentstyles_"+ss2.str(),recentstyles[i].toStdString());
        data->recentstyles[i] = recentstyles[i].toStdString();

        QAction *act = Style_Open_Recent->addAction(recentstyles[i]);
        connect (act, SIGNAL(triggered()), signalMapper, SLOT(map()));
        signalMapper->setMapping (act, i);
    }
    connect (signalMapper, SIGNAL(mapped(int)), this, SLOT(receiveStyleFile(int)));
}

void Manager::receiveStyleFile(int i)
{
    openStyleFileByName(recentstyles[i]);
}

void Manager::addRecentStyle(QString s)
{
    int index = -1;
    for (int i = 0; i < recentstyles.size(); ++i) if (recentstyles[i] == s) {
        index = i;
        break;
    }
    if (index != -1) {
        for (int i = index; i+1 < recentstyles.size(); ++i) recentstyles[i] = recentstyles[i+1];
        recentstyles[recentstyles.size()-1] = s;
    }
    else if (recentstyles.size() < MAX_RECENT_SESSIONS) recentstyles.push_back(s);
    else {
        for (int i = 0; i+1 < recentstyles.size(); ++i) recentstyles[i] = recentstyles[i+1];
        recentstyles[recentstyles.size()-1] = s;
    }
}

void Manager::loadRecentStyles()
{
    SessionFileData *data = session->getData();
    int size = data->recentstyles.size();
    Style_Open_Recent->clear();
    Style_Open_Recent->addAction("-");
    recentstyles.clear();
    for (int i = 0; i < size; ++i) {
        stringstream ss2;
        ss2<<i;
        //string s = session->getConfig()->getFunc()->getAttribute("recentstyles_"+ss2.str(),"");
        string s = data->recentstyles[i];
        recentstyles.push_back(QString::fromStdString(s));
    }
}

void Manager::newStyleFile()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    QString filename = QFileDialog::getSaveFileName(0,"Create new file", "", "*.style");
    filename = QString::fromStdString(addPrefix(filename.toStdString(),".style"));
    try {
        if (styleManager != NULL) delete styleManager;
        styleManager = NULL;
        styleManager = new StyleManager(filename.toStdString());

        if (!styleManager->save()) {
            LOG<<ERROR<<"Could not save file"<<endl<<NORMAL;
            delete styleManager;
            styleManager = NULL;
        } else {
            //session->getConfig()->getFunc()->setAttribute("laststyle",filename.toStdString());
            session->getData()->laststyle = filename.toStdString();
            addRecentStyle(filename);
            updateRecentStyleMenu();
            styleManager->save();
            LOG<<"File created: "<<filename<<endl;
        }
    } catch(ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        if (styleManager != NULL) delete styleManager;
        styleManager = NULL;
    }
    updateMenu();
}

void Manager::saveStyleFile()
{
    if (styleManager != NULL) {
        if (!styleManager->save()) {
            LOG<<ERROR<<"Could not save style"<<endl<<NORMAL;
        } else LOG<<"File saved: "<<styleManager->getFileName()<<endl;
    }
}

void Manager::saveStyleFileAs()
{
    if (styleManager != NULL) {
        string of = styleManager->getFileName();
        QString filename = QFileDialog::getSaveFileName(0,"Save as", "", "*.style");
        filename = QString::fromStdString(addPrefix(filename.toStdString(),".style"));
        styleManager->setFileName(filename.toStdString());
        if (!styleManager->save()) {
            LOG<<ERROR<<"Could not save file"<<endl<<NORMAL;
        } else {
            //session->getConfig()->getFunc()->setAttribute("laststyle",filename.toStdString());
            session->getData()->laststyle = filename.toStdString();
            addRecentStyle(filename);
            updateRecentStyleMenu();
            styleManager->save();
            LOG<<"File created: "<<filename<<endl;
        }
    }
}

void Manager::saveStyle()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (sceneManager == NULL) return;
    if (dial != NULL) delete dial;
    dial = NULL;
    TextDialog *td = new TextDialog();
    dial = td;
    dial->setWindowTitle("Save style");
    dial->setModal(true);
    dial->show();
    connect(td,SIGNAL(acceptText(QString)),this,SLOT(saveStyle(QString)));

}

void Manager::saveStyle(QString name)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (sceneManager == NULL) return;
    if (session->getCamera() == NULL) return;
    bool ok = true;
    dial->close();
    if (styleManager->isRecord(name.toStdString())) {
        ok = false;

        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(window, "Override style", "The entered name already belongs to a style. Do you want to override it?", QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes) {
            ok = true;
        } else {
            saveStyle();
        }

    }
    if (ok) {
        Camera<RTYPE> *cam = session->getCamera();
        StyleSheet *ss = new StyleSheet(name.toStdString());
        cam->getCurrentStyle(ss);
        styleManager->removeStyleSheet(name.toStdString());
        styleManager->addStyleSheet(ss);
        LOG<<"Style saved: "<<name.toStdString()<<endl;
        if (!styleManager->save()) {
            LOG<<WARNING<<"Could not autosave the styles. Try to save manully or use \"Save as\"."<<NORMAL<<endl;
        }
    }
}

void Manager::openStyleFile()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    QString filename = QFileDialog::getOpenFileName(0,"Save as", "", "*.style");
    openStyleFileByName(filename);
}

void Manager::openStyleFileByName(QString filename)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    try {
        if (styleManager != NULL) delete styleManager;
        styleManager = NULL;
        styleManager = new StyleManager(filename.toStdString());
        //session->getConfig()->getFunc()->setAttribute("laststyle",filename.toStdString());
        session->getData()->laststyle = filename.toStdString();
        addRecentStyle(filename);
        updateRecentStyleMenu();
        LOG<<"File loaded: "<<filename<<endl;
    } catch(ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        if (styleManager != NULL) delete styleManager;
        styleManager = NULL;
    }
    updateMenu();
}

void Manager::updateRecentSessionMenu() const
{
    stringstream ss;
    ss<<recentsessions.size();
    cfg->getApp()->setAttribute("numrecentsessions",ss.str());
    File_Open_Recent->clear();
    QSignalMapper* signalMapper = new QSignalMapper (window);
    if (recentsessions.size() == 0) File_Open_Recent->addAction("-");
    for (int i = recentsessions.size()-1; i >= 0; --i) {
        stringstream ss2;
        ss2<<i;
        cfg->getApp()->setAttribute("recentsessions_"+ss2.str(),recentsessions[i].toStdString());

        QAction *act = File_Open_Recent->addAction(recentsessions[i]);
        connect (act, SIGNAL(triggered()), signalMapper, SLOT(map()));
        signalMapper->setMapping (act, i);
    }
    connect (signalMapper, SIGNAL(mapped(int)), this, SLOT(receiveSessionFile(int)));
}

bool Manager::isLoaded() const
{
    return loaded;
}

void Manager::load()
{
    runtimeVRam->setVRam(NULL);
    scriptVRam->setVRam(NULL);
    loaded = false;
    try {
        stopCalc();
        loadMCfg();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        if (cfg != NULL) {
            delete cfg;
            cfg = NULL;
        }
        ReExError(QString::fromStdString("Could not load system configuration: "+Error::ERROR_STRING));
    }
    loadFromCfg();
    updateMenu();
}

void Manager::loadFromCfg()
{
    try {
        stopCalc();
        setUpCfg();
        loaded = true;
        if (dialog != NULL) dialog->close();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        if (cfg != NULL) {
            delete cfg;
            cfg = NULL;
        }
        ReExError(QString::fromStdString("Could not load system configuration: "+Error::ERROR_STRING));
    }
    string lastsession = cfg->getApp()->getAttribute("lastsession","");
    if (lastsession != "") receiveSessionFile(QString::fromStdString(lastsession));
    updateMenu();
}

void Manager::setUpCfg()
{
    loadScreen();
    checkConfig();
}

void Manager::loadMCfg()
{
    if (cfg != NULL) {
        delete cfg;
        cfg = NULL;
    }
    cfg = new MachineConfig("nviewer.conf");

    int size = readInt(cfg->getApp()->getAttribute("numrecentsessions","0"));
    File_Open_Recent->clear();
    File_Open_Recent->addAction("-");
    recentsessions.clear();
    for (int i = 0; i < size; ++i) {
        stringstream ss2;
        ss2<<i;
        string s = cfg->getApp()->getAttribute("recentsessions_"+ss2.str(),"");
        recentsessions.push_back(QString::fromStdString(s));
    }
    isDisplaying = cfg->getApp()->getAttribute("display","on") == "on";
    updateMenu();
}

void Manager::checkConfig()
{
    checkMachineConfig(cfg, MAXDISPLAYSIZE);
    keycodes.clear();
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key1", QString::number(DEFAULT_FORWARD).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key2", QString::number(DEFAULT_BACKWARD).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key3", QString::number(DEFAULT_LEFT).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key4", QString::number(DEFAULT_RIGHT).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key5", QString::number(DEFAULT_AXIS1).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key6", QString::number(DEFAULT_AXIS2).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key7", QString::number(DEFAULT_AXIS3).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key8", QString::number(DEFAULT_AXIS4).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key9", QString::number(DEFAULT_AXIS5).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key10", QString::number(DEFAULT_AXIS6).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key11", QString::number(DEFAULT_AXIS7).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key12", QString::number(DEFAULT_AXIS8).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key13", QString::number(DEFAULT_AXIS9).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key14", QString::number(DEFAULT_AXIS10).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key15", QString::number(DEFAULT_AXIS11).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key16", QString::number(DEFAULT_AXIS12).toStdString())));
    keycodes.push_back(readInt(cfg->getKeyBoard()->getAttribute("key17", QString::number(DEFAULT_AXIS13).toStdString())));
}

void Manager::loadScreen()
{
    try {
        int screenX = readInt(cfg->getWindow()->getAttribute("width", DEFAULT_WIDTH));
        int screenY = readInt(cfg->getWindow()->getAttribute("height", DEFAULT_HEIGHT));
        if (screenX < 2 || screenX > MAXDISPLAYSIZE) throw Exception(INVALID_DATA, "Screen width must be between 2 and 4096");
        if (screenY < 2 || screenY > MAXDISPLAYSIZE) throw Exception(INVALID_DATA, "Screen height must be between 2 and 4096");
        resize(screenX,screenY);
    } catch (ERROR_CODE e) {
        if (e == INVALID_INT) Error::ERROR_STRING = "Invalid screen resolution";
        throw e;
    }
}

void Manager::reLoad()
{
    load();
}

void Manager::quit()
{
    stopCalc();
    delete dialog;
    dialog = NULL;
    if (!isLoaded()) window->close();
}

void Manager::ReExError(const QString error)
{
    if (dialog == NULL) {
        dialog = new ReExDialog(error);
        dialog->setWindowTitle("Error");
        dialog->setModal(true);
        dialog->show();
        connect(dialog,SIGNAL(onquit()),this,SLOT(quit()));
        connect(dialog,SIGNAL(onretry()),this,SLOT(reLoad()));
    } else {
        dialog->setText(error);
        dialog->buzz();
    }
}

Manager::~Manager()
{
    stopCalc();
    saveCurrentScene();
    if (sceneManager != NULL) delete sceneManager;
    if (styleManager != NULL) delete styleManager;
    if (cfg != NULL) delete cfg;
    if (dialog != NULL) delete dialog;
    if (settings != NULL) delete settings;
    if (nsession != NULL) delete nsession;
    if (session != NULL) delete session;
    if (vectorDialog != NULL) delete vectorDialog;
    if (dial != NULL) delete dial;
    if (camdialog != NULL) delete camdialog;
    if (filedialog != NULL) {
        if (filedialog->isActiveWindow()) filedialog->close();
        delete filedialog;
    }
}

void Manager::resize(int w, int h)
{
    stopCalc();
    display->resize(w,h);
}

void Manager::openSettings()
{
    if (settings != NULL) delete settings;
    settings = new Settings(cfg);
    settings->setModal(true);
    settings->setWindowTitle("Settings");
    settings->show();
    connect(settings, SIGNAL(save()), this, SLOT(loadFromCfg()));
}

void Manager::newSession()
{
    if (nsession != NULL) delete nsession;
    nsession = new NewSession();
    //nsession->setModal(true);
    nsession->setWindowTitle("New session");
    nsession->show();
    connect(nsession, SIGNAL(open(QString,int,QString)),this,SLOT(openNewSession(QString,int,QString)));
    updateMenu();
}

bool Manager::isSessionNameOk(QString name)
{
    if (name.size() < 3) return false;
    /*
     * Check existence
     */
    return match(name.toStdString(),FORMAT_SESSIONAME);
}

bool Manager::isDimOk(int d)
{
    if (d >= 3) return true;
    return false;
}

bool Manager::isSessionFileOk(QString file)
{
    std::ifstream in(file.toStdString().c_str());
    if (in.is_open()) {
        in.close();
        return true;
    }
    return false;
}

void Manager::openNewSession(QString name, int dim, QString file)
{
    if (!isSessionNameOk(name)) {
        nsession->setError("Invalid or existing name");
        return;
    }
    if (!isDimOk(dim)) {
        nsession->setError("Invalid dimension number");
        return;
    }
    if (file != "" && !isSessionFileOk(file)) {
        nsession->setError("Cannot open selected file");
        return;
    }
    nsession->close();
    delete nsession;
    nsession = NULL;

    if (cfg == NULL) {
        LOG<<ERROR<<"No configuration file opened. Please restart the program."<<endl<<NORMAL;
        return;
    }

    runtimeVRam->setVRam(NULL);
    scriptVRam->setVRam(NULL);

    try {
        stopCalc();
        Session *s = session;
        if (styleManager != NULL) delete styleManager;
        styleManager = NULL;
        saveCurrentScene();
        if (sceneManager != NULL) delete sceneManager;
        sceneManager = NULL;
        session = NULL;
        if (s != NULL) delete s;
        session = new Session(name.toStdString().c_str(),dim,cfg);
        cfg->getApp()->setAttribute("lastsession",name.toStdString());
        addRecentSession(name);
        updateRecentSessionMenu();
        loadCollections();
        updateRecentCollectionMenu();
        loadRecentStyles();
        updateRecentStyleMenu();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        if (session != NULL) {
            delete session;
            session = NULL;
        }
        if (styleManager != NULL) delete styleManager;
        styleManager = NULL;
        if (sceneManager != NULL) delete sceneManager;
        sceneManager = NULL;
        session = NULL;
    }
    if (file != "") openScriptFromFile(file);
    try {
        //lastCollection = session->getConfig()->getFunc()->getAttribute("lastcollection","");
        lastCollection = session->getData()->lastcollection;
        if (lastCollection != "") openCollection(lastCollection);
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
    }
    updateMenu();
}

void Manager::loadCollections()
{
    //int size = readInt(session->getConfig()->getFunc()->getAttribute("numrecentcollections","0"));
    int size = session->getData()->recentcollections.size();
    Scene_Open_Recent->clear();
    Scene_Open_Recent->addAction("-");
    recentcollection.clear();
    for (int i = 0; i < size; ++i) {
        stringstream ss2;
        ss2<<i;
        //string s = session->getConfig()->getFunc()->getAttribute("recentcollections_"+ss2.str(),"");
        string s = session->getData()->recentcollections[i];
        recentcollection.push_back(QString::fromStdString(s));
    }
}

void Manager::receiveSessionFile(int i) {receiveSessionFile(recentsessions[i]);}

void Manager::receiveCollectionFile(int i) {openCollection(recentcollection[i]);}

void Manager::receiveSessionFile(QString file)
{
    bool bela = session != NULL && session->isScriptLoaded();
    if (!isSessionFileOk(file)) {
        LOG<<ERROR<<"Cannot open file: "<<file<<endl<<NORMAL;
        return;
    }
    if (cfg == NULL) {
        LOG<<ERROR<<"No configuration file opened. Please restart the program."<<endl<<NORMAL;
        return;
    }

    runtimeVRam->setVRam(NULL);
    scriptVRam->setVRam(NULL);
    try {
        stopCalc();
        Session *s = session;
        if (styleManager != NULL) delete styleManager;
        styleManager = NULL;
        saveCurrentScene();
        if (sceneManager != NULL) delete sceneManager;
        sceneManager = NULL;
        session = NULL;
        if (s != NULL) delete s;
        session = new Session(file.toStdString().c_str(),cfg);


        cfg->getApp()->setAttribute("lastsession",file.toStdString());
        addRecentSession(file);
        updateRecentSessionMenu();
        loadCollections();
        updateRecentCollectionMenu();
        loadRecentStyles();
        updateRecentStyleMenu();

    } catch (ERROR_CODE e) {
        //cout<<"hiba"<<endl;
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        if (session != NULL) {
            delete session;
            session = NULL;
        }
        if (styleManager != NULL) delete styleManager;
        styleManager = NULL;
        if (sceneManager != NULL) delete sceneManager;
        session = NULL;
    }
    try {
        stopCalc();
        session->loadLastScript(scriptVRam, runtimeVRam);
        setUpCalcThread();

        //lastCollection = session->getConfig()->getFunc()->getAttribute("lastcollection","");
        lastCollection = session->getData()->lastcollection;
        if (lastCollection != "") openCollection(lastCollection);
        //string lastStyle = session->getConfig()->getFunc()->getAttribute("laststyle","");
        string lastStyle = session->getData()->laststyle;
        if (lastStyle != "") openStyleFileByName(QString::fromStdString(lastStyle));
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        session->clearScript();
    }
    updateMenu();
}

void Manager::openSession()
{
    if (filedialog != NULL){
        if (filedialog->isActiveWindow()) filedialog->close();
        delete filedialog;
    }

    filedialog = new QFileDialog(0, "Open file", "", tr("*.cfg"));
    filedialog->setAcceptMode(QFileDialog::AcceptOpen);
    filedialog->setFileMode(QFileDialog::ExistingFile);
    filedialog->show();
    connect(filedialog, SIGNAL(fileSelected(QString)),this,SLOT(receiveSessionFile(QString)));
}

void Manager::openScript()
{
    if (session == NULL) {
        LOG<<ERROR<<"There is no session open."<<endl<<NORMAL;
        return;
    }
    if (cfg == NULL) {
        LOG<<ERROR<<"No configuration file opened. Please restart the program."<<endl<<NORMAL;
        return;
    }

    QString filename = QFileDialog::getOpenFileName(0,"Open file", "", "*.nvw");
    //LOG<<"Selected: "<<filename<<endl;
    if (filename != "") openScriptFromFile(filename);
    updateMenu();
}

void Manager::openScriptFromFile(QString filename)
{
    runtimeVRam->setVRam(NULL);
    scriptVRam->setVRam(NULL);
    try {
        stopCalc();
        session->loadScriptInPreview(filename.toStdString().c_str(), scriptVRam, runtimeVRam);
        setUpCalcThread();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        session->clearScript();
    }
    updateMenu();
}

void Manager::stopCalc()
{
    camController->setMatrix(NULL, 1, 1);
    if (calcThread.isRunning()) {
        calcThread.stopCalc();
        calcThread.wait(2000);
    }
}

void Manager::calcPreview()
{
    if (session == NULL) return;
    if (!session->isThereAScript()) return;
    //display
    //if (calcThread.isRunning()) calcThread.terminate();
    //Camera<RTYPE> *cam = session->getCamera();
    stopCalc();
    if (isDisplaying) calcThread.start(QThread::HighestPriority);
}

void Manager::reloadScript()
{
    if (session == NULL) {
        LOG<<ERROR<<"There is no session open."<<endl<<NORMAL;
        return;
    }
    if (cfg == NULL) {
        LOG<<ERROR<<"No configuration file opened. Please restart the program."<<endl<<NORMAL;
        return;
    }
    if (!session->scriptAssociated()) {
        LOG<<ERROR<<"There is no script associated with this session."<<endl<<NORMAL;
        return;
    }
    StyleSheet ss("current");
    StyleSheet ps("current param");
    Camera<RTYPE> *cam = session->getCamera();
    if (cam != NULL) {
        cam->getCurrentStyle(&ss);
        cam->getCurrentParam(&ps);
    }
    saveCurrentScene();

    runtimeVRam->setVRam(NULL);
    scriptVRam->setVRam(NULL);

    try {
        stopCalc();
        session->loadLastScript(scriptVRam, runtimeVRam);
        setUpCalcThread();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        session->clearScript();
    }
    openLastCollection();
    if (cam != NULL) updateStyle(&ss, &ps);
    updateMenu();
}

void Manager::setUpCalcThread()
{
    if (movestate == 0) saveCurrentScene();         //if not moving: save current scene
    if (!session->isThereAScript()) return;
    display->setTexture(NULL);
    session->getFunction()->resetVrammax();
    calcThread.setUp(readInt(cfg->getWindow()->getAttribute("width","")), readInt(cfg->getWindow()->getAttribute("height","")), session->getCamera(), readInt(cfg->getApp()->getAttribute("threads","")));
    calcPreview();
    display->setTexture(calcThread.getTexture());
    runtimeVRam->setVRam(NULL);
    scriptVRam->setVRam(session->getCamera()->getFunction()->func_vram);
    Matrix<RTYPE> m = session->getCamera()->getTransform();
    camController->setMatrix(&m, session->getCamera()->getScale(), session->getCamera()->getFocus());
}

void Manager::drawTexture()
{
    display->update();
}

void Manager::startDisplay()
{
    if (cfg == NULL) return;
    cfg->getApp()->setAttribute("display","on");
    isDisplaying = true;
    calcPreview();
    updateMenu();
}

void Manager::calcUpdate(int v)
{
    display->setProgress(v);
}

void Manager::stopDisplay()
{
    display->finish();
    if (cfg == NULL) return;
    cfg->getApp()->setAttribute("display","off");
    stopCalc();
    isDisplaying = false;
    updateMenu();
}

void Manager::onDisplayFail(QString s)
{
    stopDisplay();
    calcThread.popError();
    LOG<<ERROR<<s<<endl<<NORMAL;
}

void Manager::updateMoveTimer()
{
    qint64 elapsed = moveelapsedtimer.elapsed();
    moveelapsedtimer.restart();
    if (movestate == 0) {
        return;
    }

    if (cfg == NULL) return;
    if (session == NULL) return;
    if (!session->isScriptLoaded()) return;
    Camera<RTYPE> *cam = session->getCamera();

    double time = (double)elapsed / 1000;
    bool aroundfocus = camController->isAroundFocus();

    if (movestate == 1 || movestate == 3) {
        RTYPE dir = -(movestate-2);
        VectorN<RTYPE> trans = cam->getDirection();
        if (aroundfocus) {
            double c = time*MOVESPEED + 1.0;
            double move = 0;
            if (movestate == 1) {
                //p/s = 1/c
                //p = s/c
                RTYPE s = cam->getFocus();
                RTYPE p = s/c;
                move = s-p;
            } else {
                //p*c = s
                RTYPE p = cam->getFocus();
                RTYPE s = p*c;
                move = p-s;
            }
            trans = trans * move;
        } else {
            trans = trans * (dir * time * cam->getFocus() * MOVESPEED);
        }
        receiveCameraTranslate(trans, false, aroundfocus, false);
    } else if (movestate == 2 || movestate == 4) {
        //LOG<<axis1<<endl;
        if (axis1 != -1 && axis2 == -1) {       //one axis : move
            if (axis1 < cam->getTransform().getRows()-1) {
                int dir = 1;
                if (movestate == 4) dir = -1;
                if (!aroundfocus || axis1 == 2) {
                    VectorN<RTYPE> trans = VectorN<RTYPE>::identity(cam->getTransform().getRows()-1, axis1) * (dir * time * cam->getFocus() * MOVESPEED);
                    receiveCameraTranslate(trans, false, false, false);
                } else {
                    RTYPE s = time * ROTSPEED;
                    RTYPE totalS = M_PI*2;
                    RTYPE rotation = s/totalS;
                    rotation -= (int)rotation;          //limit to one circle
                    rotation *= -dir;
                    receiveCameraRotation(rotation,2,axis1,true,true);
                }
            }
        } else if (axis1 >= 0 && axis2 >= 0 && axis1 != axis2) {            //two axices : rotate
            int dir = 1;
            if (movestate == 4) dir = -1;
            RTYPE s = time * ROTSPEED;
            RTYPE totalS = M_PI*2;
            RTYPE rotation = s/totalS;
            rotation -= (int)rotation;          //limit to one circle
            rotation *= dir;
            receiveCameraRotation(rotation,axis1,axis2,true,aroundfocus);
        }
    }
}

void Manager::keyPressEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat()) return;
    int c = event->key();
    if (c == keycodes[0]) {// forward
        movetimer.stop();
        updateMoveTimer();
        movestate = 1;
        movetimer.start(TIMER_INTERVALL);
    } else if (c == keycodes[1]) { // back
        movetimer.stop();
        updateMoveTimer();
        movestate = 3;
        movetimer.start(TIMER_INTERVALL);
    } else if (c == keycodes[2]) { // left
        movetimer.stop();
        updateMoveTimer();
        movestate = 4;
        movetimer.start(TIMER_INTERVALL);
    } else if (c == keycodes[3]) { // right
        movetimer.stop();
        updateMoveTimer();
        movestate = 2;
        movetimer.start(TIMER_INTERVALL);
    } else {
        int i = 4;
        for (; i < keycodes.size() && c != keycodes[i]; ++i);
        if (i < keycodes.size()) {
            movetimer.stop();
            updateMoveTimer();
            int axis = i-4;
            if (axis1 == -1) axis1 = axis;
            else if (axis2 == -1) axis2 = axis;
            else axis2 = axis;
            if (axis1 > axis2 && axis2 != -1) swap(axis1,axis2);
            if (axis1 == axis2) axis2 = -1;
            if (movestate != 0) movetimer.start(TIMER_INTERVALL);
        }
    }
}

void Manager::keyReleaseEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat()) return;
    int c = event->key();
    if (c == keycodes[0] || c == keycodes[1] || c == keycodes[2] || c == keycodes[3]) {
        movetimer.stop();
        updateMoveTimer();
        movestate = 0;
    } else {
        int i = 4;
        for (; i < keycodes.size() && c != keycodes[i]; ++i);
        if (i < keycodes.size()) {
            movetimer.stop();
            updateMoveTimer();
            int axis = i-4;
            if (axis2 == axis) axis2 = -1;
            if (axis1 == axis) axis1 = -1;
            if (axis1 == -1 && axis2 != -1) swap(axis1, axis2);
            if (movestate != 0) movetimer.start(TIMER_INTERVALL);
        }
    }
}

void Manager::newCollection()
{
    QString file = QFileDialog::getSaveFileName(0,"New file","","*.scnc");
    openCollection(file.toStdString());
}

void Manager::openCollection()
{
    QString file = QFileDialog::getOpenFileName(0,"Open file","","*.scnc");
    openCollection(file.toStdString());
}

void Manager::openCollection(string file)
{
    LOG<<file<<endl;
    if (session == NULL) return;
    if (cfg == NULL) return;
    if (file == "") return;
    if (file.substr(file.length()-5, -1) != ".scnc") file += ".scnc";
    saveCurrentScene();
    if (sceneManager != NULL) {
        delete sceneManager;
        sceneManager = NULL;
    }
    try {
        sceneManager = new SceneManager(file, session->getDim());
        //session->getConfig()->getFunc()->setAttribute("lastcollection",file);
        session->getData()->lastcollection = file;
        lastCollection = file;
        addRecentCollection(QString::fromStdString(lastCollection));
        updateRecentCollectionMenu();
    } catch (ERROR_CODE e) {
        sceneManager = NULL;
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
    }
    openLastCollection();
    updateMenu();
}

void Manager::openLastCollection()
{
    if (sceneManager == NULL) return;
    if (sceneManager->isRecord(LASTSCENE_NAME)) loadSceneByName(LASTSCENE_NAME);
}

void Manager::openCollection(QString filename)
{
    string file = filename.toStdString();
    openCollection(file);
}

void Manager::capture()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (sceneManager == NULL) return;
    if (dial != NULL) delete dial;
    dial = NULL;
    TextDialog *td = new TextDialog();
    dial = td;
    dial->setWindowTitle("Save scene");
    dial->setModal(true);
    dial->show();
    connect(td,SIGNAL(acceptText(QString)),this,SLOT(captureScene(QString)));

}

void Manager::captureScene(QString name)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (sceneManager == NULL) return;
    if (session->getCamera() == NULL) return;
    bool ok = true;
    dial->close();
    if (sceneManager->isRecord(name.toStdString())) {
        ok = false;
        /*((TextDialog*)dial)->setMessage("Name is already in use");
        ((TextDialog*)dial)->buzz();*/

        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(window, "Override scene", "The entered name already belongs to a scene. Do you want to override it?", QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes) {
            ok = true;
        } else {
            capture();
        }

    }
    if (ok) {
        Camera<RTYPE> *cam = session->getCamera();
        if (!sceneManager->addEditRecord(cam,name.toStdString())) {
            LOG<<ERROR<<"Could not save scene"<<endl<<NORMAL;
        } else {
            LOG<<"Scene captured: "<<name.toStdString()<<endl;
        }
    }
    updateMenu();
}

void Manager::loadStyle()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (sceneManager == NULL) return;
    if (dial != NULL) delete dial;
    dial = NULL;
    TextDialog *td = new TextDialog();
    dial = td;
    dial->setWindowTitle("Open style");
    dial->setModal(true);
    dial->show();
    connect(td,SIGNAL(acceptText(QString)),this,SLOT(loadStyleByName(QString)));
    closeLoadSceneDialog = true;
}

void Manager::loadStyleByName(QString name)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (sceneManager == NULL) return;
    if (session->getCamera() == NULL) return;
    if (!styleManager->isRecord(name.toStdString())) {
        ((TextDialog*)dial)->setMessage("There is no such style");
        ((TextDialog*)dial)->buzz();
    } else {
        try {
            stopCalc();
            Camera<RTYPE> *cam = session->getCamera();
            StyleSheet *ss = styleManager->getStyleSheet(name.toStdString());
            cam->setStyleSheet(ss);
            LOG<<"Style loaded: "<<name.toStdString()<<endl;
            setUpCalcThread();
        } catch (ERROR_CODE e) {
            LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
            session->clearScript();
            reloadScript();
        }

        if (closeLoadSceneDialog) {
            dial->close();
            closeLoadSceneDialog = false;
        }
    }
    updateMenu();
}


void Manager::loadScene()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (sceneManager == NULL) return;
    if (dial != NULL) delete dial;
    dial = NULL;
    TextDialog *td = new TextDialog();
    dial = td;
    dial->setWindowTitle("Load scene");
    dial->setModal(true);
    dial->show();
    connect(td,SIGNAL(acceptText(QString)),this,SLOT(loadSceneByName(QString)));
    closeLoadSceneDialog = true;
}

void Manager::loadSceneByName(QString name)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (sceneManager == NULL) return;
    if (session->getCamera() == NULL) return;
    if (!session->isScriptLoaded()) return;
    if (!sceneManager->isRecord(name.toStdString())) {
        ((TextDialog*)dial)->setMessage("There is no such scene");
        ((TextDialog*)dial)->buzz();
    } else {
        try {
            stopCalc();
            Camera<RTYPE> *cam = session->getCamera();
            if (!sceneManager->loadRecord(name.toStdString(),cam)) {
                LOG<<ERROR<<"Could not load scene"<<endl<<NORMAL;
            } else {
                LOG<<"Scene loaded: "<<name.toStdString()<<endl;
            }
            setUpCalcThread();
        } catch (ERROR_CODE e) {
            LOG<<ERROR<<Error::ERROR_STRING<<endl;
            session->clearScript();
        }

        if (closeLoadSceneDialog) {
            dial->close();
            closeLoadSceneDialog = false;
        }
    }
    updateMenu();
}

void Manager::manageScenes()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (sceneManager == NULL) return;
    if (dial != NULL) delete dial;
    dial = NULL;
    SceneDialog *sd = new SceneDialog(sceneManager);
    dial = sd;
    dial->setWindowTitle("Manage scenes");
    dial->setModal(true);
    dial->show();
    connect(sd,SIGNAL(load(QString)),this,SLOT(loadSceneByName(QString)));
}

string Manager::addPrefix(string file, string prefix) const
{
    if (file.length() < prefix.length()) return file + prefix;
    if (file.substr(file.length()-prefix.length(),-1) != prefix) return file + prefix;
    else return file;
}

void Manager::manageStyles()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (styleManager == NULL) return;
    if (dial != NULL) delete dial;
    dial = NULL;
    StyleDialog *sd = new StyleDialog(styleManager);
    dial = sd;
    dial->setWindowTitle("Manage styles");
    dial->setModal(true);
    dial->show();
    connect(sd,SIGNAL(load(QString)),this,SLOT(loadStyleByName(QString)));
}

void Manager::setCurrentStyle()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (styleManager == NULL) return;
    if (session->getCamera() == NULL) return;
    Camera<RTYPE> *cam = session->getCamera();
    if (dial != NULL) delete dial;
    dial = NULL;
    StyleSheet ss("current");
    StyleSheet ps("current param");
    cam->getCurrentStyle(&ss);
    cam->getCurrentParam(&ps);
    CurrentStyleDialog *sd = new CurrentStyleDialog(ss, ps);
    dial = sd;
    dial->setWindowTitle("Styles and parameters");
    dial->setModal(true);
    dial->show();
    connect(sd, SIGNAL(updateStyle(StyleSheet*,StyleSheet*)), this, SLOT(updateStyle(StyleSheet*,StyleSheet*)));
}

void Manager::updateStyle(StyleSheet *ss, StyleSheet *ps)
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (styleManager == NULL) return;
    if (session->getCamera() == NULL) return;

    try {
        stopCalc();
        Camera<RTYPE> *cam = session->getCamera();
        cam->setStyleSheet(ss);
        cam->setStyleSheetP(ps);
        setUpCalcThread();
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<Error::ERROR_STRING<<endl<<NORMAL;
        session->clearScript();
        reloadScript();
    }

}

void Manager::saveCurrentScene()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (sceneManager == NULL) return;
    if (session->getCamera() == NULL) return;
    Camera<RTYPE> *cam = session->getCamera();
    sceneManager->addEditRecord(cam,LASTSCENE_NAME);
}

void Manager::exportRaw()
{
    if (cfg == NULL) return;
    if (session == NULL) return;
    if (styleManager == NULL) return;
    if (sceneManager == NULL) return;
    if (dial != NULL) delete dial;
    dial = NULL;
    ExportDialog *sd = new ExportDialog(session, sceneManager, styleManager);
    dial = sd;
    dial->setWindowTitle("Export");
    dial->setModal(true);
    dial->show();
}

void Manager::exportImage()
{
}
