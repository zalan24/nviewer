#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <QMainWindow>
#include <QFileDialog>
#include <QMenu>
#include <QVector>
#include <QSignalMapper>
#include <QTimer>
#include <QElapsedTimer>

#include <fstream>

#include "common/Helper/pythonmanager.h"
#include "creator/Helper/logger.h"
#include "creator/Helper/calcthread.h"
#include "common/Helper/scenemanager.h"
#include "common/Helper/stylemanager.h"
#include "creator/Widgets/displaywidget.h"
#include "creator/Widgets/vramwidget.h"
#include "creator/Widgets/cameracontroller.h"
#include "creator/Dialog/reexdialog.h"
#include "creator/Dialog/settings.h"
#include "creator/Dialog/newsession.h"
#include "creator/Dialog/vectordialog.h"
#include "creator/Dialog/cameraviewdialog.h"
#include "creator/Dialog/transformdialog.h"
#include "creator/Dialog/vectorlocaldialog.h"
#include "creator/Dialog/rotationdialog.h"
#include "creator/Dialog/textdialog.h"
#include "creator/Dialog/scenedialog.h"
#include "creator/Dialog/styledialog.h"
#include "creator/Dialog/currentstyledialog.h"
#include "creator/Dialog/exportdialog.h"
#include "common/Helper/configcheck.h"

#include "common/Math/include/log.h"
#include "common/Math/include/config.h"
#include "common/Math/include/session.h"

#define LASTSCENE_NAME "__lastscene_autogen"
#define MAX_RECENT_SESSIONS 10
#define TIMER_INTERVALL 50
#define MOVESPEED 0.2
#define ROTSPEED 1.5

//------------------------
//        Keyboard
//------------------------
#define DEFAULT_FORWARD Qt::Key_W
#define DEFAULT_BACKWARD Qt::Key_S
#define DEFAULT_LEFT Qt::Key_A
#define DEFAULT_RIGHT Qt::Key_D
#define NUMAXIS 13
#define DEFAULT_AXIS1 Qt::Key_R
#define DEFAULT_AXIS2 Qt::Key_T
#define DEFAULT_AXIS3 Qt::Key_Y
#define DEFAULT_AXIS4 Qt::Key_U
#define DEFAULT_AXIS5 Qt::Key_I
#define DEFAULT_AXIS6 Qt::Key_O
#define DEFAULT_AXIS7 Qt::Key_P
#define DEFAULT_AXIS8 Qt::Key_F
#define DEFAULT_AXIS9 Qt::Key_G
#define DEFAULT_AXIS10 Qt::Key_H
#define DEFAULT_AXIS11 Qt::Key_J
#define DEFAULT_AXIS12 Qt::Key_K
#define DEFAULT_AXIS13 Qt::Key_L

struct QMenuData
{
    QMenu *File_Open_Recent;
    QMenu *Scene_Open_Recent;
    QMenu *Style_Open_Recent;
    QMenuData(QMenu *file, QMenu *scene, QMenu *style) :
        File_Open_Recent(file),
        Scene_Open_Recent(scene),
        Style_Open_Recent(style) {}
};

class Manager : public QObject
{
    Q_OBJECT
public:
    explicit Manager(Logger *logger, DisplayWidget *display, QMenuData menudata, VRamWidget *scriptVRam, VRamWidget *runtimeVRam, CameraController *camc, QMainWindow *window);
    ~Manager();

    bool isLoaded() const;
    void load();
    void ReExError(const QString error);
    void openSettings();

    void calcPreview();

public slots:
    void reLoad();
    void quit();
    void loadFromCfg();
    void newSession();
    void openNewSession(QString name, int dim, QString file);
    void openSession();
    void receiveSessionFile(QString file);
    void receiveSessionFile(int i);
    void receiveCollectionFile(int i);
    void receiveStyleFile(int i);
    void openScript();
    void reloadScript();
    void drawTexture();
    void stopCalc();
    void startDisplay();
    void stopDisplay();
    void onDisplayFail(QString s);
    void calcUpdate(int v);
    void nextStep();
    void prevStep();
    void pauseDisplay();
    void resumeDisplay();
    void setCamPosition();
    void setCamDirection();
    void setCamScale();
    void setCamView();
    void receiveCameraPos(std::vector<RTYPE> vec);
    void receiveCameraTranslate(std::vector<RTYPE> vec, bool local);
    void receiveCameraTranslate(VectorN<RTYPE> vec, bool local, bool aroundfocus, bool keepDir);
    void receiveCameraDir(std::vector<RTYPE> vec);
    void receiveCameraScale(std::vector<RTYPE> vec);
    void receiveViewData(RTYPE, RTYPE, RTYPE);
    void receiveCameraRotation(double angle, int x, int y, bool loc, bool foc);
    void setTransform();
    void receiveTransform(Matrix<RTYPE> m);
    void receiveCamData(RTYPE scale, RTYPE focus);
    void translate();
    void rotate();
    void updateMoveTimer();
    void newCollection();
    void openCollection();
    void capture();
    void captureScene(QString name);
    void loadScene();
    void loadSceneByName(QString name);
    void manageScenes();
    void newStyleFile();
    void saveStyleFile();
    void saveStyleFileAs();
    void openStyleFile();
    void openStyleFileByName(QString filename);
    void saveStyle();
    void saveStyle(QString);
    void loadStyle();
    void loadStyleByName(QString name);
    void manageStyles();
    void setCurrentStyle();
    void updateStyle(StyleSheet *ss, StyleSheet *ps);
    void exportRaw();
    void exportImage();

signals:
    void updateMenu();

public:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    MachineConfig *getMachineCfg() const {return cfg;}
    Session *getSession() const {return session;}
    SceneManager *getSceneManager() const {return sceneManager;}
    StyleManager *getStyleManager() const {return styleManager;}
    bool isDisplayingOn() const {return isDisplaying;}

private:
    Logger *logger;
    DisplayWidget *display;
    Log log;
    MachineConfig *cfg;
    bool loaded;
    ReExDialog *dialog;
    QMainWindow *window;
    Settings *settings;
    NewSession *nsession;
    Session *session;
    QFileDialog *filedialog;
    QMenu *File_Open_Recent;
    QMenu *Scene_Open_Recent;
    QMenu *Style_Open_Recent;
    bool isDisplaying;
    VRamWidget *runtimeVRam;
    VRamWidget *scriptVRam;
    VectorDialog *vectorDialog;
    CameraViewDialog *camdialog;
    QDialog *dial;
    CameraController *camController;
    int movestate;
    int axis1;
    int axis2;
    SceneManager *sceneManager;
    bool closeLoadSceneDialog;
    StyleManager *styleManager;

    std::ofstream debugout;
    QVector<QString> recentsessions;
    QVector<QString> recentcollection;
    QVector<QString> recentstyles;
    CalcThread calcThread;
    QVector<int> keycodes;
    QTimer movetimer;
    QElapsedTimer moveelapsedtimer;
    string lastCollection;


    void loadMCfg();
    void setUpCfg();
    void loadScreen();
    void checkConfig();
    void resize(int w, int h);
    bool isSessionNameOk(QString name);
    bool isDimOk(int d);
    bool isSessionFileOk(QString file);
    void addRecentSession(QString s);
    void addRecentCollection(QString s);
    void addRecentStyle(QString s);
    void updateRecentSessionMenu() const;
    void updateRecentCollectionMenu() const;
    void updateRecentStyleMenu() const;
    void setUpCalcThread();
    void openScriptFromFile(QString filename);
    void openCollection(QString filename);
    void openCollection(string file);
    void loadCollections();
    void loadRecentStyles();
    string addPrefix(string file, string prefix) const;
    void saveCurrentScene();
    void openLastCollection();
};

#endif // MANAGER_H
