#ifndef LOGGER_H
#define LOGGER_H

#include <QTextBrowser>
#include <QVector>
#include "common/Helper/loggerbase.h"
#include "common/Math/include/log.h"

#include <iostream>

using namespace std;

class Logger : public LoggerBase
{
public:
    Logger(QTextBrowser *l, int maxlines = 1000);
    void log(QString line);
    void log(string line) {log(QString::fromStdString(line));}
    void logWarning(QString line);
    void logWarning(string line) {logWarning(QString::fromStdString(line));}
    void logError(QString line);
    void logError(string line) {logError(QString::fromStdString(line));}
    void loadText(string s);
    void logLoaded(LogModifier m);
    ~Logger() {}
private:
    QTextBrowser *console;
    int maxlen;
    QVector<QString> lines;
    int begin;
    string buffer;
};

Log& operator<<(Log&, QString);

#endif // LOGGER_H
