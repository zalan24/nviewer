#include "creator/mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    PythonManager pythonManager(argv[0]);

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
