#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QLayout>
#include <QLabel>
#include <QScrollArea>
#include <QTextBrowser>
#include <QPalette>
#include <QAction>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    log(NULL)
{
    ui->setupUi(this);

    setWindowTitle("NViewer");

    QVBoxLayout *mainlayout = new QVBoxLayout(ui->centralWidget);
    QHBoxLayout *displaylayout = new QHBoxLayout();
    QProgressBar*progressbar = new QProgressBar();
    QVBoxLayout *screenlayout = new QVBoxLayout();
    QVBoxLayout *datalayout = new QVBoxLayout();
    display = new DisplayWidget(progressbar);
    screenlayout->addWidget(display);
    screenlayout->addWidget(progressbar);
    QHBoxLayout *vramlayout = new QHBoxLayout();

    scriptVRam = new VRamWidget("Script VRam");
    runtimeVRam = new VRamWidget("Runtime VRam");

    vramlayout->addWidget(runtimeVRam);
    vramlayout->addWidget(scriptVRam);
    datalayout->addLayout(vramlayout);
    CameraController *camc = new CameraController(NULL,1,1);

    datalayout->addWidget(camc);
    displaylayout->addLayout(screenlayout);
    displaylayout->addLayout(datalayout);
    mainlayout->addLayout(displaylayout);

    QTextBrowser *consolearea = new QTextBrowser();
    consolearea->setAcceptRichText(true);
    consolearea->setStyleSheet("* {background-color:black}");
    consolearea->setTextColor(QColor("white"));

    mainlayout->addWidget(consolearea);
    log = new Logger(consolearea);

    connect(ui->actionQuit, SIGNAL(triggered()), this, SLOT(quit()));
    connect(ui->actionSettings, SIGNAL(triggered()), this, SLOT(settings()));
    connect(ui->actionNew_session, SIGNAL(triggered()), this, SLOT(newSession()));
    connect(ui->actionOpen_session, SIGNAL(triggered()), this, SLOT(openSession()));
    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(openScript()));
    connect(ui->actionReload, SIGNAL(triggered()), this, SLOT(reloadScript()));
    connect(ui->actionStart, SIGNAL(triggered()), this, SLOT(startDisplay()));
    connect(ui->actionStop, SIGNAL(triggered()), this, SLOT(stopDisplay()));
    connect(ui->actionNext_step, SIGNAL(triggered()), this, SLOT(nextStep()));
    connect(ui->actionPrevious_step, SIGNAL(triggered()), this, SLOT(prevStep()));
    connect(ui->actionPause, SIGNAL(triggered()), this, SLOT(pauseDisplay()));
    connect(ui->actionResume, SIGNAL(triggered()), this, SLOT(resumeDisplay()));
    connect(ui->actionSet_position, SIGNAL(triggered()), this, SLOT(setCamPosition()));
    connect(ui->actionSet_direction, SIGNAL(triggered()), this, SLOT(setCamDirection()));
    connect(ui->actionSet_view, SIGNAL(triggered()), this, SLOT(setCamView()));
    connect(ui->actionScale_transform, SIGNAL(triggered()), this, SLOT(scaleCamera()));
    connect(ui->actionSet_transform, SIGNAL(triggered()), this, SLOT(setTransform()));
    connect(ui->actionTranslate, SIGNAL(triggered()), this, SLOT(translate()));
    connect(ui->actionRotate, SIGNAL(triggered()), this, SLOT(rotate()));
    connect(ui->actionNew_collection, SIGNAL(triggered()), this, SLOT(newCollection()));
    connect(ui->actionOpen_collection, SIGNAL(triggered()), this, SLOT(openCollection()));
    connect(ui->actionCapture, SIGNAL(triggered(bool)), this, SLOT(capture()));
    connect(ui->actionLoad, SIGNAL(triggered(bool)), this, SLOT(loadScene()));
    connect(ui->actionManage_scenes, SIGNAL(triggered(bool)), this, SLOT(manageScenes()));
    connect(ui->actionNew_file, SIGNAL(triggered(bool)), this, SLOT(newStyleFile()));
    connect(ui->actionSave, SIGNAL(triggered(bool)), this, SLOT(saveStyleFile()));
    connect(ui->actionSave_as, SIGNAL(triggered(bool)), this, SLOT(saveStyleFileAs()));
    connect(ui->actionOpen_file, SIGNAL(triggered(bool)), this, SLOT(openStyleFile()));
    connect(ui->actionSave_style, SIGNAL(triggered(bool)), this, SLOT(saveStyle()));
    connect(ui->actionOpen_style_by_name, SIGNAL(triggered(bool)), this, SLOT(openStyleByName()));
    connect(ui->actionManage_style, SIGNAL(triggered(bool)), this, SLOT(manageStyles()));
    connect(ui->actionSet_parameters, SIGNAL(triggered(bool)), this, SLOT(setCurrentStyle()));
    connect(ui->actionExport_raw_data, SIGNAL(triggered(bool)), this, SLOT(exportRaw()));
    connect(ui->actionExport_image, SIGNAL(triggered(bool)), this, SLOT(exportImage()));

    File_Open_Recent = new QMenu("Open recent session");
    ui->menuFile->insertMenu(ui->actionOpen_session, File_Open_Recent);

    Scene_Open_Recent = new QMenu("Open recent collection");
    ui->menuScene->insertMenu(ui->actionOpen_collection, Scene_Open_Recent);

    Style_Open_Recent = new QMenu("Open recent");
    ui->menuStyles_Parameters ->insertMenu(ui->actionOpen_file, Style_Open_Recent);

    manager = new Manager(log, display, QMenuData(File_Open_Recent, Scene_Open_Recent, Style_Open_Recent), scriptVRam, runtimeVRam, camc, this);
    connect(manager, SIGNAL(updateMenu()), this, SLOT(updateMenu()));
    updateMenu();
    //PyRun_SimpleString("from time import time,ctime\n" "print 'Today is',ctime(time())\n");
}

void MainWindow::exportRaw()
{
    manager->exportRaw();
}

void MainWindow::exportImage()
{
    manager->exportImage();
}

void MainWindow::setCurrentStyle()
{
    manager->setCurrentStyle();
}

void MainWindow::manageStyles()
{
    manager->manageStyles();
}

void MainWindow::openStyleByName()
{
    manager->loadStyle();
}

void MainWindow::saveStyle()
{
    manager->saveStyle();
}

void MainWindow::newStyleFile()
{
    manager->newStyleFile();
}

void MainWindow::saveStyleFile()
{
    manager->saveStyleFile();
}

void MainWindow::saveStyleFileAs()
{
    manager->saveStyleFileAs();
}

void MainWindow::openStyleFile()
{
    manager->openStyleFile();
}

void MainWindow::manageScenes()
{
    manager->manageScenes();
}

void MainWindow::loadScene()
{
    manager->loadScene();
}

void MainWindow::capture()
{
    manager->capture();
}

void MainWindow::newCollection()
{
    manager->newCollection();
}

void MainWindow::openCollection()
{
    manager->openCollection();
}

void MainWindow::translate()
{
    manager->translate();
}

void MainWindow::rotate()
{
    manager->rotate();
}

void MainWindow::setTransform()
{
    manager->setTransform();
}

void MainWindow::scaleCamera()
{
    manager->setCamScale();
}

void MainWindow::setCamPosition()
{
    manager->setCamPosition();
}

void MainWindow::setCamDirection()
{
    manager->setCamDirection();
}

void MainWindow::setCamView()
{
    manager->setCamView();
}

void MainWindow::resumeDisplay()
{
    manager->resumeDisplay();
}

void MainWindow::pauseDisplay()
{
    manager->pauseDisplay();
}

void MainWindow::nextStep()
{
    manager->nextStep();
}

void MainWindow::prevStep()
{
    manager->prevStep();
}

void MainWindow::startDisplay()
{
    manager->startDisplay();
}

void MainWindow::stopDisplay()
{
    manager->stopDisplay();
}

void MainWindow::reloadScript()
{
    manager->reloadScript();
}

void MainWindow::openScript()
{
    manager->openScript();
}

void MainWindow::openSession()
{
    manager->openSession();
}

void MainWindow::newSession()
{
    manager->newSession();
}

void MainWindow::settings()
{
    manager->openSettings();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    manager->keyPressEvent(event);
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    manager->keyReleaseEvent(event);
}

/*void MainWindow::closeEvent(QCloseEvent *event)
{
    manager->stopCalc();
}*/

void MainWindow::quit()
{
    close();
}

MainWindow::~MainWindow()
{
    delete manager;
    if (log != NULL) delete log;
    delete ui;
}

void MainWindow::updateMenu()
{
    if (manager->getMachineCfg() == NULL) {     //File
        ui->actionNew_session->setEnabled(false);
        ui->actionOpen_session->setEnabled(false);
        Scene_Open_Recent->setEnabled(false);
    } else {
        ui->actionNew_session->setEnabled(true);
        ui->actionOpen_session->setEnabled(true);
        Scene_Open_Recent->setEnabled(true);
    }
    if (manager->getMachineCfg() == NULL || manager->getSession() == NULL) {     //Scenes
        ui->menuScene->setEnabled(false);
    } else {
        ui->menuScene->setEnabled(true);
        if (manager->getSceneManager() == NULL) {
            ui->actionCapture->setEnabled(false);
            ui->actionManage_scenes->setEnabled(false);
            ui->actionLoad->setEnabled(false);
        } else {
            ui->actionCapture->setEnabled(true);
            ui->actionManage_scenes->setEnabled(true);
            ui->actionLoad->setEnabled(true);
        }
    }
    if (manager->getMachineCfg() == NULL) {     //Display
        ui->menuDisplay->setEnabled(false);
    } else {
        ui->menuDisplay->setEnabled(true);
        if (!manager->isDisplayingOn()) {
            ui->actionPause->setEnabled(false);
            ui->actionResume->setEnabled(false);
            ui->actionNext_step->setEnabled(false);
            ui->actionPrevious_step->setEnabled(false);
            ui->actionStop->setEnabled(false);
            ui->actionStart->setEnabled(true);
        } else {
            ui->actionPause->setEnabled(true);
            ui->actionResume->setEnabled(true);
            ui->actionNext_step->setEnabled(true);
            ui->actionPrevious_step->setEnabled(true);
            ui->actionStop->setEnabled(true);
            ui->actionStart->setEnabled(false);
        }
    }
    if (manager->getMachineCfg() == NULL || manager->getSession() == NULL) {     //Script
        ui->menuScript->setEnabled(false);
    } else {
        ui->menuScript->setEnabled(true);
        if (!manager->getSession()->scriptAssociated()) {
            ui->actionReload->setEnabled(false);
        } else {
            ui->actionReload->setEnabled(true);
        }
    }
    if (manager->getMachineCfg() == NULL || manager->getSession() == NULL) {     //Camera
        ui->menuCamera->setEnabled(false);
    } else {
        ui->menuCamera->setEnabled(true);
    }
    if (manager->getMachineCfg() == NULL || manager->getSession() == NULL) {     //Styles
        ui->menuStyles_Parameters->setEnabled(false);
    } else {
        ui->menuStyles_Parameters->setEnabled(true);
        if (manager->getStyleManager() == NULL) {
            ui->actionSave_style->setEnabled(false);
            ui->actionOpen_style_by_name->setEnabled(false);
            ui->actionManage_style->setEnabled(false);
            ui->actionSet_parameters->setEnabled(false);
        } else {
            ui->actionSave_style->setEnabled(true);
            ui->actionOpen_style_by_name->setEnabled(true);
            ui->actionManage_style->setEnabled(true);
            ui->actionSet_parameters->setEnabled(true);
        }
    }
    if (manager->getMachineCfg() == NULL || manager->getSession() == NULL || manager->getStyleManager() == NULL || manager->getSceneManager() == NULL) {
        ui->menuExport->setEnabled(false);
    } else {
        ui->menuExport->setEnabled(true);
    }
}
