#include "rotationdialog.h"
#include "ui_rotationdialog.h"

RotationDialog::RotationDialog(int dim, bool foc, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RotationDialog),
    dim(dim)
{
    ui->setupUi(this);

    QGridLayout *layout = new QGridLayout;

    QLabel *xa = new QLabel("Axis 1");
    QLabel *ya = new QLabel("Axis 2");
    QLabel *an = new QLabel("Angle (Deg)");
    xaxis = new QSpinBox;
    xaxis->setMinimum(1);
    xaxis->setMaximum(dim);
    xaxis->setValue(1);
    yaxis = new QSpinBox;
    yaxis->setMinimum(1);
    yaxis->setMaximum(dim);
    yaxis->setValue(2);
    angle = new QDoubleSpinBox;
    angle->setMinimum(-360);
    angle->setMaximum(360);
    angle->setValue(0);
    local = new QCheckBox("Local transform");
    local->setChecked(true);
    aroundfocus = new QCheckBox("Keep focus");
    aroundfocus->setChecked(foc);
    xa->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    ya->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    an->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    layout->addWidget(xa,0,0,Qt::AlignTop);
    layout->addWidget(ya,1,0,Qt::AlignTop);
    layout->addWidget(an,2,0,Qt::AlignTop);
    layout->addWidget(xaxis,0,1,Qt::AlignTop);
    layout->addWidget(yaxis,1,1,Qt::AlignTop);
    layout->addWidget(angle,2,1,Qt::AlignTop);
    layout->addWidget(local,3,0,1,2,Qt::AlignTop);
    layout->addWidget(aroundfocus,4,0,1,2,Qt::AlignTop);


    layout->addWidget(ui->buttonBox,5,0,1,2,Qt::AlignTop);

    setLayout(layout);
}

RotationDialog::~RotationDialog()
{
    delete ui;
}

void RotationDialog::accept()
{
    sendData(angle->value()*M_PI/180, xaxis->value()-1, yaxis->value()-1, local->isChecked(), aroundfocus->isChecked());
    close();
}
