#ifndef SCENEDIALOG_H
#define SCENEDIALOG_H

#include "common/Helper/scenemanager.h"

#include <QDialog>
#include <QGridLayout>
#include <QDoubleSpinBox>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QTableWidget>
#include <QSignalMapper>
#include <QPushButton>
#include <QMessageBox>
#include <QLabel>
#include <vector>
#include <algorithm>

namespace Ui {
class SceneDialog;
}

bool compName(SceneRecord *sc1, SceneRecord *sc2);
bool compNameRev(SceneRecord *sc1, SceneRecord *sc2);
bool compDate(SceneRecord *sc1, SceneRecord *sc2);
bool compDateRev(SceneRecord *sc1, SceneRecord *sc2);

class SceneDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SceneDialog(SceneManager *sceneManager, QWidget *parent = 0);
    ~SceneDialog();

protected slots:
    void loadRecord(int i);
    void deleteRecord(int i);
    void closeDialog();
    void tableSectionClicked(int index);

signals:
    void load(QString name);

private:
    Ui::SceneDialog *ui;
    SceneManager *sceneManager;
    vector<SceneRecord*> records;
    QTableWidget *table;
    int sorted;

    void sortByName();
    void sortByDate();
    void updateTable();
};

#endif // SCENEDIALOG_H
