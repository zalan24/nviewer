#include "currentstyledialog.h"
#include "ui_currentstyledialog.h"

CurrentStyleDialog::CurrentStyleDialog(const StyleSheet &ss, const StyleSheet &ps, QWidget *parent) :
    QDialog(parent),
    style(ss),
    param(ps),
    ui(new Ui::CurrentStyleDialog)
{
    ui->setupUi(this);
    QVBoxLayout *layout = new QVBoxLayout;

    styleWidget = new StyleEditWidget(&style);
    paramWidget = new StyleEditWidget(&param);

    QTabWidget *tab = new QTabWidget;
    tab->addTab(styleWidget, "Styles");
    tab->addTab(paramWidget, "Parameters");
    tab->setTabToolTip(0,"Can be saved and loaded per image");
    tab->setTabToolTip(1,"Global settings");

    layout->addWidget(tab);

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    QHBoxLayout *buttonLayout2 = new QHBoxLayout;

    QPushButton *closeb, *acceptb, *applyb;
    closeb = new QPushButton("close");
    acceptb = new QPushButton("ok");
    applyb = new QPushButton("apply");
    closeb->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    acceptb->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    //applyb->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    buttonLayout->addWidget(applyb, 0, Qt::AlignLeft);

    QWidget *viewport = new QWidget;

    buttonLayout2->addWidget(acceptb);
    buttonLayout2->addWidget(closeb);
    viewport->setLayout(buttonLayout2);
    viewport->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    //buttonLayout2->setSizeConstraint(QLayout::SetFixedSize);
    //buttonLayout->addLayout(buttonLayout2);
    buttonLayout->addWidget(viewport, 0, Qt::AlignRight);

    layout->addLayout(buttonLayout);

    setLayout(layout);

    connect(closeb, SIGNAL(clicked(bool)), this, SLOT(cancel()));
    connect(applyb, SIGNAL(clicked(bool)), this, SLOT(apply()));
    connect(acceptb, SIGNAL(clicked(bool)), this, SLOT(ok()));
}

void CurrentStyleDialog::cancel()
{
    close();
}

void CurrentStyleDialog::ok()
{
    apply();
    close();
}

void CurrentStyleDialog::apply()
{
    styleWidget->save();
    paramWidget->save();
    updateStyle(&style, &param);
}

CurrentStyleDialog::~CurrentStyleDialog()
{
    delete ui;
}
