#ifndef TRANSFORMDIALOG_H
#define TRANSFORMDIALOG_H

#include <QDialog>
#include <QGridLayout>
#include <QDoubleSpinBox>
#include <QScrollArea>
#include <QVBoxLayout>

#include "common/Math/include/matrix.h"
#include "common/Math/include/info.h"

namespace Ui {
class TransformDialog;
}

class TransformDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TransformDialog(Matrix<RTYPE> m, QWidget *parent = 0);
    ~TransformDialog();

    void accept();

signals:
    void sendData(Matrix<RTYPE> m);

public slots:
    void changeItem(int i);

private:
    Ui::TransformDialog *ui;
    Matrix<RTYPE> transform;
    QVector<QDoubleSpinBox*> boxes;
};

#endif // TRANSFORMDIALOG_H
