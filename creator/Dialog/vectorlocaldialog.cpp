#include "vectorlocaldialog.h"
#include "ui_vectorlocaldialog.h"

#include <limits>
#include <cstddef>
#include <iostream>
#include <sstream>

VectorLocalDialog::VectorLocalDialog(std::vector<RTYPE> &vec, bool checked, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VectorLocalDialog),
    vec(vec)
{
    ui->setupUi(this);

    QVBoxLayout *layout = new QVBoxLayout;
    QScrollArea *scrollA = new QScrollArea;
    QGridLayout *sLayout = new QGridLayout;


    double mx = std::numeric_limits<double>::max();
    //double mn = std::numeric_limits<double>::min();
    for (int i = 0; i < vec.size() && i < MAX_DIM; ++i) {
        QDoubleSpinBox *b = new QDoubleSpinBox;
        b->setMaximum(mx);
        b->setMinimum(-mx);
        b->setDecimals(DOUBLESPINBOX_DECIMALS);
        b->setSingleStep(DOUBLESPINBOX_STEP);
        b->setValue(vec[i]);
        connect(b, SIGNAL(valueChanged(double)), this, SLOT(updateSpin()));
        spins.push_back(b);
        std::stringstream ss;
        ss<<i+1<<":";
        while (ss.str().length() < 7) ss<<" ";
        QLabel *l = new QLabel(QString::fromStdString(ss.str()));
        sLayout->addWidget(l,i,0,Qt::AlignTop);
        sLayout->addWidget(b,i,1,Qt::AlignTop);
        l->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        b->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    }

    QWidget *viewport = new QWidget;
    viewport->setLayout(sLayout);
    //viewport->setStyleSheet("background-color: black");
    viewport->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::MinimumExpanding);

    scrollA->setWidget(viewport);
    scrollA->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
    layout->addWidget(scrollA);
    QLabel *label = new QLabel("Custom:");
    QGridLayout *custom = new QGridLayout;
    layout->addWidget(label);
    customid = new QSpinBox;
    customid->setValue(1);
    customid->setMinimum(1);
    customid->setMaximum(vec.size());
    customval = new QDoubleSpinBox;
    customval->setMaximum(mx);
    customval->setMinimum(-mx);
    customval->setDecimals(DOUBLESPINBOX_DECIMALS);
    customval->setSingleStep(DOUBLESPINBOX_STEP);
    QPushButton *set = new QPushButton("set");
    set->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    custom->addWidget(customid,0,0);
    custom->addWidget(customval,0,1);
    custom->addWidget(set,0,2);
    //custom->setSizeConstraint(QLayout::SetFixedSize);

    connect(customid, SIGNAL(valueChanged(int)), this, SLOT(updateSet()));

    customid->setValue(1);
    custom->addWidget(set,0,2);
    layout->addLayout(custom);

    label->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    customval->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    customid->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    QHBoxLayout *buttonlayout = new QHBoxLayout;
    QPushButton *zero = new QPushButton("0");
    buttonlayout->addWidget(zero);
    buttonlayout->addWidget(ui->buttonBox);

    //layout->addWidget(ui->buttonBox);
    checkbox = new QCheckBox("Local transform");
    checkbox->setChecked(checked);
    layout->addWidget(checkbox);
    layout->addLayout(buttonlayout);
    setLayout(layout);

    connect(set, SIGNAL(clicked()), this, SLOT(set()));
    connect(zero, SIGNAL(clicked()), this, SLOT(setZero()));
    viewport->setMinimumWidth(width());
    layout->setSizeConstraint(QLayout::SetFixedSize);

    updateSet();
}

void VectorLocalDialog::setZero()
{
    for (int i = 0; i < vec.size(); ++i) {
        vec[i] = 0;
        if (i < spins.size()) spins[i]->setValue(vec[i]);
    }
}

void VectorLocalDialog::updateSpin()
{
    for (int i = 0; i < spins.size(); ++i) vec[i] = spins[i]->value();
    updateSet();
}

void VectorLocalDialog::updateSet()
{
    int id = customid->value()-1;
    customval->setValue(vec[id]);
}

void VectorLocalDialog::set()
{
    int id = customid->value()-1;
    double val = customval->value();
    vec[id] = val;
    if (spins.size() > id) spins[id]->setValue(val);
}

VectorLocalDialog::~VectorLocalDialog()
{
    delete ui;
}

void VectorLocalDialog::accept()
{
    sendData(vec, checkbox->isChecked());
    close();
}
