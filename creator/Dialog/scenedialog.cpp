#include "scenedialog.h"
#include "ui_scenedialog.h"

SceneDialog::SceneDialog(SceneManager *sceneManager, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SceneDialog),
    sceneManager(sceneManager),
    sorted(-1)
{
    ui->setupUi(this);
    QSignalMapper *loadSignal = new QSignalMapper(this);
    QSignalMapper *deleteSignal = new QSignalMapper(this);
    connect(loadSignal, SIGNAL(mapped(int)), this, SLOT(loadRecord(int)));
    connect(deleteSignal, SIGNAL(mapped(int)), this, SLOT(deleteRecord(int)));

    QVBoxLayout *layout = new QVBoxLayout;
    //QGridLayout *sLayout = new QGridLayout;
    for (int i = 0; i < sceneManager->size(); ++i) if (sceneManager->getRecord(i)->getName() != "") records.push_back(sceneManager->getRecord(i));
    sortByName();
    table = new QTableWidget(records.size(), 4);
    for (int i = 0; i < records.size(); ++i) {
        QLabel *name = new QLabel(QString::fromStdString(records[i]->getName()));
        QLabel *cdate = new QLabel(QString::fromStdString(dateToString(records[i]->getModifyDate())));
        //QLabel *mdate = new QLabel(QString::fromStdString(dateToString(records[i]->getModifyDate())));
        QPushButton *del = new QPushButton("Delete");
        QPushButton *loa = new QPushButton("Load");
        connect(loa, SIGNAL(clicked(bool)), loadSignal, SLOT(map()));
        connect(del, SIGNAL(clicked(bool)), deleteSignal, SLOT(map()));
        deleteSignal->setMapping(del, i);
        loadSignal->setMapping(loa, i);
        table->setCellWidget(i,0,name);
        table->setCellWidget(i,1,cdate);
        //table->setCellWidget(i,2,mdate);
        table->setCellWidget(i,2,loa);
        table->setCellWidget(i,3,del);
        cdate->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Preferred);
        del->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
        loa->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    }
    //table->setFixedWidth(table->horizontalHeader()->width());
    table->resizeRowsToContents();
    table->resizeColumnsToContents();
    table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    table->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    QStringList hlabels;
    hlabels.push_back("Id");
    hlabels.push_back("Created");
    hlabels.push_back("");
    hlabels.push_back("");
    table->setHorizontalHeaderLabels(hlabels);

    //table->setSizePolicy (QSizePolicy::Minimum, QSizePolicy::Ignored);
    layout->addWidget(table);
    QPushButton *closeb = new QPushButton("Close");
    layout->addWidget(closeb,0,Qt::AlignRight);
    connect(closeb,SIGNAL(clicked(bool)),this,SLOT(closeDialog()));
    connect(table->horizontalHeader(), SIGNAL(sectionClicked(int)), this, SLOT(tableSectionClicked(int)));
    setLayout(layout);
    //adjustSize();
}

void SceneDialog::tableSectionClicked(int index)
{
    if (index == 0) {
        sortByName();
        updateTable();
    } else if (index == 1) {
        sortByDate();
        updateTable();
    }
}

bool compName(SceneRecord *sc1, SceneRecord *sc2)
{
    return sc1->getName() <= sc2->getName();
}

bool compNameRev(SceneRecord *sc1, SceneRecord *sc2)
{
    return sc2->getName() <= sc1->getName();
}

bool compDate(SceneRecord *sc1, SceneRecord *sc2)
{
    return sc1->getModifyDate() <= sc2->getModifyDate();
}

bool compDateRev(SceneRecord *sc1, SceneRecord *sc2)
{
    return sc2->getModifyDate() <= sc1->getModifyDate();
}

void SceneDialog::sortByName()
{
    if (sorted == 0) {
        sort(records.begin(),records.end(),compNameRev);
        sorted = 1;
    } else {
        sort(records.begin(),records.end(),compName);
        sorted = 0;
    }
}

void SceneDialog::updateTable()
{
    for (int i = 0; i < records.size(); ++i) {
        QLabel *name = dynamic_cast<QLabel*>(table->cellWidget(i,0));
        QLabel *cdate = dynamic_cast<QLabel*>(table->cellWidget(i,1));
        name->setText(QString::fromStdString(records[i]->getName()));
        cdate->setText(QString::fromStdString(dateToString(records[i]->getModifyDate())));
    }
}

void SceneDialog::sortByDate()
{
    if (sorted == 2) {
        sort(records.begin(),records.end(),compDateRev);
        sorted = 3;
    } else {
        sort(records.begin(),records.end(),compDate);
        sorted = 2;
    }
}

SceneDialog::~SceneDialog()
{
    delete ui;
}

void SceneDialog::loadRecord(int i)
{
    load(QString::fromStdString(records[i]->getName()));
}

void SceneDialog::deleteRecord(int i)
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Delete scene", "Are you sure you want to delete this scene?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        if (sceneManager->deleteRecord(records[i]->getName())) {
            table->removeRow(i);
            for (int j = i; j+1 < records.size(); ++j) records[j] = records[j+1];
            records.resize(records.size()-1);
            updateTable();
        } else {
            LOG<<ERROR<<"Could not delete records. It doesn't exist."<<endl<<NORMAL;
        }
    }
}

void SceneDialog::closeDialog()
{
    close();
}
