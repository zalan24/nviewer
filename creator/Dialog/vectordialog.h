#ifndef VECTORDIALOG_H
#define VECTORDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QTextEdit>
#include <QGridLayout>
#include <QScrollArea>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QVector>

#include <vector>

#include "common/Math/include/tools.h"
#include "common/Math/include/info.h"

#define MAX_DIM 10

namespace Ui {
class VectorDialog;
}

class VectorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VectorDialog(std::vector<RTYPE> &vec, QWidget *parent = 0);
    ~VectorDialog();

    void accept();

public slots:
    void set();
    void updateSet();
    void updateSpin();
    void setZero();

signals:
    void sendVector(std::vector<RTYPE>);

private:
    Ui::VectorDialog *ui;
    std::vector<RTYPE> vec;
    QSpinBox *customid;
    QDoubleSpinBox *customval;
    QVector<QDoubleSpinBox*> spins;
};

#endif // VECTORDIALOG_H
