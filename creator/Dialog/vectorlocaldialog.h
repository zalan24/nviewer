#ifndef VECTORLOCALDIALOG_H
#define VECTORLOCALDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QTextEdit>
#include <QGridLayout>
#include <QScrollArea>
#include <QDoubleSpinBox>
#include <QSpinBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QVector>
#include <QCheckBox>

#include <vector>

#include "common/Math/include/tools.h"
#include "common/Math/include/info.h"

#define MAX_DIM 10

namespace Ui {
class VectorLocalDialog;
}

class VectorLocalDialog : public QDialog
{
    Q_OBJECT

public:
    explicit VectorLocalDialog(std::vector<RTYPE> &vec, bool checked, QWidget *parent = 0);
    ~VectorLocalDialog();

    void accept();

public slots:
    void set();
    void updateSet();
    void updateSpin();
    void setZero();

signals:
    void sendData(std::vector<RTYPE>, bool);

private:
    Ui::VectorLocalDialog *ui;
    std::vector<RTYPE> vec;
    QSpinBox *customid;
    QDoubleSpinBox *customval;
    QVector<QDoubleSpinBox*> spins;
    QCheckBox *checkbox;
};

#endif // VECTORLOCALDIALOG_H
