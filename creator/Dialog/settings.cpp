#include "settings.h"
#include "ui_settings.h"

#include <QLabel>
#include <QTabWidget>
#include <QVBoxLayout>

Settings::Settings(MachineConfig *cfg, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    system = new SystemSettings(cfg);
    input = new InputSettings(cfg);
    QTabWidget *tab = new QTabWidget();
    tab->addTab(system, "System");
    tab->addTab(input, "Input");
    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(tab);
    layout->addWidget(ui->buttonBox);
    setLayout(layout);
}

void Settings::keyPressEvent(QKeyEvent *event)
{
    input->KeyPressEvent(event);
}

void Settings::keyReleaseEvent(QKeyEvent *event)
{
    input->KeyReleaseEvent(event);
}

Settings::~Settings()
{
    delete ui;
}

void Settings::accept()
{
    system->save();
    input->save();
    save();
    close();
}
