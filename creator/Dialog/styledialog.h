#ifndef STYLEDIALOG_H
#define STYLEDIALOG_H

#include "common/Helper/stylemanager.h"
#include "common/Math/include/tools.h"

#include "creator/Dialog/stylesheeteditdialog.h"

#include <QDialog>
#include <QGridLayout>
#include <QDoubleSpinBox>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QTableWidget>
#include <QSignalMapper>
#include <QPushButton>
#include <QMessageBox>
#include <QLabel>
#include <vector>
#include <algorithm>

namespace Ui {
class StyleDialog;
}

bool compName(StyleSheet *sc1, StyleSheet *sc2);
bool compNameRev(StyleSheet *sc1, StyleSheet *sc2);
bool compDateM(StyleSheet *sc1, StyleSheet *sc2);
bool compDateMRev(StyleSheet *sc1, StyleSheet *sc2);
bool compDateC(StyleSheet *sc1, StyleSheet *sc2);
bool compDateCRev(StyleSheet *sc1, StyleSheet *sc2);

class StyleDialog : public QDialog
{
    Q_OBJECT

public:
    explicit StyleDialog(StyleManager *styleManager, QWidget *parent = 0);
    ~StyleDialog();

protected slots:
    void loadRecord(int i);
    void deleteRecord(int i);
    void edit(int i);
    void closeDialog();
    void tableSectionClicked(int index);
    void save();

signals:
    void load(QString name);

private:
    Ui::StyleDialog *ui;
    StyleManager *styleManager;
    vector<StyleSheet*> records;
    QTableWidget *table;
    int sorted;
    QSignalMapper *loadSignal;
    QSignalMapper *deleteSignal;
    QSignalMapper *editSignal;
    StyleSheetEditDialog *styledialog;

    void sortByName();
    void sortByCreationDate();
    void sortByModificationDate();
    void updateTable();
};

#endif // STYLEDIALOG_H
