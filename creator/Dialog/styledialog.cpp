#include "styledialog.h"
#include "ui_styledialog.h"


StyleDialog::StyleDialog(StyleManager *styleManager, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StyleDialog),
    styleManager(styleManager),
    sorted(-1),
    styledialog(NULL)
{
    ui->setupUi(this);
    loadSignal = new QSignalMapper(this);
    deleteSignal = new QSignalMapper(this);
    editSignal = new QSignalMapper(this);
    connect(loadSignal, SIGNAL(mapped(int)), this, SLOT(loadRecord(int)));
    connect(deleteSignal, SIGNAL(mapped(int)), this, SLOT(deleteRecord(int)));
    connect(editSignal, SIGNAL(mapped(int)), this, SLOT(edit(int)));

    QVBoxLayout *layout = new QVBoxLayout;
    //QGridLayout *sLayout = new QGridLayout;
    for (int i = 0; i < styleManager->size(); ++i) if (styleManager->getStyleSheet(i)->getName() != "") records.push_back(styleManager->getStyleSheet(i));
    sortByName();
    table = new QTableWidget(records.size(), 6);
    for (int i = 0; i < records.size(); ++i) {
        QLabel *name = new QLabel(QString::fromStdString(records[i]->getName()));
        QLabel *cdate = new QLabel(QString::fromStdString(dateToString(records[i]->getCreation())));
        QLabel *mdate = new QLabel(QString::fromStdString(dateToString(records[i]->getModification())));
        QPushButton *del = new QPushButton("Delete");
        QPushButton *loa = new QPushButton("Load");
        QPushButton *edi = new QPushButton("Edit");
        connect(loa, SIGNAL(clicked(bool)), loadSignal, SLOT(map()));
        connect(del, SIGNAL(clicked(bool)), deleteSignal, SLOT(map()));
        connect(edi, SIGNAL(clicked(bool)), editSignal, SLOT(map()));
        deleteSignal->setMapping(del, i);
        loadSignal->setMapping(loa, i);
        editSignal->setMapping(edi, i);
        table->setCellWidget(i,0,name);
        table->setCellWidget(i,1,cdate);
        table->setCellWidget(i,2,mdate);
        table->setCellWidget(i,3,loa);
        table->setCellWidget(i,4,edi);
        table->setCellWidget(i,5,del);
        cdate->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Preferred);
        mdate->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Preferred);
        del->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
        loa->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
        edi->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    }
    //table->setFixedWidth(table->horizontalHeader()->width());
    table->resizeRowsToContents();
    table->resizeColumnsToContents();
    table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    table->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    QStringList hlabels;
    hlabels.push_back("Id");
    hlabels.push_back("Created");
    hlabels.push_back("Modified");
    hlabels.push_back("");
    hlabels.push_back("");
    hlabels.push_back("");
    table->setHorizontalHeaderLabels(hlabels);

    //table->setSizePolicy (QSizePolicy::Minimum, QSizePolicy::Ignored);
    layout->addWidget(table);
    QPushButton *closeb = new QPushButton("Close");
    layout->addWidget(closeb,0,Qt::AlignRight);
    connect(closeb,SIGNAL(clicked(bool)),this,SLOT(closeDialog()));
    connect(table->horizontalHeader(), SIGNAL(sectionClicked(int)), this, SLOT(tableSectionClicked(int)));
    setLayout(layout);
    //adjustSize();
}

void StyleDialog::edit(int i)
{
    if (styledialog != NULL) delete styledialog;
    styledialog = NULL;
    StyleSheet *s = styleManager->getStyleSheet(records[i]->getName());
    //s = styleManager->getStyleSheet(i);
    styledialog = new StyleSheetEditDialog(s);
    styledialog->setWindowTitle("Edit");
    styledialog->setModal(true);
    styledialog->show();
    connect(styledialog, SIGNAL(save()), this, SLOT(save()));
}

void StyleDialog::save()
{
    styleManager->save();
    updateTable();
}

void StyleDialog::tableSectionClicked(int index)
{
    if (index == 0) {
        sortByName();
        updateTable();
    } else if (index == 1) {
        sortByCreationDate();
        updateTable();
    } else if (index == 2) {
        sortByModificationDate();
        updateTable();
    }
}

bool compName(StyleSheet *sc1, StyleSheet *sc2)
{
    return sc1->getName() <= sc2->getName();
}

bool compNameRev(StyleSheet *sc1, StyleSheet *sc2)
{
    return sc2->getName() <= sc1->getName();
}

bool compDateM(StyleSheet *sc1, StyleSheet *sc2)
{
    return sc1->getModification() <= sc2->getModification();
}

bool compDateMRev(StyleSheet *sc1, StyleSheet *sc2)
{
    return sc2->getModification() <= sc1->getModification();
}

bool compDateC(StyleSheet *sc1, StyleSheet *sc2)
{
    return sc1->getCreation() <= sc2->getCreation();
}

bool compDateCRev(StyleSheet *sc1, StyleSheet *sc2)
{
    return sc2->getCreation() <= sc1->getCreation();
}

void StyleDialog::sortByName()
{
    if (sorted == 0) {
        sort(records.begin(),records.end(),compNameRev);
        sorted = 1;
    } else {
        sort(records.begin(),records.end(),compName);
        sorted = 0;
    }
}

void StyleDialog::updateTable()
{
    for (int i = 0; i < records.size(); ++i) {
        QLabel *name = dynamic_cast<QLabel*>(table->cellWidget(i,0));
        QLabel *cdate = dynamic_cast<QLabel*>(table->cellWidget(i,1));
        QLabel *mdate = dynamic_cast<QLabel*>(table->cellWidget(i,2));
        name->setText(QString::fromStdString(records[i]->getName()));
        cdate->setText(QString::fromStdString(dateToString(records[i]->getCreation())));
        mdate->setText(QString::fromStdString(dateToString(records[i]->getModification())));
    }
}

void StyleDialog::sortByCreationDate()
{
    if (sorted == 2) {
        sort(records.begin(),records.end(),compDateCRev);
        sorted = 3;
    } else {
        sort(records.begin(),records.end(),compDateC);
        sorted = 2;
    }
}

void StyleDialog::sortByModificationDate()
{
    if (sorted == 4) {
        sort(records.begin(),records.end(),compDateMRev);
        sorted = 5;
    } else {
        sort(records.begin(),records.end(),compDateM);
        sorted = 4;
    }
}

StyleDialog::~StyleDialog()
{
    if (styledialog != NULL) delete styledialog;
    delete ui;
    /*delete loadSignal;
    delete deleteSignal;
    delete editSignal;*/
}

void StyleDialog::loadRecord(int i)
{
    load(QString::fromStdString(records[i]->getName()));
}

void StyleDialog::deleteRecord(int i)
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Delete StyleSheet", "Are you sure you want to delete this StyleSheet?", QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        styleManager->removeStyleSheet(records[i]->getName());
        table->removeRow(i);
        for (int j = i; j+1 < records.size(); ++j) records[j] = records[j+1];
        records.resize(records.size()-1);
        updateTable();
    }
}

void StyleDialog::closeDialog()
{
    close();
}

