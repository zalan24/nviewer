#include "cameraviewdialog.h"
#include "ui_cameraviewdialog.h"


CameraViewDialog::CameraViewDialog(Camera<RTYPE> *cam, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CameraViewDialog),
    cam(cam)
{
    ui->setupUi(this);

    double mx = std::numeric_limits<double>::max();
    double mn = std::numeric_limits<double>::min();

    QGridLayout *layout = new QGridLayout;

    scale = new QDoubleSpinBox;
    angle = new QSpinBox;
    focus = new QDoubleSpinBox;

    scale->setMinimum(mn);
    scale->setMaximum(mx);
    scale->setDecimals(DOUBLESPINBOX_DECIMALS);
    scale->setSingleStep(DOUBLESPINBOX_STEP);
    scale->setValue(cam->getScale());
    angle->setMinimum(1);
    angle->setMaximum(170);
    angle->setValue(cam->getView()*180/M_PI);
    focus->setMinimum(mn);
    focus->setMaximum(mx);
    focus->setDecimals(DOUBLESPINBOX_DECIMALS);
    focus->setSingleStep(DOUBLESPINBOX_STEP);
    focus->setValue(cam->getFocus());

    QLabel *scaleL = new QLabel("Scale: ");
    QLabel *focusL = new QLabel("Focus: ");
    QLabel *angleL = new QLabel("View angle: ");

    layout->addWidget(scaleL,0,0);
    layout->addWidget(scale,0,1);
    layout->addWidget(focusL,1,0);
    layout->addWidget(focus,1,1);
    layout->addWidget(angleL,2,0);
    layout->addWidget(angle,2,1);

    scale->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    angle->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    focus->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    scaleL->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    focusL->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    angleL->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);


    layout->addWidget(ui->buttonBox,3,1,1,2);
    setLayout(layout);

    layout->setSizeConstraint(QLayout::SetFixedSize);

}

void CameraViewDialog::accept()
{
    sendData(scale->value(), focus->value(), ((RTYPE)angle->value())*M_PI/180);
    close();
}

CameraViewDialog::~CameraViewDialog()
{
    delete ui;
}
