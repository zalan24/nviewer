#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QListWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>
#include <QSpinBox>
#include "common/Helper/pythonmanager.h"
#include "creator/Widgets/searchlist.h"
#include "common/Helper/scenemanager.h"
#include "common/Helper/stylemanager.h"
#include "common/Math/include/session.h"

#include <map>

using namespace std;

#define NUM_SEARCH_ITEMS 100

namespace Ui {
class ExportDialog;
}

class ExportDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ExportDialog(Session *session, SceneManager *scene, StyleManager *style, QWidget *parent = 0);
    ~ExportDialog();

protected slots:
    void addItems();
    void removeItems();
    void closeme();
    void exportfile();
    void exportimage();

private:
    Ui::ExportDialog *ui;
    SearchList *sceneList;
    SearchList *styleList;
    SearchList *exportList;
    SceneManager *scene;
    StyleManager *style;
    map<string,string> scenemap;
    map<string,string> stylemap;
    Session *session;
    bool changes;
    QSpinBox *numServers;

    string getFileFromPath(string path) const;
};

#endif // EXPORTDIALOG_H
