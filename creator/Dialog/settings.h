#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QKeyEvent>

#include "creator/Widgets/systemsettings.h"
#include "creator/Widgets/inputsettings.h"
#include "common/Math/include/config.h"

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(MachineConfig *cfg, QWidget *parent = 0);
    ~Settings();

    void accept();

signals:
    void save();

protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

private:
    Ui::Settings *ui;
    SystemSettings *system;
    InputSettings *input;
};

#endif // SETTINGS_H
