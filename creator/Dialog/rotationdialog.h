#ifndef ROTATIONDIALOG_H
#define ROTATIONDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QGridLayout>

#include "common/Math/include/info.h"

namespace Ui {
class RotationDialog;
}

class RotationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit RotationDialog(int dim, bool foc, QWidget *parent = 0);
    ~RotationDialog();

    void accept();

signals:
    void sendData(double angle, int x, int y, bool loc, bool foc);

private:
    Ui::RotationDialog *ui;
    const int dim;
    QSpinBox *xaxis;
    QSpinBox *yaxis;
    QDoubleSpinBox *angle;
    QCheckBox *local;
    QCheckBox *aroundfocus;
};

#endif // ROTATIONDIALOG_H
