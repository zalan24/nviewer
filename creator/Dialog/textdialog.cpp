#include "textdialog.h"
#include "ui_textdialog.h"

#include <QTime>

TextDialog::TextDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextDialog)
{
    ui->setupUi(this);
    QVBoxLayout *layout = new QVBoxLayout;

    text = new QLineEdit;
    msg = new QLabel;
    layout->addWidget(text);
    layout->addWidget(msg);

    text->setFocus();
    layout->addWidget(ui->buttonBox);
    setLayout(layout);
}

void TextDialog::setMessage(QString ms)
{
    msg->setText(ms);
}

void TextDialog::buzz()
{
    int xp = x();
    int yp = y();
    QTime t;

    t.start();
    for (int i = 32; i > 0;) {
        QApplication::processEvents();
        if (t.elapsed() >= 2) {
            int delta = i >> 2;
            int dir = i & 3;
            int dx = (dir == 1 || dir == 2) ? delta : -delta;
            int dy = (dir < 2) ? delta : -delta;
            move(xp+dx,yp+dy);
            t.restart();
            i--;
        }
    }
    move(xp,yp);
}

void TextDialog::accept()
{
    acceptText(text->text());
    //close();      //closed from manager.cpp
}

TextDialog::~TextDialog()
{
    delete ui;
}
