#include "transformdialog.h"
#include "ui_transformdialog.h"

#include <QTableWidget>
#include <QSignalMapper>

TransformDialog::TransformDialog(Matrix<RTYPE> m, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TransformDialog),
    transform(m)
{

    ui->setupUi(this);
    QSignalMapper *signalMapper = new QSignalMapper(this);
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(changeItem(int)));
    double mx = std::numeric_limits<double>::max();

    QVBoxLayout *layout = new QVBoxLayout;
    //QGridLayout *sLayout = new QGridLayout;
    QTableWidget *table = new QTableWidget(transform.getRows(), transform.getColoumns());
    for (int y = 0; y < transform.getColoumns(); ++y) {
        for (int x = 0; x < transform.getRows(); ++x) {
            QDoubleSpinBox *b = new QDoubleSpinBox;
            b->setMinimum(-mx);
            b->setMaximum(mx);
            b->setDecimals(DOUBLESPINBOX_DECIMALS);
            b->setSingleStep(DOUBLESPINBOX_STEP);
            b->setValue(m.get(x,y));
            connect(b, SIGNAL(valueChanged(double)), signalMapper, SLOT(map()));
            signalMapper->setMapping(b, boxes.size());
            //sLayout->addWidget(b,x,y);
            table->setCellWidget(x,y,b);
            boxes.push_back(b);
        }
    }
    layout->addWidget(table);
    layout->addWidget(ui->buttonBox);
    setLayout(layout);
}

void TransformDialog::changeItem(int i)
{
    int x,y;
    x = i % transform.getColoumns();
    y = i / transform.getColoumns();
    //LOG<<i<<" "<<x<<" "<<y<<endl;

    transform.set(x,y,boxes[i]->value());
}

TransformDialog::~TransformDialog()
{
    delete ui;
}

void TransformDialog::accept()
{
    sendData(transform);
    close();
}
