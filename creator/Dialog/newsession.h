#ifndef NEWSESSION_H
#define NEWSESSION_H

#include <QDialog>
#include <QFileDialog>
#include <QLabel>
#include <QGridLayout>
#include <QLineEdit>
#include <QSpinBox>
#include <QPushButton>

#include "common/Math/include/info.h"

namespace Ui {
class NewSession;
}

class NewSession : public QDialog
{
    Q_OBJECT

public:
    explicit NewSession(QWidget *parent = 0);
    ~NewSession();

    void accept();
    void setError(const QString s);
    void buzz();

public slots:
    void openFile();
    void receiveFile(const QString file);

signals:
    void open(QString name, int dim, QString file);

private:
    Ui::NewSession *ui;
    QFileDialog *filedialog;
    QLineEdit *namedit;
    QSpinBox *box;
    QPushButton *fileselection;
    QString fileName;
    QLabel *errorLabel;

    QString getShorterName(const QString s) const;
};

#endif // NEWSESSION_H
