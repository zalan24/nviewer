#include "stylesheeteditdialog.h"
#include "ui_stylesheeteditdialog.h"
#include <QVBoxLayout>

StyleSheetEditDialog::StyleSheetEditDialog(StyleSheet *styleSheet, QWidget *parent) :
    QDialog(parent),
    styleSheet(styleSheet),
    ui(new Ui::StyleSheetEditDialog)
{
    QVBoxLayout *layout = new QVBoxLayout;
    ui->setupUi(this);

    styleEdit = new StyleEditWidget(styleSheet);

    layout->addWidget(styleEdit);
    layout->addWidget(ui->buttonBox);
    setLayout(layout);
}

StyleSheetEditDialog::~StyleSheetEditDialog()
{
    delete ui;
}

void StyleSheetEditDialog::accept()
{
    styleEdit->save();
    save();
    close();
}
