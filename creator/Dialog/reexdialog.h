#ifndef REEXDIALOG_H
#define REEXDIALOG_H

#include <QDialog>

namespace Ui {
class ReExDialog;
}

class ReExDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ReExDialog(QString error, QWidget *parent = 0);
    ~ReExDialog();
    void closeEvent(QCloseEvent *event);
    void accept();
    void reject();
    void setText(const QString);
    void buzz();

public slots:
    void quit();
    void retry();

signals:
    void onquit();
    void onretry();

private:
    Ui::ReExDialog *ui;
};

#endif // REEXDIALOG_H
