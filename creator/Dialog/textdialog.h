#ifndef TEXTDIALOG_H
#define TEXTDIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QLabel>
#include <QVBoxLayout>

namespace Ui {
class TextDialog;
}

class TextDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TextDialog(QWidget *parent = 0);
    ~TextDialog();

    void accept();
    void setMessage(QString ms);
    void buzz();

signals:
    void acceptText(QString);

private:
    Ui::TextDialog *ui;
    QLineEdit *text;
    QLabel *msg;
};

#endif // TEXTDIALOG_H
