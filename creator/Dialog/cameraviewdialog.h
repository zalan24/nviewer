#ifndef CAMERAVIEWDIALOG_H
#define CAMERAVIEWDIALOG_H

#include <QDialog>
#include <QLabel>
#include <QGridLayout>
#include <QDoubleSpinBox>
#include <QSpinBox>

#include "common/Math/include/render.h"
#include "common/Math/include/info.h"


namespace Ui {
class CameraViewDialog;
}

class CameraViewDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CameraViewDialog(Camera<RTYPE> *cam, QWidget *parent = 0);
    ~CameraViewDialog();

    void accept();

signals:
    void sendData(RTYPE, RTYPE, RTYPE);

private:
    Ui::CameraViewDialog *ui;
    Camera<RTYPE> *cam;
    QSpinBox *angle;
    QDoubleSpinBox *focus;
    QDoubleSpinBox *scale;
};

#endif // CAMERAVIEWDIALOG_H
