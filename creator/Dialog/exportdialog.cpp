#include "exportdialog.h"
#include "ui_exportdialog.h"

ExportDialog::ExportDialog(Session *session, SceneManager *scene, StyleManager *style, QWidget *parent) :
    session(session),
    QDialog(parent),
    scene(scene),
    style(style),
    ui(new Ui::ExportDialog),
    changes(false)
{
    ui->setupUi(this);

    numServers = new QSpinBox;
    numServers->setMinimum(1);
    numServers->setMaximum(100000000);

    QVBoxLayout *vlayout = new QVBoxLayout;
    vlayout->addWidget(numServers);

    QHBoxLayout *layout = new QHBoxLayout;

    sceneList = new SearchList(NUM_SEARCH_ITEMS);
    styleList = new SearchList(NUM_SEARCH_ITEMS);
    exportList = new SearchList(NUM_SEARCH_ITEMS);

    layout->addWidget(sceneList);
    layout->addWidget(styleList);

    for (int i = 0; i < scene->size(); ++i) {
        sceneList->addElements(QString::fromStdString(scene->getRecord(i)->getName()));
    }
    sceneList->update();

    for (int i = 0; i < style->size(); ++i) {
        styleList->addElements(QString::fromStdString(style->getStyleSheet(i)->getName()));
    }
    styleList->update();

    QVBoxLayout *buttonlayout = new QVBoxLayout;

    QPushButton *button = new QPushButton(">>");
    buttonlayout->addWidget(button,0,Qt::AlignBottom);
    QPushButton *remove = new QPushButton("<<");
    buttonlayout->addWidget(remove,0,Qt::AlignTop);


    layout->addLayout(buttonlayout);
    layout->addWidget(exportList);

    vlayout->addLayout(layout);

    QPushButton *closeb = new QPushButton("close");
    QPushButton *file = new QPushButton("Export");
    QPushButton *image = new QPushButton("Export image");

    QHBoxLayout *layout2 = new QHBoxLayout;
    layout2->addWidget(closeb, 0, Qt::AlignRight);
    layout2->addWidget(file, 0, Qt::AlignRight);
    layout2->addWidget(image, 0, Qt::AlignRight);
    closeb->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    file->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    image->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    layout2->setAlignment(Qt::AlignRight);

    vlayout->addLayout(layout2);

    setLayout(vlayout);

    connect(button, SIGNAL(clicked(bool)), this, SLOT(addItems()));
    connect(remove, SIGNAL(clicked(bool)), this, SLOT(removeItems()));
    connect(closeb, SIGNAL(clicked(bool)), this, SLOT(closeme()));
    connect(file, SIGNAL(clicked(bool)), this, SLOT(exportfile()));
    connect(image, SIGNAL(clicked(bool)), this, SLOT(exportimage()));
}

void ExportDialog::closeme()
{
    if (exportList->size() != 0 && changes) {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Exit", "Do you really want to close the window without exporting the selected configuration?", QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes) {
            close();
        } else {
            return;
        }
    } else close();
}

string ExportDialog::getFileFromPath(string path) const
{
    int n = path.length()-1;
    while (n >= 0 && path[n] != '/' && path[n] != '\\') n--;
    return path.substr(n+1, -1);
}

void ExportDialog::exportfile()
{
    int numservers = numServers->value();
    string file = QFileDialog::getSaveFileName(0,"Export","","*.zip").toStdString();
    if (file.length() >= 4 && file.substr(file.length()-4, -1) == ".zip") file = file.substr(0, file.length()-4);

    QStringList l = exportList->getItems();
    if (l.size() < numservers) numservers = l.size();

    vector<string> files;
    session->getFunction()->collectFiles(files);

    for (int s = 0; s < numservers; ++s) {
        PM.createTemp(TEMP_FILE);
        PM.createScripts(TEMP_FILE);

        for (int i = 0; i < files.size(); ++i) {
            string f = files[i];
            PM.copyScript(f, TEMP_FILE);
        }
        PM.copyFile(style->getFileName(), TEMP_FILE);
        PM.copyFile(scene->getFilename(), TEMP_FILE);
        PM.copyFile(session->getFile()->getFilename(), TEMP_FILE);


        stringstream ss;
        if (numservers > 1) ss<<"_"<<s;
        string filename = file + ss.str() + ".zip";
        ofstream exportdata((string(TEMP_FILE) + "/data.nvwexport").c_str());
        string mainfile = "";
        string scenefile = "";
        string stylefile = "";
        string sessionfile = "";
        mainfile = getFileFromPath(files[0]);
        stylefile = getFileFromPath(style->getFileName());
        scenefile = getFileFromPath(scene->getFilename());
        sessionfile = getFileFromPath(session->getFile()->getFilename());
        exportdata<<mainfile<<endl;
        exportdata<<stylefile<<endl;
        exportdata<<scenefile<<endl;
        exportdata<<sessionfile<<endl;

        for (int i = s; i < l.size(); i+=numservers) exportdata<<l[i].toStdString()<<endl;

        exportdata.close();

        PM.zipDir(TEMP_FILE, filename);
        PM.deleteDir(TEMP_FILE);
        LOG<<"Raw data exported: "<<filename<<endl;
    }
    changes = false;
}

void ExportDialog::exportimage()
{

}

ExportDialog::~ExportDialog()
{
    delete ui;
}

void ExportDialog::removeItems()
{
    exportList->removeSelected();
    changes = true;
}

void ExportDialog::addItems()
{
    changes = true;
    QStringList sscene, sstyle;
    sceneList->getSelectedItems(sscene);
    styleList->getSelectedItems(sstyle);
    if (sscene.size() == 0) QMessageBox::warning(this, "Error", "Select scenes to export");
    else if (sstyle.size() == 0) QMessageBox::warning(this, "Error", "Select styles to export");
    else {
        for (int i = 0; i < sscene.size(); ++i) {
            for (int j = 0; j <  sstyle.size(); ++j) {
                QString s = sscene[i] + " : " + sstyle[j];
                if (!exportList->isRecord(s)) {
                    scenemap[s.toStdString()] = sscene[i].toStdString();
                    stylemap[s.toStdString()] = sstyle[j].toStdString();
                    exportList->addElements(s);
                }
                //LOG<<"export: "<<sscene[i].toStdString()<<"  -  "<<sstyle[j].toStdString()<<endl;
            }
        }
        exportList->update();
    }
}
