#include "newsession.h"
#include "ui_newsession.h"

#include <QTime>

NewSession::NewSession(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewSession),
    filedialog(NULL),
    fileName("")
{
    ui->setupUi(this);
    QGridLayout *layout = new QGridLayout();
    QLabel *name = new QLabel("Name: ");
    QLabel *dim = new QLabel("Dimensions: ");
    QLabel *file = new QLabel("NViewer script: ");
    errorLabel = new QLabel;
    errorLabel->setWordWrap(true);
    errorLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    layout->addWidget(errorLabel,0,0,1,2);
    layout->addWidget(name,1,0);
    layout->addWidget(dim,2,0);
    layout->addWidget(file,3,0);

    namedit = new QLineEdit;
    box = new QSpinBox();
    box->setMinimum(3);
    box->setMaximum(0xffffff);      //3 bytes
    box->setValue(3);
    fileselection = new QPushButton("Selection new file");

    /*name->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    dim->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    file->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    namedit->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    box->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    fileselection->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    namedit->setAlignment(Qt::AlignRight);
    box->setAlignment(Qt::AlignRight);*/
    //fileselection->setAlignment(Qt::AlignRight);

    layout->addWidget(namedit,1,1);
    layout->addWidget(box,2,1);
    layout->addWidget(fileselection,3,1);

    layout->addWidget(ui->buttonBox,4,1,1,1,Qt::AlignRight);
    setLayout(layout);

    connect(fileselection, SIGNAL(clicked()), this, SLOT(openFile()));
}

void NewSession::setError(const QString s)
{
    errorLabel->setText("<font color='red'>" + s + "</font>");
    buzz();
}

NewSession::~NewSession()
{
    if (filedialog != NULL) delete filedialog;
    delete ui;
}

void NewSession::openFile()
{
    if (filedialog != NULL){
        if (filedialog->isActiveWindow()) filedialog->close();
        delete filedialog;
    }
    QString f = tr("*.nvw");
    filedialog = new QFileDialog(0, "Open file", "", f);
    filedialog->setAcceptMode(QFileDialog::AcceptOpen);
    filedialog->setFileMode(QFileDialog::ExistingFile);
    filedialog->show();
    //filedialog->setFilter( (tr("*.nvw"));
    //filedialog->setNameFilter(f);
    //filedialog->setDefaultSuffix(".nvw");
    connect(filedialog, SIGNAL(fileSelected(QString)),this,SLOT(receiveFile(const QString)));
}

void NewSession::receiveFile(const QString file)
{
    fileName = file;
    fileselection->setText(getShorterName(file));
}

QString NewSession::getShorterName(const QString s) const
{
    const int size = 25;
    if (s.size() < size+3) return s;
    return "..." + QString::fromStdString(s.toStdString().substr(s.size()-size,-1));
}

void NewSession::accept()
{
    open(namedit->text(),box->value(),fileName);
}

void NewSession::buzz()
{
    int xp = x();
    int yp = y();
    QTime t;

    t.start();
    for (int i = 32; i > 0;) {
        QApplication::processEvents();
        if (t.elapsed() >= 2) {
            int delta = i >> 2;
            int dir = i & 3;
            int dx = (dir == 1 || dir == 2) ? delta : -delta;
            int dy = (dir < 2) ? delta : -delta;
            move(xp+dx,yp+dy);
            t.restart();
            i--;
        }
    }
    move(xp,yp);
}
