#include "reexdialog.h"
#include "ui_reexdialog.h"

#include <iostream>
#include <QTime>

ReExDialog::ReExDialog(QString error, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ReExDialog)
{
    ui->setupUi(this);
    ui->message->setText(error);

    connect(ui->quit,SIGNAL(clicked()),this,SLOT(quit()));
    connect(ui->retry,SIGNAL(clicked()),this,SLOT(retry()));

    ui->retry->setFocus();
}

void ReExDialog::buzz()
{
    int xp = x();
    int yp = y();
    QTime t;

    t.start();
    for (int i = 32; i > 0;) {
        QApplication::processEvents();
        if (t.elapsed() >= 2) {
            int delta = i >> 2;
            int dir = i & 3;
            int dx = (dir == 1 || dir == 2) ? delta : -delta;
            int dy = (dir < 2) ? delta : -delta;
            move(xp+dx,yp+dy);
            t.restart();
            i--;
        }
    }
    move(xp,yp);
}

void ReExDialog::setText(const QString t)
{
    ui->message->setText(t);
}

ReExDialog::~ReExDialog()
{
    delete ui;
}

void ReExDialog::closeEvent(QCloseEvent *)
{
    onquit();
}

void ReExDialog::quit()
{
    close();
}

void ReExDialog::retry()
{
    onretry();
}

void ReExDialog::accept()
{
    close();
}

void ReExDialog::reject()
{
    close();
}
