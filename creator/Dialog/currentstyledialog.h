#ifndef CURRENTSTYLEDIALOG_H
#define CURRENTSTYLEDIALOG_H

#include <QDialog>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QHBoxLayout>

#include "creator/Widgets/styleeditwidget.h"

#include "common/Helper/stylemanager.h"

namespace Ui {
class CurrentStyleDialog;
}

class CurrentStyleDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CurrentStyleDialog(const StyleSheet &ss, const StyleSheet &param, QWidget *parent = 0);
    ~CurrentStyleDialog();

protected slots:
    void cancel();
    void ok();
    void apply();

signals:
    void updateStyle(StyleSheet *ss, StyleSheet *ps);

private:
    Ui::CurrentStyleDialog *ui;
    StyleEditWidget *styleWidget;
    StyleEditWidget *paramWidget;
    StyleSheet style;
    StyleSheet param;
};

#endif // CURRENTSTYLEDIALOG_H
