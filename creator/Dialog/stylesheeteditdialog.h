#ifndef STYLESHEETEDITDIALOG_H
#define STYLESHEETEDITDIALOG_H

#include <QDialog>

#include "common/Helper/stylemanager.h"
#include "creator/Widgets/styleeditwidget.h"

namespace Ui {
class StyleSheetEditDialog;
}

class StyleSheetEditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit StyleSheetEditDialog(StyleSheet *styleSheet, QWidget *parent = 0);
    ~StyleSheetEditDialog();

    void accept();

signals:
    void save();

private:
    Ui::StyleSheetEditDialog *ui;
    StyleSheet *styleSheet;
    StyleEditWidget *styleEdit;
};

#endif // STYLESHEETEDITDIALOG_H
