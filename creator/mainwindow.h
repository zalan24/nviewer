#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <QMenu>
#include <Python.h>

#include "creator/Helper/logger.h"
#include "creator/Helper/manager.h"

#include "creator/Widgets/displaywidget.h"
#include "creator/Widgets/vramwidget.h"
#include "creator/Widgets/cameracontroller.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void quit();
    void settings();
    void newSession();
    void openSession();
    void openScript();
    void reloadScript();
    void startDisplay();
    void stopDisplay();
    void nextStep();
    void prevStep();
    void pauseDisplay();
    void resumeDisplay();
    void setCamPosition();
    void setCamDirection();
    void setCamView();
    void scaleCamera();
    void setTransform();
    void translate();
    void rotate();
    void newCollection();
    void openCollection();
    void capture();
    void loadScene();
    void manageScenes();
    void newStyleFile();
    void saveStyleFile();
    void saveStyleFileAs();
    void openStyleFile();
    void saveStyle();
    void openStyleByName();
    void manageStyles();
    void setCurrentStyle();
    void exportRaw();
    void exportImage();

protected:
    //void closeEvent(QCloseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

protected slots:
    void updateMenu();

private:
    Ui::MainWindow *ui;
    Logger *log;
    DisplayWidget *display;
    Manager *manager;
    QMenu *File_Open_Recent;
    QMenu *Scene_Open_Recent;
    QMenu *Style_Open_Recent;
    VRamWidget *scriptVRam;
    VRamWidget *runtimeVRam;
};

#endif // MAINWINDOW_H
