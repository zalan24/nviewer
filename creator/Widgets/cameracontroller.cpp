#include "cameracontroller.h"

#include <iostream>
#include <sstream>

using namespace std;

CameraController::CameraController(Matrix<RTYPE> *m, RTYPE sc, RTYPE fc, QWidget *parent) :
    QWidget(parent),
    transform(m),
    camscale(sc),
    camfocus(fc),
    lockchange(false),
    transformcp(NULL)
{

    double mx = std::numeric_limits<double>::max();
    double mn = std::numeric_limits<double>::min();

    QVBoxLayout *layout = new QVBoxLayout;
    campos = new QLabel;
    camdir = new QLabel;
    campos->setAlignment(Qt::AlignTop);
    camdir->setAlignment(Qt::AlignTop);
    campos->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Preferred);
    camdir->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Preferred);
    layout->addWidget(campos);
    layout->addWidget(camdir);

    QGridLayout *controls = new QGridLayout;

    //QHBoxLayout *focuslayout = new QHBoxLayout;
    QLabel *flabel = new QLabel("Focus");
    controls->addWidget(flabel,0,0);
    focus = new QSlider(Qt::Horizontal);
    focus->setMinimum(1);
    focus->setMaximum(100);
    focus->setValue(1);
    focus->setToolTip("Distance from the camera to the focus point relatively to the scale");
    controls->addWidget(focus,0,1);
    focusbox = new QDoubleSpinBox;
    focusbox->setToolTip("Distance from the camera to the focus point");
    focusbox->setDecimals(10);
    focusbox->setMinimum(pow(10,-10));
    focusbox->setMaximum(mx);
    controls->addWidget(focusbox,0,2,1,2);
    //layout->addLayout(focuslayout);


    //QHBoxLayout *scalelayout = new QHBoxLayout;
    QLabel *slabel = new QLabel("Scale");
    controls->addWidget(slabel,1,0);
    scale = new QSlider(Qt::Horizontal);
    scale->setMinimum(1);
    scale->setMaximum(100);
    scale->setValue(1);
    scale->setToolTip("Scale of the visible space");
    controls->addWidget(scale,1,1);
    scalebox = new QDoubleSpinBox;
    scalebox->setDecimals(10);
    scalebox->setToolTip(scale->toolTip());
    scalebox->setMinimum(pow(10,-10));
    scalebox->setMaximum(mx);
    controls->addWidget(scalebox,1,2,1,2);
    //layout->addLayout(scalelayout);

    //QHBoxLayout *translatelayout = new QHBoxLayout;
    QLabel *stranslate = new QLabel("Move");
    controls->addWidget(stranslate,2,0);
    translate= new QSlider(Qt::Horizontal);
    translate->setMinimum(0);
    translate->setMaximum(100);
    translate->setValue(50);
    translate->setToolTip("Move towards focus point");
    controls->addWidget(translate,2,1);
    translatebox = new QDoubleSpinBox;
    translatebox->setDecimals(10);
    translatebox->setToolTip(scale->toolTip());
    translatebox->setMinimum(-mx);
    translatebox->setMaximum(mx);
    translatebox->setValue(0);
    controls->addWidget(translatebox,2,2);
    QPushButton *apply = new QPushButton("Apply");
    controls->addWidget(apply,2,3);

    scale->setMinimumWidth(100);
    focus->setMinimumWidth(100);
    translate->setMinimumWidth(100);

    localtrans = new QCheckBox("Stick to focus point");
    localtrans->setChecked(false);
    controls->addWidget(localtrans,3,0,1,4);

    layout->addLayout(controls);


    scalebox->setDecimals(DOUBLESPINBOX_DECIMALS);
    scalebox->setSingleStep(DOUBLESPINBOX_STEP);
    focusbox->setDecimals(DOUBLESPINBOX_DECIMALS);
    focusbox->setSingleStep(DOUBLESPINBOX_STEP);
    translatebox->setDecimals(DOUBLESPINBOX_DECIMALS);
    translatebox->setSingleStep(DOUBLESPINBOX_STEP);

    layout->setAlignment(Qt::AlignTop);
    setLayout(layout);

    setUpPosNDir();

    connect(scalebox, SIGNAL(valueChanged(double)), this, SLOT(updateScale()));
    connect(focusbox, SIGNAL(valueChanged(double)), this, SLOT(updateFocus()));
    connect(translatebox, SIGNAL(valueChanged(double)), this, SLOT(updateTranslate()));
    connect(scale, SIGNAL(valueChanged(int)), this, SLOT(updateScaleSlider()));
    connect(focus, SIGNAL(valueChanged(int)), this, SLOT(updateFocusSlider()));
    connect(translate, SIGNAL(valueChanged(int)), this, SLOT(updateTranslateSlider()));
    connect(scale, SIGNAL(sliderPressed()), this, SLOT(lockSliders()));
    connect(focus, SIGNAL(sliderPressed()), this, SLOT(lockSliders()));
    connect(translate, SIGNAL(sliderPressed()), this, SLOT(lockSliders()));
    connect(scale, SIGNAL(sliderReleased()), this, SLOT(unlockSliderScale()));
    connect(focus, SIGNAL(sliderReleased()), this, SLOT(unlockSliderFocus()));
    connect(translate, SIGNAL(sliderReleased()), this, SLOT(unlockSliderTranslate()));
    connect(apply, SIGNAL(clicked()), this, SLOT(applyTranslate()));
    connect(localtrans, SIGNAL(toggled(bool)), this, SLOT(updateAround(bool)));


    setMatrix(m,sc,fc);
}

bool CameraController::isAroundFocus() const
{
    return localtrans->isChecked();
}

void CameraController::updateAround(bool trans)
{
}

void CameraController::applyTranslate()
{
    if (transformcp == NULL || camfocus == 0 || camscale == 0) return;
    double diff = translatebox->value();
    translatebox->setValue(0);
    VectorN<RTYPE> dir = VectorN<RTYPE>::identity(transformcp->getRows()-1, 2);
    Matrix<RTYPE> m2(transformcp->getRows()-1,transformcp->getColoumns()-1);
    for (int i = 0; i < transformcp->getRows()-1; ++i) for (int j = 0; j < transformcp->getColoumns()-1; ++j)
        m2.set(i,j,transformcp->get(i,j));
    dir = m2 * dir;
    dir.normalize();                    //Required because of the custom matrix
    dir = dir * diff;
    //cout<<dir<<endl;
    //cout<<*transformcp<<endl;
    for (int i = 0; i < dir.size(); ++i) transformcp->set(i,transformcp->getColoumns()-1,transformcp->get(i,transformcp->getColoumns()-1) + dir[i]);
    updateMatrix(*transformcp);
    //camfocus = focusbox->value();
    if (localtrans->isChecked()) {
        camfocus -= diff;
        if (camfocus < std::numeric_limits<double>::min() || camfocus <= 0) camfocus = std::numeric_limits<double>::min();
        focusbox->setValue(camfocus);
        //updateCamera(camscale, camfocus);
        updateFocus();
    }
}

void CameraController::lockSliders()
{
    lockchange = true;
}

void CameraController::unlockSliderScale()
{
    lockchange = false;
    updateScaleSlider();
}

void CameraController::unlockSliderFocus()
{
    lockchange = false;
    updateFocusSlider();
}

void CameraController::unlockSliderTranslate()
{
    lockchange = false;
    updateTranslateSlider();
}

void CameraController::updateScaleSlider()
{
    if (lockchange || scale->value() == 50) return;
    camscale *= (double)scale->value()/50;
    //cout<<scale->value()<<endl;
    scalebox->setValue(camscale);
}

void CameraController::updateFocusSlider()
{
    if (lockchange) return;
    camfocus = (double)(focus->value())/100 * camscale;
    focusbox->setValue(camfocus);
}

void CameraController::updateTranslateSlider()
{
    if (lockchange) return;
    double f = (double)(translate->value()-50)/50;
    f *= camfocus;
    translatebox->setValue(f);
}

void CameraController::updateScale()
{
    if (transform == NULL || camfocus == 0 || camscale == 0) return;
    camscale = scalebox->value();
    updateCamera(camscale, camfocus);
    lockchange = true;
    focus->setValue(100*camfocus/camscale);
    scale->setValue(50);
    lockchange = false;
}

void CameraController::updateFocus()
{
    if (transform == NULL || camfocus == 0 || camscale == 0) return;
    camfocus = focusbox->value();
    updateCamera(camscale, camfocus);
    lockchange = true;
    focus->setValue(100*camfocus/camscale);
    lockchange = false;
    updateTranslate();
}

void CameraController::updateTranslate()
{
    lockchange = true;
    translate->setValue(50 * translatebox->value()/camfocus + 50);
    lockchange = false;
}

void CameraController::setMatrix(Matrix<RTYPE> *m, RTYPE sc, RTYPE fc)
{
    if (transformcp != NULL) delete transformcp;
    transformcp = NULL;
    if (m != NULL) transformcp = new Matrix<RTYPE>(*m);
    camscale = sc;
    camfocus = fc;
    if (m == NULL) transform = m;
    scalebox->setValue(sc);
    focusbox->setValue(fc);
    translatebox->setValue(fc);
    //VectorN<RTYPE> pos(m->getRows()-1);
    //for (int i = 0; i < m->getRows()-1; ++i) pos[i] = m->get(i, m->getColoumns()-1);
    //cout<<"asd: "<<50*sc<<endl;
    scale->setValue(50);
    translate->setValue(50);
    focus->setValue(100);
    transform = m;
    setUpPosNDir();
}

void CameraController::setUpPosNDir()
{
    if (transform == NULL) {
        campos->setText("Position: -");
        camdir->setText("Direction: -");
    } else {

        VectorN<RTYPE> pos(transform->getRows()-1);
        for (int i = 0; i < transform->getRows()-1; ++i) pos[i] = transform->get(i, transform->getColoumns()-1);

        Matrix<RTYPE> m = *transform;
        for (int i = 0; i < m.getRows(); ++i) m.set(i,m.getColoumns()-1,0);
        VectorN<RTYPE> dir = m*VectorN<RTYPE>::identity(transform->getRows()-1, 2);

        campos->setText("Position: "+stringFromVector(pos));
        camdir->setText("Direction: "+stringFromVector(dir));
    }
}

QString CameraController::stringFromVector(VectorN<RTYPE> vec)
{
    RTYPE max = 0;
    int total = vec.size();
    int nonzero = 0;
    for (int i = 0; i < vec.size(); ++i) if (vec[i] != 0) {
        if (vec[i] > max) max = vec[i];
        nonzero++;
    }
    RTYPE pr = max * PRECISION;
    int l = log10(pr);
    int len = 0;
    if (l < 0) len = -l;
    if (len > 10) len = 10;
    bool all;
    if (total * (len+3) < nonzero * (len+8) || total*(len+3) < DOESN_MATTER) all = true;
    else all = false;
    QString s = "";
    int n = 0;
    len += 2;
    for (int i = 0; i < vec.size(); ++i) {
        if (all || vec[i] != 0) {
            n++;
            stringstream ss,ss2;
            if (!all) ss2<<"["<<i<<"] ";
            ss<<vec[i];
            string st = ss.str();
            int comma = st.find('.');
            if (comma != -1 && st.length()-comma-1 > len) st = st.substr(0,comma) + "." + st.substr(comma+1,len);
            s += QString::fromStdString(ss2.str() + st + " ");
            if (n >= MAX_SHOW) break;
        }
    }
    return s;
}

CameraController::~CameraController()
{
    if (transformcp != NULL) delete transformcp;
}
