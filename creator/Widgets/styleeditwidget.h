#ifndef STYLEEDITWIDGET_H
#define STYLEEDITWIDGET_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QSignalMapper>

#include <vector>

#include "common/Helper/stylemanager.h"

namespace Ui {
class StyleEditWidget;
}

class StyleEditWidget : public QWidget
{
    Q_OBJECT

public:
    explicit StyleEditWidget(StyleSheet *styleSheet, QWidget *parent = 0);
    ~StyleEditWidget();

    void save();

protected slots:
    void edited(int i);

private:
    Ui::StyleEditWidget *ui;
    StyleSheet *styleSheet;
    std::vector<StyleRecord> texts;
    std::vector<QLineEdit*> lines;
};

#endif // STYLEEDITWIDGET_H
