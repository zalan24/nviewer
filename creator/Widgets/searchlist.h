#ifndef SEARCHLIST_H
#define SEARCHLIST_H

#include <QWidget>
#include <QListWidget>
#include <QLineEdit>
#include <QVBoxLayout>

#include "common/Math/include/tools.h"

#include <vector>

using namespace std;

namespace Ui {
class SearchList;
}

class SearchList : public QWidget
{
    Q_OBJECT

public:
    explicit SearchList(int maxnum, QWidget *parent = 0);
    ~SearchList();

    void addElements(QString s) {elements.append(s);}
    void addElement(QString s) {addElements(s); update();}
    int size() const {return elements.size();}
    void removeElementAt(int index) {elements.removeAt(index); update();}
    void getSelectedItems(QStringList &l);
    bool isRecord(QString s) const;
    void removeSelected();
    QStringList getItems() const {return elements;}

public slots:
    void update();

private:
    Ui::SearchList *ui;
    QLineEdit *text;
    QListWidget *list;
    int maxnum;
    QStringList elements;
    QStringList shown;

    void getList(QStringList &list);
};

#endif // SEARCHLIST_H
