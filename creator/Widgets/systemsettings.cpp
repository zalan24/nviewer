#include "systemsettings.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QSpinBox>
#include <QGridLayout>

SystemSettings::SystemSettings(MachineConfig *cfg, QWidget *parent) : QWidget(parent),
    cfg(cfg)
{
    QGridLayout *layout = new QGridLayout;


    int n = 0;
    settings.push_back(new CfgSetter(cfg->getWindow(),layout,n++,
                                      "Display width",
                                      "Width of preview.",
                                      "width",
                                      2,MAXDISPLAYSIZE));
    settings.push_back(new CfgSetter(cfg->getWindow(),layout,n++,
                                      "Display height",
                                      "Height of preview.",
                                      "height",
                                      2,MAXDISPLAYSIZE));
    settings.push_back(new CfgSetter(cfg->getApp(),layout,n++,
                                      "Number of threads",
                                      "Each threads calculates some coloumns of the preview images.",
                                      "threads",
                                      1,MAXDISPLAYSIZE*3));
    settings.push_back(new CfgSetter(cfg->getApp(),layout,n++,
                                      "Optimization level",
                                      "Some layers of the image are pre-calculated to boost the later process. Increasing the optimization level will lead to higher RAM usage.",
                                      "occlusion",
                                      0,MAXDISPLAYSIZE*2));
    settings.push_back(new CfgSetter(cfg->getApp(),layout,n++,
                                      "Runtime vram",
                                      "Memory for the calculations. Each thread will have this amount of space.",
                                      "vram",
                                      1024/1024,268435456/1024,1024));
    settings[n-1]->getBox()->setSuffix(" KiB");
    settings.push_back(new CfgSetter(cfg->getApp(),layout,n++,
                                      "Script vram",
                                      "Memory for building the script. This space is only allocated once, at the beginning.",
                                      "functionvram",
                                      1,2147483648/1024/1024,1024*1024));
    settings[n-1]->getBox()->setSuffix(" MiB");
    settings.push_back(new CfgSetter(cfg->getApp(),layout,n++,
                                      "Quality of light",
                                      "Better light quality will mean more CPU and RAM usage. More layers are calculated and the optimization is less effective.",
                                      "lighting",
                                      0,MAXDISPLAYSIZE));
    settings.push_back(new CfgSetter(cfg->getApp(),layout,n++,
                                      "Recursion limit",
                                      "Recursive funstion are limited to this number of recursive executions. This functions uses the script vram.",
                                      "reclimit",
                                      1,67108864));
    settings.push_back(new CfgSetter(cfg->getApp(),layout,n++,
                                      "Dispaly depth",
                                      "The number of layers calculated for the preview: steps * depth",
                                      "depth",
                                      1,MAXDISPLAYSIZE*MAXDISPLAYSIZE));
    settings.push_back(new CfgSetter(cfg->getApp(),layout,n++,
                                      "Dispaly steps",
                                      "The preview calculation process is divided into this number of steps. The quality (and calculation time) increases at each step.",
                                      "numd",
                                      1,MAXDISPLAYSIZE));

    setLayout(layout);
}

SystemSettings::~SystemSettings()
{
    for (int i = 0; i < settings.size(); ++i) delete settings[i];
}

void SystemSettings::save()
{
    for (int i = 0; i < settings.size(); ++i) settings[i]->save();
    int numd;
    int resx, resy;
    try {
        numd = readInt(cfg->getApp()->getAttribute("numd",DEFAULT_SNUMD));
        resx = readInt(cfg->getWindow()->getAttribute("width",DEFAULT_WIDTH));
        resy = readInt(cfg->getWindow()->getAttribute("height",DEFAULT_HEIGHT));
        if (numd < 1) throw Exception(INVALID_DATA,"Number of steps cannot be less than 1");
    } catch (ERROR_CODE e) {
        LOG<<ERROR<<"Something went wrong: "<<Error::ERROR_STRING<<endl<<NORMAL;
        numd = 1;
        resx = 16;
        resy = 16;
    }
    int mx = max(resx,resy);
    int dens = mx / numd;
    cfg->getApp()->setAttributeT("density",dens);
}

