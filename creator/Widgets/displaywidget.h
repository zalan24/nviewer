#ifndef DISPLAYWIDGET_H
#define DISPLAYWIDGET_H

#include <QWidget>
#include <QProgressBar>

#include "common/Helper/texture.h"

class DisplayWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DisplayWidget(QProgressBar *bar, QWidget *parent = 0);
    ~DisplayWidget();

    void setProgress(int p);
    int getProgress() const;
    void resize(int w, int h);
    void start();
    void finish();

    void setTexture(Texture *t);

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

private:
    QProgressBar *progressbar;
    Texture *text;
    int progress;
};

#endif // DISPLAYWIDGET_H
