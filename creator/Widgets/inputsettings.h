#ifndef INPUTSETTINGS_H
#define INPUTSETTINGS_H

#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QVector>
#include <QSignalMapper>
#include <QKeyEvent>

#include "common/Math/include/config.h"

class InputSettings : public QWidget
{
    Q_OBJECT
public:
    explicit InputSettings(MachineConfig *cfg, QWidget *parent = 0);

    void save();

    void KeyPressEvent(QKeyEvent *event);
    void KeyReleaseEvent(QKeyEvent *event);

signals:

public slots:
    void selectButton(int i);

private:
    MachineConfig *cfg;
    QVector<QPushButton*> keys;
    QVector<int> codes;
    int selected;

    void setSelected(int s);
    void updateButton(int i);
    bool isValidCode(int c) const;
};

#endif // INPUTSETTINGS_H
