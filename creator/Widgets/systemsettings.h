#ifndef SYSTEMSETTINGS_H
#define SYSTEMSETTINGS_H

#include <QWidget>
#include <QVector>

#include "common/Math/include/config.h"
#include "creator/Helper/cfgsetter.h"

#define MAXDISPLAYSIZE 1024

class SystemSettings : public QWidget
{
    Q_OBJECT
public:
    explicit SystemSettings(MachineConfig *cfg, QWidget *parent = 0);
    ~SystemSettings();

    void save();

signals:

public slots:

private:
    MachineConfig *cfg;
    QVector<CfgSetter*> settings;
};

#endif // SYSTEMSETTINGS_H
