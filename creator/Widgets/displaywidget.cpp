#include "displaywidget.h"

#include <QBrush>
#include <QPainter>
#include <QPaintEvent>

DisplayWidget::DisplayWidget(QProgressBar *bar, QWidget *parent) : QWidget(parent), progressbar(bar), text(NULL), progress(0)
{
    finish();
    resize(512,512);
    progressbar->setFixedHeight(15);

}

void DisplayWidget::mouseReleaseEvent(QMouseEvent*)
{
    setFocus();
}

void DisplayWidget::resize(int w, int h)
{
    setFixedSize(w, h);
    progressbar->setFixedWidth(width());
    /*Texture *text2 = new Texture(w,h);
    if (text != NULL) {
        delete text;
        text = NULL;
    }
    text = text2;*/
}

void DisplayWidget::setTexture(Texture *t) {text = t;}

DisplayWidget::~DisplayWidget()
{
}

void DisplayWidget::setProgress(int p)
{
    if (progress < 0 && p >= 0) {
        progressbar->setMinimum(0);
        progressbar->setMaximum(100);
        progressbar->setValue(p);
    } else if (progress >= 0 && p < 0) {
        progressbar->setMinimum(0);
        progressbar->setMaximum(0);
        //progressbar->setValue(0);
    } else if (p >= 0) {
        progressbar->setValue(p);
    }
    progress = p;
}


int DisplayWidget::getProgress() const
{
    return progress;
}

void DisplayWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);

    QBrush background = QBrush(QColor(0, 0, 0));
    painter.fillRect(event->rect(), background);
    if (text != NULL) for (int i = 0; i < text->getX(); ++i) {
        for (int j = 0; j < text->getY(); ++j) {
            Colour c = text->get(i,j);
            painter.setPen(QColor(c.r*255, c.g*255, c.b*255));
            painter.drawPoint(i,text->getY()-j-1);
        }
    }

    painter.end();
}

void DisplayWidget::start()
{
    setProgress(0);
}

void DisplayWidget::finish()
{
    setProgress(100);
}
