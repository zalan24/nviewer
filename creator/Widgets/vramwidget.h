#ifndef VRAMWIDGET_H
#define VRAMWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QProgressBar>
#include <QVBoxLayout>

#include "common/Helper/vram.h"
#include "common/Helper/inotifiable.h"

class VRamWidget : public QWidget, public INotifiable
{
    Q_OBJECT
public:
    explicit VRamWidget(QString title, QWidget *parent = 0);

    void setVRam(VRam *vr);

    void notify(void*);
    void notify(int);

signals:

private:
    VRam *vram;
    QLabel *title;
    QProgressBar *bar;
    QLabel *data;

    QString getSpaceFormat(int) const;
};

#endif // VRAMWIDGET_H
