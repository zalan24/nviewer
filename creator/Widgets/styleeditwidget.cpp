#include "styleeditwidget.h"
#include "ui_styleeditwidget.h"

StyleEditWidget::StyleEditWidget(StyleSheet *styleSheet, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StyleEditWidget),
    styleSheet(styleSheet)
{
    ui->setupUi(this);
    for (int i = 0; i < styleSheet->size(); ++i) {
        string id = styleSheet->getStyleId(i);
        string value = styleSheet->getValue(id);
        string type = styleSheet->getType(id);
        texts.push_back(StyleRecord(type,id,value));
    }

    QGridLayout *grid = new QGridLayout;
    QVBoxLayout *layout = new QVBoxLayout;

    QScrollArea *scroll = new QScrollArea;

    QSignalMapper *signalMapper = new QSignalMapper;

    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(edited(int)));

    for (int i = 0; i < texts.size(); ++i) {
        QString typetext = QString::fromStdString(texts[i].type);
        if (typetext.length() > 0 && typetext[0] == '%') typetext = "-";
        QLabel *type = new QLabel(typetext);
        QLabel *label = new QLabel(QString::fromStdString(texts[i].id));
        QLineEdit *lineEdit = new QLineEdit(QString::fromStdString(texts[i].value));
        type->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        label->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        lineEdit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        grid->addWidget(type,i,0);
        grid->addWidget(label,i,1);
        grid->addWidget(lineEdit,i,2);
        lines.push_back(lineEdit);
        connect(lineEdit, SIGNAL(editingFinished()), signalMapper, SLOT(map()));
        signalMapper->setMapping(lineEdit,i);
    }

    QWidget *viewport = new QWidget;
    viewport->setLayout(grid);
    //viewport->setStyleSheet("background-color: black");
    viewport->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    scroll->setWidget(viewport);
    scroll->setWidgetResizable(true);
    scroll->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
    layout->addWidget(scroll);

    setLayout(layout);
}

void StyleEditWidget::edited(int i)
{
    texts[i].value = lines[i]->text().toStdString();
}

StyleEditWidget::~StyleEditWidget()
{
    delete ui;
}

void StyleEditWidget::save()
{
    for (int i = 0; i < texts.size(); ++i) styleSheet->setStyle(texts[i].id, texts[i].value);
    styleSheet->modify();
}
