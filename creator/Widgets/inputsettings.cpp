#include "inputsettings.h"

#include <QLabel>
#include <QVBoxLayout>

InputSettings::InputSettings(MachineConfig *cfg, QWidget *parent) : QWidget(parent),
    cfg(cfg), selected(-1)
{
    QGridLayout *layout = new QGridLayout;

    layout->setAlignment(Qt::AlignTop);

    QSignalMapper *signalMapper = new QSignalMapper(this);
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(selectButton(int)));

    keys.push_back(new QPushButton("Move forward"));
    keys.push_back(new QPushButton("Move backward"));
    keys.push_back(new QPushButton("Left"));
    keys.push_back(new QPushButton("Right"));
    keys.push_back(new QPushButton("Axis 1"));
    keys.push_back(new QPushButton("Axis 2"));
    keys.push_back(new QPushButton("Axis 3"));
    keys.push_back(new QPushButton("Axis 4"));
    keys.push_back(new QPushButton("Axis 5"));
    keys.push_back(new QPushButton("Axis 6"));
    keys.push_back(new QPushButton("Axis 7"));
    keys.push_back(new QPushButton("Axis 8"));
    keys.push_back(new QPushButton("Axis 9"));
    keys.push_back(new QPushButton("Axis 10"));
    keys.push_back(new QPushButton("Axis 11"));
    keys.push_back(new QPushButton("Axis 12"));
    keys.push_back(new QPushButton("Axis 13"));

    for (int i = 0; i < keys.size(); ++i) {
        QString attr = "key" + QString::number(i+1);
        int code = readInt(cfg->getKeyBoard()->getAttribute(attr.toStdString(),"0"));
        codes.push_back(code);
        QLabel *label = new QLabel(keys[i]->text());
        updateButton(i);
        layout->addWidget(label,i,0);
        layout->addWidget(keys[i],i,1);
        connect(keys[i], SIGNAL(clicked()), signalMapper, SLOT(map()));
        signalMapper->setMapping(keys[i], i);
    }

    setLayout(layout);
}

void InputSettings::save()
{
    for (int i = 0; i < codes.size(); ++i) {
        QString attr = "key" + QString::number(i+1);
        cfg->getKeyBoard()->setAttribute(attr.toStdString(),QString::number(codes[i]).toStdString());
    }
    cfg->WriteConfig();
}

void InputSettings::selectButton(int i)
{
    setSelected(i);
}

void InputSettings::updateButton(int i)
{
    if (selected == i) keys[i]->setText("Press a key");
    else if (codes[i] == 0) keys[i]->setText("Click to set");
    else keys[i]->setText(QString((char)codes[i]).toUpper());
}

void InputSettings::setSelected(int b)
{
    int o = selected;
    selected = b;
    if (o != -1) updateButton(o);
    if (selected != -1) updateButton(selected);
}

void InputSettings::KeyPressEvent(QKeyEvent *)
{

}

void InputSettings::KeyReleaseEvent(QKeyEvent *event)
{
    int code = event->key();
    if (selected != -1) {
        if (!isValidCode(code)) {
            if (code == Qt::Key_Escape) code = 0;
            else {
                setSelected(-1);
                return;
            }
        }
        int found = codes.size();
        if (code != 0) for (found = 0; found < codes.size() && codes[found] != code; ++found);
        if (found < codes.size()) {
            codes[found] = 0;
            updateButton(found);
        }
        codes[selected] = code;
        setSelected(-1);
    }
}

bool InputSettings::isValidCode(int c) const
{
    string s = "abcdefghijklmnopqrstuvwxyz0123456789-.,?:_~+!%/=()[]{}*<>ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return (int)s.find((char)c) != -1;
}
