#include "searchlist.h"
#include "ui_searchlist.h"

SearchList::SearchList(int maxnum, QWidget *parent) :
    QWidget(parent),
    maxnum(maxnum),
    ui(new Ui::SearchList)
{
    ui->setupUi(this);

    QVBoxLayout *layout = new QVBoxLayout;

    text = new QLineEdit;
    layout->addWidget(text);

    list = new QListWidget;
    layout->addWidget(list);
    list->setSelectionMode(QAbstractItemView::MultiSelection);

    setLayout(layout);

    connect(text, SIGNAL(textChanged(QString)), this, SLOT(update()));
}

void SearchList::getSelectedItems(QStringList &l)
{
    l.clear();
    QList<QListWidgetItem*> items = list->selectedItems();
    for (int i = 0; i < items.size(); ++i) l.append(items[i]->text());
}

SearchList::~SearchList()
{
    delete ui;
}

void SearchList::getList(QStringList &list)
{
    string special = "[]()-+.*^$|";
    string filter = text->text().toStdString();
    string f = "";
    for (int i = 0; i < filter.length(); ++i) {
        if (special.find(filter[i]) != -1) f += "\\";
        f += filter[i];
    }
    filter = FORMAT_FILTER_BEGIN + f + FORMAT_FILTER_END;
    for (int i = 0; i < elements.size() && list.size() < maxnum; ++i) {
        if (match(elements[i].toStdString(), filter)) {
            list.append(elements[i]);
        }
    }
}

bool SearchList::isRecord(QString s) const
{
    for (int i = 0; i < elements.size(); ++i) if (elements[i] == s) return true;
    return false;
}

void SearchList::update()
{
    list->clear();
    shown.clear();
    getList(shown);
    list->addItems(shown);
}

void SearchList::removeSelected()
{
    QList<QListWidgetItem*> sel = list->selectedItems();
    for (int i = 0; i < sel.size(); ++i) {
        QString s = sel[i]->text();
        elements.removeOne(s);
    }
    update();
}
