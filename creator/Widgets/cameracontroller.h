#ifndef CAMERACONTROLLER_H
#define CAMERACONTROLLER_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QGridLayout>
#include <QDoubleSpinBox>
#include <QVBoxLayout>
#include <QSlider>
#include <QCheckBox>
#include <QKeyEvent>

#include "common/Math/include/matrix.h"
#include "common/Math/include/info.h"

#define MAX_SHOW 30
#define DOESN_MATTER 50

class CameraController : public QWidget
{
    Q_OBJECT
public:
    explicit CameraController(Matrix<RTYPE> *m, RTYPE sc, RTYPE fc, QWidget *parent = 0);
    ~CameraController();

    void setMatrix(Matrix<RTYPE> *m, RTYPE sc, RTYPE fc);
    bool isAroundFocus() const;

signals:
    void updateCamera(RTYPE sc, RTYPE fc);
    void updateMatrix(Matrix<RTYPE> m);

public slots:
    void updateScale();
    void updateFocus();
    void updateTranslate();
    void updateScaleSlider();
    void updateFocusSlider();
    void updateTranslateSlider();
    void lockSliders();
    void unlockSliderScale();
    void unlockSliderFocus();
    void unlockSliderTranslate();
    void applyTranslate();
    void updateAround(bool trans);

private:
    Matrix<RTYPE> *transform;
    Matrix<RTYPE> *transformcp;
    QLabel *campos;
    QLabel *camdir;
    QSlider *focus;
    QSlider *scale;
    QSlider *translate;
    QCheckBox *localtrans;
    QDoubleSpinBox *focusbox;
    QDoubleSpinBox *scalebox;
    QDoubleSpinBox *translatebox;
    RTYPE camscale;
    RTYPE camfocus;
    bool lockchange;

    void setUpPosNDir();
    QString stringFromVector(VectorN<RTYPE> vec);
};

#endif // CAMERACONTROLLER_H
