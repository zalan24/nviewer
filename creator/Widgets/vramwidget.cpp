#include "vramwidget.h"

#include <sstream>

VRamWidget::VRamWidget(QString t, QWidget *parent) : QWidget(parent), vram(NULL)
{
    title = new QLabel(t);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(title);
    bar = new QProgressBar();
    layout->addWidget(bar);
    data = new QLabel;
    layout->addWidget(data);

    title->setAlignment(Qt::AlignHCenter);
    title->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    bar->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    data->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);

    setLayout(layout);

    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);

    setVRam(NULL);
}

void VRamWidget::notify(void * vr)
{
    VRam *v = (VRam*)vr;
    setVRam(v);
}

void VRamWidget::notify(int p)
{
    bar->setValue(p);
}

QString VRamWidget::getSpaceFormat(int n) const
{
    int len = 4;
    int m = 0;
    QString qs[] = {"B", "KiB", "MiB", "GiB"};
    int d = 0;
    while (m+1 < len && n >= 1024) {
        m++;
        d = n % 1024;
        n /= 1024;
    }
    std::stringstream ss;
    ss<<n;
    if (d > 1024/10) ss<<"."<<(10*d/1024);
    QString ret = QString::fromStdString(ss.str()) + " " + qs[m];
    return ret;
}

void VRamWidget::setVRam(VRam *vr)
{
    vram = vr;
    if (vram == NULL) {
        data->setText("--- / ---");
        bar->setValue(0);
    } else {
        data->setText(getSpaceFormat(vram->getMax())+" / "+getSpaceFormat(vram->size()));
        bar->setValue((ulli)100*vram->getMax() / vram->size());
    }
}

