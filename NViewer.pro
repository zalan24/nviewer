#-------------------------------------------------
#
# Project created by QtCreator 2016-04-25T17:25:40
#
#-------------------------------------------------

QT       += core gui

INCLUDEPATH += /usr/include/python2.7
LIBS += -lpython2.7

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NViewer
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11
LIBS += -pthread

SOURCES += creator/main.cpp\
        creator/mainwindow.cpp \
    creator/Helper/logger.cpp \
    creator/Widgets/displaywidget.cpp \
    creator/Helper/manager.cpp \
    common/Math/src/config.cpp \
    common/Math/src/errors.cpp \
    common/Math/src/tools.cpp \
    common/Math/src/log.cpp \
    common/Helper/vram.cpp \
    common/Helper/texture.cpp \
    creator/Dialog/reexdialog.cpp \
    creator/Dialog/settings.cpp \
    creator/Widgets/systemsettings.cpp \
    creator/Widgets/inputsettings.cpp \
    creator/Helper/cfgsetter.cpp \
    creator/Dialog/newsession.cpp \
    common/Math/src/function.cpp \
    common/Math/src/image.cpp \
    common/Math/src/process.cpp \
    common/Math/src/session.cpp \
    creator/Helper/calcthread.cpp \
    creator/Widgets/vramwidget.cpp \
    creator/Dialog/vectordialog.cpp \
    creator/Dialog/cameraviewdialog.cpp \
    creator/Dialog/transformdialog.cpp \
    creator/Widgets/cameracontroller.cpp \
    creator/Dialog/vectorlocaldialog.cpp \
    creator/Dialog/rotationdialog.cpp \
    common/Helper/scenemanager.cpp \
    creator/Dialog/textdialog.cpp \
    creator/Dialog/scenedialog.cpp \
    common/Helper/stylemanager.cpp \
    creator/Dialog/styledialog.cpp \
    creator/Dialog/stylesheeteditdialog.cpp \
    creator/Widgets/styleeditwidget.cpp \
    creator/Dialog/currentstyledialog.cpp \
    common/Helper/sessionfile.cpp \
    creator/Dialog/exportdialog.cpp \
    creator/Widgets/searchlist.cpp \
    common/Helper/pythonmanager.cpp \
    common/Helper/configcheck.cpp

HEADERS  += creator/mainwindow.h \
    creator/Helper/logger.h \
    creator/Widgets/displaywidget.h \
    creator/Helper/manager.h \
    common/Math/include/config.h \
    common/Math/include/errors.h \
    common/Math/include/info.h \
    common/Math/include/tools.h \
    common/Math/include/log.h \
    common/Math/include/formats.h \
    common/Helper/vram.h \
    common/Helper/texture.h \
    creator/Dialog/reexdialog.h \
    creator/Dialog/settings.h \
    creator/Widgets/systemsettings.h \
    creator/Widgets/inputsettings.h \
    creator/Helper/cfgsetter.h \
    creator/Dialog/newsession.h \
    common/Math/include/custumfunctionclass.h \
    common/Math/include/function.h \
    common/Math/include/image.h \
    common/Math/include/matrix.h \
    common/Math/include/optimized_matrix.h \
    common/Math/include/path.h \
    common/Math/include/process.h \
    common/Math/include/render.h \
    common/Math/include/scriptfunctions.h \
    common/Math/include/scriptparser.h \
    common/Math/include/scriptqueries.h \
    common/Math/include/session.h \
    common/Math/include/transformable.h \
    common/Math/include/vectorn.h \
    creator/Helper/calcthread.h \
    common/Helper/inotifiable.h \
    creator/Widgets/vramwidget.h \
    creator/Dialog/vectordialog.h \
    creator/Dialog/cameraviewdialog.h \
    common/Math/base.h \
    creator/Dialog/transformdialog.h \
    creator/Widgets/cameracontroller.h \
    creator/Dialog/vectorlocaldialog.h \
    creator/Dialog/rotationdialog.h \
    common/Helper/scenemanager.h \
    creator/Dialog/textdialog.h \
    creator/Dialog/scenedialog.h \
    common/Helper/stylemanager.h \
    creator/Dialog/styledialog.h \
    creator/Dialog/stylesheeteditdialog.h \
    creator/Widgets/styleeditwidget.h \
    creator/Dialog/currentstyledialog.h \
    common/Helper/sessionfile.h \
    creator/Dialog/exportdialog.h \
    creator/Widgets/searchlist.h \
    common/Helper/pythonmanager.h \
    common/Helper/pythoncodes.h \
    common/Helper/loggerbase.h \
    common/Helper/configcheck.h

FORMS    += mainwindow.ui \
    creator/Dialog/reexdialog.ui \
    creator/Dialog/settings.ui \
    creator/Dialog/newsession.ui \
    creator/Dialog/vectordialog.ui \
    creator/Dialog/cameraviewdialog.ui \
    creator/Dialog/transformdialog.ui \
    creator/Dialog/vectorlocaldialog.ui \
    creator/Dialog/rotationdialog.ui \
    creator/Dialog/textdialog.ui \
    creator/Dialog/scenedialog.ui \
    creator/Dialog/styledialog.ui \
    creator/Dialog/stylesheeteditdialog.ui \
    creator/Widgets/styleeditwidget.ui \
    creator/Dialog/currentstyledialog.ui \
    creator/Dialog/exportdialog.ui \
    creator/Widgets/searchlist.ui

