# -*- coding: utf-8 -*-

import os
import shutil
import zipfile

def createTemp(path):
	if os.path.exists(path):
		shutil.rmtree(path, ignore_errors=True)
	os.mkdir(path)

def createScripts(path):
	os.mkdir(os.path.join(path, 'Scripts'))

def copyFile(file, path):
	filename = file.split('/')[-1]
	shutil.copyfile(file, os.path.join(path, filename))

def copyScript(filename, path):
	path = os.path.join(path,'Scripts')
	if ';' not in filename:
		copyFile(filename, path)
	else:
		begin, end = filename.split(';')
		path = os.path.join(path, '/'.join(end.split('/')[0:-1]))
		os.makedirs(path)
		copyFile(os.path.join(begin,end), path)

def zipDir(path, filename):
	zipf = zipfile.ZipFile(filename, 'w', zipfile.ZIP_DEFLATED)
	for root, dirs, files in os.walk(path):
		for file in files:
			zipf.write(os.path.join(root, file))
	zipf.close()
