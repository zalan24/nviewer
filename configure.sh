#!/bin/sh
 
std="gen/std"
helper="gen/helper"
include="common/Math/include"
generator="$helper/functiongen.sh"
scripfunctions="$std/scriptfunctions.fdata"
scriptfh="$include/scriptfunctions.h"

sh $generator $scripfunctions > $scriptfh

echo "Done."
