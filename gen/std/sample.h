#pragma once

#include "custumfunctionclass.h"


template <class T>
class Function_print_n : public FunctionClass<T>
{
public:
  Function_print_n(int dim, VRam *vr) : FunctionClass<T>("print", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    T * ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr)));
    cout<<*ret<<endl;
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_print_n<T> *r = (Function_print_n<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_print_n<T>));
    return new (r) Function_print_n<T>(*this);
  }
};


template <class T>
class Function_clamp01_n : public FunctionClass<T>
{
public:
  Function_clamp01_n(int dim, VRam *vr) : FunctionClass<T>("clamp01", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    T * ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = *((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr)));
    if (*ret < 0) *ret = 0;
    else if (*ret > 1) *ret = 1;
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_clamp01_n<T> *r = (Function_clamp01_n<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_clamp01_n<T>));
    return new (r) Function_clamp01_n<T>(*this);
  }
};

template <class T>
class Function_negate_r : public FunctionClass<T>
{
public:
  Function_negate_r(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    T * ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = -(*((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr))));
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_negate_r<T> *r = (Function_negate_r<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_negate_r<T>));
    return new (r) Function_negate_r<T>(*this);
  }
};

template <class T>
class Function_sin_n : public FunctionClass<T>
{
public:
  Function_sin_n(int dim, VRam *vr) : FunctionClass<T>("sin", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    T * ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = sin(*((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr))));
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_sin_n<T> *r = (Function_sin_n<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_sin_n<T>));
    return new (r) Function_sin_n<T>(*this);
  }
};

template <class T>
class Function_cos_n : public FunctionClass<T>
{
public:
  Function_cos_n(int dim, VRam *vr) : FunctionClass<T>("cos", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    T * ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = cos(*((T*)(FunctionClass<T>::parameters[0]->calc(data,data2,vr))));
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_cos_n<T> *r = (Function_cos_n<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_cos_n<T>));
    return new (r) Function_cos_n<T>(*this);
  }
};

template <class T>
class Function_constcol_nnnn : public FunctionClass<T>
{
public:
  Function_constcol_nnnn(int dim, VRam *vr) : FunctionClass<T>("Colour", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN+","+TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_COLOUR_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    void *ret = FunctionClass<T>::outConstructor->construct(vr);
    Colour *lret = (Colour*)ret;
    lret->r = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr));
    lret->g = *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr));
    lret->b = *((T*)FunctionClass<T>::parameters[2]->calc(data,data2,vr));
    lret->a = *((T*)FunctionClass<T>::parameters[3]->calc(data,data2,vr));
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_constcol_nnnn<T> *r = (Function_constcol_nnnn<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constcol_nnnn<T>));
    return new (r) Function_constcol_nnnn<T>(*this);
  }
};

template <class T>
class Function_constcplx_nn : public FunctionClass<T>
{
public:
  Function_constcplx_nn(int dim, VRam *vr) : FunctionClass<T>("Complex", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_CPLX_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    complex<T> *ret = (complex<T>*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = complex<T>(*((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr)), *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr)));
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_constcplx_nn<T> *r = (Function_constcplx_nn<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_constcplx_nn<T>));
    return new (r) Function_constcplx_nn<T>(*this);
  }
};

template <class T>
class Function_lt_nn : public FunctionClass<T>
{
public:
  Function_lt_nn(int dim, VRam *vr) : FunctionClass<T>("<", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr)) < *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr));
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_lt_nn<T> *r = (Function_lt_nn<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_lt_nn<T>));
    return new (r) Function_lt_nn<T>(*this);
  }
};

template <class T>
class Function_gt_nn : public FunctionClass<T>
{
public:
  Function_gt_nn(int dim, VRam *vr) : FunctionClass<T>(">", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr)) > *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr));
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_gt_nn<T> *r = (Function_gt_nn<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_gt_nn<T>));
    return new (r) Function_gt_nn<T>(*this);
  }
};

template <class T>
class Function_and_ll : public FunctionClass<T>
{
public:
  Function_and_ll(int dim, VRam *vr) : FunctionClass<T>("&&", dim, vr) {FunctionClass<T>::setUp(TYPE_BOOL_SIGN+","+TYPE_BOOL_SIGN,TYPE_BOOL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    bool *ret = (bool*)FunctionClass<T>::outConstructor->construct(vr);
    if (*((bool*)FunctionClass<T>::parameters[0]->calc(data,data2,vr)) == false) *ret = false;
    else if (*((bool*)FunctionClass<T>::parameters[1]->calc(data,data2,vr)) == false) *ret = false;
    else *ret = true;
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_and_ll<T> *r = (Function_and_ll<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_and_ll<T>));
    return new (r) Function_and_ll<T>(*this);
  }
};

template <class T>
class Function_real_c : public FunctionClass<T>
{
public:
  Function_real_c(int dim, VRam *vr) : FunctionClass<T>("Re", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = ((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr))->real();
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_real_c<T> *r = (Function_real_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_real_c<T>));
    return new (r) Function_real_c<T>(*this);
  }
};

template <class T>
class Function_imag_c : public FunctionClass<T>
{
public:
  Function_imag_c(int dim, VRam *vr) : FunctionClass<T>("Im", dim, vr) {FunctionClass<T>::setUp(TYPE_CPLX_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = ((complex<T>*)FunctionClass<T>::parameters[0]->calc(data,data2,vr))->imag();
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_imag_c<T> *r = (Function_imag_c<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_imag_c<T>));
    return new (r) Function_imag_c<T>(*this);
  }
};

template <class T>
class Function_plus_nn : public FunctionClass<T>
{
public:
  Function_plus_nn(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr)) + *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr));
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_nn<T> *r = (Function_plus_nn<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_nn<T>));
    return new (r) Function_plus_nn<T>(*this);
  }
};

template <class T>
class Function_plus_ii : public FunctionClass<T>
{
public:
  Function_plus_ii(int dim, VRam *vr) : FunctionClass<T>("+", dim, vr) {FunctionClass<T>::setUp(TYPE_INT_SIGN+","+TYPE_INT_SIGN,TYPE_INT_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    int *ret = (int*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = *((int*)FunctionClass<T>::parameters[0]->calc(data,data2,vr)) + *((int*)FunctionClass<T>::parameters[1]->calc(data,data2,vr));
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_plus_ii<T> *r = (Function_plus_ii<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_plus_ii<T>));
    return new (r) Function_plus_ii<T>(*this);
  }
};

template <class T>
class Function_minus_nn : public FunctionClass<T>
{
public:
  Function_minus_nn(int dim, VRam *vr) : FunctionClass<T>("-", dim, vr) {FunctionClass<T>::setUp(TYPE_REAL_SIGN+","+TYPE_REAL_SIGN,TYPE_REAL_SIGN);}
  void* calc(void *data, void *data2, VRam &vr) {
    T *ret = (T*)FunctionClass<T>::outConstructor->construct(vr);
    *ret = *((T*)FunctionClass<T>::parameters[0]->calc(data,data2,vr)) - *((T*)FunctionClass<T>::parameters[1]->calc(data,data2,vr));
    return ret;
  }
  FunctionClass<T>* copy() {
    Function_minus_nn<T> *r = (Function_minus_nn<T>*)FunctionClass<T>::func_vram->allocate(sizeof(Function_minus_nn<T>));
    return new (r) Function_minus_nn<T>(*this);
  }
};

template <class T>
void addScriptFunctions(vector<FunctionClass<T>*> &functions, VRam *vram, int dim)
{
  functions.push_back(new ((Function_print_n<T>*)vram->allocate(sizeof(Function_print_n<T>))) Function_print_n<T>(dim,vram));
  functions.push_back(new ((Function_clamp01_n<T>*)vram->allocate(sizeof(Function_clamp01_n<T>))) Function_clamp01_n<T>(dim,vram));
  functions.push_back(new ((Function_sin_n<T>*)vram->allocate(sizeof(Function_sin_n<T>))) Function_sin_n<T>(dim,vram));
  functions.push_back(new ((Function_cos_n<T>*)vram->allocate(sizeof(Function_cos_n<T>))) Function_cos_n<T>(dim,vram));
  functions.push_back(new ((Function_negate_r<T>*)vram->allocate(sizeof(Function_negate_r<T>))) Function_negate_r<T>(dim,vram));
  functions.push_back(new ((Function_constcol_nnnn<T>*)vram->allocate(sizeof(Function_constcol_nnnn<T>))) Function_constcol_nnnn<T>(dim,vram));
  functions.push_back(new ((Function_constcplx_nn<T>*)vram->allocate(sizeof(Function_constcplx_nn<T>))) Function_constcplx_nn<T>(dim,vram));
  functions.push_back(new ((Function_lt_nn<T>*)vram->allocate(sizeof(Function_lt_nn<T>))) Function_lt_nn<T>(dim,vram));
  functions.push_back(new ((Function_gt_nn<T>*)vram->allocate(sizeof(Function_gt_nn<T>))) Function_gt_nn<T>(dim,vram));
  functions.push_back(new ((Function_and_ll<T>*)vram->allocate(sizeof(Function_and_ll<T>))) Function_and_ll<T>(dim,vram));
  functions.push_back(new ((Function_real_c<T>*)vram->allocate(sizeof(Function_real_c<T>))) Function_real_c<T>(dim,vram));
  functions.push_back(new ((Function_imag_c<T>*)vram->allocate(sizeof(Function_imag_c<T>))) Function_imag_c<T>(dim,vram));
  functions.push_back(new ((Function_plus_nn<T>*)vram->allocate(sizeof(Function_plus_nn<T>))) Function_plus_nn<T>(dim,vram));
  functions.push_back(new ((Function_plus_ii<T>*)vram->allocate(sizeof(Function_plus_ii<T>))) Function_plus_ii<T>(dim,vram));
  functions.push_back(new ((Function_minus_nn<T>*)vram->allocate(sizeof(Function_minus_nn<T>))) Function_minus_nn<T>(dim,vram));
}