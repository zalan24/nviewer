#!/bin/sh

file=$1

db=`wc -l $file | cut -f1 -d" "`
db=`expr $db + 1`

echo "#pragma once\n\n#include \"custumfunctionclass.h\""

name="NAME"
funcs=""

for i in `seq 1 $db`; do
    line=`head -$i $file | tail -1 | sed s/"*"/"&#42;"/g`
    nm=`echo $line | cut -f1 -d"="`
    if [ "$nm" = "\$name" ]; then
        name=`echo $line | cut -f2 -d"="`
        fline="functions.push_back(new (($name<T>*)vram->allocate(sizeof($name<T>))) $name<T>(dim,vram));"
        funcs=`printf "$funcs \n $fline"`
    else
        
        f1="template <class T> class $name : public FunctionClass<T> {\npublic:\n  $name(int dim, VRam *vr) : FunctionClass<T>("
        f2=", dim, vr) {FunctionClass<T>::setUp"
        f3=";}\n  void* calc(void *data, void *data2, VRam \&vr, int thread) {"
        f4="  }\n  FunctionClass<T>* copy() {\n    $name<T> *r = ($name<T>*)FunctionClass<T>::func_vram->allocate(sizeof($name<T>));\n    return new (r) $name<T>(*this);\n  }\n};"
        constr="FunctionClass<T>::outConstructor->construct(vr)"
        p="FunctionClass<T>::parameters"
        c="->calc(data,data2,vr,thread)"
    
        echo $line | sed s/"\$f1"/"$f1"/g | sed s/"\$f2"/"$f2"/g | sed s/"\$f3"/"$f3"/g | sed s/"\$f4"/"$f4"/g | sed s/"\$constr"/"$constr"/g | sed s/"\$p"/"$p"/g | sed s/"\$c"/"$c"/g | sed s/"*#42;"/"*"/g
    fi
done

printf "template <class T>\nvoid addScriptFunctions(vector<FunctionClass<T>*> &functions, VRam *vram, int dim)\n{"
printf "$funcs\n"
echo "}"

