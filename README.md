# NViewer

## Description

This project implements a volumetric CPU-renderer, that processes multidimensional data. The data is provided in a form of scripts, rather than textures. This way it is possible to define fractals and zoom in on them arbitrarily (only limited by float precision). The project features a custom functional programming language for defining the data. Here is an example: `Scripts/fractals/mandelbox.nvw` (or check the `Scripts/` folder for more).

The renderer is works at interactive framerates it the preview resolution, but it's possible to generate images offline with increased quality settings and resolution.

## A few images

### Generated

![Mandelbox](images/box3.png)
![Gorange](images/gorange.png)
![Red](images/red.png)
![Sample box](images/script.png)
![Mandelbox 2](doc/images/mandelbox.png)

### The UI

![UI](doc/images/ui.png)
![config](doc/images/config.png)
